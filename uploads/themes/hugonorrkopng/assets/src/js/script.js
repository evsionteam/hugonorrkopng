
function preloader(param) {
    this.time = param.time;
    this.init = function () {
        var time = this.time;
        $(window).load(function () {
            $('body').animate({
                opacity: 1
            }, time);
        })
    }
}

function lookNfill() {

    var that = this;
    //constructor function
    this.init = function () {
        this.headerFix();
        jQuery(window).load(function(){
            that.bannerTextAnimation();
            that.isAppleProduct();
        });
    };

    //HEADER FIX
    this.headerFix = function () {

        var $header = jQuery('header#masthead'),
            $window = jQuery(window),
            $document = jQuery(document),
            fixHeaderClass = 'fix-header',
            headerHeight = 30;//$header.outerHeight();

            $document.ready(fixHeader);
            $window.scroll(fixHeader);

            function fixHeader() {
                var scrollPosition = $window.scrollTop();
                if (scrollPosition >= headerHeight) {
                    if (!$header.hasClass(fixHeaderClass)) {
                        $header.addClass(fixHeaderClass);
                    }
                } else {
                    $header.removeClass(fixHeaderClass);
                }

            } // End fixHea

    }; //header fix fn

    // CHECKING APPLE PRODUCT OR NOT
    this.isAppleProduct = function(){
        var isiOSSafari    = (navigator.userAgent.match(/like Mac OS X/i)) ? true : false,
            $window        = jQuery( window ).width(),
            $bannerText  = jQuery( '.banner-text' ),
            $imageWrapper  = jQuery( '.img-wrapper' );
    
            if( isiOSSafari && $window < 1370 && $bannerText.length > 0 ){
                $bannerText.addClass( 'ani' );
            }else if( isiOSSafari && $window < 1370 && $imageWrapper.length > 0 ){
                $imageWrapper.parent().addClass( 'animation' );
            }
    };
    
    // BANNER TEXT ANIMATION
    this.bannerTextAnimation = function(){
        var $bannerText = jQuery('.banner-text')
            $imageCover = jQuery('.image-cover'),
            className   = 'ani';
            
            // for video banner
            if( $bannerText.length > 0 ){
                $bannerText.addClass( className );
                
                //hide banner text
                setTimeout( function( ){ 
                    $bannerText.removeClass( className );
                },5000 );
            }
            
            //inner banner image
            if( $imageCover.length > 0 ){
                console.log( 'ok here' );
                $imageCover.addClass( className );
            }
    };
    
} //End lookNfill 


//DOM ready
jQuery(document).ready(function () {
    new lookNfill().init();

    new preloader({
        time: 0
    }).init();

});


//scroll down plugin
jQuery.fn.scrollDown = function () {
    this.click(function ( e ) {
        e.preventDefault();
        var $this = jQuery(this),
            $element = jQuery('body'),
            target = $this.attr('data-target'),
            reduceHeightAttr = $this.attr('data-reduce-offset'),
            animationDuration = $this.attr('data-animation-duration'),
            //elementOffset
            targetOffset = jQuery(target).offset().top,
            //settingDefaultValue
            animationDurationValue = (typeof animationDuration != 'undefined') ? parseInt(animationDuration) : 500,
            //calculatingActualOffset
            actualOffset = targetOffset - reduceHeight(reduceHeightAttr);

            function reduceHeight(param) {
                var reduceHeight;
                if (jQuery(param).length > 0) {
                    reduceHeight = jQuery(param).outerHeight();
                    return reduceHeight;
                } else if (jQuery.isNumeric(param)) {
                    return parseInt(param);
                }
                return 0;
            }

            //scrollAnimation 
            $element.stop().animate({
                scrollTop: actualOffset
            }, animationDurationValue);

    });
}

jQuery(document).ready(function () {
    $("#fs-down-arrow").scrollDown();
});


jQuery(document).ready(function ($) {
    /*$(".load-custom-page > a").click(function (e) {
     $("#modal-restaurant-block .row").empty();
     $('#model-article .img-wrapper').removeClass('active');
     $('#model-article .img-wrapper').attr('style', '');
     $('#model-article .caption .btn-link').empty();
     $('#model-article .img-wrapper .caption .entry-header').empty();
     $('#model-article .container .entry-content').empty();
     $('#model-article .img-wrapper .caption .restro-btn').hide();
     $('.loading-img').show();
     $('#model-article .container #text-loader').show();
     $('#model-article .container .opening-hours').empty();
     $('#model-article .container .contact-form').empty();
     $('#model-article .container #daily-lunch-menu').empty();
     $('#model-article .container .daily-lunch-desc').empty();
     $('#model-article .container .daily-lunch-pdf').empty();
     $('#model-article .container #contact-page-data').removeClass('c-form-n-op-hours');
     e.preventDefault();
     var post_id = $(this).attr('page-id');
     $("#myModal").attr('page-id', post_id);
     $("#myModal").modal('show');
     $('#myModal').on('show.bs.modal', function (e) {
     var post_id = $(this).attr('page-id');
     jQuery.ajax({
     type: 'post',
     url: hugoAjax.ajaxurl,
     data: {
     action: 'load_page_results',
     page_id: post_id
     },
     dataType: 'json',
     success: function (response) {
     jQuery("#modal-restaurant-block").removeClass('restaurant-block');
     jQuery('#model-article .container #text-loader').hide();
     jQuery('.loading-img').hide();
     jQuery('#model-article .img-wrapper').addClass('active').css({
     'background-image': 'url(' + response.data.post_image_url + ')'
     });
     jQuery('#model-article .img-wrapper .caption .entry-header').append(response.data.post_title);
     jQuery('#model-article .container .entry-content').append(response.data.post_content);
     
     if (response.data.btn_html) {
     jQuery('#model-article .caption .btn-link').append(response.data.btn_html);
     }
     jQuery('#model-article .img-wrapper .caption .restro-btn').show();
     if ((response.menu_data) && (response.menu_data).length > 0) {
     jQuery('#modal-restaurant-block').addClass('restaurant-block');
     jQuery(response.menu_data).each(function (index, value) {
     jQuery("#modal-restaurant-block .row").append('<div class="col-sm-4 col-xs-6 restro-block">' + value + '</div>');
     });
     }
     *//*Contact page data*//*
      if (response.contact_page_data) {
      jQuery('#model-article .container #contact-page-data').addClass('c-form-n-op-hours');
      if (response.contact_page_data.opening_hours) {
      jQuery('#model-article .container .opening-hours').append(response.contact_page_data.opening_hours);
      }
      if (response.contact_page_data.contact_form) {
      jQuery('#model-article .container .contact-form').append('<h2 class="modal-sub-title">KONTAKTA OSS</h2>' + response.contact_page_data.contact_form);
      }
      }
      *//**//*
       
       *//*Daily Lunch data*//*
        if (response.lunch_page_data) {
        if(response.lunch_page_data.daily_menu){
        jQuery('#model-article .container #daily-lunch-menu').append('<div class="daily-lunch-menu">'+response.lunch_page_data.daily_menu+'</div>');
        }
        if(response.lunch_page_data.daily_lunch_desc){
        jQuery('#model-article .container .daily-lunch-desc').append(response.lunch_page_data.daily_lunch_desc);
        }
        if(response.lunch_page_data.daily_lunch_pdf){
        jQuery('#model-article .container .daily-lunch-pdf').append(response.lunch_page_data.daily_lunch_pdf);
        }
        }
        *//**//*
         
         jQuery('div.wpcf7 > form').wpcf7InitForm();
         }
         });
         });*/

    jQuery('.daily-lunch-pdf .week-pdf .title a').on('click', function (e) {
        e.preventDefault();
        var date = jQuery(this).data('date');
        var week = jQuery(this).data('week');
        var _that = jQuery(this);
        if (date) {
            jQuery.ajax({
                type: 'post',
                url: hugoAjax.ajaxurl,
                data: {
                    action: 'get_daily_lunch_menu_items',
                    menu_date: date
                },
                dataType: 'json',
                beforeSend: function () {
                    _that.addClass('loading');
                    jQuery('#daily-lunch-menu').css('visibility', 'hidden');
                },
                success: function (response) {
                    _that.removeClass('loading');
                    jQuery('#daily-lunch-menu').css('visibility', 'visible');
                    jQuery('.daily-lunch-menu .current-week').html('Lunchmeny vecka ' + week);
                    jQuery('.daily-lunch-menu .row').remove();
                    jQuery('.daily-lunch-menu').append(response);
                }
            });
        }
    });

});



