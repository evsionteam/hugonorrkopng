<?php
/* Template Name: Night Homepage Template */
get_header();
global $hugo_opt;

$detect = new Mobile_Detect();
$image = ( isset($hugo_opt['banner-image']['url']) && !empty($hugo_opt['banner-image']['url']) ) ? $hugo_opt['banner-image']['url'] : 'https://placeholdit.imgix.net/~text?txtsize=105&bg=000000&txtclr=ffffff&txt=1400%C3%971400&w=1400&h=1400';
$mp4_video = isset($hugo_opt['banner-video']) ? $hugo_opt['banner-video'] : '';
?>


<!-- Banner Section Starts-->
<?php if ($detect->isMobile()): ?>
    <div class="banner night-banner" style="background-image:url(' <?php echo $image; ?> '); background-size: cover; background-position: center;">
        <div class="banner-text" id="banner-text">
            <h1><?php echo $hugo_opt['banner-title']; ?></h1>
            <span><?php echo $hugo_opt['banner-sub-title']; ?></span>
        </div>
    </div>
<?php elseif ($mp4_video):
    $video_overlay_class = '';
    if(isset($hugo_opt['video-overlay'])){
        if(1 == $hugo_opt['video-overlay']){
            $video_overlay_class = 'overlay';
        }
    }
    ?>
    <div class="banner night-banner <?php echo $video_overlay_class;?>" id="nojen-banner" data-jarallax-video="<?php echo $hugo_opt['banner-video']; ?>">
        <div class="banner-text" id="banner-text">
            <h1><?php echo $hugo_opt['banner-title']; ?></h1> 
            <span><?php echo $hugo_opt['banner-sub-title']; ?></span>
        </div>
    </div>
<?php endif; ?><!-- Banner Title and Subtitle-->


<!-- FORNOLES SKULL -->
<div class="fornoles-skull">
    <h2><?php if(isset($hugo_opt['fornoles-skull-title'])){
            echo $hugo_opt['fornoles-skull-title'];
        }?>
    </h2>
    <div class="desciption">
        <?php
        if(isset($hugo_opt['fornoles-skull-description'])){
            echo wpautop($hugo_opt['fornoles-skull-description']);
        }
        ?>
    </div>
    <div class="down-arrow" id="fs-down-arrow" data-target="#home-slider" data-reduce-offset="82">
    </div>
</div><!-- /.fornoles-skull -->


<!-- EVENT NAME SLIDER -->
<?php
$args = array('post_type' => 'home_slider');
$query = new WP_Query($args);
$j = 0;
?>

<div id="home-slider" class="carousel slide event-name" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">

        <?php for ($i = 0; $i < $query->post_count; $i++) { ?>
            <li data-target="#home-slider" data-slide-to="<?php echo $i; ?>" <?php if ($i == 0) { ?>class="active"><?php } ?></li>
        <?php } ?>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner restro-slider" role="listbox">
        <?php

        $slider_overlay_class = '';
        if(isset($hugo_opt['slider-overlay'])){
            if(1 == $hugo_opt['slider-overlay']){
                $slider_overlay_class = 'overlay';
            }
        }

        while ($query->have_posts()) {
            $query->the_post();

            $des_meta = get_post_meta($post->ID, 'slider_design_select_meta_box', true);
            $url = '';
            $thumb = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'home-large-slider');
            $url = $thumb[0];
            switch ($des_meta) {
                case "design_1":
                    ?>
                    <div class="item <?php
                    if ($j == 0) {
                        echo 'active';
                    }
                    echo ' '.$slider_overlay_class;
                    ?>">
                        <div class="restro-design restro-design-1" style="background-image:url('<?php echo $url; ?>');">
                                    <!-- <img src="<?php //echo $url;      ?>"> -->
                            <div class="restro-text">
                                <div class="restro-title"><?php the_title(); ?></div>
                                <div class="restro-sub-title"><?php the_content(); ?></div>
                                <?php
                                $slider_btn_txt = get_post_meta(get_the_ID(),'slider_button_slider_meta',true);
                                $slider_btn_link = get_post_meta(get_the_ID(),'custom_link_slider_meta',true);
                                if(!empty($slider_btn_txt) && !empty($slider_btn_link)){
                                    echo '<a href="'.$slider_btn_link.'" class="restro-btn">'.$slider_btn_txt.'</a>';
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <?php
                    break;
                case "design_2":
                    ?>
                    <div class="item <?php
                    if ($j == 0) {
                        echo 'active';
                    }
                    echo ' '.$slider_overlay_class;
                    ?>">
                        <div class="restro-design restro-design-2">
                            <div class="row">
                                <div class="col-md-5">
                                    <?php
                                    $url = '';
                                    $thumb = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'home-square-slider');
                                    $url = $thumb[0];
                                    ?>
                                    <img src="<?php echo $url; ?>">
                                </div>
                                <div class="col-md-7">
                                    <div class="restro-text">
                                        <div class="restro-title"><?php the_title(); ?></div>
                                        <div class="restro-sub-para"><?php the_content(); ?></div>
                                        <?php
                                        $slider_btn_txt = get_post_meta(get_the_ID(),'slider_button_slider_meta',true);
                                        $slider_btn_link = get_post_meta(get_the_ID(),'custom_link_slider_meta',true);
                                        if(!empty($slider_btn_txt) && !empty($slider_btn_link)){
                                            echo '<a href="'.$slider_btn_link.'" class="restro-btn">'.$slider_btn_txt.'</a>';
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    break;
                default:
                    break;
            }
            ?>
            <?php
            $j++;
        } wp_reset_query();
        ?>
    </div>

    <?php if($query->found_posts != 1):?>
    <!-- Left and right controls -->
    <a class="left carousel-control" href="#home-slider" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true">
            <img src="<?php echo get_template_directory_uri() ?>/assets/build/img/arrow-left.png" alt="arror left"/>
        </span>
        <span class="sr-only"><?php _e('Previous', 'hugonorrkopng'); ?></span>
    </a>
    <a class="right carousel-control" href="#home-slider" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true">
            <img src="<?php echo get_template_directory_uri() ?>/assets/build/img/arrow-right.png" alt="arror right"/>
        </span>
        <span class="sr-only"><?php _e('Next', 'hugonorrkopng'); ?></span>
    </a>
    <?php endif;?>
</div><!-- /.event-name -->


<!-- ARRANGEMANG -->
<?php
$arrangements_args = array(
'post_type'=>'arrangements',
'post_status'=> 'publish'
);
$arrangements = new WP_Query($arrangements_args);
if($arrangements->have_posts()):
?>
<div class=" restaurant-block arrangemang">
    <div class="container">
        <div class="row">
            <?php if(isset($hugo_opt['arrangemang-title'])){
                echo "<h2>".$hugo_opt['arrangemang-title']."</h2>";
            }?>
            <div class="info-wrapper">
                <?php
                $counter = 1;
                while($arrangements->have_posts()): $arrangements->the_post();
                    $link = get_post_meta(get_the_ID(),'fb_link',true);
                    if(empty($link)){
                        $link =  get_permalink();
                    }
                    if (strpos($link, 'facebook') !== false){
                        $target = 'target="_blank"';
                    }
                    else{
                        $target = '';
                    }
                    ?>
                    <div class="col-sm-4 col-xs-12 restro-block">
                        <?php if(has_post_thumbnail()):?>
                            <a href="<?php echo $link; ?>" <?php echo ' '.$target;?>>
                                <div class="img">
                                    <img src="<?php the_post_thumbnail_url();?>" class="img-responsive" alt="img"/>
                                </div>
                            </a>
                        <?php endif;?>
                        <div class="resturant-title">
                            <a href="<?php echo $link; ?>" <?php echo ' '.$target;?>><?php the_title();?></a>
                        </div>
                        <?php echo '<p>'.mb_strimwidth(get_the_excerpt(), 0, 60, " ...").'</p>';?>
                        <a class="see-more" href="<?php echo $link; ?>" <?php echo ' '.$target;?>>Läs mer</a>
                    </div><!-- /.restro-block -->
                    <?php
                    if($counter % 2 == 0){
                        echo '<div class="clearfix visible-xs "></div>';
                    }
                    if($counter % 3 == 0){
                        echo '<div class="clearfix visible-sm "></div>';
                    }
                    $counter++;

                endwhile;wp_reset_postdata();?>
            </div><!-- info-wrapper -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.arrangemang -->
<?php endif; ?>
<!---->

<?php get_footer();