<?php
/* Template Name: Day Homepage Template */
get_header();
global $hugo_opt;

$detect = new Mobile_Detect();
$image = isset($hugo_opt['banner-image']['url']) ? $hugo_opt['banner-image']['url'] : 'https://placeholdit.imgix.net/~text?txtsize=105&bg=000000&txtclr=ffffff&txt=1400%C3%971400&w=1400&h=1400';
$mp4_video = isset($hugo_opt['banner-video']) ? $hugo_opt['banner-video'] : '';
?>

<!-- VIDEO BANNER SECTION -->
<?php if ($detect->isMobile()): ?>
    <div class="banner" style="background-image: url('<?php echo $image; ?>');background-size:cover;">
        <div class="banner-text" id="banner-text">
            <h1><?php echo $hugo_opt['banner-title']; ?></h1>
            <span><?php echo $hugo_opt['banner-sub-title']; ?></span>
        </div>
    </div>
    <?php
    elseif ($mp4_video):
    $video_overlay_class = '';
    if (isset($hugo_opt['video-overlay'])) {
        if (1 == $hugo_opt['video-overlay']) {
            $video_overlay_class = 'overlay';
        }
    }
    ?>
    <div id="video-section">

        <div class="jarallax banner <?php echo $video_overlay_class; ?>" data-jarallax-video="<?php echo $mp4_video; ?>" id="main-slider">
            <div class="banner-text" id="banner-text">
                <h1><?php echo $hugo_opt['banner-title']; ?></h1>
                <span><?php echo $hugo_opt['banner-sub-title']; ?></span>
            </div>
        </div>
    </div>
<?php endif; ?><!-- Banner Section Ends-->


<!-- FORNOLES SKULL -->
<div class='day-fornoles-skull'>
    <div class="container-fluid">
        <div class="title">
            <?php
            if (isset($hugo_opt['fornoles-skull-title'])) {
                echo $hugo_opt['fornoles-skull-title'];
            }
            ?>
        </div>
        <div class="desc">
            <?php
            if (isset($hugo_opt['fornoles-skull-description'])) {
                echo wpautop($hugo_opt['fornoles-skull-description']);
            }
            ?>
        </div>
    </div>
</div><!-- /.day-fornoles-skull -->


<!-- RESTAURANT-BLOCK -->
<div class="restaurant-block clear">
    <div class="container-fluid">
        <h2>
            <?php
            if (isset($hugo_opt['restaurang-title'])) {
                echo $hugo_opt['restaurang-title'];
            }
            ?>
        </h2>

        <?php
        /* Feature Menu Post type query */
        $ft_menu_args = array(
            'post_type' => 'menu',
            'posts_per_page' => 4,
            'meta_query' => array(
                array(
                    'key' => 'featured_menu',
                    'value' => 'yes',
                ),
            ),
        );
        $counter = 1;
        $featured_menu = new WP_Query($ft_menu_args);
        if ($featured_menu->have_posts()):
            while ($featured_menu->have_posts()):$featured_menu->the_post();
                $pdf_link = '';
                $attach_pdf_id = get_post_meta(get_the_ID(), '_image_id', true);
                if (!empty($attach_pdf_id)) {
                    $pdf_link = wp_get_attachment_url($attach_pdf_id);
                }
                ?>
                <div class="col-sm-6 col-xs-6 col-md-3 restro-block">
                    <a href = "<?php echo $pdf_link; ?>" target='_blank'>
                        <div class="resturant-img">
                            <img src = "<?php the_post_thumbnail_url(); ?>" class="img-responsive">
                        </div>
                    </a>
                    <!-- Menu Title -->
                    <div class="resturant-title">
                        <a href = "<?php echo $pdf_link; ?>" target='_blank' ><?php the_title(); ?></a>
                    </div>
                    <!-- Menu Description -->
                    <?php the_excerpt(); ?>
                    <!-- Full Menu 1 Link -->
                    <a class="see-more" target='_blank' href ="<?php echo $pdf_link; ?>" ><?php _e('Se meny', 'hugonorrkopng'); ?></a>
                </div>
                <?php
                if ($counter % 2 == 0) {
                    echo '<div class="clearfix visible-xs visible-sm"></div>';
                }
                $counter++;
            endwhile;
            wp_reset_postdata();
        endif;
        /**/
        ?>
    </div><!-- /.container-fuild -->
</div> <!-- /resturant-block -->


<!-- BOWLING-N MUSIKQUIZ  -->
<div class="bowling-n-musikquiz design-lunch">
    <div class="col-md-6">
        <div class="row">
            <div class="cp-right">
                <?php if(isset($hugo_opt['first-col-image']) && !empty($hugo_opt['first-col-image'])):?>
                    <div class="bowling-sec image" style="background-image:url(<?php echo $hugo_opt['first-col-image']['url']?>)">
                        <div class="text">
                            <div class="caption">
                                <div class="restro-title">
                                    <?php
                                    if (isset($hugo_opt['first-col-img-title'])) {
                                        echo $hugo_opt['first-col-img-title'];
                                    }
                                    ?>
                                </div>
                                <div class="sub-para">
                                    <?php
                                    if (isset($hugo_opt['first-col-img-desc'])) {
                                        echo wpautop($hugo_opt['first-col-img-desc']);
                                    }
                                    ?>
                                </div>
                                <?php
                                if (isset($hugo_opt['first-col-btn-text']) && !empty($hugo_opt['first-col-btn-text'])) {
                                    $first_btn_link = '#';
                                    if (isset($hugo_opt['first-col-btn-link'])) {
                                        $first_btn_link = $hugo_opt['first-col-btn-link'];
                                    }
                                    echo '<a href="' . $first_btn_link . '" class="restro-btn">' . $hugo_opt['first-col-btn-text'] . '</a>';
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                <?php endif;?>
            </div>
        </div>
    </div>
    
    <div class="col-md-6">
        <div class="row">
            <div class="cp-left">
                <?php if(isset($hugo_opt['second-col-image']) && !empty($hugo_opt['second-col-image']) ):?>
                    <div class="musikquiz image" style="background-image:url(<?php echo $hugo_opt['second-col-image']['url']?>)">
                        <div class="text">
                            <div class="caption">
                                <div class="restro-title">
                                    <?php
                                    if (isset($hugo_opt['second-col-img-title'])) {
                                        echo $hugo_opt['second-col-img-title'];
                                    }
                                    ?>
                                </div>
                                <div class="sub-para">
                                    <?php
                                    if (isset($hugo_opt['second-col-img-desc'])) {
                                        echo wpautop($hugo_opt['second-col-img-desc']);
                                    }
                                    ?>
                                </div>
                                <?php
                                if (isset($hugo_opt['second-col-btn-text']) && !empty($hugo_opt['second-col-btn-text'])) {
                                    $second_btn_link = '#';
                                    if (isset($hugo_opt['second-col-btn-link'])) {
                                        $second_btn_link = $hugo_opt['second-col-btn-link'];
                                    }
                                    echo '<a href="' . $second_btn_link . '" class="restro-btn">' . $hugo_opt['second-col-btn-text'] . '</a>';
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                <?php endif;?>
            </div>
        </div>
    </div>
</div> <!-- /.bowling-n-musikquiz -->

<!-- DESIGN LUNCH -->
<div class="design-lunch">

    <div class="col-md-6 col-md-push-6">
        <div class="text">
            <div class="caption">
                <div class="restro-title">
                    <?php
                    if (isset($hugo_opt['daily-lunch-title'])) {
                        echo wpautop($hugo_opt['daily-lunch-title']);
                    }
                    ?>
                </div>
                <div class="sub-para">
                    <?php
                    if (isset($hugo_opt['daily-lunch-desc'])) {
                        echo wpautop($hugo_opt['daily-lunch-desc']);
                    }
                    ?>
                </div>
                <?php
                if (isset($hugo_opt['daily-lunch-btn-text'])) {
                    $link = '#';
                    if (isset($hugo_opt['daily-lunch-btn-link'])) {
                        $link = $hugo_opt['daily-lunch-btn-link'];
                    }
                    echo '<a href="' . $link . '" class="restro-btn">' . $hugo_opt['daily-lunch-btn-text'] . '</a>';
                }
                ?>
            </div><!-- /.caption -->
        </div><!-- /.text -->
    </div><!-- /.col-md-6 -->

    <!--  -->
    <!-- <div class="cp-right-f">-->
        <?php
        if (isset($hugo_opt['daily-lunch-image'])) {
            $daily_lunch_img = $hugo_opt['daily-lunch-image']['url'];
            if (!empty($daily_lunch_img)) {
                ?>
                <div class="col-md-6 col-md-pull-6">
                    <div class="row">
                        <div class="cp-right">
                            <div class="image" style="background-image:url('<?php echo $daily_lunch_img?>')"></div>
                        </div>
                    </div>
                </div>
                <?php
                //echo '<div class="col-md-6 image col-md-pull-6" style="background-image:url(' . $daily_lunch_img . ')"></div>';
            } else {
                echo '<div class="col-md-6 image"></div>';
            }
        } else {
            echo '<div class="col-md-6 image"></div>';
        }
        ?>
    <!--</div>  /.cp-right--->

</div><!-- /.design-lunch -->

<!-- Boka Calender Section Starts -->
<div class="boka-board">
    <div class="container">
        <div class="col-md-7 col-md-push-5 text-left">
            <div class="boka-bord-n-bowl">
                <h2>
                    <?php
                    if (isset($hugo_opt['bordsbokning-title'])) {
                        echo $hugo_opt['bordsbokning-title'];
                    }
                    ?>
                </h2>

                <div class="discription">
                    <?php
                    if (isset($hugo_opt['bordsbokning-description'])) {
                        echo wpautop($hugo_opt['bordsbokning-description']);
                    }
                    ?>
                </div>
            </div><!-- boka-bord-n-bowl -->
        </div><!-- /col-md-7 -->
        <div class="col-md-5 col-md-pull-7">
            <iframe height="600" width="300" src="https://api.caspeco.net/externalBookingClient/?publicID=612f287944b7e96aea7827782b4b0084&unitID=13&fixedSize=1&advanced=true" ></iframe>
        </div><!-- /.col-md-7 -->
    </div><!-- /.container -->
</div><!-- Boka Calender Section Ends -->


<!-- SPELA PARTYBOWLING -->
<div class="slide spela-party-bowling">
    <div class="restro-slider">
        <?php
        $bowling_overlay_class = '';
        if (isset($hugo_opt['bowling-img-overlay'])) {
            if (1 == $hugo_opt['bowling-img-overlay']) {
                $bowling_overlay_class = 'overlay';
            }
        }
        if (isset($hugo_opt['party-bowling-image'])) {
            $party_bowling_img = $hugo_opt['party-bowling-image']['url'];
            if (!empty($party_bowling_img)) {
                ?>
                <div data-jarallax='{"speed": 0.2}' class='jarallax restro-design restro-design-1 <?php echo $bowling_overlay_class; ?>' style='background-image: url("<?php echo $party_bowling_img; ?>");'>
                    <?php
                } else {
                    ?>
                    <div data-jarallax='{"speed": 0.2}' class='jarallax restro-design restro-design-1 <?php echo $bowling_overlay_class; ?>'>
                        <?php
                    }
                } else {
                    ?>
                    <div data-jarallax='{"speed": 0.2}' class='jarallax restro-design restro-design-1 <?php echo $bowling_overlay_class; ?>'>
                        <?php
                    }
                    ?>
                    <div class="restro-text">
                        <?php
                        if (isset($hugo_opt['party-bowling-title'])) {
                            echo '<strong class="restro-title">' . $hugo_opt['party-bowling-title'] . '</strong>';
                        }

                        if (isset($hugo_opt['party-bowling-desc'])) {
                            echo '<div class="restro-sub-title">' . wpautop($hugo_opt['party-bowling-desc']) . '</div>';
                        }

                        if (isset($hugo_opt['party-bowling-btn-text'])) {
                            $link = '#';
                            if (isset($hugo_opt['party-bowling-btn-link'])) {
                                $link = $hugo_opt['party-bowling-btn-link'];
                            }
                            echo '<a href="' . $link . '" class="restro-btn">' . $hugo_opt['party-bowling-btn-text'] . '</a>';
                        }
                        ?>
                    </div>
                </div>
            </div><!-- /.restro-slider -->
        </div><!-- /.slider -->

        <?php
        get_footer();
        