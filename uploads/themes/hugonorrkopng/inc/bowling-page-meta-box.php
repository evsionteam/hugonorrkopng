<?php

/*-------- Price and time meta box ------------------------------------------------*/
add_action('add_meta_boxes', 'bowling_price_n_time_meta', 1);
function bowling_price_n_time_meta() {
    global $post;
    if ( 'template-parts/template-bowling.php' == get_post_meta( $post->ID, '_wp_page_template', true ) ) {
        add_meta_box( 'bowling-price-n-meta', __( 'Price and Time' ), 'bowling_page_repeatable_meta_box_display', 'page', 'normal', 'default');
        add_meta_box( 'bowling-booking-form', __( 'Booking Form' ), 'booking_form_meta_box_display', 'page', 'normal', 'default');
    }
}

function bowling_page_repeatable_meta_box_display($post) {

    $prices = get_post_meta($post->ID, 'time_n_price', true);
    wp_nonce_field( 'prices_repeatable_meta_box_nonce', 'prices_repeatable_meta_box' );
    ?>
    <script type="text/javascript">
        jQuery(document).ready(function( $ ){
            $( '#add-row-prices' ).on('click', function() {
                var row = $( '.empty-row-prices.screen-reader-text' ).clone(true);
                row.removeClass( 'empty-row-prices screen-reader-text' );
                row.insertBefore( '#repeatable-fieldset-one-prices tbody>tr:last' );
                return false;
            });

            $( '.remove-row-prices' ).on('click', function() {
                $(this).parents('tr').remove();
                return false;
            });
        });
    </script>

    <table id="repeatable-fieldset-one-prices" width="100%">
        <tbody>
        <p>Format: Time | Price ( Eg : Måndag 16:00 – 23:00 | 149:- / bana ) </p>
        <p><a id="add-row-prices" class="button" href="#">Add another</a></p>
        <?php
        if ( $prices ) :
            foreach ( $prices as $field_prices ) {
                ?>
                <tr>
                    <td>
                        <input type="text" class="widefat" name="name_prices[]" value="<?php if($field_prices['name_prices'] != '') echo esc_attr( $field_prices['name_prices'] ); ?>" />
                    </td>
                    <td><a class="button remove-row-prices" href="#">Remove</a></td>
                </tr>
            <?php
            }
        else :
            // show a blank one
            ?>
            <tr>
                <td>
                    <input type="text" class="widefat" name="name_prices[]" value=""/>
                </td>
                <td><a class="button remove-row-prices" href="#">Remove</a></td>
            </tr>
        <?php endif; ?>

        <!-- empty hidden one for jQuery -->
        <tr class="empty-row-prices screen-reader-text">
            <td>
                <input type="text" class="widefat" name="name_prices[]" value=""/>
            </td>
            <td><a class="button remove-row-prices" href="#">Remove</a></td>
        </tr>
        </tbody>
    </table>


<?php
}


function booking_form_meta_box_display($post){
    $booking_form = get_post_meta($post->ID, 'booking_form', true);
    wp_nonce_field( 'bowling_meta_box_nonce', 'bowling_meta_box' );
?>
    <p>
        <label for="booking-form"><?php _e( 'Booking Form: ', 'prfx-textdomain' )?></label><br/>
        <textarea name="booking_form" id="booking-form" rows="5" cols="80"><?php echo $booking_form; ?></textarea>
    </p>
<?php
}


add_action('save_post', 'prices_repeatable_meta_box_save');
function prices_repeatable_meta_box_save($post_id) {

    if ( ! isset( $_POST['prices_repeatable_meta_box'] ) || ! wp_verify_nonce( $_POST['prices_repeatable_meta_box'], 'prices_repeatable_meta_box_nonce' ) )
        return;

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return;

    if (!current_user_can('edit_post', $post_id))
        return;

    $old_prices = get_post_meta($post_id, 'time_n_price', true);
    $new_prices = array();

    $names_prices = $_POST['name_prices'];

    $count_prices = count( $names_prices );

    for ( $i_prices = 0; $i_prices < $count_prices; $i_prices++ ) {
        if ( $names_prices[$i_prices] != '' ) :
            $new_prices[$i_prices]['name_prices'] = stripslashes( strip_tags( $names_prices[$i_prices] ) );
        endif;
    }
    if ( !empty( $new_prices ) && $new_prices != $old_prices )
        update_post_meta( $post_id, 'time_n_price', $new_prices );
    elseif ( empty($new_prices) && $old_prices )
        delete_post_meta( $post_id, 'time_n_price', $old_prices );
}

add_action('save_post', 'booking_form_meta_box_save');
function booking_form_meta_box_save($post_id) {

    if ( ! isset( $_POST['bowling_meta_box'] ) || ! wp_verify_nonce( $_POST['bowling_meta_box'], 'bowling_meta_box_nonce' ) )
        return;

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return;

    if (!current_user_can('edit_post', $post_id))
        return;

    if( isset( $_POST[ 'booking_form' ] ) ) {
        update_post_meta( $post_id, 'booking_form', $_POST[ 'booking_form' ] );
    }
}
