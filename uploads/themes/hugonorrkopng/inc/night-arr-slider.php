<?php
// Register Custom Post Type
function hugo_register_night_arrangements_slider() {

    $labels = array(
        'name'               => _x( 'Arrangements Slider', 'post type general name', 'hugonorrkopng' ),
        'singular_name'      => _x( 'Arrangement Slider', 'post type singular name', 'hugonorrkopng' ),
        'menu_name'          => _x( 'Arrangements Slider', 'admin menu', 'hugonorrkopng' ),
        'name_admin_bar'     => _x( 'Arrangement Slider', 'add new on admin bar', 'hugonorrkopng' ),
        'add_new'            => _x( 'Add New', 'Arrangement Slider', 'hugonorrkopng' ),
        'add_new_item'       => __( 'Add New Arrangement Slider', 'hugonorrkopng' ),
        'new_item'           => __( 'New Arrangement Slider', 'hugonorrkopng' ),
        'edit_item'          => __( 'Edit Arrangement Slider', 'hugonorrkopng' ),
        'view_item'          => __( 'View Arrangement Slider', 'hugonorrkopng' ),
        'all_items'          => __( 'All Arrangements Slider', 'hugonorrkopng' ),
        'search_items'       => __( 'Search Arrangements Slider', 'hugonorrkopng' ),
        'parent_item_colon'  => __( 'Parent Arrangements Slider:', 'hugonorrkopng' ),
        'not_found'          => __( 'No Arrangements Slider found.', 'hugonorrkopng' ),
        'not_found_in_trash' => __( 'No Arrangements Slider found in Trash.', 'hugonorrkopng' )
    );

    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'menu_icon'          => 'dashicons-admin-page',
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'arrangemang-slider' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt' )
    );

    register_post_type( 'arrangements_slider', $args );
}
add_action( 'init', 'hugo_register_night_arrangements_slider', 0 );


add_action( 'add_meta_boxes', 'hugo_night_arr_slider_custom_link' );
function hugo_night_arr_slider_custom_link()
{
    add_meta_box( 'night-arr-custom-link', 'Button Settings', 'night_arr_slider_custom_link_callback', 'arrangements_slider', 'normal', 'high' );
}

function night_arr_slider_custom_link_callback($post){
    wp_nonce_field( 'night_arr_slider_nonce', 'night_arr_slider' );
    $post_meta = get_post_meta($post->ID);

    $btn_text = isset($post_meta['btn_text']) ? array_pop($post_meta['btn_text']): '';
    $btn_link = isset($post_meta['btn_link']) ? array_pop($post_meta['btn_link']) : '';
    ?>
    <p>
        <label for="btn-text"><?php _e('Enter Button text: ','hugonorrkopng')?></label><br/>
        <input type="text" name="btn_text" id="btn-text" size="30" value="<?php echo $btn_text;?>">
    </p>
    <p>
        <label for="btn-link"><?php _e('Enter Button link: ','hugonorrkopng')?></label><br/>
        <input type="text" name="btn_link" id="btn-link" size="50" value="<?php echo $btn_link;?>">
    </p>
<?php
}

add_action( 'save_post', 'save_night_arr_slider' );
function save_night_arr_slider( $post_id ){

    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

    if( !isset( $_POST['night_arr_slider'] ) || !wp_verify_nonce( $_POST['night_arr_slider'], 'night_arr_slider_nonce' ) ) return;

    if( !current_user_can( 'edit_post',$post_id ) ) return;

    if( isset( $_POST['btn_text'] ) ){
        update_post_meta( $post_id, 'btn_text', $_POST['btn_text'] );
    }
    if( isset( $_POST['btn_link'] ) ){
        update_post_meta( $post_id, 'btn_link', $_POST['btn_link'] );
    }
}