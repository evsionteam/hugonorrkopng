<?php
// Register Custom Post Type
function hugo_register_arrangements_post() {

    $labels = array(
        'name'               => _x( 'Arrangemang', 'post type general name', 'hugonorrkopng' ),
        'singular_name'      => _x( 'Arrangemang', 'post type singular name', 'hugonorrkopng' ),
        'menu_name'          => _x( 'Arrangemang', 'admin menu', 'hugonorrkopng' ),
        'name_admin_bar'     => _x( 'Arrangemang', 'add new on admin bar', 'hugonorrkopng' ),
        'add_new'            => _x( 'Add New', 'Arrangement', 'hugonorrkopng' ),
        'add_new_item'       => __( 'Add New Arrangement', 'hugonorrkopng' ),
        'new_item'           => __( 'New Arrangement', 'hugonorrkopng' ),
        'edit_item'          => __( 'Edit Arrangement', 'hugonorrkopng' ),
        'view_item'          => __( 'View Arrangement', 'hugonorrkopng' ),
        'all_items'          => __( 'All Arrangements', 'hugonorrkopng' ),
        'search_items'       => __( 'Search Arrangements', 'hugonorrkopng' ),
        'parent_item_colon'  => __( 'Parent Arrangements:', 'hugonorrkopng' ),
        'not_found'          => __( 'No Arrangements found.', 'hugonorrkopng' ),
        'not_found_in_trash' => __( 'No Arrangements found in Trash.', 'hugonorrkopng' )
    );

    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'menu_icon'          => 'dashicons-calendar-alt',
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'arrangemang' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt' )
    );

    register_post_type( 'arrangements', $args );
}
add_action( 'init', 'hugo_register_arrangements_post', 0 );