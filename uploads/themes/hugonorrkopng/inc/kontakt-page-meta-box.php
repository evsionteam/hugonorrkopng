<?php

/*-------- contact page meta boxes ------------------------------------------------*/
add_action('add_meta_boxes', 'contact_page_meta_boxes', 1);
function contact_page_meta_boxes() {
    global $post;
    if ( 'template-parts/template-kontakt.php' == get_post_meta( $post->ID, '_wp_page_template', true ) ) {
        add_meta_box( 'contact-google-map-meta', __( 'Google Map Settings' ), 'contact_gmap_settings_display', 'page', 'normal', 'default');
    }
}

function contact_gmap_settings_display($post) {

    $enable_map = get_post_meta( $post->ID, 'enable_map', true);
    $latitude = get_post_meta( $post->ID, 'latitude', true);
    $longitude = get_post_meta( $post->ID, 'longitude', true);
    $map_zoom = get_post_meta( $post->ID, 'map_zoom', true);

    wp_nonce_field( 'gmap_meta_box_nonce', 'gmap_meta_box' );
    ?>
    <p>
        <label for="enable-map">
            <input type="checkbox" name="enable_map" id="enable-map" value="yes" <?php checked( $enable_map, 'yes' ); ?> />
            <?php _e( 'Enable Map?', 'hugonorrkopng' )?>
        </label>
    </p>
    <p>
        <label for="latitude"><?php _e( 'Latitude: ', 'hugonorrkopng' )?></label><br/>
        <input type="text" name="latitude" id="latitude" value="<?php echo $latitude;?>" size="50" />
    </p>
    <p>
        <label for="longitude"><?php _e( 'Longitude: ', 'hugonorrkopng' )?></label><br/>
        <input type="text" name="longitude" id="longitude" value="<?php echo $longitude;?>" size="50" />
    </p>
    <p>
        <label for="map-zoom"><?php _e( 'Map Zoom: ', 'hugonorrkopng' )?></label><br/>
        <input type="text" name="map_zoom" id="map-zoom" value="<?php echo $map_zoom;?>" size="50" />
    </p>
<?php
}


add_action('save_post', 'contact_meta_box_save');
function contact_meta_box_save($post_id) {

    if ( ! isset( $_POST['gmap_meta_box'] ) || ! wp_verify_nonce( $_POST['gmap_meta_box'], 'gmap_meta_box_nonce' ) )
        return;

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return;

    if (!current_user_can('edit_post', $post_id))
        return;

    if( isset( $_POST[ 'enable_map' ] ) ) {
        update_post_meta( $post_id, 'enable_map', 'yes' );
    } else {
        update_post_meta( $post_id, 'enable_map', '' );
    }

    if( isset( $_POST[ 'latitude' ] ) ) {
        update_post_meta( $post_id, 'latitude', $_POST[ 'latitude' ] );
    }

    if( isset( $_POST[ 'longitude' ] ) ) {
        update_post_meta( $post_id, 'longitude', $_POST[ 'longitude' ] );
    }

    if( isset( $_POST[ 'map_zoom' ] ) ) {
        update_post_meta( $post_id, 'map_zoom', $_POST[ 'map_zoom' ] );
    }
}
