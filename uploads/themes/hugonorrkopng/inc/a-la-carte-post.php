<?php
// Register Custom Post Type
function custom_post_type() {

    $labels = array(
        'name'                  => _x( 'A la carte', 'Post Type General Name', 'text_domain' ),
        'singular_name'         => _x( 'A la carte', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'             => __( 'A la carte', 'text_domain' ),
        'name_admin_bar'        => __( 'A la carte', 'text_domain' ),
        'archives'              => __( 'Item Archives', 'text_domain' ),
        'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
        'all_items'             => __( 'All Items', 'text_domain' ),
        'add_new_item'          => __( 'Add New Item', 'text_domain' ),
        'add_new'               => __( 'Add New', 'text_domain' ),
        'new_item'              => __( 'New Item', 'text_domain' ),
        'edit_item'             => __( 'Edit Item', 'text_domain' ),
        'update_item'           => __( 'Update Item', 'text_domain' ),
        'view_item'             => __( 'View Item', 'text_domain' ),
        'search_items'          => __( 'Search Item', 'text_domain' ),
        'not_found'             => __( 'Not found', 'text_domain' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
        'featured_image'        => __( 'Featured Image', 'text_domain' ),
        'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
        'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
        'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
        'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
        'items_list'            => __( 'Items list', 'text_domain' ),
        'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
        'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
    );
    $args = array(
        'label'                 => __( 'Carte', 'text_domain' ),
        'labels'                => $labels,
        'supports'              => array( 'title' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'a_la_carte', $args );

}
add_action( 'init', 'custom_post_type', 0 );





class Custom_Post_Type_Image_Upload {


    public function __construct() {

        if ( is_admin() ) {
            add_action( 'admin_init', array( &$this, 'admin_init' ) );
        }
        add_action( 'admin_enqueue_scripts', array($this,'load_admin_things' ) );

    }

    function load_admin_things() {
        wp_enqueue_media();
        /*wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');
        wp_enqueue_style('thickbox');*/
    }



    /** Admin methods ******************************************************/


    /**
     * Initialize the admin, adding actions to properly display and handle
     * the a_la_carte custom post type add/edit page
     */
    public function admin_init() {
        global $pagenow;

        if ( $pagenow == 'post-new.php' || $pagenow == 'post.php' || $pagenow == 'edit.php' ) {

            add_action( 'add_meta_boxes', array( &$this, 'meta_boxes' ) );
            add_filter( 'enter_title_here', array( &$this, 'enter_title_here' ), 1, 2 );

            add_action( 'save_post', array( &$this, 'meta_boxes_save' ), 1, 2 );
        }
    }


    /**
     * Save meta boxes
     *
     * Runs when a post is saved and does an action which the write panel save scripts can hook into.
     */
    public function meta_boxes_save( $post_id, $post ) {
        if ( empty( $post_id ) || empty( $post ) || empty( $_POST ) ) return;
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
        if ( is_int( wp_is_post_revision( $post ) ) ) return;
        if ( is_int( wp_is_post_autosave( $post ) ) ) return;
        if ( ! current_user_can( 'edit_post', $post_id ) ) return;
        if ( $post->post_type != 'a_la_carte' ) return;

        $this->process_a_la_carte_meta( $post_id, $post );
    }


    /**
     * Function for processing and storing all a_la_carte data.
     */
    private function process_a_la_carte_meta( $post_id, $post ) {
        update_post_meta( $post_id, '_image_id', $_POST['upload_image_id'] );
    }


    /**
     * Set a more appropriate placeholder text for the New a_la_carte title field
     */
    public function enter_title_here( $text, $post ) {
        if ( $post->post_type == 'a_la_carte' ) return __( 'a_la_carte Title' );
        return $text;
    }


    /**
     * Add and remove meta boxes from the edit page
     */
    public function meta_boxes() {
        add_meta_box( 'a_la_carte-image', __( 'a_la_carte Image' ), array( &$this, 'a_la_carte_image_meta_box' ), 'a_la_carte', 'normal', 'high' );
    }


    /**
     * Display the image meta box
     */
    public function a_la_carte_image_meta_box() {
        global $post;

        $image_src = '';

        $image_id = get_post_meta( $post->ID, '_image_id', true );
        $image_src = wp_get_attachment_url( $image_id );

        $image = wp_get_attachment_url( $image_id );
        if( $image) : 

        ?>
       <!--  <img id="a_la_carte_image" src="<?php echo $image_src ?>" style="max-width:100%;" /> -->

        <p><?php _e('File Name');?>: <span id="pdf-name"><?php echo basename($image);?></span></p>
        <p><?php _e('File URL');?>: <span id="pdf-url"><?php echo $image;?></span></p>
        <?php endif;?>
        <input type="hidden" name="upload_image_id" id="upload_image_id" value="<?php echo ($image_id)? $image_id:''; ?>" />
        <p>            
            <a class="upload-custom-img " title="<?php esc_attr_e( 'Set a_la_carte image' ) ?>" href="#" id="set-a_la_carte-image"><?php _e( 'Set a_la_carte image' ) ?></a>
            <a class="delete-custom-img" title="<?php esc_attr_e( 'Remove a_la_carte image' ) ?>" href="#" id="remove-a_la_carte-image" style="<?php echo ( ! $image_id ? 'display:none;' : '' ); ?>"><?php _e( 'Remove a_la_carte image' ) ?></a>
        </p>
        
        

        <script type="text/javascript">
            jQuery(document).ready(function($) {
                // Set all variables to be used in scope
                 var frame,
                     metaBox = $('.postbox-container'), // Your meta box id here
                     addImgLink = metaBox.find('.upload-custom-img'),
                     delImgLink = metaBox.find( '.delete-custom-img'),
                     // imgContainer = metaBox.find( '#a_la_carte_image'),
                     imgIdInput = metaBox.find( '#upload_image_id' ),
                     pdfName = metaBox.find('#pdf-name'),
                     pdfUrl = metaBox.find('#pdf-url');

                 // ADD IMAGE LINK
                 addImgLink.on( 'click', function( event ){                   
                   event.preventDefault();                   
                   // If the media frame already exists, reopen it.
                   if ( frame ) {
                     frame.open();
                     return;
                   }
                   
                   // Create a new media frame
                   frame = wp.media({
                     title: 'Select or Upload Media Of Your Chosen Persuasion',
                     button: {
                       text: 'Use this media'
                     },
                     multiple: false  // Set to true to allow multiple files to be selected
                   });

                   
                   // When an image is selected in the media frame...
                   frame.on( 'select', function() {                     
                     // Get media attachment details from the frame state
                     var attachment = frame.state().get('selection').first().toJSON();

                     // Send the attachment URL to our custom image input field.
                     // imgContainer.append( '<img src="'+attachment.url+'" alt="" style="max-width:100%;"/>' );

                     // Send the attachment id to our hidden input
                     imgIdInput.val( attachment.id );

                     pdfName.html(attachment.filename);
                     pdfUrl.html( attachment.url);
                     // pdfLink.html( attachment.link);

                     // Hide the add image link
                     // addImgLink.addClass( 'hidden' );

                     // Unhide the remove image link
                     delImgLink.removeClass( 'hidden' );
                   });

                   // Finally, open the modal on click
                   frame.open();
                 });
                 
                 
                 // DELETE IMAGE LINK
                 delImgLink.on( 'click', function( event ){

                   event.preventDefault();

                   // Clear out the preview image
                   // imgContainer.html( '' );
                       pdfName.html('');
                       pdfUrl.html('');

                   // Un-hide the add image link
                   addImgLink.removeClass( 'hidden' );

                   // Hide the delete image link
                   delImgLink.addClass( 'hidden' );

                   // Delete the image id from the hidden input
                   imgIdInput.val( '' );

                 });
               

            });
        </script>
    <?php
    }
}

// finally instantiate our plugin class and add it to the set of globals
$GLOBALS['custom_post_type_image_upload'] = new Custom_Post_Type_Image_Upload();



/*-------------------------------------------------------------------------------------------------------------------*/



/*add_action("admin_init", "pdf_init");
add_action('save_post', 'save_pdf_link');
function pdf_init(){
    add_meta_box("my-pdf", "PDF Document", "pdf_link", "a_la_carte", "normal", "low");
}
function pdf_link(){
    global $post;
    $custom  = get_post_custom($post->ID);
    $link    = $custom["link"][0];
    $count   = 0;
    echo '<div class="link_header">';
    $query_pdf_args = array(
        'post_type' => 'attachment',
        'post_mime_type' =>'application/pdf',
        'post_status' => 'inherit',
        'posts_per_page' => -1,
    );
    $query_pdf = new WP_Query( $query_pdf_args );
    $pdf = array();
    echo '<select name="link">';
    echo '<option class="pdf_select">SELECT pdf FILE</option>';
    foreach ( $query_pdf->posts as $file) {
        if($link == $pdf[]= $file->guid){
            echo '<option value="'.$pdf[]= $file->guid.'" selected="true">'.$pdf[]= $file->guid.'</option>';
        }else{
            echo '<option value="'.$pdf[]= $file->guid.'">'.$pdf[]= $file->guid.'</option>';
        }
        $count++;
    }
    echo '</select><br /></div>';
    echo '<p>Please Upload the PDF file in Media Gallery and Select from above</p>';
}
function save_pdf_link(){
    global $post;
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){ return $post->ID; }
    update_post_meta($post->ID, "link", $_POST["link"]);
}
add_action( 'admin_head', 'pdf_css' );
function pdf_css() {
    echo '<style type="text/css">
        .pdf_select{
                font-weight:bold;
                background:#e5e5e5;
                }
        .pdf_count{
                font-size:9px;
                color:#0066ff;
                text-transform:uppercase;
                background:#f3f3f3;
                border-top:solid 1px #e5e5e5;
                padding:6px 6px 6px 12px;
                margin:0px -6px -8px -6px;
                -moz-border-radius:0px 0px 6px 6px;
                -webkit-border-radius:0px 0px 6px 6px;
                border-radius:0px 0px 6px 6px;
                }
        .pdf_count span{color:#666;}
                </style>';
}
function pdf_file_url(){
    global $wp_query;
    $custom = get_post_custom($wp_query->post->ID);
    echo $custom['link'][0];
}
*/

/*--------------------------------------------------------------------------------------------------------------------*/

