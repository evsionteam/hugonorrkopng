<?php

function hugo_column_shortcode( $atts, $content = null ) {
    $atts = shortcode_atts( array(
        'class' => '',
    ), $atts, 'col' );
    return '<div class="'.$atts['class'].'">' . do_shortcode($content) . '</div>';
}
add_shortcode( 'col', 'hugo_column_shortcode' );


function hugo_opening_hours_shortcode($atts) {

    $atts = shortcode_atts( array(
        'in_footer' => '',
    ), $atts, 'opening_hours' );

    global $hugo_opt;

    if( isset($hugo_opt['restaurang-hours-val'])){
        $hugo_opt['restaurang-hours-val'] = array_filter($hugo_opt['restaurang-hours-val'],"hugo_check_empty_arr");
        if(!empty($hugo_opt['restaurang-hours-val'])){
        ?>
            <div class="restaurang-hours">
                <h2 class="modal-sub-title">
                    <?php if('yes' == $atts['in_footer']){
                        echo 'Restaurang';
                    }
                    else{
                        echo 'Öppettider Restaurang';
                    }
                    ?>
                </h2>
                <table>
                    <?php
                        foreach($hugo_opt['restaurang-hours-val'] as $restaurang_hour){
                            $restaurang_hour_arr = explode('|',$restaurang_hour);
                            $day = $restaurang_hour_arr[0];
                            $time = $restaurang_hour_arr[1];
                            ?>
                                <tr>
                                    <td><?php echo $day;?></td>
                                    <td><?php echo ' : '?></td>
                                    <td><?php echo $time;?></td>
                                </tr>
                            <?php
                        }
                    ?>
                </table>
            </div>
        <?php
        }
    }

    if( isset($hugo_opt['bowling-hours-val']) ){
        $hugo_opt['bowling-hours-val'] = array_filter($hugo_opt['bowling-hours-val'],"hugo_check_empty_arr");
        if(!empty($hugo_opt['bowling-hours-val'])){
        ?>
            <div class="bowling-hours">
                <h2 class="modal-sub-title">
                    <?php if('yes' == $atts['in_footer']){
                        echo 'Bowling';
                    }
                    else{
                        echo 'Öppettider Bowling';
                    }
                    ?>
                </h2>
                <table>
                    <?php
                    foreach($hugo_opt['bowling-hours-val'] as $bowling_hour){
                        $bowling_hour_arr = explode('|',$bowling_hour);
                        $day = $bowling_hour_arr[0];
                        $time = $bowling_hour_arr[1];
                        ?>
                        <tr>
                            <td><?php echo $day;?></td>
                            <td><?php echo ' : '?></td>
                            <td><?php echo $time;?></td>
                        </tr>
                    <?php
                    }
                    ?>
                </table>
            </div>
        <?php
        }
    }

    if( isset($hugo_opt['nightclub-hours-val']) ){
        $hugo_opt['nightclub-hours-val'] = array_filter($hugo_opt['nightclub-hours-val'],"hugo_check_empty_arr");
        if(!empty($hugo_opt['nightclub-hours-val'])){
            ?>
            <div class="nightclub-hours">
                <h2 class="modal-sub-title">
                    <?php if('yes' == $atts['in_footer']){
                        echo 'Nattklubb';
                    }
                    else{
                        echo 'Öppettider Nattklubb';
                    }
                    ?>
                </h2>
                <table>
                    <?php
                    foreach($hugo_opt['nightclub-hours-val'] as $nightclub_hour){
                        $nightclub_hour_arr = explode('|',$nightclub_hour);
                        $day = $nightclub_hour_arr[0];
                        $time = $nightclub_hour_arr[1];
                        ?>
                        <tr>
                            <td><?php echo $day;?></td>
                            <td><?php echo ' : '?></td>
                            <td><?php echo $time;?></td>
                        </tr>
                    <?php
                    }
                    ?>
                </table>
            </div>
        <?php
        }
    }
}
add_shortcode( 'opening_hours', 'hugo_opening_hours_shortcode' );

function hugo_wrapper_shortcode($atts, $content = null ){
    $atts = shortcode_atts( array(
        'class' => '',
        'type' => 'h2'
    ), $atts, 'wrapper' );
    return '<'.$atts['type'].' class="'.$atts['class'].'">' . do_shortcode($content) . '</'.$atts['type'].'>';
}
add_shortcode( 'wrapper', 'hugo_wrapper_shortcode' );

function hugo_check_empty_arr($value){
    return !empty($value);
}