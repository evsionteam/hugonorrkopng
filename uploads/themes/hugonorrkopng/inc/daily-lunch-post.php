<?php
// Register Custom Post Type
function daily_lunch_post_type() {

    $labels = array(
        'name'                  => _x( 'Daily lunch', 'Post Type General Name', 'text_domain' ),
        'singular_name'         => _x( 'Lunch', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'             => __( 'Daily lunch', 'text_domain' ),
        'name_admin_bar'        => __( 'Daily lunch', 'text_domain' ),
        'archives'              => __( 'Item Archives', 'text_domain' ),
        'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
        'all_items'             => __( 'All Items', 'text_domain' ),
        'add_new_item'          => __( 'Add New Item', 'text_domain' ),
        'add_new'               => __( 'Add New', 'text_domain' ),
        'new_item'              => __( 'New Item', 'text_domain' ),
        'edit_item'             => __( 'Edit Item', 'text_domain' ),
        'update_item'           => __( 'Update Item', 'text_domain' ),
        'view_item'             => __( 'View Item', 'text_domain' ),
        'search_items'          => __( 'Search Item', 'text_domain' ),
        'not_found'             => __( 'Not found', 'text_domain' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
        'featured_image'        => __( 'Featured Image', 'text_domain' ),
        'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
        'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
        'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
        'items_list'            => __( 'Items list', 'text_domain' ),
        'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
        'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
    );
    $args = array(
        'label'                 => __( 'Daily lunch', 'text_domain' ),
        'labels'                => $labels,
        'supports'              => array( 'title' ),
        'hierarchical'          => false,
        'public'                => true,
        'menu_icon'             => 'dashicons-carrot',
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'daily_lunch', $args );

}
add_action( 'init', 'daily_lunch_post_type', 0 ); 



/*------------------------------Start Date Start----------------------------------------------------------*/

add_action( 'add_meta_boxes', 'daily_lunch_start_time' );
function daily_lunch_start_time()
{
    add_meta_box( 'daily-lunch-starts', 'Date', 'meta_box_callback3', 'daily_lunch', 'normal', 'high' );
}


function meta_box_callback3( $post )
{
    $values1 = get_post_custom( $post->ID );
    $selected1 = isset( $values1['daily_lunch_start_date'] ) ? date('F j, Y',get_post_meta( $post->ID, 'daily_lunch_start_date', true )) : '';
    $selected2 = isset( $values1['daily_lunch_end_date'] ) ? date('F j, Y',get_post_meta( $post->ID, 'daily_lunch_end_date', true )) : '';

    wp_nonce_field( 'daily_lunch_start_date_nonce', 'my_daily_lunch_start_date_nonce' ); 
    wp_nonce_field( 'daily_lunch_end_date_nonce', 'my_daily_lunch_end_date_nonce' ); ?>


    <?php 
   echo '<input type="text" id="datepicker" name="daily_lunch_start_date" value="' . $selected1 . '">';
   echo " to ";
   echo '<input type="text" id="datepicker2" name="daily_lunch_end_date" value="' . $selected2 . '">';
       
}

add_action( 'save_post', 'daily_lunch_start_time_save' );
function daily_lunch_start_time_save( $post_id )
{
    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

    // if our nonce isn't there, or we can't verify it, bail
    if( !isset( $_POST['my_daily_lunch_start_date_nonce'] ) || !wp_verify_nonce( $_POST['my_daily_lunch_start_date_nonce'], 'daily_lunch_start_date_nonce' ) ) return;
    if( !isset( $_POST['my_daily_lunch_end_date_nonce'] ) || !wp_verify_nonce( $_POST['my_daily_lunch_start_date_nonce'], 'daily_lunch_start_date_nonce' ) ) return;

    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ,$post_id ) ) return;

    // now we can actually save the data
    $allowed = array( 
        'a' => array( // on allow a tags
            'href' => array() // and those anchords can only have href attribute
        )
    );

    // Probably a good idea to make sure your data is set

    if( isset( $_POST['daily_lunch_start_date'] ) )
        update_post_meta( $post_id, 'daily_lunch_start_date', strtotime($_POST['daily_lunch_start_date']) );



    if( isset( $_POST['daily_lunch_end_date'] ) )
        update_post_meta( $post_id, 'daily_lunch_end_date', strtotime($_POST['daily_lunch_end_date']) );

}

/*------------------------------Start Date Ends ----------------------------------------------------------*/




/*-------- Sunday Meta Box Starts ------------------------------------------------*/

add_action('admin_init', 'sunday_meta_boxes', 1);
function sunday_meta_boxes() {
    add_meta_box( 'sunday-repeatable-fields', 'Sunday', 'sunday_repeatable_meta_box_display', 'daily_lunch', 'normal', 'default');
}
function sunday_repeatable_meta_box_display() {
    global $post;
    $sunday = get_post_meta($post->ID, 'sunday', true);
    
      
    
    wp_nonce_field( 'sunday_repeatable_meta_box_nonce', 'sunday_repeatable_meta_box_nonce' );
    ?>
    <script type="text/javascript">
    jQuery(document).ready(function( $ ){
        $( '#add-row-sunday' ).on('click', function() {
            var row = $( '.empty-row-sunday.screen-reader-text' ).clone(true);
            row.removeClass( 'empty-row-sunday screen-reader-text' );
            row.insertBefore( '#repeatable-fieldset-one-sunday tbody>tr:last' );
            return false;
        });
    
        $( '.remove-row-sunday' ).on('click', function() {
            $(this).parents('tr').remove();
            return false;
        });
    });
    </script>
  
    <table id="repeatable-fieldset-one-sunday" width="100%">
    <thead>
        <tr>
            <th width="40%">Name</th>
        </tr>
    </thead>
    <tbody>
        <p><a id="add-row-sunday" class="button" href="#">Add another</a></p>
    <?php
    
    if ( $sunday ) :
    
    foreach ( $sunday as $field_sunday ) {
    ?>
    <tr>
        <td><input type="text" class="widefat" name="name_sunday[]" value="<?php if($field_sunday['name_sunday'] != '') echo esc_attr( $field_sunday['name_sunday'] ); ?>" /></td>
    
        <td><a class="button remove-row-sunday" href="#">Remove</a></td>
    </tr>
    <?php
    }
    else :
    // show a blank one
    ?>
    <tr>
        <td><input type="text" class="widefat" name="name_sunday[]" /></td>
    
        <td><a class="button remove-row-sunday" href="#">Remove</a></td>
    </tr>
    <?php endif; ?>
    
    <!-- empty hidden one for jQuery -->
    <tr class="empty-row-sunday screen-reader-text">
        <td><input type="text" class="widefat" name="name_sunday[]" /></td>
          
        <td><a class="button remove-row-sunday" href="#">Remove</a></td>
    </tr>
    </tbody>
    </table>
    
    
    <?php
}
add_action('save_post', 'sunday_repeatable_meta_box_save');
function sunday_repeatable_meta_box_save($post_id) {
    if ( ! isset( $_POST['sunday_repeatable_meta_box_nonce'] ) ||
    ! wp_verify_nonce( $_POST['sunday_repeatable_meta_box_nonce'], 'sunday_repeatable_meta_box_nonce' ) )
        return;
    
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return;
    
    if (!current_user_can('edit_post', $post_id))
        return;
    
    $old_sunday = get_post_meta($post_id, 'sunday', true);
    $new_sunday = array();
   
    
    $names_sunday = $_POST['name_sunday'];

    
    $count_sunday = count( $names_sunday );
    
    for ( $i_sunday = 0; $i_sunday < $count_sunday; $i_sunday++ ) {
        if ( $names_sunday[$i_sunday] != '' ) :
            $new_sunday[$i_sunday]['name_sunday'] = stripslashes( strip_tags( $names_sunday[$i_sunday] ) );
        endif;
    }
    if ( !empty( $new_sunday ) && $new_sunday != $old_sunday )
        update_post_meta( $post_id, 'sunday', $new_sunday );
    elseif ( empty($new_sunday) && $old_sunday )
        delete_post_meta( $post_id, 'sunday', $old_sunday );
}


/*-------- Sunday Meta Box Ends ------------------------------------------------*/



/*-------- Monday Meta Box Starts ------------------------------------------------*/

add_action('admin_init', 'monday_meta_boxes', 1);
function monday_meta_boxes() {
    add_meta_box( 'monday-repeatable-fields', 'Monday', 'monday_repeatable_meta_box_display', 'daily_lunch', 'normal', 'default');
}
function monday_repeatable_meta_box_display() {
    global $post;
    $monday = get_post_meta($post->ID, 'monday', true);
    
      
    
    wp_nonce_field( 'monday_repeatable_meta_box_nonce', 'monday_repeatable_meta_box_nonce' );
    ?>
    <script type="text/javascript">
    jQuery(document).ready(function( $ ){
        $( '#add-row-monday' ).on('click', function() {
            var row = $( '.empty-row-monday.screen-reader-text' ).clone(true);
            row.removeClass( 'empty-row-monday screen-reader-text' );
            row.insertBefore( '#repeatable-fieldset-one-monday tbody>tr:last' );
            return false;
        });
    
        $( '.remove-row-monday' ).on('click', function() {
            $(this).parents('tr').remove();
            return false;
        });
    });
    </script>
  
    <table id="repeatable-fieldset-one-monday" width="100%">
    <thead>
        <tr>
            <th width="40%">Name</th>
        </tr>
    </thead>
    <tbody>
        <p><a id="add-row-monday" class="button" href="#">Add another</a></p>
    <?php
    
    if ( $monday ) :
    
    foreach ( $monday as $field_monday ) {
    ?>
    <tr>
        <td><input type="text" class="widefat" name="name_monday[]" value="<?php if($field_monday['name_monday'] != '') echo esc_attr( $field_monday['name_monday'] ); ?>" /></td>
    
        <td><a class="button remove-row-monday" href="#">Remove</a></td>
    </tr>
    <?php
    }
    else :
    // show a blank one
    ?>
    <tr>
        <td><input type="text" class="widefat" name="name_monday[]" /></td>
    
        <td><a class="button remove-row-monday" href="#">Remove</a></td>
    </tr>
    <?php endif; ?>
    
    <!-- empty hidden one for jQuery -->
    <tr class="empty-row-monday screen-reader-text">
        <td><input type="text" class="widefat" name="name_monday[]" /></td>
          
        <td><a class="button remove-row-monday" href="#">Remove</a></td>
    </tr>
    </tbody>
    </table>
    
    
    <?php
}
add_action('save_post', 'monday_repeatable_meta_box_save');
function monday_repeatable_meta_box_save($post_id) {
    if ( ! isset( $_POST['monday_repeatable_meta_box_nonce'] ) ||
    ! wp_verify_nonce( $_POST['monday_repeatable_meta_box_nonce'], 'monday_repeatable_meta_box_nonce' ) )
        return;
    
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return;
    
    if (!current_user_can('edit_post', $post_id))
        return;
    
    $old_monday = get_post_meta($post_id, 'monday', true);
    $new_monday = array();
   
    
    $names_monday = $_POST['name_monday'];

    
    $count_monday = count( $names_monday );
    
    for ( $i_monday = 0; $i_monday < $count_monday; $i_monday++ ) {
        if ( $names_monday[$i_monday] != '' ) :
            $new_monday[$i_monday]['name_monday'] = stripslashes( strip_tags( $names_monday[$i_monday] ) );
        endif;
    }
    if ( !empty( $new_monday ) && $new_monday != $old_monday )
        update_post_meta( $post_id, 'monday', $new_monday );
    elseif ( empty($new_monday) && $old_monday )
        delete_post_meta( $post_id, 'monday', $old_monday );
}


/*-------- Monday Meta Box Ends ------------------------------------------------*/




/*-------- Tuesday Meta Box Starts ------------------------------------------------*/

add_action('admin_init', 'tuesday_meta_boxes', 1);
function tuesday_meta_boxes() {
    add_meta_box( 'tuesday-repeatable-fields', 'Tuesday', 'tuesday_repeatable_meta_box_display', 'daily_lunch', 'normal', 'default');
}
function tuesday_repeatable_meta_box_display() {
    global $post;
    $tuesday = get_post_meta($post->ID, 'tuesday', true);
    
       
    
    wp_nonce_field( 'tuesday_repeatable_meta_box_nonce', 'tuesday_repeatable_meta_box_nonce' );
    ?>
    <script type="text/javascript">
    jQuery(document).ready(function( $ ){
        $( '#add-row-tuesday' ).on('click', function() {
            var row = $( '.empty-row-tuesday.screen-reader-text' ).clone(true);
            row.removeClass( 'empty-row-tuesday screen-reader-text' );
            row.insertBefore( '#repeatable-fieldset-one-tuesday tbody>tr:last' );
            return false;
        });
    
        $( '.remove-row-tuesday' ).on('click', function() {
            $(this).parents('tr').remove();
            return false;
        });
    });
    </script>
  
    <table id="repeatable-fieldset-one-tuesday" width="100%">
    <thead>
        <tr>
            <th width="40%">Name</th>
        </tr>
    </thead>
    <tbody>
        <p><a id="add-row-tuesday" class="button" href="#">Add another</a></p>
    <?php
    
    if ( $tuesday ) :
    
    foreach ( $tuesday as $field_tuesday ) {
    ?>
    <tr>
        <td><input type="text" class="widefat" name="name_tuesday[]" value="<?php if($field_tuesday['name_tuesday'] != '') echo esc_attr( $field_tuesday['name_tuesday'] ); ?>" /></td>
    
        <td><a class="button remove-row-tuesday" href="#">Remove</a></td>
    </tr>
    <?php
    }
    else :
    // show a blank one
    ?>
    <tr>
        <td><input type="text" class="widefat" name="name_tuesday[]" /></td>
    
        <td><a class="button remove-row-tuesday" href="#">Remove</a></td>
    </tr>
    <?php endif; ?>
    
    <!-- empty hidden one for jQuery -->
    <tr class="empty-row-tuesday screen-reader-text">
        <td><input type="text" class="widefat" name="name_tuesday[]" /></td>
          
        <td><a class="button remove-row-tuesday" href="#">Remove</a></td>
    </tr>
    </tbody>
    </table>
    
    
    <?php
}
add_action('save_post', 'tuesday_repeatable_meta_box_save');
function tuesday_repeatable_meta_box_save($post_id) {
    if ( ! isset( $_POST['tuesday_repeatable_meta_box_nonce'] ) ||
    ! wp_verify_nonce( $_POST['tuesday_repeatable_meta_box_nonce'], 'tuesday_repeatable_meta_box_nonce' ) )
        return;
    
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return;
    
    if (!current_user_can('edit_post', $post_id))
        return;
    
    $old_tuesday = get_post_meta($post_id, 'tuesday', true);
    $new_tuesday = array();
   
    
    $names_tuesday = $_POST['name_tuesday'];

    
    $count_tuesday = count( $names_tuesday );
    
    for ( $i_tuesday = 0; $i_tuesday < $count_tuesday; $i_tuesday++ ) {
        if ( $names_tuesday[$i_tuesday] != '' ) :
            $new_tuesday[$i_tuesday]['name_tuesday'] = stripslashes( strip_tags( $names_tuesday[$i_tuesday] ) );
        endif;
    }
    if ( !empty( $new_tuesday ) && $new_tuesday != $old_tuesday )
        update_post_meta( $post_id, 'tuesday', $new_tuesday );
    elseif ( empty($new_tuesday) && $old_tuesday )
        delete_post_meta( $post_id, 'tuesday', $old_tuesday );
}


/*-------- Tuesday Meta Box Ends ------------------------------------------------*/



/*-------- Wednesday Meta Box Starts ------------------------------------------------*/

add_action('admin_init', 'wednesday_meta_boxes', 1);
function wednesday_meta_boxes() {
    add_meta_box( 'wednesday-repeatable-fields', 'Wednesday', 'wednesday_repeatable_meta_box_display', 'daily_lunch', 'normal', 'default');
}
function wednesday_repeatable_meta_box_display() {
    global $post;
    $wednesday = get_post_meta($post->ID, 'wednesday', true);
    
    
    
    
    wp_nonce_field( 'wednesday_repeatable_meta_box_nonce', 'wednesday_repeatable_meta_box_nonce' );
    ?>
    <script type="text/javascript">
    jQuery(document).ready(function( $ ){
        $( '#add-row-wednesday' ).on('click', function() {
            var row = $( '.empty-row-wednesday.screen-reader-text' ).clone(true);
            row.removeClass( 'empty-row-wednesday screen-reader-text' );
            row.insertBefore( '#repeatable-fieldset-one-wednesday tbody>tr:last' );
            return false;
        });
    
        $( '.remove-row-wednesday' ).on('click', function() {
            $(this).parents('tr').remove();
            return false;
        });
    });
    </script>
  
    <table id="repeatable-fieldset-one-wednesday" width="100%">
    <thead>
        <tr>
            <th width="40%">Name</th>
        </tr>
    </thead>
    <tbody>
        <p><a id="add-row-wednesday" class="button" href="#">Add another</a></p>
    <?php
    
    if ( $wednesday ) :
    
    foreach ( $wednesday as $field_wednesday ) {
    ?>
    <tr>
        <td><input type="text" class="widefat" name="name_wednesday[]" value="<?php if($field_wednesday['name_wednesday'] != '') echo esc_attr( $field_wednesday['name_wednesday'] ); ?>" /></td>
    
        <td><a class="button remove-row-wednesday" href="#">Remove</a></td>
    </tr>
    <?php
    }
    else :
    // show a blank one
    ?>
    <tr>
        <td><input type="text" class="widefat" name="name_wednesday[]" /></td>
    
        <td><a class="button remove-row-wednesday" href="#">Remove</a></td>
    </tr>
    <?php endif; ?>
    
    <!-- empty hidden one for jQuery -->
    <tr class="empty-row-wednesday screen-reader-text">
        <td><input type="text" class="widefat" name="name_wednesday[]" /></td>
          
        <td><a class="button remove-row-wednesday" href="#">Remove</a></td>
    </tr>
    </tbody>
    </table>
    
    
    <?php
}
add_action('save_post', 'wednesday_repeatable_meta_box_save');
function wednesday_repeatable_meta_box_save($post_id) {
    if ( ! isset( $_POST['wednesday_repeatable_meta_box_nonce'] ) ||
    ! wp_verify_nonce( $_POST['wednesday_repeatable_meta_box_nonce'], 'wednesday_repeatable_meta_box_nonce' ) )
        return;
    
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return;
    
    if (!current_user_can('edit_post', $post_id))
        return;
    
    $old_wednesday = get_post_meta($post_id, 'wednesday', true);
    $new_wednesday = array();
   
    
    $names_wednesday = $_POST['name_wednesday'];

    
    $count_wednesday = count( $names_wednesday );
    
    for ( $i_wednesday = 0; $i_wednesday < $count_wednesday; $i_wednesday++ ) {
        if ( $names_wednesday[$i_wednesday] != '' ) :
            $new_wednesday[$i_wednesday]['name_wednesday'] = stripslashes( strip_tags( $names_wednesday[$i_wednesday] ) );
        endif;
    }
    if ( !empty( $new_wednesday ) && $new_wednesday != $old_wednesday )
        update_post_meta( $post_id, 'wednesday', $new_wednesday );
    elseif ( empty($new_wednesday) && $old_wednesday )
        delete_post_meta( $post_id, 'wednesday', $old_wednesday );
}


/*-------- Wednesday Meta Box Ends ------------------------------------------------*/





/*-------- Thursday Meta Box Starts ------------------------------------------------*/

add_action('admin_init', 'thursday_meta_boxes', 1);
function thursday_meta_boxes() {
    add_meta_box( 'thursday-repeatable-fields', 'Thursday', 'thursday_repeatable_meta_box_display', 'daily_lunch', 'normal', 'default');
}
function thursday_repeatable_meta_box_display() {
    global $post;
    $thursday = get_post_meta($post->ID, 'thursday', true);
    
        
    
    wp_nonce_field( 'thursday_repeatable_meta_box_nonce', 'thursday_repeatable_meta_box_nonce' );
    ?>
    <script type="text/javascript">
    jQuery(document).ready(function( $ ){
        $( '#add-row-thursday' ).on('click', function() {
            var row = $( '.empty-row-thursday.screen-reader-text' ).clone(true);
            row.removeClass( 'empty-row-thursday screen-reader-text' );
            row.insertBefore( '#repeatable-fieldset-one-thursday tbody>tr:last' );
            return false;
        });
    
        $( '.remove-row-thursday' ).on('click', function() {
            $(this).parents('tr').remove();
            return false;
        });
    });
    </script>
  
    <table id="repeatable-fieldset-one-thursday" width="100%">
    <thead>
        <tr>
            <th width="40%">Name</th>
        </tr>
    </thead>
    <tbody>
        <p><a id="add-row-thursday" class="button" href="#">Add another</a></p>
    <?php
    
    if ( $thursday ) :
    
    foreach ( $thursday as $field_thursday ) {
    ?>
    <tr>
        <td><input type="text" class="widefat" name="name_thursday[]" value="<?php if($field_thursday['name_thursday'] != '') echo esc_attr( $field_thursday['name_thursday'] ); ?>" /></td>
    
        <td><a class="button remove-row-thursday" href="#">Remove</a></td>
    </tr>
    <?php
    }
    else :
    // show a blank one
    ?>
    <tr>
        <td><input type="text" class="widefat" name="name_thursday[]" /></td>
    
        <td><a class="button remove-row-thursday" href="#">Remove</a></td>
    </tr>
    <?php endif; ?>
    
    <!-- empty hidden one for jQuery -->
    <tr class="empty-row-thursday screen-reader-text">
        <td><input type="text" class="widefat" name="name_thursday[]" /></td>
          
        <td><a class="button remove-row-thursday" href="#">Remove</a></td>
    </tr>
    </tbody>
    </table>
    
    
    <?php
}
add_action('save_post', 'thursday_repeatable_meta_box_save');
function thursday_repeatable_meta_box_save($post_id) {
    if ( ! isset( $_POST['thursday_repeatable_meta_box_nonce'] ) ||
    ! wp_verify_nonce( $_POST['thursday_repeatable_meta_box_nonce'], 'thursday_repeatable_meta_box_nonce' ) )
        return;
    
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return;
    
    if (!current_user_can('edit_post', $post_id))
        return;
    
    $old_thursday = get_post_meta($post_id, 'thursday', true);
    $new_thursday = array();
   
    
    $names_thursday = $_POST['name_thursday'];

    
    $count_thursday = count( $names_thursday );
    
    for ( $i_thursday = 0; $i_thursday < $count_thursday; $i_thursday++ ) {
        if ( $names_thursday[$i_thursday] != '' ) :
            $new_thursday[$i_thursday]['name_thursday'] = stripslashes( strip_tags( $names_thursday[$i_thursday] ) );
        endif;
    }
    if ( !empty( $new_thursday ) && $new_thursday != $old_thursday )
        update_post_meta( $post_id, 'thursday', $new_thursday );
    elseif ( empty($new_thursday) && $old_thursday )
        delete_post_meta( $post_id, 'thursday', $old_thursday );
}


/*-------- Thursday Meta Box Ends ------------------------------------------------*/





/*-------- Friday Meta Box Starts ------------------------------------------------*/

add_action('admin_init', 'friday_meta_boxes', 1);
function friday_meta_boxes() {
    add_meta_box( 'friday-repeatable-fields', 'Friday', 'friday_repeatable_meta_box_display', 'daily_lunch', 'normal', 'default');
}
function friday_repeatable_meta_box_display() {
    global $post;
    $friday = get_post_meta($post->ID, 'friday', true);
    
      
    
    wp_nonce_field( 'friday_repeatable_meta_box_nonce', 'friday_repeatable_meta_box_nonce' );
    ?>
    <script type="text/javascript">
    jQuery(document).ready(function( $ ){
        $( '#add-row-friday' ).on('click', function() {
            var row = $( '.empty-row-friday.screen-reader-text' ).clone(true);
            row.removeClass( 'empty-row-friday screen-reader-text' );
            row.insertBefore( '#repeatable-fieldset-one-friday tbody>tr:last' );
            return false;
        });
    
        $( '.remove-row-friday' ).on('click', function() {
            $(this).parents('tr').remove();
            return false;
        });
    });
    </script>
  
    <table id="repeatable-fieldset-one-friday" width="100%">
    <thead>
        <tr>
            <th width="40%">Name</th>
        </tr>
    </thead>
    <tbody>
        <p><a id="add-row-friday" class="button" href="#">Add another</a></p>
    <?php
    
    if ( $friday ) :
    
    foreach ( $friday as $field_friday ) {
    ?>
    <tr>
        <td><input type="text" class="widefat" name="name_friday[]" value="<?php if($field_friday['name_friday'] != '') echo esc_attr( $field_friday['name_friday'] ); ?>" /></td>
    
        <td><a class="button remove-row-friday" href="#">Remove</a></td>
    </tr>
    <?php
    }
    else :
    // show a blank one
    ?>
    <tr>
        <td><input type="text" class="widefat" name="name_friday[]" /></td>
    
        <td><a class="button remove-row-friday" href="#">Remove</a></td>
    </tr>
    <?php endif; ?>
    
    <!-- empty hidden one for jQuery -->
    <tr class="empty-row-friday screen-reader-text">
        <td><input type="text" class="widefat" name="name_friday[]" /></td>
          
        <td><a class="button remove-row-friday" href="#">Remove</a></td>
    </tr>
    </tbody>
    </table>
    
    
    <?php
}
add_action('save_post', 'friday_repeatable_meta_box_save');
function friday_repeatable_meta_box_save($post_id) {
    if ( ! isset( $_POST['friday_repeatable_meta_box_nonce'] ) ||
    ! wp_verify_nonce( $_POST['friday_repeatable_meta_box_nonce'], 'friday_repeatable_meta_box_nonce' ) )
        return;
    
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return;
    
    if (!current_user_can('edit_post', $post_id))
        return;
    
    $old_friday = get_post_meta($post_id, 'friday', true);
    $new_friday = array();
   
    
    $names_friday = $_POST['name_friday'];

    
    $count_friday = count( $names_friday );
    
    for ( $i_friday = 0; $i_friday < $count_friday; $i_friday++ ) {
        if ( $names_friday[$i_friday] != '' ) :
            $new_friday[$i_friday]['name_friday'] = stripslashes( strip_tags( $names_friday[$i_friday] ) );
        endif;
    }
    if ( !empty( $new_friday ) && $new_friday != $old_friday )
        update_post_meta( $post_id, 'friday', $new_friday );
    elseif ( empty($new_friday) && $old_friday )
        delete_post_meta( $post_id, 'friday', $old_friday );
}


/*-------- Friday Meta Box Ends ------------------------------------------------*/





/*-------- Saturday Meta Box Starts ------------------------------------------------*/

add_action('admin_init', 'saturday_meta_boxes', 1);
function saturday_meta_boxes() {
    add_meta_box( 'saturday-repeatable-fields', 'Saturday', 'saturday_repeatable_meta_box_display', 'daily_lunch', 'normal', 'default');
}
function saturday_repeatable_meta_box_display() {
    global $post;
    $saturday = get_post_meta($post->ID, 'saturday', true);
    
        
    
    wp_nonce_field( 'saturday_repeatable_meta_box_nonce', 'saturday_repeatable_meta_box_nonce' );
    ?>
    <script type="text/javascript">
    jQuery(document).ready(function( $ ){
        $( '#add-row-saturday' ).on('click', function() {
            var row = $( '.empty-row-saturday.screen-reader-text' ).clone(true);
            row.removeClass( 'empty-row-saturday screen-reader-text' );
            row.insertBefore( '#repeatable-fieldset-one-saturday tbody>tr:last' );
            return false;
        });
    
        $( '.remove-row-saturday' ).on('click', function() {
            $(this).parents('tr').remove();
            return false;
        });
    });
    </script>
  
    <table id="repeatable-fieldset-one-saturday" width="100%">
    <thead>
        <tr>
            <th width="40%">Name</th>
        </tr>
    </thead>
    <tbody>
        <p><a id="add-row-saturday" class="button" href="#">Add another</a></p>
    <?php
    
    if ( $saturday ) :
    
    foreach ( $saturday as $field_saturday ) {
    ?>
    <tr>
        <td><input type="text" class="widefat" name="name_saturday[]" value="<?php if($field_saturday['name_saturday'] != '') echo esc_attr( $field_saturday['name_saturday'] ); ?>" /></td>
    
        <td><a class="button remove-row-saturday" href="#">Remove</a></td>
    </tr>
    <?php
    }
    else :
    // show a blank one
    ?>
    <tr>
        <td><input type="text" class="widefat" name="name_saturday[]" /></td>
    
        <td><a class="button remove-row-saturday" href="#">Remove</a></td>
    </tr>
    <?php endif; ?>
    
    <!-- empty hidden one for jQuery -->
    <tr class="empty-row-saturday screen-reader-text">
        <td><input type="text" class="widefat" name="name_saturday[]" /></td>
          
        <td><a class="button remove-row-saturday" href="#">Remove</a></td>
    </tr>
    </tbody>
    </table>
    
    
    <?php
}
add_action('save_post', 'saturday_repeatable_meta_box_save');
function saturday_repeatable_meta_box_save($post_id) {
    if ( ! isset( $_POST['saturday_repeatable_meta_box_nonce'] ) ||
    ! wp_verify_nonce( $_POST['saturday_repeatable_meta_box_nonce'], 'saturday_repeatable_meta_box_nonce' ) )
        return;
    
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return;
    
    if (!current_user_can('edit_post', $post_id))
        return;
    
    $old_saturday = get_post_meta($post_id, 'saturday', true);
    $new_saturday = array();
   
    
    $names_saturday = $_POST['name_saturday'];

    
    $count_saturday = count( $names_saturday );
    
    for ( $i_saturday = 0; $i_saturday < $count_saturday; $i_saturday++ ) {
        if ( $names_saturday[$i_saturday] != '' ) :
            $new_saturday[$i_saturday]['name_saturday'] = stripslashes( strip_tags( $names_saturday[$i_saturday] ) );
        endif;
    }
    if ( !empty( $new_saturday ) && $new_saturday != $old_saturday )
        update_post_meta( $post_id, 'saturday', $new_saturday );
    elseif ( empty($new_saturday) && $old_saturday )
        delete_post_meta( $post_id, 'saturday', $old_saturday );
}


/*-------- Saturday Meta Box Ends ------------------------------------------------*/


/*-------- Other Info Meta Box Starts ------------------------------------------------*/

add_action('admin_init', 'other_info_meta_boxes', 1);
function other_info_meta_boxes() {
    add_meta_box( 'other_info-repeatable-fields', 'Other Info', 'other_info_repeatable_meta_box_display', 'daily_lunch', 'normal', 'default');
}
function other_info_repeatable_meta_box_display() {
    global $post;
    $other_info = get_post_meta($post->ID, 'other_info', true);
    
        
    
    wp_nonce_field( 'other_info_repeatable_meta_box_nonce', 'other_info_repeatable_meta_box_nonce' );
    ?>
    <script type="text/javascript">
    jQuery(document).ready(function( $ ){
        $( '#add-row-other_info' ).on('click', function() {
            var row = $( '.empty-row-other_info.screen-reader-text' ).clone(true);
            row.removeClass( 'empty-row-other_info screen-reader-text' );
            row.insertBefore( '#repeatable-fieldset-one-other_info tbody>tr:last' );
            return false;
        });
    
        $( '.remove-row-other_info' ).on('click', function() {
            $(this).parents('tr').remove();
            return false;
        });
    });
    </script>
  
    <table id="repeatable-fieldset-one-other_info" width="100%">
    <thead>
        <tr>
            <th width="40%">Name</th>
        </tr>
    </thead>
    <tbody>
        <p><a id="add-row-other_info" class="button" href="#">Add another</a></p>
    <?php
    
    if ( $other_info ) :
    
    foreach ( $other_info as $field_other_info ) {
    ?>
    <tr>
        <td><input type="text" class="widefat" name="name_other_info[]" value="<?php if($field_other_info['name_other_info'] != '') echo esc_attr( $field_other_info['name_other_info'] ); ?>" /></td>
    
        <td><a class="button remove-row-other_info" href="#">Remove</a></td>
    </tr>
    <?php
    }
    else :
    // show a blank one
    ?>
    <tr>
        <td><input type="text" class="widefat" name="name_other_info[]" /></td>
    
        <td><a class="button remove-row-other_info" href="#">Remove</a></td>
    </tr>
    <?php endif; ?>
    
    <!-- empty hidden one for jQuery -->
    <tr class="empty-row-other_info screen-reader-text">
        <td><input type="text" class="widefat" name="name_other_info[]" /></td>
          
        <td><a class="button remove-row-other_info" href="#">Remove</a></td>
    </tr>
    </tbody>
    </table>
    
    
    <?php
}
add_action('save_post', 'other_info_repeatable_meta_box_save');
function other_info_repeatable_meta_box_save($post_id) {
    if ( ! isset( $_POST['other_info_repeatable_meta_box_nonce'] ) ||
    ! wp_verify_nonce( $_POST['other_info_repeatable_meta_box_nonce'], 'other_info_repeatable_meta_box_nonce' ) )
        return;
    
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return;
    
    if (!current_user_can('edit_post', $post_id))
        return;
    
    $old_other_info = get_post_meta($post_id, 'other_info', true);
    $new_other_info = array();
   
    
    $names_other_info = $_POST['name_other_info'];

    
    $count_other_info = count( $names_other_info );
    
    for ( $i_other_info = 0; $i_other_info < $count_other_info; $i_other_info++ ) {
        if ( $names_other_info[$i_other_info] != '' ) :
            $new_other_info[$i_other_info]['name_other_info'] = stripslashes( strip_tags( $names_other_info[$i_other_info] ) );
        endif;
    }
    if ( !empty( $new_other_info ) && $new_other_info != $old_other_info )
        update_post_meta( $post_id, 'other_info', $new_other_info );
    elseif ( empty($new_other_info) && $old_other_info )
        delete_post_meta( $post_id, 'other_info', $old_other_info );
}


/*-------- other_info Meta Box Ends ------------------------------------------------*/


