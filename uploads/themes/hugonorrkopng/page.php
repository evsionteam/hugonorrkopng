<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package hugonorrkopng
 */

get_header();

$overlay_class = '';
$overlay_value = get_post_meta(get_the_ID(),'page_overlay',true);
if('yes' == $overlay_value){
    $overlay_class = ' overlay ';
}
?>
    <div class="inner-page contact">
        <!-- IMG WRAPPER -->
        <div class="image-cover">
            <div class="img-wrapper jarallax <?php echo $overlay_class;?>" data-jarallax='{"speed": 0.2}' style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id(get_the_ID())); ?>');">
                <!-- Caption -->
                <div class="caption">
                    <header class="entry-header"><?php the_title(); ?></header>
                    <!-- .entry-header -->
                    <?php $button_text = get_post_meta(get_the_ID(), 'button_text', true); ?>
                    <?php $button_link = get_post_meta(get_the_ID(), 'button_link', true); ?>
                    <?php if (!empty($button_link) && !empty($button_text)) { ?>
                        <div class="btn-link"><a href="<?php echo get_post_meta(get_the_ID(), 'button_link', true); ?>" class="restro-btn"><?php echo get_post_meta(get_the_ID(), 'button_text', true); ?></a></div>
                    <?php } ?>
                </div>
            </div><!-- /.img-wrapper -->
        </div><!-- /.img-wrapper -->

        <!-- DESCRIPTION -->
        <div class="container">
            <div class="description">
                <div class="entry-content">
                    <div class="text-left">
                        <?php
                        while ( have_posts() ) : the_post();
                            //get_template_part( 'template-parts/content', 'page' );
                            the_content();
                        endwhile; // End of the loop.
                        ?>
                    </div>
                </div>
            </div><!-- /.description -->
        </div><!-- /.container -->
    </div><!-- /.contact -->
<?php
get_footer();
