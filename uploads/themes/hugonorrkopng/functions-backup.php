<?php

if ( ! function_exists( 'hugonorrkopng_setup' ) ) :

function hugonorrkopng_setup() {
	load_theme_textdomain( 'hugonorrkopng', get_template_directory() . '/languages' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );

	register_nav_menus( array(
		'left_primary_menu' => esc_html__( 'Left Header Menu', 'hugonorrkopng' ),
		'right_primary_menu' => esc_html__( 'Right Header Menu', 'hugonorrkopng' ),
	) );

	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );



	add_image_size( 'home-large-slider', 1349, 392, true );
	add_image_size( 'home-square-slider', 392, 392, true );

}
endif;
add_action( 'after_setup_theme', 'hugonorrkopng_setup' );


function hugonorrkopng_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'hugonorrkopng_content_width', 640 );
}
add_action( 'after_setup_theme', 'hugonorrkopng_content_width', 0 );


function hugonorrkopng_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'hugonorrkopng' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'hugonorrkopng' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
		register_sidebar( array(
		'name'          => esc_html__( 'Footer 1', 'hugonorrkopng' ),
		'id'            => 'footer-1',
		'description'   => esc_html__( 'Add widgets here.', 'hugonorrkopng' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
		register_sidebar( array(
		'name'          => esc_html__( 'Footer 2', 'hugonorrkopng' ),
		'id'            => 'footer-2',
		'description'   => esc_html__( 'Add widgets here.', 'hugonorrkopng' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'hugonorrkopng_widgets_init' );


function hugonorrkopng_scripts() {
	wp_enqueue_style( 'hugonorrkopng-style', get_stylesheet_uri() );
	wp_register_style( 'jquery-ui', 'http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css' );
    wp_enqueue_style( 'jquery-ui' ); 

    wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'hugonorrkopng-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'hugonorrkopng-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	wp_enqueue_script( 'hugonorrkopng-datepck', get_template_directory_uri() . '/js/custom.js', array('jquery-ui-datepicker'), '', true );
	



	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	$assets_url = get_stylesheet_directory_uri().'/assets/build';

   $suffix = '.min';    if( defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ){

       $suffix = '';

   }    wp_enqueue_script( 'hugo-script', $assets_url . '/js/script' . $suffix . '.js', array( 'jquery'), null, false );    $main_css = $assets_url . '/css/main'.$suffix.'.css';

   $script = "

      var head  = document.getElementsByTagName('head')[0];

      var link  = document.createElement('link');

      link.rel  = 'stylesheet';

      link.type = 'text/css';

      link.href = '".$main_css."';

      link.media = 'all';

      head.appendChild(link);

   ";

   wp_add_inline_script( 'hugo-script',$script );

       wp_localize_script( 'hugo-script', 'hugoAjax' ,array(
            'ajaxurl' => admin_url( 'admin-ajax.php' ),
        )
    );
}
add_action( 'wp_enqueue_scripts', 'hugonorrkopng_scripts' );



require get_template_directory() . '/inc/template-tags.php';
require get_template_directory() . '/inc/extras.php';
require get_template_directory() . '/inc/daily-lunch-post.php';
require get_template_directory() . '/inc/home-slider-post.php';



add_action( 'admin_enqueue_scripts', 'function_name' );
function function_name() {
	
	wp_register_style( 'jquery-ui', 'http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css' );
    wp_enqueue_style( 'jquery-ui' ); 

	wp_enqueue_script( 'hugonorrkopng-datepck', get_template_directory_uri() . '/js/custom.js', array('jquery-ui-datepicker'), '', true );



	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}


require_once ( __DIR__ . '/redux/config.php' );
require_once ( __DIR__ . '/inc/Mobile_Detect.php' );

add_action( 'init', 'my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}


 add_filter( 'nav_menu_link_attributes', 'themeprefix_menu_attribute_add', 10, 3 );
function themeprefix_menu_attribute_add( $atts, $item, $args )
{
 
    $atts['page-id'] = $item->object_id;
    /*$ajax_page_id = $atts['page-id']*/
  
  //Return the new attribute
  return $atts;
}

function load_page_results(){

    $output['data'] = '';
    $page_id = $_POST['page_id'];

    ob_start();
    ?>
 
 	<?php 
 	global $post;
 	$temp = get_post($page_id); 
 	$post = $temp;
 	setup_postdata( $post ); 
 	
 	get_template_part( 'template-parts/content', 'page' );


 	?>
        

    <?php
    $output['data'][] = ob_get_clean();

    echo json_encode($output);
    exit;
}
add_action('wp_ajax_nopriv_load_page_results', 'load_page_results');
add_action('wp_ajax_load_page_results', 'load_page_results');


function page_menu_link( $sorted_menu_items, $args ) {

foreach ($sorted_menu_items as $sorted_menu_item) {
	if($sorted_menu_item->object == 'page'){
		$sorted_menu_item->url = '#';
	}
}
return $sorted_menu_items;
}
add_filter( 'wp_nav_menu_objects', 'page_menu_link', 10, 3 );