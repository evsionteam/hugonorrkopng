<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package hugonorrkopng
 */
global $temp;
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<!-- IMG WRAPPER -->
	<div class="img-wrapper">
		<?php the_post_thumbnail(); ?>

		<!-- Caption -->
		<div class="caption">
			<header class="entry-header">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			</header><!-- .entry-header -->
			<a href="" class="restro-btn">Boka</a>
		</div><!-- /caption -->
		
	</div><!-- /.img-wrapper -->

	<!-- DESCRIPTION -->
	<div class="description">
		<div class="entry-content">
			<?php
				the_content();

				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'hugonorrkopng' ),
					'after'  => '</div>',
				) );
			?>
		</div><!-- .entry-content -->
	</div><!-- /description -->

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
				edit_post_link(
					sprintf(
						/* translators: %s: Name of current post */
						esc_html__( 'Edit %s', 'hugonorrkopng' ),
						the_title( '<span class="screen-reader-text">"', '"</span>', false )
					),
					'<span class="edit-link">',
					'</span>'
				);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-## -->
