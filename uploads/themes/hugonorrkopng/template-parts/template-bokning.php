<?php
/* Template Name: Bokning */
get_header();
while (have_posts()) : the_post();
    $overlay_class = '';
    $overlay_value = get_post_meta(get_the_ID(),'page_overlay',true);
    if('yes' == $overlay_value){
        $overlay_class = ' overlay ';
    }
    ?>
<div class="inner-page ">
    <div class="bokning">

        <!-- IMG WRAPPER -->
        <div class="image-cover">

            <div class="img-wrapper jarallax <?php echo $overlay_class;?>" data-jarallax='{"speed": 0.2}' style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id(get_the_ID())); ?>');">
                <!-- Caption -->

                <div class="caption">
                    <header class="entry-header"><?php the_title(); ?></header>
                    <!-- .entry-header -->
                    <?php $button_text = get_post_meta(get_the_ID(), 'button_text', true); ?>
                    <?php $button_link = get_post_meta(get_the_ID(), 'button_link', true); ?>
                    <?php if (!empty($button_link) && !empty($button_text)) { ?>
                        <div class="btn-link"><a href="<?php echo get_post_meta(get_the_ID(), 'button_link', true); ?>" class="restro-btn"><?php echo get_post_meta(get_the_ID(), 'button_text', true); ?></a></div>
                    <?php } ?>
                </div>
            </div><!-- /.img-wrapper -->
        </div><!-- /.img-wrapper -->

        <div class="container">
            <!-- DESCRIPTION -->
            <div class="description">

                <div class="entry-content"><p><?php the_content(); ?></p>
                </div>
            </div><!-- /.description -->
            <!-- /description -->
            <?php $menu_select = get_post_meta( get_the_ID(),'slider_design_select_meta_box1', true ); ?>
                        <div id="modal-restaurant-block">
                            <div class="container">
                            	<?php $args = array('post__in' => $menu_select, 'post_type' => 'menu');
                    		$query = new WP_Query($args); ?>
                            <?php while ($query->have_posts()) : $query->the_post(); ?>
                            <?php 
                            	$attach_pdf_id = '';
                            	$pdf_link = '';
                            	$attach_pdf_id = get_post_meta( get_the_ID(), '_image_id', true );
                            	if(!empty($attach_pdf_id)){
                                 $pdf_link = wp_get_attachment_url( $attach_pdf_id );
                             }
                            ?>
                        	<div class="col-sm-4 col-md-3 col-xs-6 restro-block">
                                            <a href="<?php echo $pdf_link; ?>">
                                                <div class="resturant-img">
                                                    <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );?>" alt="image">
                                                </div>
                                            </a>
                                            <a href="<?php echo $pdf_link; ?>">
                                                <div class="resturant-title"><?php the_title(); ?></div>
                                            </a>
                                            <p></p><?php the_content(); ?>
											<p></p>
                                            <a href="<?php echo $pdf_link; ?>" class="see-more">
                                                Se meny
                                            </a>
                                            </div>

                                            <?php endwhile; ?> 
                            				<?php wp_reset_postdata(); 
                            			?>
                                <div class="row"></div>
                            </div>
                        </div>
        </div><!-- /.container -->
    </div><!-- /.inner-page bokning -->
</div><!-- /.inner-page -->

    <?php
endwhile;
wp_reset_query();
get_footer();
?>