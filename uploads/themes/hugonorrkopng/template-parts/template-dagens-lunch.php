<?php
/* Template Name: Dagens Lunch */
?>
<?php
get_header();
global $hugo_opt;
while (have_posts()) : the_post();
    $overlay_class = '';
    $overlay_value = get_post_meta(get_the_ID(), 'page_overlay', true);
    if ('yes' == $overlay_value) {
        $overlay_class = ' overlay ';
    }
    ?>

    <div class="inner-page dagen-lunch">
        <!-- IMG WRAPPER -->
        <div class="image-cover">
            <div class="img-wrapper jarallax <?php echo $overlay_class; ?>" data-jarallax='{"speed": 0.2}' style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id(get_the_ID())); ?>');">
                <!-- Caption -->
                <div class="caption">
                    <header class="entry-header"><?php the_title(); ?></header>
                    <!-- .entry-header -->
                    <?php $button_text = get_post_meta(get_the_ID(), 'button_text', true); ?>
                    <?php $button_link = get_post_meta(get_the_ID(), 'button_link', true); ?>
                    <?php if (!empty($button_link) && !empty($button_text)) { ?>
                        <div class="btn-link">
                            <a href="<?php echo get_post_meta(get_the_ID(), 'button_link', true); ?>" class="restro-btn" id="fs-down-arrow" data-target="#daily-lunch-menu" data-reduce-offset="90" data-animation-duration="1000">
                                <?php echo get_post_meta(get_the_ID(), 'button_text', true); ?>
                            </a>
                        </div>
                    <?php } ?>
                </div>
            </div><!-- /.img-wrapper -->
            
        </div><!-- /.img-wrapper -->

        <div class="container">
            <!-- DESCRIPTION -->
            <div class="container">
                <div class="description">
                    <!-- TEXT LOADER -->
                    <div class="entry-content"></div>
                    <div class="daily-lunch-wrapper text-left">
                        <div class="row">
                            <div class="col-sm-9">
                                <div class="daily-lunch-desc">
                                    <?php the_content(); ?>
                                </div>
                            </div>

                            <?php
                            $heading = $daily_lunch_pdf_output = '';

                            $current_weekNumber = date("W");
                            $current_date = strtotime(date('F j, Y'));

                            $next_monday = strtotime('next monday');
                            $next_monday_weekNumber = date('W', $next_monday);

                            $current_week_menu_items = get_daily_lunch_menu_items($current_date);
                            $next_week_menu_items = get_daily_lunch_menu_items($next_monday);

                            $daily_lunch_pdf_output .= '<div class="daily-lunch-pdf-wrapper">';

                            if (!empty($current_week_menu_items)) {
                                $heading = '<div class="current-week">Lunchmeny vecka ' . $current_weekNumber . '</div>';

                                $daily_lunch_pdf_output .= '<div class="current-week-pdf week-pdf">';
                                $daily_lunch_pdf_output .= '<div class="title"><a href="#" data-date="' . $current_date . '" data-week="' . $current_weekNumber . '">Lunchmeny vecka ' . $current_weekNumber . '</a></div>';
                                if(isset($hugo_opt['daily-lunch-pdf-desc'])){
                                    $daily_lunch_pdf_output .= '<div class="download-info">'.$hugo_opt['daily-lunch-pdf-desc'].'</div>';
                                }
                                $daily_lunch_pdf_output .= '<a href="' . get_site_url('', '?action=print-menu-this-week') . '" class="download-link" target="_blank">Ladda ner PDF</a>';
                                $daily_lunch_pdf_output .= '</div>';
                            }

                            if (!empty($next_week_menu_items)) {
                                $daily_lunch_pdf_output .= '<div class="next-week-pdf week-pdf">';
                                $daily_lunch_pdf_output .= '<div class="title"><a href="#" data-date="' . $next_monday . '" data-week="' . $next_monday_weekNumber . '">Lunchmeny vecka ' . $next_monday_weekNumber . '</a></div>';
                                if(isset($hugo_opt['daily-lunch-pdf-desc'])){
                                    $daily_lunch_pdf_output .= '<div class="download-info">'.$hugo_opt['daily-lunch-pdf-desc'].'</div>';
                                }
                                $daily_lunch_pdf_output .= '<a href="' . get_site_url('', '?action=print-menu-next-week') . '" class="download-link" target="_blank">Ladda ner PDF</a>';
                                $daily_lunch_pdf_output .= '</div>';
                            }

                            $daily_lunch_pdf_output .= '</div>';
                            ?>
                            <div class="col-sm-3">
                                <div class="daily-lunch-pdf">
                                    <?php echo $daily_lunch_pdf_output; ?>
                                </div>
                            </div>
                        </div>

                        <div id="daily-lunch-menu">
                            <div class="daily-lunch-menu">
                                <?php
                                echo $heading;
                                echo $current_week_menu_items;
                                ?>
                            </div>
                        </div>
                    </div>
                </div><!-- /.description -->
            </div><!-- /.container -->
            <!-- /description -->

            <!-- MODAL RESTAURANT BLOCK -->
            <?php $menu_select = get_post_meta(get_the_ID(), 'slider_design_select_meta_box1', true);
            ?>
            <div id="modal-restaurant-block" class="restaurant-block">

                <?php
                $args = array('post__in' => $menu_select, 'post_type' => 'menu');
                $query = new WP_Query($args);
                ?>
                <?php while ($query->have_posts()) : $query->the_post(); ?>
                    <?php
                    $attach_pdf_id = get_post_meta(get_the_ID(), '_image_id', true);
                    if (!empty($attach_pdf_id)) {
                        $pdf_link = wp_get_attachment_url($attach_pdf_id);
                    }
                    ?>
                    <div class="col-sm-4 col-md-3 col-xs-6 restro-block">
                        <a href="<?php echo $pdf_link; ?>">
                            <div class="resturant-img">
                                <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id(get_the_ID())); ?>" alt="image">
                            </div>
                        </a>
                        <a href="<?php echo $pdf_link; ?>">
                            <div class="resturant-title"><?php the_title(); ?></div>
                        </a>
                        <p></p><?php the_content(); ?>
                        <p></p>
                        <a href="<?php echo $pdf_link; ?>" class="see-more">
                            Se meny
                        </a>
                    </div>

                <?php endwhile; ?> 
                <?php wp_reset_postdata();
                ?>

            </div>
        </div>
    </div><!-- /dagen-lunch -->

    <?php
endwhile;
wp_reset_query();
get_footer();
?>