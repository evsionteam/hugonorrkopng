<?php global $hugo_opt; ?>

<div class="modal fade c-modal-box" id="myModal" role="dialog" tabindex="-1">

    <!-- close button -->
    <div data-dismiss="modal" class="modal-close">
        <span></span>
        <span></span>
    </div>

    <!-- bootstrap modal dailog box -->
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">

                <div id="model-content">
                    <?php esc_html__('Loading', 'hugonorrkopng'); ?>
                    <article id="model-article">

                        <!-- IMG WRAPPER -->
                        <div class="image-cover">
                            <div class="loading-img" style="display:block">
                                <img src="<?php echo $hugo_opt['loader-image']['url']; ?>">
                            </div>
                            <div class="img-wrapper">
                                <img id="post-image">
                                <!-- Caption -->
                                <div class="caption">
                                    <header class="entry-header">
                                        <!-- Place Title Here -->
                                    </header>
                                    <!-- .entry-header -->
                                    <div class="btn-link">
                                        <!-- Place Button Here -->
                                    </div>
                                </div>
                            </div><!-- /.img-wrapper -->
                        </div><!-- /.img-wrapper -->

                        <!-- DESCRIPTION -->
                        <div class="container">
                            <div class="description">
                                <!-- TEXT LOADER -->
                                <div class="text-loader" id="text-loader">
                                    <div class="line-collection">
                                        <div class="l-title">
                                            <div class="line common"></div>
                                            <div class="line common"></div>
                                        </div>
                                        <div class="l-content">
                                            <div class="line common"></div>
                                            <div class="line common"></div>
                                            <div class="line common"></div>
                                            <div class="line common"></div>
                                            <div class="line common"></div>
                                            <div class="line common"></div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.text-loader -->

                                <div class="entry-content">
                                    <!-- Content Here -->
                                </div>
                                <div id="contact-page-data">
                                    <div class="row">
                                        <!-- .entry-content -->
                                        <div class="col-sm-6 opening-hours text-left">
                                            <!-- Opening Hours here -->
                                        </div>
                                        <div class="col-sm-6 contact-form text-left">
                                            <!-- Contact form here -->
                                        </div>
                                    </div>
                                </div>
                                <div class="daily-lunch-wrapper text-left">
                                    <div class="row">
                                        <div class="col-sm-9">
                                            <div class="daily-lunch-desc">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="daily-lunch-pdf">
                                            </div>
                                        </div>
                                    </div>
                                    <div id="daily-lunch-menu">
                                    </div>
                                </div>
                            </div><!-- /.description -->
                        </div><!-- /.container -->
                        <!-- /description -->

                        <!-- MODAL RESTAURANT BLOCK -->
                        <div id="modal-restaurant-block">
                            <div class="container">
                                <div class="row">
                                </div>
                            </div>
                        </div>
                        <!-- /#modal-restaurant-block -->

                    </article>
                    <!-- #post-## -->
                </div>
                <!-- /#model-content -->

            </div>
            <!-- /.modal-body -->
        </div>
        <!-- /.modal-content -->

    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->
