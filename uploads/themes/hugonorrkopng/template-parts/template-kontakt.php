<?php
/* Template Name: Kontakt */
?>

<?php
get_header();
global $hugo_opt;
while (have_posts()) : the_post();
    ?>
    <div class="contact">

        <?php
        $enable_map = get_post_meta( $post->ID, 'enable_map', true);
        if('yes' == $enable_map){
            ?>
            <!--Google Map-->
            <div id="map" style="width: 100%;height: 500px;background-color: grey;"></div>
            <!---->
        <?php
        }
        ?>

        <!-- DESCRIPTION -->
        <?php
        $container_class = '';
        if(is_multisite()){
            $blog_details = get_blog_details();
            if (strpos($blog_details->path, 'nojen') !== false) {
                $container_class = 'night-contact';
            }
        }
        ?>
        <div class="<?php echo $container_class;?> contact-detail">
            <div class="container">
                <div class="description">
                    <div class="entry-content">
                        <div class="text-left">
                            <?php the_content(); ?>
                        </div>
                    </div>

                    <div id="contact-page-data" class="c-form-n-op-hours">
                        <div class="row">
                            <!-- .entry-content -->
                            <div class="col-sm-6 opening-hours text-left">
                                <?php echo do_shortcode('[opening_hours]'); ?>
                            </div>
                            <div class="col-sm-6 contact-form text-left">
                                <?php if(isset($hugo_opt['contact-form']) && !empty($hugo_opt['contact-form'])):?>
                                    <h2 class="modal-sub-title">KONTAKTA OSS</h2>
                                    <?php echo do_shortcode($hugo_opt['contact-form']); ?>
                                <?php endif;?>
                            </div>
                        </div>

                    </div>
                </div><!-- /.description -->
            </div><!-- /.container -->
        </div>
    </div><!-- /.contact -->

    <?php
endwhile;
wp_reset_query();
get_footer();
?>