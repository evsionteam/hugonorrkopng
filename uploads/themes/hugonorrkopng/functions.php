<?php

if (!function_exists('hugonorrkopng_setup')) :

    function hugonorrkopng_setup() {
        load_theme_textdomain('hugonorrkopng', get_template_directory() . '/languages');
        add_theme_support('automatic-feed-links');
        add_theme_support('title-tag');
        add_theme_support('post-thumbnails');

        register_nav_menus(array(
            'left_primary_menu' => esc_html__('Left Header Menu', 'hugonorrkopng'),
            'right_primary_menu' => esc_html__('Right Header Menu', 'hugonorrkopng'),
        ));

        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));



        add_image_size('home-large-slider', 1349, 392, true);
        add_image_size('home-square-slider', 392, 392, true);
    }

endif;
add_action('after_setup_theme', 'hugonorrkopng_setup');

function hugonorrkopng_content_width() {
    $GLOBALS['content_width'] = apply_filters('hugonorrkopng_content_width', 640);
}

add_action('after_setup_theme', 'hugonorrkopng_content_width', 0);

function hugonorrkopng_widgets_init() {
    register_sidebar(array(
        'name' => esc_html__('Sidebar', 'hugonorrkopng'),
        'id' => 'sidebar-1',
        'description' => esc_html__('Add widgets here.', 'hugonorrkopng'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
    register_sidebar(array(
        'name' => esc_html__('Footer 1', 'hugonorrkopng'),
        'id' => 'footer-1',
        'description' => esc_html__('Add widgets here.', 'hugonorrkopng'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
    /*register_sidebar(array(
        'name' => esc_html__('Footer 2', 'hugonorrkopng'),
        'id' => 'footer-2',
        'description' => esc_html__('Add widgets here.', 'hugonorrkopng'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));*/
}

add_action('widgets_init', 'hugonorrkopng_widgets_init');

function hugonorrkopng_scripts() {
    wp_enqueue_style('hugonorrkopng-style', get_stylesheet_uri());
    wp_enqueue_style('hugonorrkopng-above-fold', get_template_directory_uri()."/assets/build/css/above-fold.css");
    wp_register_style('jquery-ui', 'http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css');
    wp_enqueue_style('jquery-ui');
    wp_enqueue_style('font-google', 'https://fonts.googleapis.com/css?family=Raleway:200,300,400,400i,500,600,700');
    wp_enqueue_script('hugonorrkopng-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true);
    wp_enqueue_script('hugonorrkopng-datepick', get_template_directory_uri() . '/js/custom.js', array('jquery-ui-datepicker', 'contact-form-7'), '', true);

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
    wp_add_inline_style( 'hugonorrkopng-style','body{
        opacity: 0
    }');

    $assets_url = get_stylesheet_directory_uri() . '/assets/build';

    $suffix = '.min';
    if (defined('SCRIPT_DEBUG') && SCRIPT_DEBUG) {

        $suffix = '';
    } wp_enqueue_script('hugo-script', $assets_url . '/js/script' . $suffix . '.js', array('jquery'), null, false);
    $main_css = $assets_url . '/css/main' . $suffix . '.css';

    $script = "

      var head  = document.getElementsByTagName('head')[0];

      var link  = document.createElement('link');

      link.rel  = 'stylesheet';

      link.type = 'text/css';

      link.href = '" . $main_css . "';

      link.media = 'all';

      head.appendChild(link);

   ";

    wp_add_inline_script('hugo-script', $script);

    wp_localize_script('hugo-script', 'hugoAjax', array('ajaxurl' => admin_url('admin-ajax.php')));

    #wp_enqueue_style('hugo-johan', get_stylesheet_directory_uri() . '/johan-css.css');

    if (is_page_template('template-parts/template-kontakt.php')) {
        global $post;

        $enable_map = get_post_meta($post->ID, 'enable_map', true);

        if ('yes' == $enable_map) {

            $latitude = floatval(get_post_meta($post->ID, 'latitude', true));
            $longitude = floatval(get_post_meta($post->ID, 'longitude', true));
            $map_zoom = intval(get_post_meta($post->ID, 'map_zoom', true));

            wp_enqueue_script('hugo-google-map-js', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCbEWhMXXvHeFMgdJyAzop35kJrhycZtIo&callback=initMap', '', '', true);
            $map_script = "
            function initMap() {
                var uluru = {lat: $latitude, lng: $longitude};
                var map = new google.maps.Map(document.getElementById('map'), {
                  zoom: $map_zoom,
                  scrollwheel: false,
                  center: uluru
                });
                var marker = new google.maps.Marker({
                  position: uluru,
                  map: map
                });
              }
            ";
            wp_add_inline_script('hugo-script', $map_script);
        }
    }
}

add_action('wp_enqueue_scripts', 'hugonorrkopng_scripts');



require get_template_directory() . '/inc/template-tags.php';
require get_template_directory() . '/inc/extras.php';
require get_template_directory() . '/inc/daily-lunch-post.php';
require get_template_directory() . '/inc/home-slider-post.php';
require get_template_directory() . '/inc/menu-post-types.php';
require get_template_directory() . '/inc/arrangement-post.php';
require get_template_directory() . '/inc/vip-packet.php';
require get_template_directory() . '/inc/night-arr-slider.php';
require get_template_directory() . '/inc/bowling-page-meta-box.php';
require get_template_directory() . '/inc/kontakt-page-meta-box.php';
require get_template_directory() . '/inc/arrangement-post-meta-box.php';



add_action('admin_enqueue_scripts', 'function_name');

function function_name() {

    wp_register_style('jquery-ui', 'http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css');
    wp_enqueue_style('jquery-ui');

    wp_enqueue_script('hugonorrkopng-datepck', get_template_directory_uri() . '/js/custom.js', array('jquery-ui-datepicker'), '', true);



    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

require_once ( __DIR__ . '/redux/config.php' );
require_once ( __DIR__ . '/inc/Mobile_Detect.php' );
require_once ( __DIR__ . '/inc/shortcodes.php' );
require_once ( __DIR__ . '/inc/module-daily-lunch/pdf.php' );

add_action('init', 'my_add_excerpts_to_pages');

function my_add_excerpts_to_pages() {
    add_post_type_support('page', 'excerpt');
}

add_filter('nav_menu_link_attributes', 'themeprefix_menu_attribute_add', 10, 3);

function themeprefix_menu_attribute_add($atts, $item, $args) {

    $atts['page-id'] = $item->object_id;
    /* $ajax_page_id = $atts['page-id'] */

    //Return the new attribute
    return $atts;
}

function load_page_results() {

    global $hugo_opt;

    $output = [
        'status' => esc_html__('404', 'hugonorrkopng'),
        'message' => esc_html__('Post Not Found', 'hugonorrkopng'),
        'data' => ''
    ];

    $page_id = $_POST['page_id'];

    $temp = get_post($page_id);
    if ($temp) {

        $menu_output = array();

        $output['data']['post_title'] = $temp->post_title;
        $output['data']['post_content'] = apply_filters('the_content', $temp->post_content);
        $output['data']['post_image_url'] = wp_get_attachment_url(get_post_thumbnail_id($page_id));
        $output['page_id'] = $page_id;
        $output['status'] = esc_html__('200', 'hugonorrkopng');
        $output['message'] = esc_html__('Post Found', 'hugonorrkopng');

        $btn_text = get_post_meta($page_id, 'button_text', true);
        $btn_link = get_post_meta($page_id, 'button_link', true);

        if (!empty($btn_link) && !empty($btn_text)) {
            $output['data']['btn_html'] = '<a href="' . $btn_link . '" class="restro-btn">' . $btn_text . '</a>';
        }

        $selected_menu = get_post_meta($page_id, 'slider_design_select_meta_box1', true);
        if (!empty($selected_menu)):
            $args = array('post__in' => $selected_menu, 'post_type' => 'menu');
            $query = new WP_Query($args);
            if ($query->have_posts()):

                while ($query->have_posts()): $query->the_post();
                    global $post;
                    $temp_array = array();
                    $pdf_link = '';
                    $attach_pdf_id = get_post_meta(get_the_ID(), '_image_id', true);
                    if (!empty($attach_pdf_id)) {
                        $pdf_link = wp_get_attachment_url($attach_pdf_id);
                    }

                    $temp_array[] = '
                                            <a href="' . $pdf_link . '">
                                                <div class="resturant-img">
                                                    <img src="' . wp_get_attachment_url(get_post_thumbnail_id(get_the_ID())) . '" alt="image">
                                                </div>
                                            </a>
                                            <a href="' . $pdf_link . '">
                                                <div class="resturant-title">' . get_the_title() . '</div>
                                            </a>
                                            <p>' . apply_filters('the_content', $post->post_content) . '</p>
                                            <a href="' . $pdf_link . '" class="see-more">
                                                Se meny
                                            </a>
                                            ';
                    $menu_output[] = $temp_array;
                endwhile;
                wp_reset_query();
            endif;

            $output['menu_data'] = $menu_output;
        endif;

        /* check if is contact page */
        if ('kontakt' == $temp->post_name) {
            $output['contact_page_data']['opening_hours'] = do_shortcode('[opening_hours]');
            if (isset($hugo_opt['contact-form'])) {
                $output['contact_page_data']['contact_form'] = do_shortcode('[contact-form-7 id="229" title="Contact form"]');
            }
        }
        /**/

        /* check if is daily lunch page */
        if ('dagens-lunch' == $temp->post_name) {

            $heading = $daily_lunch_pdf_output = '';

            $current_weekNumber = date("W");
            $current_date = strtotime(date('F j, Y'));

            $next_monday = strtotime('next monday');
            $next_monday_weekNumber = date('W', $next_monday);

            $current_week_menu_items = get_daily_lunch_menu_items($current_date);
            $next_week_menu_items = get_daily_lunch_menu_items($next_monday);

            $daily_lunch_pdf_output .= '<div class="daily-lunch-pdf-wrapper">';

            if (!empty($current_week_menu_items)) {
                $heading = '<div class="current-week">Lunchmeny vecka ' . $current_weekNumber . '</div>';
                $daily_lunch_pdf_output .= '<div class="current-week-pdf week-pdf">';
                $daily_lunch_pdf_output .= '<div class="title">Lunchmeny vecka ' . $current_weekNumber . '</div>';
                $daily_lunch_pdf_output .= '<div class="download-info">Download Info Here</div>';
                $daily_lunch_pdf_output .= '<a href="' . get_site_url('', '?action=print-menu-this-week') . '" class="download-link" target="_blank">Ladda ner PDF</a>';
                $daily_lunch_pdf_output .= '</div>';
            }

            if (!empty($next_week_menu_items)) {
                $daily_lunch_pdf_output .= '<div class="next-week-pdf week-pdf">';
                $daily_lunch_pdf_output .= '<div class="title">Lunchmeny vecka ' . $next_monday_weekNumber . '</div>';
                $daily_lunch_pdf_output .= '<div class="download-info">Download Info Here</div>';
                $daily_lunch_pdf_output .= '<a href="' . get_site_url('', '?action=print-menu-next-week') . '" class="download-link" target="_blank">Ladda ner PDF</a>';
                $daily_lunch_pdf_output .= '</div>';
            }

            $daily_lunch_pdf_output .= '</div>';

            /* daily lunch menu */
            $output['lunch_page_data']['daily_menu'] = $heading . $current_week_menu_items;
            /**/

            /* daily lunch desc */
            if (isset($hugo_opt['daily-lunch-desc'])) {
                $output['lunch_page_data']['daily_lunch_desc'] = wpautop($hugo_opt['daily-lunch-desc']);
            }

            /* daily lunch pdf */
            $output['lunch_page_data']['daily_lunch_pdf'] = $daily_lunch_pdf_output;
            /**/
        }
        /**/

        echo json_encode($output);
        exit;
    }
}

add_action('wp_ajax_nopriv_load_page_results', 'load_page_results');
add_action('wp_ajax_load_page_results', 'load_page_results');


/* get daily lunch menu items of the week as per date */
add_action('wp_ajax_nopriv_get_daily_lunch_menu_items', 'get_daily_lunch_menu_items');
add_action('wp_ajax_get_daily_lunch_menu_items', 'get_daily_lunch_menu_items');

function get_daily_lunch_menu_items($date) {

    if (empty($date)) {
        /* if from ajax call */
        $date = $_POST['menu_date'];
        /**/
        if (empty($date)) {
            return '';
        }
    }

    $menu_items = '';
    $args = array(
        'post_type' => 'daily_lunch',
        'posts_per_page' => 1,
        'meta_query' => array(
            'relation' => 'AND',
            array(
                'key' => 'daily_lunch_start_date',
                'value' => $date,
                'compare' => '<=',
            ),
            array(
                'key' => 'daily_lunch_end_date',
                'value' => $date,
                'compare' => '>=',
            ),
        ),
    );
    $query = new WP_Query($args);
    if ($query->have_posts()):
        while ($query->have_posts()):$query->the_post();

            $temp = '';

            $day_array = array(
                'monday' => 'MÅNDAG',
                'tuesday' => 'TISDAG',
                'wednesday' => 'ONSDAG',
                'thursday' => 'TORSDAG',
                'friday' => 'FREDAG',
                'saturday' => 'LÖRDAG',
                'sunday' => 'SÖNDAG',
                'other_info' => 'ÖVRIG INFORMATION'
            );
             $count = 1;
            foreach ($day_array as $day => $value) {
                $each_day_menus = get_post_meta(get_the_ID(), $day, true);
                if (!empty($each_day_menus)) {
                    $temp .= '<div class="daily-menu-items col-sm-4">';
                    $temp .= '<div class="day">' . $value . '</div>';
                    foreach ($each_day_menus as $menu) {
                        $temp .= '<div class="menu-items">' . $menu["name_$day"] . '</div>';
                    }
                    $temp .= '</div>';
                
                    if( $count%3 == 0 ){
                        $temp .= '<div class="clearfix"></div>';
                    }
                    $count++;
                }
            }

            if (!empty($temp)) {
                $temp = '<div class="row">' . $temp . '</div>';
            }
            $menu_items = $temp;

        endwhile;
        wp_reset_postdata();

    endif;

    /* if from ajax call */
    if (isset($_POST['menu_date'])) {
        echo json_encode($menu_items);
        die;
    }
    /**/
    return $menu_items;
}

/**/

function page_menu_link($sorted_menu_items, $args) {
    foreach ($sorted_menu_items as $sorted_menu_item) {
        if ($sorted_menu_item->object == 'page') {
            $sorted_menu_item->url = '#';
        }
    }
    return $sorted_menu_items;
}

//add_filter( 'wp_nav_menu_objects', 'page_menu_link', 10, 3 );



add_filter('wpcf7_form_action_url', 'alter_form_action');

function alter_form_action($url) {
    global $wp;
    $url_frag = explode('#', $url);

    if (isset($url_frag[1])) {
        $url = home_url(add_query_arg(array(), $wp->request)) . '/#' . $url_frag[1];
    }

    return $url;
}

function remove_empty_p($content) {
    $content = force_balance_tags($content);
    $content = preg_replace('#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content);
    $content = preg_replace('~\s?<p>(\s|&nbsp;)+</p>\s?~', '', $content);
    return $content;
}

add_filter('the_content', 'remove_empty_p', 20, 1);

function shortcode_empty_paragraph_fix($content) {

    // define your shortcodes to filter, '' filters all shortcodes
    $shortcodes = array('col', 'wrapper');

    foreach ($shortcodes as $shortcode) {

        $array = array(
            '<p>[' . $shortcode => '[' . $shortcode,
            '<p>[/' . $shortcode => '[/' . $shortcode,
            $shortcode . ']</p>' => $shortcode . ']',
            $shortcode . ']<br />' => $shortcode . ']'
        );

        $content = strtr($content, $array);
    }

    return $content;
}

add_filter('the_content', 'shortcode_empty_paragraph_fix');

function fb_change_mce_options($initArray) {
    // Comma separated string od extendes tags
    // Command separated string of extended elements
    $ext = 'pre[id|name|class|style],iframe[align|longdesc|name|width|height|frameborder|scrolling|marginheight|marginwidth|src]';

    if (isset($initArray['extended_valid_elements'])) {
        $initArray['extended_valid_elements'] .= ',' . $ext;
    } else {
        $initArray['extended_valid_elements'] = $ext;
    }
    // maybe; set tiny paramter verify_html
    //$initArray['verify_html'] = false;

    return $initArray;
}

add_filter('tiny_mce_before_init', 'fb_change_mce_options');

add_shortcode('iframe', 'mycustom_shortcode_iframe');

function mycustom_shortcode_iframe($args, $content) {
    $keys = array("src", "width", "height", "scrolling", "marginwidth", "marginheight", "frameborder");
    $arguments = mycustom_extract_shortcode_arguments($args, $keys);
    return '<iframe ' . $arguments . '></iframe>';
}

function mycustom_extract_shortcode_arguments($args, $keys) {
    $result = "";
    foreach ($keys as $key) {
        if (isset($args[$key])) {
            $result .= $key . '="' . $args[$key] . '" ';
        }
    }
    return $result;
}