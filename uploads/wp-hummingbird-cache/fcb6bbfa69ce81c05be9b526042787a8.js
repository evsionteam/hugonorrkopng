/** handles (scripts) :comment-reply  */var addComment={moveForm:function(commId,parentId,respondId,postId){var div,element,style,cssHidden,t=this,comm=t.I(commId),respond=t.I(respondId),cancel=t.I('cancel-comment-reply-link'),parent=t.I('comment_parent'),post=t.I('comment_post_ID'),commentForm=respond.getElementsByTagName('form')[0];if(!comm||!respond||!cancel||!parent||!commentForm){return}
t.respondId=respondId;postId=postId||!1;if(!t.I('wp-temp-form-div')){div=document.createElement('div');div.id='wp-temp-form-div';div.style.display='none';respond.parentNode.insertBefore(div,respond)}
comm.parentNode.insertBefore(respond,comm.nextSibling);if(post&&postId){post.value=postId}
parent.value=parentId;cancel.style.display='';cancel.onclick=function(){var t=addComment,temp=t.I('wp-temp-form-div'),respond=t.I(t.respondId);if(!temp||!respond){return}
t.I('comment_parent').value='0';temp.parentNode.insertBefore(respond,temp);temp.parentNode.removeChild(temp);this.style.display='none';this.onclick=null;return !1};try{for(var i=0;i<commentForm.elements.length;i++){element=commentForm.elements[i];cssHidden=!1;if('getComputedStyle' in window){style=window.getComputedStyle(element)}else if(document.documentElement.currentStyle){style=element.currentStyle}
if((element.offsetWidth<=0&&element.offsetHeight<=0)||style.visibility==='hidden'){cssHidden=!0}
if('hidden'===element.type||element.disabled||cssHidden){continue}
element.focus();break}}catch(er){}
return !1},I:function(id){return document.getElementById(id)}}