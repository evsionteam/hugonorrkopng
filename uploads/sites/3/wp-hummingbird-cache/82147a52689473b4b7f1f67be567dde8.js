/** handles (scripts) :hugo-script  */+(function($){(function(){var isWebkit=navigator.userAgent.toLowerCase().indexOf('webkit')>-1,isOpera=navigator.userAgent.toLowerCase().indexOf('opera')>-1,isIe=navigator.userAgent.toLowerCase().indexOf('msie')>-1;if((isWebkit||isOpera||isIe)&&document.getElementById&&window.addEventListener){window.addEventListener('hashchange',function(){var id=location.hash.substring(1),element;if(!(/^[A-z0-9_-]+$/.test(id))){return}
element=document.getElementById(id);if(element){if(!(/^(?:a|select|input|button|textarea)$/i.test(element.tagName))){element.tabIndex=-1}
element.focus()}},!1)}})();(function(factory){if(typeof define==='function'&&define.amd){define(['jquery'],factory)}else if(typeof exports==='object'){module.exports=factory(require('jquery'))}else{factory(jQuery)}}(function(jQuery){"use strict";var domfocus=!1;var mousefocus=!1;var tabindexcounter=0;var ascrailcounter=2000;var globalmaxzindex=0;var $=jQuery;function getScriptPath(){var scripts=document.getElementsByTagName('script');var path=scripts.length?scripts[scripts.length-1].src.split('?')[0]:'';return(path.split('/').length>0)?path.split('/').slice(0,-1).join('/')+'/':''}
var vendors=['webkit','ms','moz','o'];var setAnimationFrame=window.requestAnimationFrame||!1;var clearAnimationFrame=window.cancelAnimationFrame||!1;if(!setAnimationFrame){for(var vx in vendors){var v=vendors[vx];setAnimationFrame=window[v+'RequestAnimationFrame'];if(setAnimationFrame){clearAnimationFrame=window[v+'CancelAnimationFrame']||window[v+'CancelRequestAnimationFrame'];break}}}
var ClsMutationObserver=window.MutationObserver||window.WebKitMutationObserver||!1;var _globaloptions={zindex:"auto",cursoropacitymin:0,cursoropacitymax:1,cursorcolor:"#424242",cursorwidth:"6px",cursorborder:"1px solid #fff",cursorborderradius:"5px",scrollspeed:60,mousescrollstep:8*3,touchbehavior:!1,hwacceleration:!0,usetransition:!0,boxzoom:!1,dblclickzoom:!0,gesturezoom:!0,grabcursorenabled:!0,autohidemode:!0,background:"",iframeautoresize:!0,cursorminheight:32,preservenativescrolling:!0,railoffset:!1,railhoffset:!1,bouncescroll:!0,spacebarenabled:!0,railpadding:{top:0,right:0,left:0,bottom:0},disableoutline:!0,horizrailenabled:!0,railalign:"right",railvalign:"bottom",enabletranslate3d:!0,enablemousewheel:!0,enablekeyboard:!0,smoothscroll:!0,sensitiverail:!0,enablemouselockapi:!0,cursorfixedheight:!1,directionlockdeadzone:6,hidecursordelay:400,nativeparentscrolling:!0,enablescrollonselection:!0,overflowx:!0,overflowy:!0,cursordragspeed:0.3,rtlmode:"auto",cursordragontouch:!1,oneaxismousemode:"auto",scriptpath:getScriptPath(),preventmultitouchscrolling:!0,disablemutationobserver:!1};var browserdetected=!1;var getBrowserDetection=function(){if(browserdetected)return browserdetected;var _el=document.createElement('DIV'),_style=_el.style,_agent=navigator.userAgent,_platform=navigator.platform,d={};d.haspointerlock="pointerLockElement" in document||"webkitPointerLockElement" in document||"mozPointerLockElement" in document;d.isopera=("opera" in window);d.isopera12=(d.isopera&&("getUserMedia" in navigator));d.isoperamini=(Object.prototype.toString.call(window.operamini)==="[object OperaMini]");d.isie=(("all" in document)&&("attachEvent" in _el)&&!d.isopera);d.isieold=(d.isie&&!("msInterpolationMode" in _style));d.isie7=d.isie&&!d.isieold&&(!("documentMode" in document)||(document.documentMode==7));d.isie8=d.isie&&("documentMode" in document)&&(document.documentMode==8);d.isie9=d.isie&&("performance" in window)&&(document.documentMode==9);d.isie10=d.isie&&("performance" in window)&&(document.documentMode==10);d.isie11=("msRequestFullscreen" in _el)&&(document.documentMode>=11);d.isieedge12=(navigator.userAgent.match(/Edge\/12\./));d.isieedge=("msOverflowStyle" in _el);d.ismodernie=d.isie11||d.isieedge;d.isie9mobile=/iemobile.9/i.test(_agent);if(d.isie9mobile)d.isie9=!1;d.isie7mobile=(!d.isie9mobile&&d.isie7)&&/iemobile/i.test(_agent);d.ismozilla=("MozAppearance" in _style);d.iswebkit=("WebkitAppearance" in _style);d.ischrome=("chrome" in window);d.ischrome38=(d.ischrome&&("touchAction" in _style));d.ischrome22=(!d.ischrome38)&&(d.ischrome&&d.haspointerlock);d.ischrome26=(!d.ischrome38)&&(d.ischrome&&("transition" in _style));d.cantouch=("ontouchstart" in document.documentElement)||("ontouchstart" in window);d.hasw3ctouch=(window.PointerEvent||!1)&&((navigator.MaxTouchPoints>0)||(navigator.msMaxTouchPoints>0));d.hasmstouch=(!d.hasw3ctouch)&&(window.MSPointerEvent||!1);d.ismac=/^mac$/i.test(_platform);d.isios=(d.cantouch&&/iphone|ipad|ipod/i.test(_platform));d.isios4=((d.isios)&&!("seal" in Object));d.isios7=((d.isios)&&("webkitHidden" in document));d.isios8=((d.isios)&&("hidden" in document));d.isandroid=(/android/i.test(_agent));d.haseventlistener=("addEventListener" in _el);d.trstyle=!1;d.hastransform=!1;d.hastranslate3d=!1;d.transitionstyle=!1;d.hastransition=!1;d.transitionend=!1;var a;var check=['transform','msTransform','webkitTransform','MozTransform','OTransform'];for(a=0;a<check.length;a++){if(_style[check[a]]!==undefined){d.trstyle=check[a];break}}
d.hastransform=(!!d.trstyle);if(d.hastransform){_style[d.trstyle]="translate3d(1px,2px,3px)";d.hastranslate3d=/translate3d/.test(_style[d.trstyle])}
d.transitionstyle=!1;d.prefixstyle='';d.transitionend=!1;check=['transition','webkitTransition','msTransition','MozTransition','OTransition','OTransition','KhtmlTransition'];var prefix=['','-webkit-','-ms-','-moz-','-o-','-o','-khtml-'];var evs=['transitionend','webkitTransitionEnd','msTransitionEnd','transitionend','otransitionend','oTransitionEnd','KhtmlTransitionEnd'];for(a=0;a<check.length;a++){if(check[a]in _style){d.transitionstyle=check[a];d.prefixstyle=prefix[a];d.transitionend=evs[a];break}}
if(d.ischrome26){d.prefixstyle=prefix[1]}
d.hastransition=(d.transitionstyle);function detectCursorGrab(){var lst=['grab','-webkit-grab','-moz-grab'];if((d.ischrome&&!d.ischrome38)||d.isie)lst=[];for(var a=0;a<lst.length;a++){var p=lst[a];_style.cursor=p;if(_style.cursor==p)return p}
return 'url(//patriciaportfolio.googlecode.com/files/openhand.cur),n-resize'}
d.cursorgrabvalue=detectCursorGrab();d.hasmousecapture=("setCapture" in _el);d.hasMutationObserver=(ClsMutationObserver!==!1);_el=null;browserdetected=d;return d};var NiceScrollClass=function(myopt,me){var self=this;this.version='3.6.8';this.name='nicescroll';this.me=me;this.opt={doc:$("body"),win:!1};$.extend(this.opt,_globaloptions);this.opt.snapbackspeed=80;if(myopt||!1){for(var a in self.opt){if(myopt[a]!==undefined)self.opt[a]=myopt[a]}}
if(self.opt.disablemutationobserver)ClsMutationObserver=!1;this.doc=self.opt.doc;this.iddoc=(this.doc&&this.doc[0])?this.doc[0].id||'':'';this.ispage=/^BODY|HTML/.test((self.opt.win)?self.opt.win[0].nodeName:this.doc[0].nodeName);this.haswrapper=(self.opt.win!==!1);this.win=self.opt.win||(this.ispage?$(window):this.doc);this.docscroll=(this.ispage&&!this.haswrapper)?$(window):this.win;this.body=$("body");this.viewport=!1;this.isfixed=!1;this.iframe=!1;this.isiframe=((this.doc[0].nodeName=='IFRAME')&&(this.win[0].nodeName=='IFRAME'));this.istextarea=(this.win[0].nodeName=='TEXTAREA');this.forcescreen=!1;this.canshowonmouseevent=(self.opt.autohidemode!="scroll");this.onmousedown=!1;this.onmouseup=!1;this.onmousemove=!1;this.onmousewheel=!1;this.onkeypress=!1;this.ongesturezoom=!1;this.onclick=!1;this.onscrollstart=!1;this.onscrollend=!1;this.onscrollcancel=!1;this.onzoomin=!1;this.onzoomout=!1;this.view=!1;this.page=!1;this.scroll={x:0,y:0};this.scrollratio={x:0,y:0};this.cursorheight=20;this.scrollvaluemax=0;if(this.opt.rtlmode=="auto"){var target=this.win[0]==window?this.body:this.win;var writingMode=target.css("writing-mode")||target.css("-webkit-writing-mode")||target.css("-ms-writing-mode")||target.css("-moz-writing-mode");if(writingMode=="horizontal-tb"||writingMode=="lr-tb"||writingMode==""){this.isrtlmode=(target.css("direction")=="rtl");this.isvertical=!1}else{this.isrtlmode=(writingMode=="vertical-rl"||writingMode=="tb"||writingMode=="tb-rl"||writingMode=="rl-tb");this.isvertical=(writingMode=="vertical-rl"||writingMode=="tb"||writingMode=="tb-rl")}}else{this.isrtlmode=(this.opt.rtlmode===!0);this.isvertical=!1}
this.scrollrunning=!1;this.scrollmom=!1;this.observer=!1;this.observerremover=!1;this.observerbody=!1;do{this.id="ascrail"+(ascrailcounter++)}while(document.getElementById(this.id));this.rail=!1;this.cursor=!1;this.cursorfreezed=!1;this.selectiondrag=!1;this.zoom=!1;this.zoomactive=!1;this.hasfocus=!1;this.hasmousefocus=!1;this.visibility=!0;this.railslocked=!1;this.locked=!1;this.hidden=!1;this.cursoractive=!0;this.wheelprevented=!1;this.overflowx=self.opt.overflowx;this.overflowy=self.opt.overflowy;this.nativescrollingarea=!1;this.checkarea=0;this.events=[];this.saved={};this.delaylist={};this.synclist={};this.lastdeltax=0;this.lastdeltay=0;this.detected=getBrowserDetection();var cap=$.extend({},this.detected);this.canhwscroll=(cap.hastransform&&self.opt.hwacceleration);this.ishwscroll=(this.canhwscroll&&self.haswrapper);if(!this.isrtlmode){this.hasreversehr=!1}else if(this.isvertical){this.hasreversehr=!(cap.iswebkit||cap.isie||cap.isie11)}else{this.hasreversehr=!(cap.iswebkit||(cap.isie&&!cap.isie10&&!cap.isie11))}
this.istouchcapable=!1;if(!cap.cantouch&&(cap.hasw3ctouch||cap.hasmstouch)){this.istouchcapable=!0}else if(cap.cantouch&&!cap.isios&&!cap.isandroid&&(cap.iswebkit||cap.ismozilla)){this.istouchcapable=!0}
if(!self.opt.enablemouselockapi){cap.hasmousecapture=!1;cap.haspointerlock=!1}
this.debounced=function(name,fn,tm){if(!self)return;var dd=self.delaylist[name]||!1;if(!dd){fn.call(self);self.delaylist[name]={h:setAnimationFrame(function(){self.delaylist[name].fn.call(self);self.delaylist[name]=!1},tm)}}
self.delaylist[name].fn=fn};var _onsync=!1;this.synched=function(name,fn){function requestSync(){if(_onsync)return;setAnimationFrame(function(){if(!self)return;_onsync=!1;for(var nn in self.synclist){var fn=self.synclist[nn];if(fn)fn.call(self);self.synclist[nn]=!1}});_onsync=!0}
self.synclist[name]=fn;requestSync();return name};this.unsynched=function(name){if(self.synclist[name])self.synclist[name]=!1};this.css=function(el,pars){for(var n in pars){self.saved.css.push([el,n,el.css(n)]);el.css(n,pars[n])}};this.scrollTop=function(val){return(val===undefined)?self.getScrollTop():self.setScrollTop(val)};this.scrollLeft=function(val){return(val===undefined)?self.getScrollLeft():self.setScrollLeft(val)};var BezierClass=function(st,ed,spd,p1,p2,p3,p4){this.st=st;this.ed=ed;this.spd=spd;this.p1=p1||0;this.p2=p2||1;this.p3=p3||0;this.p4=p4||1;this.ts=(new Date()).getTime();this.df=this.ed-this.st};BezierClass.prototype={B2:function(t){return 3*t*t*(1-t)},B3:function(t){return 3*t*(1-t)*(1-t)},B4:function(t){return(1-t)*(1-t)*(1-t)},getNow:function(){var nw=(new Date()).getTime();var pc=1-((nw-this.ts)/this.spd);var bz=this.B2(pc)+this.B3(pc)+this.B4(pc);return(pc<0)?this.ed:this.st+Math.round(this.df*bz)},update:function(ed,spd){this.st=this.getNow();this.ed=ed;this.spd=spd;this.ts=(new Date()).getTime();this.df=this.ed-this.st;return this}};function getMatrixValues(){var tr=self.doc.css(cap.trstyle);if(tr&&(tr.substr(0,6)=="matrix")){return tr.replace(/^.*\((.*)\)$/g,"$1").replace(/px/g,'').split(/, +/)}
return !1}
if(this.ishwscroll){this.doc.translate={x:0,y:0,tx:"0px",ty:"0px"};if(cap.hastranslate3d&&cap.isios)this.doc.css("-webkit-backface-visibility","hidden");this.getScrollTop=function(last){if(!last){var mtx=getMatrixValues();if(mtx)return(mtx.length==16)?-mtx[13]:-mtx[5];if(self.timerscroll&&self.timerscroll.bz)return self.timerscroll.bz.getNow()}
return self.doc.translate.y};this.getScrollLeft=function(last){if(!last){var mtx=getMatrixValues();if(mtx)return(mtx.length==16)?-mtx[12]:-mtx[4];if(self.timerscroll&&self.timerscroll.bh)return self.timerscroll.bh.getNow()}
return self.doc.translate.x};this.notifyScrollEvent=function(el){var e=document.createEvent("UIEvents");e.initUIEvent("scroll",!1,!0,window,1);e.niceevent=!0;el.dispatchEvent(e)};var cxscrollleft=(this.isrtlmode)?1:-1;if(cap.hastranslate3d&&self.opt.enabletranslate3d){this.setScrollTop=function(val,silent){self.doc.translate.y=val;self.doc.translate.ty=(val*-1)+"px";self.doc.css(cap.trstyle,"translate3d("+self.doc.translate.tx+","+self.doc.translate.ty+",0px)");if(!silent)self.notifyScrollEvent(self.win[0])};this.setScrollLeft=function(val,silent){self.doc.translate.x=val;self.doc.translate.tx=(val*cxscrollleft)+"px";self.doc.css(cap.trstyle,"translate3d("+self.doc.translate.tx+","+self.doc.translate.ty+",0px)");if(!silent)self.notifyScrollEvent(self.win[0])}}else{this.setScrollTop=function(val,silent){self.doc.translate.y=val;self.doc.translate.ty=(val*-1)+"px";self.doc.css(cap.trstyle,"translate("+self.doc.translate.tx+","+self.doc.translate.ty+")");if(!silent)self.notifyScrollEvent(self.win[0])};this.setScrollLeft=function(val,silent){self.doc.translate.x=val;self.doc.translate.tx=(val*cxscrollleft)+"px";self.doc.css(cap.trstyle,"translate("+self.doc.translate.tx+","+self.doc.translate.ty+")");if(!silent)self.notifyScrollEvent(self.win[0])}}}else{this.getScrollTop=function(){return self.docscroll.scrollTop()};this.setScrollTop=function(val){return setTimeout(function(){(self)&&self.docscroll.scrollTop(val)},1)};this.getScrollLeft=function(){var val;if(!self.hasreversehr){val=self.docscroll.scrollLeft()}else if(self.detected.ismozilla){val=self.page.maxw-Math.abs(self.docscroll.scrollLeft())}else{val=self.page.maxw-self.docscroll.scrollLeft()}
return val};this.setScrollLeft=function(val){return setTimeout(function(){if(!self)return;if(self.hasreversehr){if(self.detected.ismozilla){val=-(self.page.maxw-val)}else{val=self.page.maxw-val}}
return self.docscroll.scrollLeft(val)},1)}}
this.getTarget=function(e){if(!e)return !1;if(e.target)return e.target;if(e.srcElement)return e.srcElement;return !1};this.hasParent=function(e,id){if(!e)return !1;var el=e.target||e.srcElement||e||!1;while(el&&el.id!=id){el=el.parentNode||!1}
return(el!==!1)};function getZIndex(){var dom=self.win;if("zIndex" in dom)return dom.zIndex();while(dom.length>0){if(dom[0].nodeType==9)return !1;var zi=dom.css('zIndex');if(!isNaN(zi)&&zi!=0)return parseInt(zi);dom=dom.parent()}
return !1}
var _convertBorderWidth={"thin":1,"medium":3,"thick":5};function getWidthToPixel(dom,prop,chkheight){var wd=dom.css(prop);var px=parseFloat(wd);if(isNaN(px)){px=_convertBorderWidth[wd]||0;var brd=(px==3)?((chkheight)?(self.win.outerHeight()-self.win.innerHeight()):(self.win.outerWidth()-self.win.innerWidth())):1;if(self.isie8&&px)px+=1;return(brd)?px:0}
return px}
this.getDocumentScrollOffset=function(){return{top:window.pageYOffset||document.documentElement.scrollTop,left:window.pageXOffset||document.documentElement.scrollLeft}};this.getOffset=function(){if(self.isfixed){var ofs=self.win.offset();var scrl=self.getDocumentScrollOffset();ofs.top-=scrl.top;ofs.left-=scrl.left;return ofs}
var ww=self.win.offset();if(!self.viewport)return ww;var vp=self.viewport.offset();return{top:ww.top-vp.top,left:ww.left-vp.left}};this.updateScrollBar=function(len){var pos,off;if(self.ishwscroll){self.rail.css({height:self.win.innerHeight()-(self.opt.railpadding.top+self.opt.railpadding.bottom)});if(self.railh)self.railh.css({width:self.win.innerWidth()-(self.opt.railpadding.left+self.opt.railpadding.right)})}else{var wpos=self.getOffset();pos={top:wpos.top,left:wpos.left-(self.opt.railpadding.left+self.opt.railpadding.right)};pos.top+=getWidthToPixel(self.win,'border-top-width',!0);pos.left+=(self.rail.align)?self.win.outerWidth()-getWidthToPixel(self.win,'border-right-width')-self.rail.width:getWidthToPixel(self.win,'border-left-width');off=self.opt.railoffset;if(off){if(off.top)pos.top+=off.top;if(off.left)pos.left+=off.left}
if(!self.railslocked)self.rail.css({top:pos.top,left:pos.left,height:((len)?len.h:self.win.innerHeight())-(self.opt.railpadding.top+self.opt.railpadding.bottom)});if(self.zoom){self.zoom.css({top:pos.top+1,left:(self.rail.align==1)?pos.left-20:pos.left+self.rail.width+4})}
if(self.railh&&!self.railslocked){pos={top:wpos.top,left:wpos.left};off=self.opt.railhoffset;if(off){if(off.top)pos.top+=off.top;if(off.left)pos.left+=off.left}
var y=(self.railh.align)?pos.top+getWidthToPixel(self.win,'border-top-width',!0)+self.win.innerHeight()-self.railh.height:pos.top+getWidthToPixel(self.win,'border-top-width',!0);var x=pos.left+getWidthToPixel(self.win,'border-left-width');self.railh.css({top:y-(self.opt.railpadding.top+self.opt.railpadding.bottom),left:x,width:self.railh.width})}}};this.doRailClick=function(e,dbl,hr){var fn,pg,cur,pos;if(self.railslocked)return;self.cancelEvent(e);if(dbl){fn=(hr)?self.doScrollLeft:self.doScrollTop;cur=(hr)?((e.pageX-self.railh.offset().left-(self.cursorwidth/2))*self.scrollratio.x):((e.pageY-self.rail.offset().top-(self.cursorheight/2))*self.scrollratio.y);fn(cur)}else{fn=(hr)?self.doScrollLeftBy:self.doScrollBy;cur=(hr)?self.scroll.x:self.scroll.y;pos=(hr)?e.pageX-self.railh.offset().left:e.pageY-self.rail.offset().top;pg=(hr)?self.view.w:self.view.h;fn((cur>=pos)?pg:-pg)}};self.hasanimationframe=(setAnimationFrame);self.hascancelanimationframe=(clearAnimationFrame);if(!self.hasanimationframe){setAnimationFrame=function(fn){return setTimeout(fn,15-Math.floor((+new Date())/1000)%16)};clearAnimationFrame=clearTimeout}else if(!self.hascancelanimationframe)clearAnimationFrame=function(){self.cancelAnimationFrame=!0};this.init=function(){self.saved.css=[];if(cap.isie7mobile)return !0;if(cap.isoperamini)return !0;var _touchaction=(cap.isie10)?'-ms-touch-action':'touch-action';if(cap.hasmstouch)self.css((self.ispage)?$("html"):self.win,{_touchaction:'none'});var _scrollyhidden=(cap.ismodernie||cap.isie10)?{'-ms-overflow-style':'none'}:{'overflow-y':'hidden'};self.zindex="auto";if(!self.ispage&&self.opt.zindex=="auto"){self.zindex=getZIndex()||"auto"}else{self.zindex=self.opt.zindex}
if(!self.ispage&&self.zindex!="auto"&&self.zindex>globalmaxzindex){globalmaxzindex=self.zindex}
if(self.isie&&self.zindex==0&&self.opt.zindex=="auto"){self.zindex="auto"}
if(!self.ispage||(!cap.cantouch&&!cap.isieold&&!cap.isie9mobile)){var cont=self.docscroll;if(self.ispage)cont=(self.haswrapper)?self.win:self.doc;if(!cap.isie9mobile)self.css(cont,_scrollyhidden);if(self.ispage&&cap.isie7){if(self.doc[0].nodeName=='BODY')self.css($("html"),{'overflow-y':'hidden'});else if(self.doc[0].nodeName=='HTML')self.css($("body"),_scrollyhidden)}
if(cap.isios&&!self.ispage&&!self.haswrapper)self.css($("body"),{"-webkit-overflow-scrolling":"touch"});var cursor=$(document.createElement('div'));cursor.css({position:"relative",top:0,"float":"right",width:self.opt.cursorwidth,height:0,'background-color':self.opt.cursorcolor,border:self.opt.cursorborder,'background-clip':'padding-box','-webkit-border-radius':self.opt.cursorborderradius,'-moz-border-radius':self.opt.cursorborderradius,'border-radius':self.opt.cursorborderradius});cursor.hborder=parseFloat(cursor.outerHeight()-cursor.innerHeight());cursor.addClass('nicescroll-cursors');self.cursor=cursor;var rail=$(document.createElement('div'));rail.attr('id',self.id);rail.addClass('nicescroll-rails nicescroll-rails-vr');var v,a,kp=["left","right","top","bottom"];for(var n in kp){a=kp[n];v=self.opt.railpadding[a];(v)?rail.css("padding-"+a,v+"px"):self.opt.railpadding[a]=0}
rail.append(cursor);rail.width=Math.max(parseFloat(self.opt.cursorwidth),cursor.outerWidth());rail.css({width:rail.width+"px",zIndex:self.zindex,background:self.opt.background,cursor:"default"});rail.visibility=!0;rail.scrollable=!0;rail.align=(self.opt.railalign=="left")?0:1;self.rail=rail;self.rail.drag=!1;var zoom=!1;if(self.opt.boxzoom&&!self.ispage&&!cap.isieold){zoom=document.createElement('div');self.bind(zoom,"click",self.doZoom);self.bind(zoom,"mouseenter",function(){self.zoom.css('opacity',self.opt.cursoropacitymax)});self.bind(zoom,"mouseleave",function(){self.zoom.css('opacity',self.opt.cursoropacitymin)});self.zoom=$(zoom);self.zoom.css({cursor:"pointer",zIndex:self.zindex,backgroundImage:'url('+self.opt.scriptpath+'zoomico.png)',height:18,width:18,backgroundPosition:'0px 0px'});if(self.opt.dblclickzoom)self.bind(self.win,"dblclick",self.doZoom);if(cap.cantouch&&self.opt.gesturezoom){self.ongesturezoom=function(e){if(e.scale>1.5)self.doZoomIn(e);if(e.scale<0.8)self.doZoomOut(e);return self.cancelEvent(e)};self.bind(self.win,"gestureend",self.ongesturezoom)}}
self.railh=!1;var railh;if(self.opt.horizrailenabled){self.css(cont,{overflowX:'hidden'});var cursor=$(document.createElement('div'));cursor.css({position:"absolute",top:0,height:self.opt.cursorwidth,width:0,backgroundColor:self.opt.cursorcolor,border:self.opt.cursorborder,backgroundClip:'padding-box','-webkit-border-radius':self.opt.cursorborderradius,'-moz-border-radius':self.opt.cursorborderradius,'border-radius':self.opt.cursorborderradius});if(cap.isieold)cursor.css('overflow','hidden');cursor.wborder=parseFloat(cursor.outerWidth()-cursor.innerWidth());cursor.addClass('nicescroll-cursors');self.cursorh=cursor;railh=$(document.createElement('div'));railh.attr('id',self.id+'-hr');railh.addClass('nicescroll-rails nicescroll-rails-hr');railh.height=Math.max(parseFloat(self.opt.cursorwidth),cursor.outerHeight());railh.css({height:railh.height+"px",'zIndex':self.zindex,"background":self.opt.background});railh.append(cursor);railh.visibility=!0;railh.scrollable=!0;railh.align=(self.opt.railvalign=="top")?0:1;self.railh=railh;self.railh.drag=!1}
if(self.ispage){rail.css({position:"fixed",top:0,height:"100%"});(rail.align)?rail.css({right:0}):rail.css({left:0});self.body.append(rail);if(self.railh){railh.css({position:"fixed",left:0,width:"100%"});(railh.align)?railh.css({bottom:0}):railh.css({top:0});self.body.append(railh)}}else{if(self.ishwscroll){if(self.win.css('position')=='static')self.css(self.win,{'position':'relative'});var bd=(self.win[0].nodeName=='HTML')?self.body:self.win;$(bd).scrollTop(0).scrollLeft(0);if(self.zoom){self.zoom.css({position:"absolute",top:1,right:0,"margin-right":rail.width+4});bd.append(self.zoom)}
rail.css({position:"absolute",top:0});(rail.align)?rail.css({right:0}):rail.css({left:0});bd.append(rail);if(railh){railh.css({position:"absolute",left:0,bottom:0});(railh.align)?railh.css({bottom:0}):railh.css({top:0});bd.append(railh)}}else{self.isfixed=(self.win.css("position")=="fixed");var rlpos=(self.isfixed)?"fixed":"absolute";if(!self.isfixed)self.viewport=self.getViewport(self.win[0]);if(self.viewport){self.body=self.viewport;if((/fixed|absolute/.test(self.viewport.css("position")))==!1)self.css(self.viewport,{"position":"relative"})}
rail.css({position:rlpos});if(self.zoom)self.zoom.css({position:rlpos});self.updateScrollBar();self.body.append(rail);if(self.zoom)self.body.append(self.zoom);if(self.railh){railh.css({position:rlpos});self.body.append(railh)}}
if(cap.isios)self.css(self.win,{'-webkit-tap-highlight-color':'rgba(0,0,0,0)','-webkit-touch-callout':'none'});if(cap.isie&&self.opt.disableoutline)self.win.attr("hideFocus","true");if(cap.iswebkit&&self.opt.disableoutline)self.win.css('outline','none')}
if(self.opt.autohidemode===!1){self.autohidedom=!1;self.rail.css({opacity:self.opt.cursoropacitymax});if(self.railh)self.railh.css({opacity:self.opt.cursoropacitymax})}else if((self.opt.autohidemode===!0)||(self.opt.autohidemode==="leave")){self.autohidedom=$().add(self.rail);if(cap.isie8)self.autohidedom=self.autohidedom.add(self.cursor);if(self.railh)self.autohidedom=self.autohidedom.add(self.railh);if(self.railh&&cap.isie8)self.autohidedom=self.autohidedom.add(self.cursorh)}else if(self.opt.autohidemode=="scroll"){self.autohidedom=$().add(self.rail);if(self.railh)self.autohidedom=self.autohidedom.add(self.railh)}else if(self.opt.autohidemode=="cursor"){self.autohidedom=$().add(self.cursor);if(self.railh)self.autohidedom=self.autohidedom.add(self.cursorh)}else if(self.opt.autohidemode=="hidden"){self.autohidedom=!1;self.hide();self.railslocked=!1}
if(cap.isie9mobile){self.scrollmom=new ScrollMomentumClass2D(self);self.onmangotouch=function(){var py=self.getScrollTop();var px=self.getScrollLeft();if((py==self.scrollmom.lastscrolly)&&(px==self.scrollmom.lastscrollx))return !0;var dfy=py-self.mangotouch.sy;var dfx=px-self.mangotouch.sx;var df=Math.round(Math.sqrt(Math.pow(dfx,2)+Math.pow(dfy,2)));if(df==0)return;var dry=(dfy<0)?-1:1;var drx=(dfx<0)?-1:1;var tm=+new Date();if(self.mangotouch.lazy)clearTimeout(self.mangotouch.lazy);if(((tm-self.mangotouch.tm)>80)||(self.mangotouch.dry!=dry)||(self.mangotouch.drx!=drx)){self.scrollmom.stop();self.scrollmom.reset(px,py);self.mangotouch.sy=py;self.mangotouch.ly=py;self.mangotouch.sx=px;self.mangotouch.lx=px;self.mangotouch.dry=dry;self.mangotouch.drx=drx;self.mangotouch.tm=tm}else{self.scrollmom.stop();self.scrollmom.update(self.mangotouch.sx-dfx,self.mangotouch.sy-dfy);self.mangotouch.tm=tm;var ds=Math.max(Math.abs(self.mangotouch.ly-py),Math.abs(self.mangotouch.lx-px));self.mangotouch.ly=py;self.mangotouch.lx=px;if(ds>2){self.mangotouch.lazy=setTimeout(function(){self.mangotouch.lazy=!1;self.mangotouch.dry=0;self.mangotouch.drx=0;self.mangotouch.tm=0;self.scrollmom.doMomentum(30)},100)}}};var top=self.getScrollTop();var lef=self.getScrollLeft();self.mangotouch={sy:top,ly:top,dry:0,sx:lef,lx:lef,drx:0,lazy:!1,tm:0};self.bind(self.docscroll,"scroll",self.onmangotouch)}else{if(cap.cantouch||self.istouchcapable||self.opt.touchbehavior||cap.hasmstouch){self.scrollmom=new ScrollMomentumClass2D(self);self.ontouchstart=function(e){if(e.pointerType&&e.pointerType!=2&&e.pointerType!="touch")return !1;self.hasmoving=!1;if(!self.railslocked){var tg;if(cap.hasmstouch){tg=(e.target)?e.target:!1;while(tg){var nc=$(tg).getNiceScroll();if((nc.length>0)&&(nc[0].me==self.me))break;if(nc.length>0)return !1;if((tg.nodeName=='DIV')&&(tg.id==self.id))break;tg=(tg.parentNode)?tg.parentNode:!1}}
self.cancelScroll();tg=self.getTarget(e);if(tg){var skp=(/INPUT/i.test(tg.nodeName))&&(/range/i.test(tg.type));if(skp)return self.stopPropagation(e)}
if(!("clientX" in e)&&("changedTouches" in e)){e.clientX=e.changedTouches[0].clientX;e.clientY=e.changedTouches[0].clientY}
if(self.forcescreen){var le=e;e={"original":(e.original)?e.original:e};e.clientX=le.screenX;e.clientY=le.screenY}
self.rail.drag={x:e.clientX,y:e.clientY,sx:self.scroll.x,sy:self.scroll.y,st:self.getScrollTop(),sl:self.getScrollLeft(),pt:2,dl:!1};if(self.ispage||!self.opt.directionlockdeadzone){self.rail.drag.dl="f"}else{var view={w:$(window).width(),h:$(window).height()};var page={w:Math.max(document.body.scrollWidth,document.documentElement.scrollWidth),h:Math.max(document.body.scrollHeight,document.documentElement.scrollHeight)};var maxh=Math.max(0,page.h-view.h);var maxw=Math.max(0,page.w-view.w);if(!self.rail.scrollable&&self.railh.scrollable)self.rail.drag.ck=(maxh>0)?"v":!1;else if(self.rail.scrollable&&!self.railh.scrollable)self.rail.drag.ck=(maxw>0)?"h":!1;else self.rail.drag.ck=!1;if(!self.rail.drag.ck)self.rail.drag.dl="f"}
if(self.opt.touchbehavior&&self.isiframe&&cap.isie){var wp=self.win.position();self.rail.drag.x+=wp.left;self.rail.drag.y+=wp.top}
self.hasmoving=!1;self.lastmouseup=!1;self.scrollmom.reset(e.clientX,e.clientY);if(!cap.cantouch&&!this.istouchcapable&&!e.pointerType){var ip=(tg)?/INPUT|SELECT|TEXTAREA/i.test(tg.nodeName):!1;if(!ip){if(!self.ispage&&cap.hasmousecapture)tg.setCapture();if(self.opt.touchbehavior){if(tg.onclick&&!(tg._onclick||!1)){tg._onclick=tg.onclick;tg.onclick=function(e){if(self.hasmoving)return !1;tg._onclick.call(this,e)}}
return self.cancelEvent(e)}
return self.stopPropagation(e)}
if(/SUBMIT|CANCEL|BUTTON/i.test($(tg).attr('type'))){pc={"tg":tg,"click":!1};self.preventclick=pc}}}};self.ontouchend=function(e){if(!self.rail.drag)return !0;if(self.rail.drag.pt==2){if(e.pointerType&&e.pointerType!=2&&e.pointerType!="touch")return !1;self.scrollmom.doMomentum();self.rail.drag=!1;if(self.hasmoving){self.lastmouseup=!0;self.hideCursor();if(cap.hasmousecapture)document.releaseCapture();if(!cap.cantouch)return self.cancelEvent(e)}}
else if(self.rail.drag.pt==1){return self.onmouseup(e)}};var moveneedoffset=(self.opt.touchbehavior&&self.isiframe&&!cap.hasmousecapture);self.ontouchmove=function(e,byiframe){if(!self.rail.drag)return !1;if(e.targetTouches&&self.opt.preventmultitouchscrolling){if(e.targetTouches.length>1)return !1}
if(e.pointerType&&e.pointerType!=2&&e.pointerType!="touch")return !1;if(self.rail.drag.pt==2){if(cap.cantouch&&(cap.isios)&&e.original===undefined)return !0;self.hasmoving=!0;if(self.preventclick&&!self.preventclick.click){self.preventclick.click=self.preventclick.tg.onclick||!1;self.preventclick.tg.onclick=self.onpreventclick}
var ev=$.extend({"original":e},e);e=ev;if(("changedTouches" in e)){e.clientX=e.changedTouches[0].clientX;e.clientY=e.changedTouches[0].clientY}
if(self.forcescreen){var le=e;e={"original":(e.original)?e.original:e};e.clientX=le.screenX;e.clientY=le.screenY}
var ofy,ofx;ofx=ofy=0;if(moveneedoffset&&!byiframe){var wp=self.win.position();ofx=-wp.left;ofy=-wp.top}
var fy=e.clientY+ofy;var my=(fy-self.rail.drag.y);var fx=e.clientX+ofx;var mx=(fx-self.rail.drag.x);var ny=self.rail.drag.st-my;if(self.ishwscroll&&self.opt.bouncescroll){if(ny<0){ny=Math.round(ny/2)}else if(ny>self.page.maxh){ny=self.page.maxh+Math.round((ny-self.page.maxh)/2)}}else{if(ny<0){ny=0;fy=0}
if(ny>self.page.maxh){ny=self.page.maxh;fy=0}}
var nx;if(self.railh&&self.railh.scrollable){nx=(self.isrtlmode)?mx-self.rail.drag.sl:self.rail.drag.sl-mx;if(self.ishwscroll&&self.opt.bouncescroll){if(nx<0){nx=Math.round(nx/2)}else if(nx>self.page.maxw){nx=self.page.maxw+Math.round((nx-self.page.maxw)/2)}}else{if(nx<0){nx=0;fx=0}
if(nx>self.page.maxw){nx=self.page.maxw;fx=0}}}
var grabbed=!1;if(self.rail.drag.dl){grabbed=!0;if(self.rail.drag.dl=="v")nx=self.rail.drag.sl;else if(self.rail.drag.dl=="h")ny=self.rail.drag.st}else{var ay=Math.abs(my);var ax=Math.abs(mx);var dz=self.opt.directionlockdeadzone;if(self.rail.drag.ck=="v"){if(ay>dz&&(ax<=(ay*0.3))){self.rail.drag=!1;return !0}else if(ax>dz){self.rail.drag.dl="f";$("body").scrollTop($("body").scrollTop())}}else if(self.rail.drag.ck=="h"){if(ax>dz&&(ay<=(ax*0.3))){self.rail.drag=!1;return !0}else if(ay>dz){self.rail.drag.dl="f";$("body").scrollLeft($("body").scrollLeft())}}}
self.synched("touchmove",function(){if(self.rail.drag&&(self.rail.drag.pt==2)){if(self.prepareTransition)self.prepareTransition(0);if(self.rail.scrollable)self.setScrollTop(ny);self.scrollmom.update(fx,fy);if(self.railh&&self.railh.scrollable){self.setScrollLeft(nx);self.showCursor(ny,nx)}else{self.showCursor(ny)}
if(cap.isie10)document.selection.clear()}});if(cap.ischrome&&self.istouchcapable)grabbed=!1;if(grabbed)return self.cancelEvent(e)}
else if(self.rail.drag.pt==1){return self.onmousemove(e)}}}
self.onmousedown=function(e,hronly){if(self.rail.drag&&self.rail.drag.pt!=1)return;if(self.railslocked)return self.cancelEvent(e);self.cancelScroll();self.rail.drag={x:e.clientX,y:e.clientY,sx:self.scroll.x,sy:self.scroll.y,pt:1,hr:(!!hronly)};var tg=self.getTarget(e);if(!self.ispage&&cap.hasmousecapture)tg.setCapture();if(self.isiframe&&!cap.hasmousecapture){self.saved.csspointerevents=self.doc.css("pointer-events");self.css(self.doc,{"pointer-events":"none"})}
self.hasmoving=!1;return self.cancelEvent(e)};self.onmouseup=function(e){if(self.rail.drag){if(self.rail.drag.pt!=1)return !0;if(cap.hasmousecapture)document.releaseCapture();if(self.isiframe&&!cap.hasmousecapture)self.doc.css("pointer-events",self.saved.csspointerevents);self.rail.drag=!1;if(self.hasmoving)self.triggerScrollEnd();return self.cancelEvent(e)}};self.onmousemove=function(e){if(self.rail.drag){if(self.rail.drag.pt!=1)return;if(cap.ischrome&&e.which==0)return self.onmouseup(e);self.cursorfreezed=!0;self.hasmoving=!0;if(self.rail.drag.hr){self.scroll.x=self.rail.drag.sx+(e.clientX-self.rail.drag.x);if(self.scroll.x<0)self.scroll.x=0;var mw=self.scrollvaluemaxw;if(self.scroll.x>mw)self.scroll.x=mw}else{self.scroll.y=self.rail.drag.sy+(e.clientY-self.rail.drag.y);if(self.scroll.y<0)self.scroll.y=0;var my=self.scrollvaluemax;if(self.scroll.y>my)self.scroll.y=my}
self.synched('mousemove',function(){if(self.rail.drag&&(self.rail.drag.pt==1)){self.showCursor();if(self.rail.drag.hr){if(self.hasreversehr){self.doScrollLeft(self.scrollvaluemaxw-Math.round(self.scroll.x*self.scrollratio.x),self.opt.cursordragspeed)}else{self.doScrollLeft(Math.round(self.scroll.x*self.scrollratio.x),self.opt.cursordragspeed)}}
else self.doScrollTop(Math.round(self.scroll.y*self.scrollratio.y),self.opt.cursordragspeed)}});return self.cancelEvent(e)}
else{self.checkarea=0}};if(cap.cantouch||self.opt.touchbehavior){self.onpreventclick=function(e){if(self.preventclick){self.preventclick.tg.onclick=self.preventclick.click;self.preventclick=!1;return self.cancelEvent(e)}};self.bind(self.win,"mousedown",self.ontouchstart);self.onclick=(cap.isios)?!1:function(e){if(self.lastmouseup){self.lastmouseup=!1;return self.cancelEvent(e)}else{return !0}};if(self.opt.grabcursorenabled&&cap.cursorgrabvalue){self.css((self.ispage)?self.doc:self.win,{'cursor':cap.cursorgrabvalue});self.css(self.rail,{'cursor':cap.cursorgrabvalue})}}else{var checkSelectionScroll=function(e){if(!self.selectiondrag)return;if(e){var ww=self.win.outerHeight();var df=(e.pageY-self.selectiondrag.top);if(df>0&&df<ww)df=0;if(df>=ww)df-=ww;self.selectiondrag.df=df}
if(self.selectiondrag.df==0)return;var rt=-Math.floor(self.selectiondrag.df/6)*2;self.doScrollBy(rt);self.debounced("doselectionscroll",function(){checkSelectionScroll()},50)};if("getSelection" in document){self.hasTextSelected=function(){return(document.getSelection().rangeCount>0)}}else if("selection" in document){self.hasTextSelected=function(){return(document.selection.type!="None")}}else{self.hasTextSelected=function(){return !1}}
self.onselectionstart=function(e){if(self.ispage)return;self.selectiondrag=self.win.offset()};self.onselectionend=function(e){self.selectiondrag=!1};self.onselectiondrag=function(e){if(!self.selectiondrag)return;if(self.hasTextSelected())self.debounced("selectionscroll",function(){checkSelectionScroll(e)},250)}}
if(cap.hasw3ctouch){self.css(self.rail,{'touch-action':'none'});self.css(self.cursor,{'touch-action':'none'});self.bind(self.win,"pointerdown",self.ontouchstart);self.bind(document,"pointerup",self.ontouchend);self.bind(document,"pointermove",self.ontouchmove)}else if(cap.hasmstouch){self.css(self.rail,{'-ms-touch-action':'none'});self.css(self.cursor,{'-ms-touch-action':'none'});self.bind(self.win,"MSPointerDown",self.ontouchstart);self.bind(document,"MSPointerUp",self.ontouchend);self.bind(document,"MSPointerMove",self.ontouchmove);self.bind(self.cursor,"MSGestureHold",function(e){e.preventDefault()});self.bind(self.cursor,"contextmenu",function(e){e.preventDefault()})}else if(this.istouchcapable){self.bind(self.win,"touchstart",self.ontouchstart);self.bind(document,"touchend",self.ontouchend);self.bind(document,"touchcancel",self.ontouchend);self.bind(document,"touchmove",self.ontouchmove)}
if(self.opt.cursordragontouch||(!cap.cantouch&&!self.opt.touchbehavior)){self.rail.css({cursor:"default"});self.railh&&self.railh.css({cursor:"default"});self.jqbind(self.rail,"mouseenter",function(){if(!self.ispage&&!self.win.is(":visible"))return !1;if(self.canshowonmouseevent)self.showCursor();self.rail.active=!0});self.jqbind(self.rail,"mouseleave",function(){self.rail.active=!1;if(!self.rail.drag)self.hideCursor()});if(self.opt.sensitiverail){self.bind(self.rail,"click",function(e){self.doRailClick(e,!1,!1)});self.bind(self.rail,"dblclick",function(e){self.doRailClick(e,!0,!1)});self.bind(self.cursor,"click",function(e){self.cancelEvent(e)});self.bind(self.cursor,"dblclick",function(e){self.cancelEvent(e)})}
if(self.railh){self.jqbind(self.railh,"mouseenter",function(){if(!self.ispage&&!self.win.is(":visible"))return !1;if(self.canshowonmouseevent)self.showCursor();self.rail.active=!0});self.jqbind(self.railh,"mouseleave",function(){self.rail.active=!1;if(!self.rail.drag)self.hideCursor()});if(self.opt.sensitiverail){self.bind(self.railh,"click",function(e){self.doRailClick(e,!1,!0)});self.bind(self.railh,"dblclick",function(e){self.doRailClick(e,!0,!0)});self.bind(self.cursorh,"click",function(e){self.cancelEvent(e)});self.bind(self.cursorh,"dblclick",function(e){self.cancelEvent(e)})}}}
if(!cap.cantouch&&!self.opt.touchbehavior){self.bind((cap.hasmousecapture)?self.win:document,"mouseup",self.onmouseup);self.bind(document,"mousemove",self.onmousemove);if(self.onclick)self.bind(document,"click",self.onclick);self.bind(self.cursor,"mousedown",self.onmousedown);self.bind(self.cursor,"mouseup",self.onmouseup);if(self.railh){self.bind(self.cursorh,"mousedown",function(e){self.onmousedown(e,!0)});self.bind(self.cursorh,"mouseup",self.onmouseup)}
if(!self.ispage&&self.opt.enablescrollonselection){self.bind(self.win[0],"mousedown",self.onselectionstart);self.bind(document,"mouseup",self.onselectionend);self.bind(self.cursor,"mouseup",self.onselectionend);if(self.cursorh)self.bind(self.cursorh,"mouseup",self.onselectionend);self.bind(document,"mousemove",self.onselectiondrag)}
if(self.zoom){self.jqbind(self.zoom,"mouseenter",function(){if(self.canshowonmouseevent)self.showCursor();self.rail.active=!0});self.jqbind(self.zoom,"mouseleave",function(){self.rail.active=!1;if(!self.rail.drag)self.hideCursor()})}}else{self.bind((cap.hasmousecapture)?self.win:document,"mouseup",self.ontouchend);self.bind(document,"mousemove",self.ontouchmove);if(self.onclick)self.bind(document,"click",self.onclick);if(self.opt.cursordragontouch){self.bind(self.cursor,"mousedown",self.onmousedown);self.bind(self.cursor,"mouseup",self.onmouseup);self.cursorh&&self.bind(self.cursorh,"mousedown",function(e){self.onmousedown(e,!0)});self.cursorh&&self.bind(self.cursorh,"mouseup",self.onmouseup)}else{self.bind(self.rail,"mousedown",function(e){e.preventDefault()});self.railh&&self.bind(self.railh,"mousedown",function(e){e.preventDefault()})}}
if(self.opt.enablemousewheel){if(!self.isiframe)self.mousewheel((cap.isie&&self.ispage)?document:self.win,self.onmousewheel);self.mousewheel(self.rail,self.onmousewheel);if(self.railh)self.mousewheel(self.railh,self.onmousewheelhr)}
if(!self.ispage&&!cap.cantouch&&!(/HTML|^BODY/.test(self.win[0].nodeName))){if(!self.win.attr("tabindex"))self.win.attr({"tabindex":tabindexcounter++});self.jqbind(self.win,"focus",function(e){domfocus=(self.getTarget(e)).id||!0;self.hasfocus=!0;if(self.canshowonmouseevent)self.noticeCursor()});self.jqbind(self.win,"blur",function(e){domfocus=!1;self.hasfocus=!1});self.jqbind(self.win,"mouseenter",function(e){mousefocus=(self.getTarget(e)).id||!0;self.hasmousefocus=!0;if(self.canshowonmouseevent)self.noticeCursor()});self.jqbind(self.win,"mouseleave",function(){mousefocus=!1;self.hasmousefocus=!1;if(!self.rail.drag)self.hideCursor()})}}
self.onkeypress=function(e){if(self.railslocked&&self.page.maxh==0)return !0;e=(e)?e:window.e;var tg=self.getTarget(e);if(tg&&/INPUT|TEXTAREA|SELECT|OPTION/.test(tg.nodeName)){var tp=tg.getAttribute('type')||tg.type||!1;if((!tp)||!(/submit|button|cancel/i.tp))return !0}
if($(tg).attr('contenteditable'))return !0;if(self.hasfocus||(self.hasmousefocus&&!domfocus)||(self.ispage&&!domfocus&&!mousefocus)){var key=e.keyCode;if(self.railslocked&&key!=27)return self.cancelEvent(e);var ctrl=e.ctrlKey||!1;var shift=e.shiftKey||!1;var ret=!1;switch(key){case 38:case 63233:self.doScrollBy(24*3);ret=!0;break;case 40:case 63235:self.doScrollBy(-24*3);ret=!0;break;case 37:case 63232:if(self.railh){(ctrl)?self.doScrollLeft(0):self.doScrollLeftBy(24*3);ret=!0}
break;case 39:case 63234:if(self.railh){(ctrl)?self.doScrollLeft(self.page.maxw):self.doScrollLeftBy(-24*3);ret=!0}
break;case 33:case 63276:self.doScrollBy(self.view.h);ret=!0;break;case 34:case 63277:self.doScrollBy(-self.view.h);ret=!0;break;case 36:case 63273:(self.railh&&ctrl)?self.doScrollPos(0,0):self.doScrollTo(0);ret=!0;break;case 35:case 63275:(self.railh&&ctrl)?self.doScrollPos(self.page.maxw,self.page.maxh):self.doScrollTo(self.page.maxh);ret=!0;break;case 32:if(self.opt.spacebarenabled){(shift)?self.doScrollBy(self.view.h):self.doScrollBy(-self.view.h);ret=!0}
break;case 27:if(self.zoomactive){self.doZoom();ret=!0}
break}
if(ret)return self.cancelEvent(e)}};if(self.opt.enablekeyboard)self.bind(document,(cap.isopera&&!cap.isopera12)?"keypress":"keydown",self.onkeypress);self.bind(document,"keydown",function(e){var ctrl=e.ctrlKey||!1;if(ctrl)self.wheelprevented=!0});self.bind(document,"keyup",function(e){var ctrl=e.ctrlKey||!1;if(!ctrl)self.wheelprevented=!1});self.bind(window,"blur",function(e){self.wheelprevented=!1});self.bind(window,'resize',self.lazyResize);self.bind(window,'orientationchange',self.lazyResize);self.bind(window,"load",self.lazyResize);if(cap.ischrome&&!self.ispage&&!self.haswrapper){var tmp=self.win.attr("style");var ww=parseFloat(self.win.css("width"))+1;self.win.css('width',ww);self.synched("chromefix",function(){self.win.attr("style",tmp)})}
self.onAttributeChange=function(e){self.lazyResize(self.isieold?250:30)};if((!self.isie11)&&(ClsMutationObserver!==!1)){self.observerbody=new ClsMutationObserver(function(mutations){mutations.forEach(function(mut){if(mut.type=="attributes"){return($("body").hasClass("modal-open")&&$("body").hasClass("modal-dialog")&&!$.contains($('.modal-dialog')[0],self.doc[0]))?self.hide():self.show()}});if(document.body.scrollHeight!=self.page.maxh)return self.lazyResize(30)});self.observerbody.observe(document.body,{childList:!0,subtree:!0,characterData:!1,attributes:!0,attributeFilter:['class']})}
if(!self.ispage&&!self.haswrapper){if(ClsMutationObserver!==!1){self.observer=new ClsMutationObserver(function(mutations){mutations.forEach(self.onAttributeChange)});self.observer.observe(self.win[0],{childList:!0,characterData:!1,attributes:!0,subtree:!1});self.observerremover=new ClsMutationObserver(function(mutations){mutations.forEach(function(mo){if(mo.removedNodes.length>0){for(var dd in mo.removedNodes){if(!!self&&(mo.removedNodes[dd]==self.win[0]))return self.remove()}}})});self.observerremover.observe(self.win[0].parentNode,{childList:!0,characterData:!1,attributes:!1,subtree:!1})}else{self.bind(self.win,(cap.isie&&!cap.isie9)?"propertychange":"DOMAttrModified",self.onAttributeChange);if(cap.isie9)self.win[0].attachEvent("onpropertychange",self.onAttributeChange);self.bind(self.win,"DOMNodeRemoved",function(e){if(e.target==self.win[0])self.remove()})}}
if(!self.ispage&&self.opt.boxzoom)self.bind(window,"resize",self.resizeZoom);if(self.istextarea){self.bind(self.win,"keydown",self.lazyResize);self.bind(self.win,"mouseup",self.lazyResize)}
self.lazyResize(30)}
if(this.doc[0].nodeName=='IFRAME'){var oniframeload=function(){self.iframexd=!1;var doc;try{doc='contentDocument' in this?this.contentDocument:this.contentWindow.document;var a=doc.domain}catch(e){self.iframexd=!0;doc=!1}
if(self.iframexd){if("console" in window)console.log('NiceScroll error: policy restriced iframe');return !0}
self.forcescreen=!0;if(self.isiframe){self.iframe={"doc":$(doc),"html":self.doc.contents().find('html')[0],"body":self.doc.contents().find('body')[0]};self.getContentSize=function(){return{w:Math.max(self.iframe.html.scrollWidth,self.iframe.body.scrollWidth),h:Math.max(self.iframe.html.scrollHeight,self.iframe.body.scrollHeight)}};self.docscroll=$(self.iframe.body)}
if(!cap.isios&&self.opt.iframeautoresize&&!self.isiframe){self.win.scrollTop(0);self.doc.height("");var hh=Math.max(doc.getElementsByTagName('html')[0].scrollHeight,doc.body.scrollHeight);self.doc.height(hh)}
self.lazyResize(30);if(cap.isie7)self.css($(self.iframe.html),_scrollyhidden);self.css($(self.iframe.body),_scrollyhidden);if(cap.isios&&self.haswrapper){self.css($(doc.body),{'-webkit-transform':'translate3d(0,0,0)'})}
if('contentWindow' in this){self.bind(this.contentWindow,"scroll",self.onscroll)}else{self.bind(doc,"scroll",self.onscroll)}
if(self.opt.enablemousewheel){self.mousewheel(doc,self.onmousewheel)}
if(self.opt.enablekeyboard)self.bind(doc,(cap.isopera)?"keypress":"keydown",self.onkeypress);if(cap.cantouch||self.opt.touchbehavior){self.bind(doc,"mousedown",self.ontouchstart);self.bind(doc,"mousemove",function(e){return self.ontouchmove(e,!0)});if(self.opt.grabcursorenabled&&cap.cursorgrabvalue)self.css($(doc.body),{'cursor':cap.cursorgrabvalue})}
self.bind(doc,"mouseup",self.ontouchend);if(self.zoom){if(self.opt.dblclickzoom)self.bind(doc,'dblclick',self.doZoom);if(self.ongesturezoom)self.bind(doc,"gestureend",self.ongesturezoom)}};if(this.doc[0].readyState&&this.doc[0].readyState=="complete"){setTimeout(function(){oniframeload.call(self.doc[0],!1)},500)}
self.bind(this.doc,"load",oniframeload)}};this.showCursor=function(py,px){if(self.cursortimeout){clearTimeout(self.cursortimeout);self.cursortimeout=0}
if(!self.rail)return;if(self.autohidedom){self.autohidedom.stop().css({opacity:self.opt.cursoropacitymax});self.cursoractive=!0}
if(!self.rail.drag||self.rail.drag.pt!=1){if(py!==undefined&&py!==!1){self.scroll.y=Math.round(py*1/self.scrollratio.y)}
if(px!==undefined){self.scroll.x=Math.round(px*1/self.scrollratio.x)}}
self.cursor.css({height:self.cursorheight,top:self.scroll.y});if(self.cursorh){var lx=(self.hasreversehr)?self.scrollvaluemaxw-self.scroll.x:self.scroll.x;(!self.rail.align&&self.rail.visibility)?self.cursorh.css({width:self.cursorwidth,left:lx+self.rail.width}):self.cursorh.css({width:self.cursorwidth,left:lx});self.cursoractive=!0}
if(self.zoom)self.zoom.stop().css({opacity:self.opt.cursoropacitymax})};this.hideCursor=function(tm){if(self.cursortimeout)return;if(!self.rail)return;if(!self.autohidedom)return;if(self.hasmousefocus&&self.opt.autohidemode=="leave")return;self.cursortimeout=setTimeout(function(){if(!self.rail.active||!self.showonmouseevent){self.autohidedom.stop().animate({opacity:self.opt.cursoropacitymin});if(self.zoom)self.zoom.stop().animate({opacity:self.opt.cursoropacitymin});self.cursoractive=!1}
self.cursortimeout=0},tm||self.opt.hidecursordelay)};this.noticeCursor=function(tm,py,px){self.showCursor(py,px);if(!self.rail.active)self.hideCursor(tm)};this.getContentSize=(self.ispage)?function(){return{w:Math.max(document.body.scrollWidth,document.documentElement.scrollWidth),h:Math.max(document.body.scrollHeight,document.documentElement.scrollHeight)}}:(self.haswrapper)?function(){return{w:self.doc.outerWidth()+parseInt(self.win.css('paddingLeft'))+parseInt(self.win.css('paddingRight')),h:self.doc.outerHeight()+parseInt(self.win.css('paddingTop'))+parseInt(self.win.css('paddingBottom'))}}:function(){return{w:self.docscroll[0].scrollWidth,h:self.docscroll[0].scrollHeight}};this.onResize=function(e,page){if(!self||!self.win)return !1;if(!self.haswrapper&&!self.ispage){if(self.win.css('display')=='none'){if(self.visibility)self.hideRail().hideRailHr();return !1}else{if(!self.hidden&&!self.visibility)self.showRail().showRailHr()}}
var premaxh=self.page.maxh;var premaxw=self.page.maxw;var preview={h:self.view.h,w:self.view.w};self.view={w:(self.ispage)?self.win.width():parseInt(self.win[0].clientWidth),h:(self.ispage)?self.win.height():parseInt(self.win[0].clientHeight)};self.page=(page)?page:self.getContentSize();self.page.maxh=Math.max(0,self.page.h-self.view.h);self.page.maxw=Math.max(0,self.page.w-self.view.w);if((self.page.maxh==premaxh)&&(self.page.maxw==premaxw)&&(self.view.w==preview.w)&&(self.view.h==preview.h)){if(!self.ispage){var pos=self.win.offset();if(self.lastposition){var lst=self.lastposition;if((lst.top==pos.top)&&(lst.left==pos.left))return self}
self.lastposition=pos}else{return self}}
if(self.page.maxh==0){self.hideRail();self.scrollvaluemax=0;self.scroll.y=0;self.scrollratio.y=0;self.cursorheight=0;self.setScrollTop(0);if(self.rail)self.rail.scrollable=!1}else{self.page.maxh-=(self.opt.railpadding.top+self.opt.railpadding.bottom);self.rail.scrollable=!0}
if(self.page.maxw==0){self.hideRailHr();self.scrollvaluemaxw=0;self.scroll.x=0;self.scrollratio.x=0;self.cursorwidth=0;self.setScrollLeft(0);if(self.railh){self.railh.scrollable=!1}}else{self.page.maxw-=(self.opt.railpadding.left+self.opt.railpadding.right);if(self.railh)self.railh.scrollable=(self.opt.horizrailenabled)}
self.railslocked=(self.locked)||((self.page.maxh==0)&&(self.page.maxw==0));if(self.railslocked){if(!self.ispage)self.updateScrollBar(self.view);return !1}
if(!self.hidden&&!self.visibility){self.showRail().showRailHr()}
else if(self.railh&&(!self.hidden&&!self.railh.visibility))self.showRailHr();if(self.istextarea&&self.win.css('resize')&&self.win.css('resize')!='none')self.view.h-=20;self.cursorheight=Math.min(self.view.h,Math.round(self.view.h*(self.view.h/self.page.h)));self.cursorheight=(self.opt.cursorfixedheight)?self.opt.cursorfixedheight:Math.max(self.opt.cursorminheight,self.cursorheight);self.cursorwidth=Math.min(self.view.w,Math.round(self.view.w*(self.view.w/self.page.w)));self.cursorwidth=(self.opt.cursorfixedheight)?self.opt.cursorfixedheight:Math.max(self.opt.cursorminheight,self.cursorwidth);self.scrollvaluemax=self.view.h-self.cursorheight-self.cursor.hborder-(self.opt.railpadding.top+self.opt.railpadding.bottom);if(self.railh){self.railh.width=(self.page.maxh>0)?(self.view.w-self.rail.width):self.view.w;self.scrollvaluemaxw=self.railh.width-self.cursorwidth-self.cursorh.wborder-(self.opt.railpadding.left+self.opt.railpadding.right)}
if(!self.ispage)self.updateScrollBar(self.view);self.scrollratio={x:(self.page.maxw/self.scrollvaluemaxw),y:(self.page.maxh/self.scrollvaluemax)};var sy=self.getScrollTop();if(sy>self.page.maxh){self.doScrollTop(self.page.maxh)}else{self.scroll.y=Math.round(self.getScrollTop()*(1/self.scrollratio.y));self.scroll.x=Math.round(self.getScrollLeft()*(1/self.scrollratio.x));if(self.cursoractive)self.noticeCursor()}
if(self.scroll.y&&(self.getScrollTop()==0))self.doScrollTo(Math.floor(self.scroll.y*self.scrollratio.y));return self};this.resize=self.onResize;this.hlazyresize=0;this.lazyResize=function(tm){if(!self.haswrapper)self.hide();if(self.hlazyresize)clearTimeout(self.hlazyresize);self.hlazyresize=setTimeout(function(){self&&self.show().resize()},240);return self};function _modernWheelEvent(dom,name,fn,bubble){self._bind(dom,name,function(e){var e=(e)?e:window.event;var event={original:e,target:e.target||e.srcElement,type:"wheel",deltaMode:e.type=="MozMousePixelScroll"?0:1,deltaX:0,deltaZ:0,preventDefault:function(){e.preventDefault?e.preventDefault():e.returnValue=!1;return !1},stopImmediatePropagation:function(){(e.stopImmediatePropagation)?e.stopImmediatePropagation():e.cancelBubble=!0}};if(name=="mousewheel"){e.wheelDeltaX&&(event.deltaX=-1/40*e.wheelDeltaX);e.wheelDeltaY&&(event.deltaY=-1/40*e.wheelDeltaY);!event.deltaY&&!event.deltaX&&(event.deltaY=-1/40*e.wheelDelta)}else{event.deltaY=e.detail}
return fn.call(dom,event)},bubble)}
this.jqbind=function(dom,name,fn){self.events.push({e:dom,n:name,f:fn,q:!0});$(dom).bind(name,fn)};this.mousewheel=function(dom,fn,bubble){var el=("jquery" in dom)?dom[0]:dom;if("onwheel" in document.createElement("div")){self._bind(el,"wheel",fn,bubble||!1)}else{var wname=(document.onmousewheel!==undefined)?"mousewheel":"DOMMouseScroll";_modernWheelEvent(el,wname,fn,bubble||!1);if(wname=="DOMMouseScroll")_modernWheelEvent(el,"MozMousePixelScroll",fn,bubble||!1)}};if(cap.haseventlistener){this.bind=function(dom,name,fn,bubble){var el=("jquery" in dom)?dom[0]:dom;self._bind(el,name,fn,bubble||!1)};this._bind=function(el,name,fn,bubble){self.events.push({e:el,n:name,f:fn,b:bubble,q:!1});el.addEventListener(name,fn,bubble||!1)};this.cancelEvent=function(e){if(!e)return !1;var e=(e.original)?e.original:e;if(e.cancelable)e.preventDefault();e.stopPropagation();if(e.preventManipulation)e.preventManipulation();return !1};this.stopPropagation=function(e){if(!e)return !1;var e=(e.original)?e.original:e;e.stopPropagation();return !1};this._unbind=function(el,name,fn,bub){el.removeEventListener(name,fn,bub)}}else{this.bind=function(dom,name,fn,bubble){var el=("jquery" in dom)?dom[0]:dom;self._bind(el,name,function(e){e=e||window.event||!1;if(e&&e.srcElement){e.target=e.srcElement}
if(!("pageY" in e)){e.pageX=e.clientX+document.documentElement.scrollLeft;e.pageY=e.clientY+document.documentElement.scrollTop}
return((fn.call(el,e)===!1)||bubble===!1)?self.cancelEvent(e):!0})};this._bind=function(el,name,fn,bubble){self.events.push({e:el,n:name,f:fn,b:bubble,q:!1});if(el.attachEvent){el.attachEvent("on"+name,fn)}else{el["on"+name]=fn}};this.cancelEvent=function(e){var e=window.event||!1;if(!e)return !1;e.cancelBubble=!0;e.cancel=!0;e.returnValue=!1;return !1};this.stopPropagation=function(e){var e=window.event||!1;if(!e)return !1;e.cancelBubble=!0;return !1};this._unbind=function(el,name,fn,bub){if(el.detachEvent){el.detachEvent('on'+name,fn)}else{el['on'+name]=!1}}}
this.unbindAll=function(){for(var a=0;a<self.events.length;a++){var r=self.events[a];(r.q)?r.e.unbind(r.n,r.f):self._unbind(r.e,r.n,r.f,r.b)}};this.showRail=function(){if((self.page.maxh!=0)&&(self.ispage||self.win.css('display')!='none')){self.visibility=!0;self.rail.visibility=!0;self.rail.css('display','block')}
return self};this.showRailHr=function(){if(!self.railh)return self;if((self.page.maxw!=0)&&(self.ispage||self.win.css('display')!='none')){self.railh.visibility=!0;self.railh.css('display','block')}
return self};this.hideRail=function(){self.visibility=!1;self.rail.visibility=!1;self.rail.css('display','none');return self};this.hideRailHr=function(){if(!self.railh)return self;self.railh.visibility=!1;self.railh.css('display','none');return self};this.show=function(){self.hidden=!1;self.railslocked=!1;return self.showRail().showRailHr()};this.hide=function(){self.hidden=!0;self.railslocked=!0;return self.hideRail().hideRailHr()};this.toggle=function(){return(self.hidden)?self.show():self.hide()};this.remove=function(){self.stop();if(self.cursortimeout)clearTimeout(self.cursortimeout);for(var n in self.delaylist)if(self.delaylist[n])clearAnimationFrame(self.delaylist[n].h);self.doZoomOut();self.unbindAll();if(cap.isie9)self.win[0].detachEvent("onpropertychange",self.onAttributeChange);if(self.observer!==!1)self.observer.disconnect();if(self.observerremover!==!1)self.observerremover.disconnect();if(self.observerbody!==!1)self.observerbody.disconnect();self.events=null;if(self.cursor){self.cursor.remove()}
if(self.cursorh){self.cursorh.remove()}
if(self.rail){self.rail.remove()}
if(self.railh){self.railh.remove()}
if(self.zoom){self.zoom.remove()}
for(var a=0;a<self.saved.css.length;a++){var d=self.saved.css[a];d[0].css(d[1],(d[2]===undefined)?'':d[2])}
self.saved=!1;self.me.data('__nicescroll','');var lst=$.nicescroll;lst.each(function(i){if(!this)return;if(this.id===self.id){delete lst[i];for(var b=++i;b<lst.length;b++,i++)lst[i]=lst[b];lst.length--;if(lst.length)delete lst[lst.length]}});for(var i in self){self[i]=null;delete self[i]}
self=null};this.scrollstart=function(fn){this.onscrollstart=fn;return self};this.scrollend=function(fn){this.onscrollend=fn;return self};this.scrollcancel=function(fn){this.onscrollcancel=fn;return self};this.zoomin=function(fn){this.onzoomin=fn;return self};this.zoomout=function(fn){this.onzoomout=fn;return self};this.isScrollable=function(e){var dom=(e.target)?e.target:e;if(dom.nodeName=='OPTION')return !0;while(dom&&(dom.nodeType==1)&&!(/^BODY|HTML/.test(dom.nodeName))){var dd=$(dom);var ov=dd.css('overflowY')||dd.css('overflowX')||dd.css('overflow')||'';if(/scroll|auto/.test(ov))return(dom.clientHeight!=dom.scrollHeight);dom=(dom.parentNode)?dom.parentNode:!1}
return !1};this.getViewport=function(me){var dom=(me&&me.parentNode)?me.parentNode:!1;while(dom&&(dom.nodeType==1)&&!(/^BODY|HTML/.test(dom.nodeName))){var dd=$(dom);if(/fixed|absolute/.test(dd.css("position")))return dd;var ov=dd.css('overflowY')||dd.css('overflowX')||dd.css('overflow')||'';if((/scroll|auto/.test(ov))&&(dom.clientHeight!=dom.scrollHeight))return dd;if(dd.getNiceScroll().length>0)return dd;dom=(dom.parentNode)?dom.parentNode:!1}
return !1};this.triggerScrollEnd=function(){if(!self.onscrollend)return;var px=self.getScrollLeft();var py=self.getScrollTop();var info={type:"scrollend",current:{x:px,y:py},end:{x:px,y:py}};self.onscrollend.call(self,info)};function execScrollWheel(e,hr,chkscroll){var px,py;if(e.deltaMode==0){px=-Math.floor(e.deltaX*(self.opt.mousescrollstep/(18*3)));py=-Math.floor(e.deltaY*(self.opt.mousescrollstep/(18*3)))}else if(e.deltaMode==1){px=-Math.floor(e.deltaX*self.opt.mousescrollstep);py=-Math.floor(e.deltaY*self.opt.mousescrollstep)}
if(hr&&self.opt.oneaxismousemode&&(px==0)&&py){px=py;py=0;if(chkscroll){var hrend=(px<0)?(self.getScrollLeft()>=self.page.maxw):(self.getScrollLeft()<=0);if(hrend){py=px;px=0}}}
if(self.isrtlmode)px=-px;if(px){if(self.scrollmom){self.scrollmom.stop()}
self.lastdeltax+=px;self.debounced("mousewheelx",function(){var dt=self.lastdeltax;self.lastdeltax=0;if(!self.rail.drag){self.doScrollLeftBy(dt)}},15)}
if(py){if(self.opt.nativeparentscrolling&&chkscroll&&!self.ispage&&!self.zoomactive){if(py<0){if(self.getScrollTop()>=self.page.maxh)return !0}else{if(self.getScrollTop()<=0)return !0}}
if(self.scrollmom){self.scrollmom.stop()}
self.lastdeltay+=py;self.synched("mousewheely",function(){var dt=self.lastdeltay;self.lastdeltay=0;if(!self.rail.drag){self.doScrollBy(dt)}},15)}
e.stopImmediatePropagation();return e.preventDefault()}
this.onmousewheel=function(e){if(self.wheelprevented)return;if(self.railslocked){self.debounced("checkunlock",self.resize,250);return !0}
if(self.rail.drag)return self.cancelEvent(e);if(self.opt.oneaxismousemode=="auto"&&e.deltaX!=0)self.opt.oneaxismousemode=!1;if(self.opt.oneaxismousemode&&e.deltaX==0){if(!self.rail.scrollable){if(self.railh&&self.railh.scrollable){return self.onmousewheelhr(e)}else{return !0}}}
var nw=+(new Date());var chk=!1;if(self.opt.preservenativescrolling&&((self.checkarea+600)<nw)){self.nativescrollingarea=self.isScrollable(e);chk=!0}
self.checkarea=nw;if(self.nativescrollingarea)return !0;var ret=execScrollWheel(e,!1,chk);if(ret)self.checkarea=0;return ret};this.onmousewheelhr=function(e){if(self.wheelprevented)return;if(self.railslocked||!self.railh.scrollable)return !0;if(self.rail.drag)return self.cancelEvent(e);var nw=+(new Date());var chk=!1;if(self.opt.preservenativescrolling&&((self.checkarea+600)<nw)){self.nativescrollingarea=self.isScrollable(e);chk=!0}
self.checkarea=nw;if(self.nativescrollingarea)return !0;if(self.railslocked)return self.cancelEvent(e);return execScrollWheel(e,!0,chk)};this.stop=function(){self.cancelScroll();if(self.scrollmon)self.scrollmon.stop();self.cursorfreezed=!1;self.scroll.y=Math.round(self.getScrollTop()*(1/self.scrollratio.y));self.noticeCursor();return self};this.getTransitionSpeed=function(dif){var sp=Math.round(self.opt.scrollspeed*10);var ex=Math.min(sp,Math.round((dif/20)*self.opt.scrollspeed));return(ex>20)?ex:0};if(!self.opt.smoothscroll){this.doScrollLeft=function(x,spd){var y=self.getScrollTop();self.doScrollPos(x,y,spd)};this.doScrollTop=function(y,spd){var x=self.getScrollLeft();self.doScrollPos(x,y,spd)};this.doScrollPos=function(x,y,spd){var nx=(x>self.page.maxw)?self.page.maxw:x;if(nx<0)nx=0;var ny=(y>self.page.maxh)?self.page.maxh:y;if(ny<0)ny=0;self.synched('scroll',function(){self.setScrollTop(ny);self.setScrollLeft(nx)})};this.cancelScroll=function(){}}else if(self.ishwscroll&&cap.hastransition&&self.opt.usetransition&&!!self.opt.smoothscroll){this.prepareTransition=function(dif,istime){var ex=(istime)?((dif>20)?dif:0):self.getTransitionSpeed(dif);var trans=(ex)?cap.prefixstyle+'transform '+ex+'ms ease-out':'';if(!self.lasttransitionstyle||self.lasttransitionstyle!=trans){self.lasttransitionstyle=trans;self.doc.css(cap.transitionstyle,trans)}
return ex};this.doScrollLeft=function(x,spd){var y=(self.scrollrunning)?self.newscrolly:self.getScrollTop();self.doScrollPos(x,y,spd)};this.doScrollTop=function(y,spd){var x=(self.scrollrunning)?self.newscrollx:self.getScrollLeft();self.doScrollPos(x,y,spd)};this.doScrollPos=function(x,y,spd){var py=self.getScrollTop();var px=self.getScrollLeft();if(((self.newscrolly-py)*(y-py)<0)||((self.newscrollx-px)*(x-px)<0))self.cancelScroll();if(self.opt.bouncescroll==!1){if(y<0)y=0;else if(y>self.page.maxh)y=self.page.maxh;if(x<0)x=0;else if(x>self.page.maxw)x=self.page.maxw}
if(self.scrollrunning&&x==self.newscrollx&&y==self.newscrolly)return !1;self.newscrolly=y;self.newscrollx=x;self.newscrollspeed=spd||!1;if(self.timer)return !1;self.timer=setTimeout(function(){var top=self.getScrollTop();var lft=self.getScrollLeft();var dst={};dst.x=x-lft;dst.y=y-top;dst.px=lft;dst.py=top;var dd=Math.round(Math.sqrt(Math.pow(dst.x,2)+Math.pow(dst.y,2)));var ms=(self.newscrollspeed&&self.newscrollspeed>1)?self.newscrollspeed:self.getTransitionSpeed(dd);if(self.newscrollspeed&&self.newscrollspeed<=1)ms*=self.newscrollspeed;self.prepareTransition(ms,!0);if(self.timerscroll&&self.timerscroll.tm)clearInterval(self.timerscroll.tm);if(ms>0){if(!self.scrollrunning&&self.onscrollstart){var info={"type":"scrollstart","current":{"x":lft,"y":top},"request":{"x":x,"y":y},"end":{"x":self.newscrollx,"y":self.newscrolly},"speed":ms};self.onscrollstart.call(self,info)}
if(cap.transitionend){if(!self.scrollendtrapped){self.scrollendtrapped=!0;self.bind(self.doc,cap.transitionend,self.onScrollTransitionEnd,!1)}}else{if(self.scrollendtrapped)clearTimeout(self.scrollendtrapped);self.scrollendtrapped=setTimeout(self.onScrollTransitionEnd,ms)}
var py=top;var px=lft;self.timerscroll={bz:new BezierClass(py,self.newscrolly,ms,0,0,0.58,1),bh:new BezierClass(px,self.newscrollx,ms,0,0,0.58,1)};if(!self.cursorfreezed)self.timerscroll.tm=setInterval(function(){self.showCursor(self.getScrollTop(),self.getScrollLeft())},60)}
self.synched("doScroll-set",function(){self.timer=0;if(self.scrollendtrapped)self.scrollrunning=!0;self.setScrollTop(self.newscrolly);self.setScrollLeft(self.newscrollx);if(!self.scrollendtrapped)self.onScrollTransitionEnd()})},50)};this.cancelScroll=function(){if(!self.scrollendtrapped)return !0;var py=self.getScrollTop();var px=self.getScrollLeft();self.scrollrunning=!1;if(!cap.transitionend)clearTimeout(cap.transitionend);self.scrollendtrapped=!1;self._unbind(self.doc[0],cap.transitionend,self.onScrollTransitionEnd);self.prepareTransition(0);self.setScrollTop(py);if(self.railh)self.setScrollLeft(px);if(self.timerscroll&&self.timerscroll.tm)clearInterval(self.timerscroll.tm);self.timerscroll=!1;self.cursorfreezed=!1;self.showCursor(py,px);return self};this.onScrollTransitionEnd=function(){if(self.scrollendtrapped)self._unbind(self.doc[0],cap.transitionend,self.onScrollTransitionEnd);self.scrollendtrapped=!1;self.prepareTransition(0);if(self.timerscroll&&self.timerscroll.tm)clearInterval(self.timerscroll.tm);self.timerscroll=!1;var py=self.getScrollTop();var px=self.getScrollLeft();self.setScrollTop(py);if(self.railh)self.setScrollLeft(px);self.noticeCursor(!1,py,px);self.cursorfreezed=!1;if(py<0)py=0;else if(py>self.page.maxh)py=self.page.maxh;if(px<0)px=0;else if(px>self.page.maxw)px=self.page.maxw;if((py!=self.newscrolly)||(px!=self.newscrollx))return self.doScrollPos(px,py,self.opt.snapbackspeed);if(self.onscrollend&&self.scrollrunning){self.triggerScrollEnd()}
self.scrollrunning=!1}}else{this.doScrollLeft=function(x,spd){var y=(self.scrollrunning)?self.newscrolly:self.getScrollTop();self.doScrollPos(x,y,spd)};this.doScrollTop=function(y,spd){var x=(self.scrollrunning)?self.newscrollx:self.getScrollLeft();self.doScrollPos(x,y,spd)};this.doScrollPos=function(x,y,spd){var y=(y===undefined||y===!1)?self.getScrollTop(!0):y;if((self.timer)&&(self.newscrolly==y)&&(self.newscrollx==x))return !0;if(self.timer)clearAnimationFrame(self.timer);self.timer=0;var py=self.getScrollTop();var px=self.getScrollLeft();if(((self.newscrolly-py)*(y-py)<0)||((self.newscrollx-px)*(x-px)<0))self.cancelScroll();self.newscrolly=y;self.newscrollx=x;if(!self.bouncescroll||!self.rail.visibility){if(self.newscrolly<0){self.newscrolly=0}else if(self.newscrolly>self.page.maxh){self.newscrolly=self.page.maxh}}
if(!self.bouncescroll||!self.railh.visibility){if(self.newscrollx<0){self.newscrollx=0}else if(self.newscrollx>self.page.maxw){self.newscrollx=self.page.maxw}}
self.dst={};self.dst.x=x-px;self.dst.y=y-py;self.dst.px=px;self.dst.py=py;var dst=Math.round(Math.sqrt(Math.pow(self.dst.x,2)+Math.pow(self.dst.y,2)));self.dst.ax=self.dst.x/dst;self.dst.ay=self.dst.y/dst;var pa=0;var pe=dst;if(self.dst.x==0){pa=py;pe=y;self.dst.ay=1;self.dst.py=0}else if(self.dst.y==0){pa=px;pe=x;self.dst.ax=1;self.dst.px=0}
var ms=self.getTransitionSpeed(dst);if(spd&&spd<=1)ms*=spd;if(ms>0){self.bzscroll=(self.bzscroll)?self.bzscroll.update(pe,ms):new BezierClass(pa,pe,ms,0,1,0,1)}else{self.bzscroll=!1}
if(self.timer)return;if((py==self.page.maxh&&y>=self.page.maxh)||(px==self.page.maxw&&x>=self.page.maxw))self.checkContentSize();var sync=1;function scrolling(){if(self.cancelAnimationFrame)return !0;self.scrollrunning=!0;sync=1-sync;if(sync)return(self.timer=setAnimationFrame(scrolling)||1);var done=0;var sx,sy;var sc=sy=self.getScrollTop();if(self.dst.ay){sc=(self.bzscroll)?self.dst.py+(self.bzscroll.getNow()*self.dst.ay):self.newscrolly;var dr=sc-sy;if((dr<0&&sc<self.newscrolly)||(dr>0&&sc>self.newscrolly))sc=self.newscrolly;self.setScrollTop(sc);if(sc==self.newscrolly)done=1}else{done=1}
var scx=sx=self.getScrollLeft();if(self.dst.ax){scx=(self.bzscroll)?self.dst.px+(self.bzscroll.getNow()*self.dst.ax):self.newscrollx;var dr=scx-sx;if((dr<0&&scx<self.newscrollx)||(dr>0&&scx>self.newscrollx))scx=self.newscrollx;self.setScrollLeft(scx);if(scx==self.newscrollx)done+=1}else{done+=1}
if(done==2){self.timer=0;self.cursorfreezed=!1;self.bzscroll=!1;self.scrollrunning=!1;if(sc<0)sc=0;else if(sc>self.page.maxh)sc=Math.max(0,self.page.maxh);if(scx<0)scx=0;else if(scx>self.page.maxw)scx=self.page.maxw;if((scx!=self.newscrollx)||(sc!=self.newscrolly))self.doScrollPos(scx,sc);else{if(self.onscrollend){self.triggerScrollEnd()}}}else{self.timer=setAnimationFrame(scrolling)||1}}
self.cancelAnimationFrame=!1;self.timer=1;if(self.onscrollstart&&!self.scrollrunning){var info={"type":"scrollstart","current":{"x":px,"y":py},"request":{"x":x,"y":y},"end":{"x":self.newscrollx,"y":self.newscrolly},"speed":ms};self.onscrollstart.call(self,info)}
scrolling();if((py==self.page.maxh&&y>=py)||(px==self.page.maxw&&x>=px))self.checkContentSize();self.noticeCursor()};this.cancelScroll=function(){if(self.timer)clearAnimationFrame(self.timer);self.timer=0;self.bzscroll=!1;self.scrollrunning=!1;return self}}
this.doScrollBy=function(stp,relative){var ny=0;if(relative){ny=Math.floor((self.scroll.y-stp)*self.scrollratio.y)}else{var sy=(self.timer)?self.newscrolly:self.getScrollTop(!0);ny=sy-stp}
if(self.bouncescroll){var haf=Math.round(self.view.h/2);if(ny<-haf)ny=-haf;else if(ny>(self.page.maxh+haf))ny=(self.page.maxh+haf)}
self.cursorfreezed=!1;var py=self.getScrollTop(!0);if(ny<0&&py<=0)return self.noticeCursor();else if(ny>self.page.maxh&&py>=self.page.maxh){self.checkContentSize();return self.noticeCursor()}
self.doScrollTop(ny)};this.doScrollLeftBy=function(stp,relative){var nx=0;if(relative){nx=Math.floor((self.scroll.x-stp)*self.scrollratio.x)}else{var sx=(self.timer)?self.newscrollx:self.getScrollLeft(!0);nx=sx-stp}
if(self.bouncescroll){var haf=Math.round(self.view.w/2);if(nx<-haf)nx=-haf;else if(nx>(self.page.maxw+haf))nx=(self.page.maxw+haf)}
self.cursorfreezed=!1;var px=self.getScrollLeft(!0);if(nx<0&&px<=0)return self.noticeCursor();else if(nx>self.page.maxw&&px>=self.page.maxw)return self.noticeCursor();self.doScrollLeft(nx)};this.doScrollTo=function(pos,relative){var ny=(relative)?Math.round(pos*self.scrollratio.y):pos;if(ny<0)ny=0;else if(ny>self.page.maxh)ny=self.page.maxh;self.cursorfreezed=!1;self.doScrollTop(pos)};this.checkContentSize=function(){var pg=self.getContentSize();if((pg.h!=self.page.h)||(pg.w!=self.page.w))self.resize(!1,pg)};self.onscroll=function(e){if(self.rail.drag)return;if(!self.cursorfreezed){self.synched('scroll',function(){self.scroll.y=Math.round(self.getScrollTop()*(1/self.scrollratio.y));if(self.railh)self.scroll.x=Math.round(self.getScrollLeft()*(1/self.scrollratio.x));self.noticeCursor()})}};self.bind(self.docscroll,"scroll",self.onscroll);this.doZoomIn=function(e){if(self.zoomactive)return;self.zoomactive=!0;self.zoomrestore={style:{}};var lst=['position','top','left','zIndex','backgroundColor','marginTop','marginBottom','marginLeft','marginRight'];var win=self.win[0].style;for(var a in lst){var pp=lst[a];self.zoomrestore.style[pp]=(win[pp]!==undefined)?win[pp]:''}
self.zoomrestore.style.width=self.win.css('width');self.zoomrestore.style.height=self.win.css('height');self.zoomrestore.padding={w:self.win.outerWidth()-self.win.width(),h:self.win.outerHeight()-self.win.height()};if(cap.isios4){self.zoomrestore.scrollTop=$(window).scrollTop();$(window).scrollTop(0)}
self.win.css({position:(cap.isios4)?"absolute":"fixed",top:0,left:0,zIndex:globalmaxzindex+100,margin:0});var bkg=self.win.css("backgroundColor");if(bkg==""||/transparent|rgba\(0, 0, 0, 0\)|rgba\(0,0,0,0\)/.test(bkg))self.win.css("backgroundColor","#fff");self.rail.css({zIndex:globalmaxzindex+101});self.zoom.css({zIndex:globalmaxzindex+102});self.zoom.css('backgroundPosition','0px -18px');self.resizeZoom();if(self.onzoomin)self.onzoomin.call(self);return self.cancelEvent(e)};this.doZoomOut=function(e){if(!self.zoomactive)return;self.zoomactive=!1;self.win.css("margin","");self.win.css(self.zoomrestore.style);if(cap.isios4){$(window).scrollTop(self.zoomrestore.scrollTop)}
self.rail.css({"z-index":self.zindex});self.zoom.css({"z-index":self.zindex});self.zoomrestore=!1;self.zoom.css('backgroundPosition','0px 0px');self.onResize();if(self.onzoomout)self.onzoomout.call(self);return self.cancelEvent(e)};this.doZoom=function(e){return(self.zoomactive)?self.doZoomOut(e):self.doZoomIn(e)};this.resizeZoom=function(){if(!self.zoomactive)return;var py=self.getScrollTop();self.win.css({width:$(window).width()-self.zoomrestore.padding.w+"px",height:$(window).height()-self.zoomrestore.padding.h+"px"});self.onResize();self.setScrollTop(Math.min(self.page.maxh,py))};this.init();$.nicescroll.push(this)};var ScrollMomentumClass2D=function(nc){var self=this;this.nc=nc;this.lastx=0;this.lasty=0;this.speedx=0;this.speedy=0;this.lasttime=0;this.steptime=0;this.snapx=!1;this.snapy=!1;this.demulx=0;this.demuly=0;this.lastscrollx=-1;this.lastscrolly=-1;this.chkx=0;this.chky=0;this.timer=0;this.time=function(){return+new Date()};this.reset=function(px,py){self.stop();var now=self.time();self.steptime=0;self.lasttime=now;self.speedx=0;self.speedy=0;self.lastx=px;self.lasty=py;self.lastscrollx=-1;self.lastscrolly=-1};this.update=function(px,py){var now=self.time();self.steptime=now-self.lasttime;self.lasttime=now;var dy=py-self.lasty;var dx=px-self.lastx;var sy=self.nc.getScrollTop();var sx=self.nc.getScrollLeft();var newy=sy+dy;var newx=sx+dx;self.snapx=(newx<0)||(newx>self.nc.page.maxw);self.snapy=(newy<0)||(newy>self.nc.page.maxh);self.speedx=dx;self.speedy=dy;self.lastx=px;self.lasty=py};this.stop=function(){self.nc.unsynched("domomentum2d");if(self.timer)clearTimeout(self.timer);self.timer=0;self.lastscrollx=-1;self.lastscrolly=-1};this.doSnapy=function(nx,ny){var snap=!1;if(ny<0){ny=0;snap=!0}else if(ny>self.nc.page.maxh){ny=self.nc.page.maxh;snap=!0}
if(nx<0){nx=0;snap=!0}else if(nx>self.nc.page.maxw){nx=self.nc.page.maxw;snap=!0}(snap)?self.nc.doScrollPos(nx,ny,self.nc.opt.snapbackspeed):self.nc.triggerScrollEnd()};this.doMomentum=function(gp){var t=self.time();var l=(gp)?t+gp:self.lasttime;var sl=self.nc.getScrollLeft();var st=self.nc.getScrollTop();var pageh=self.nc.page.maxh;var pagew=self.nc.page.maxw;self.speedx=(pagew>0)?Math.min(60,self.speedx):0;self.speedy=(pageh>0)?Math.min(60,self.speedy):0;var chk=l&&(t-l)<=60;if((st<0)||(st>pageh)||(sl<0)||(sl>pagew))chk=!1;var sy=(self.speedy&&chk)?self.speedy:!1;var sx=(self.speedx&&chk)?self.speedx:!1;if(sy||sx){var tm=Math.max(16,self.steptime);if(tm>50){var xm=tm/50;self.speedx*=xm;self.speedy*=xm;tm=50}
self.demulxy=0;self.lastscrollx=self.nc.getScrollLeft();self.chkx=self.lastscrollx;self.lastscrolly=self.nc.getScrollTop();self.chky=self.lastscrolly;var nx=self.lastscrollx;var ny=self.lastscrolly;var onscroll=function(){var df=((self.time()-t)>600)?0.04:0.02;if(self.speedx){nx=Math.floor(self.lastscrollx-(self.speedx*(1-self.demulxy)));self.lastscrollx=nx;if((nx<0)||(nx>pagew))df=0.10}
if(self.speedy){ny=Math.floor(self.lastscrolly-(self.speedy*(1-self.demulxy)));self.lastscrolly=ny;if((ny<0)||(ny>pageh))df=0.10}
self.demulxy=Math.min(1,self.demulxy+df);self.nc.synched("domomentum2d",function(){if(self.speedx){var scx=self.nc.getScrollLeft();self.chkx=nx;self.nc.setScrollLeft(nx)}
if(self.speedy){var scy=self.nc.getScrollTop();self.chky=ny;self.nc.setScrollTop(ny)}
if(!self.timer){self.nc.hideCursor();self.doSnapy(nx,ny)}});if(self.demulxy<1){self.timer=setTimeout(onscroll,tm)}else{self.stop();self.nc.hideCursor();self.doSnapy(nx,ny)}};onscroll()}else{self.doSnapy(self.nc.getScrollLeft(),self.nc.getScrollTop())}}};var _scrollTop=jQuery.fn.scrollTop;jQuery.cssHooks.pageYOffset={get:function(elem,computed,extra){var nice=$.data(elem,'__nicescroll')||!1;return(nice&&nice.ishwscroll)?nice.getScrollTop():_scrollTop.call(elem)},set:function(elem,value){var nice=$.data(elem,'__nicescroll')||!1;(nice&&nice.ishwscroll)?nice.setScrollTop(parseInt(value)):_scrollTop.call(elem,value);return this}};jQuery.fn.scrollTop=function(value){if(value===undefined){var nice=(this[0])?$.data(this[0],'__nicescroll')||!1:!1;return(nice&&nice.ishwscroll)?nice.getScrollTop():_scrollTop.call(this)}else{return this.each(function(){var nice=$.data(this,'__nicescroll')||!1;(nice&&nice.ishwscroll)?nice.setScrollTop(parseInt(value)):_scrollTop.call($(this),value)})}};var _scrollLeft=jQuery.fn.scrollLeft;$.cssHooks.pageXOffset={get:function(elem,computed,extra){var nice=$.data(elem,'__nicescroll')||!1;return(nice&&nice.ishwscroll)?nice.getScrollLeft():_scrollLeft.call(elem)},set:function(elem,value){var nice=$.data(elem,'__nicescroll')||!1;(nice&&nice.ishwscroll)?nice.setScrollLeft(parseInt(value)):_scrollLeft.call(elem,value);return this}};jQuery.fn.scrollLeft=function(value){if(value===undefined){var nice=(this[0])?$.data(this[0],'__nicescroll')||!1:!1;return(nice&&nice.ishwscroll)?nice.getScrollLeft():_scrollLeft.call(this)}else{return this.each(function(){var nice=$.data(this,'__nicescroll')||!1;(nice&&nice.ishwscroll)?nice.setScrollLeft(parseInt(value)):_scrollLeft.call($(this),value)})}};var NiceScrollArray=function(doms){var self=this;this.length=0;this.name="nicescrollarray";this.each=function(fn){$.each(self,fn);return self};this.push=function(nice){self[self.length]=nice;self.length++};this.eq=function(idx){return self[idx]};if(doms){for(var a=0;a<doms.length;a++){var nice=$.data(doms[a],'__nicescroll')||!1;if(nice){this[this.length]=nice;this.length++}}}
return this};function mplex(el,lst,fn){for(var a=0;a<lst.length;a++)fn(el,lst[a]);}
mplex(NiceScrollArray.prototype,['show','hide','toggle','onResize','resize','remove','stop','doScrollPos'],function(e,n){e[n]=function(){var args=arguments;return this.each(function(){this[n].apply(this,args)})}});jQuery.fn.getNiceScroll=function(index){if(index===undefined){return new NiceScrollArray(this)}else{return this[index]&&$.data(this[index],'__nicescroll')||!1}};jQuery.expr[':'].nicescroll=function(a){return $.data(a,'__nicescroll')!==undefined};$.fn.niceScroll=function(wrapper,opt){if(opt===undefined&&typeof wrapper=="object"&&!("jquery" in wrapper)){opt=wrapper;wrapper=!1}
opt=$.extend({},opt);var ret=new NiceScrollArray();if(opt===undefined)opt={};if(wrapper||!1){opt.doc=$(wrapper);opt.win=$(this)}
var docundef=!("doc" in opt);if(!docundef&&!("win" in opt))opt.win=$(this);this.each(function(){var nice=$(this).data('__nicescroll')||!1;if(!nice){opt.doc=(docundef)?$(this):opt.doc;nice=new NiceScrollClass(opt,$(this));$(this).data('__nicescroll',nice)}
ret.push(nice)});return(ret.length==1)?ret[0]:ret};window.NiceScroll={getjQuery:function(){return jQuery}};if(!$.nicescroll){$.nicescroll=new NiceScrollArray();$.nicescroll.options=_globaloptions}}));if(typeof jQuery==='undefined'){throw new Error('Bootstrap\'s JavaScript requires jQuery')}+function($){'use strict';var version=$.fn.jquery.split(' ')[0].split('.')
if((version[0]<2&&version[1]<9)||(version[0]==1&&version[1]==9&&version[2]<1)||(version[0]>3)){throw new Error('Bootstrap\'s JavaScript requires jQuery version 1.9.1 or higher, but lower than version 4')}}(jQuery);+function($){'use strict';function transitionEnd(){var el=document.createElement('bootstrap')
var transEndEventNames={WebkitTransition:'webkitTransitionEnd',MozTransition:'transitionend',OTransition:'oTransitionEnd otransitionend',transition:'transitionend'}
for(var name in transEndEventNames){if(el.style[name]!==undefined){return{end:transEndEventNames[name]}}}
return !1}
$.fn.emulateTransitionEnd=function(duration){var called=!1
var $el=this
$(this).one('bsTransitionEnd',function(){called=!0})
var callback=function(){if(!called)$($el).trigger($.support.transition.end)}
setTimeout(callback,duration)
return this}
$(function(){$.support.transition=transitionEnd()
if(!$.support.transition)return
$.event.special.bsTransitionEnd={bindType:$.support.transition.end,delegateType:$.support.transition.end,handle:function(e){if($(e.target).is(this))return e.handleObj.handler.apply(this,arguments)}}})}(jQuery);+function($){'use strict';var dismiss='[data-dismiss="alert"]'
var Alert=function(el){$(el).on('click',dismiss,this.close)}
Alert.VERSION='3.3.7'
Alert.TRANSITION_DURATION=150
Alert.prototype.close=function(e){var $this=$(this)
var selector=$this.attr('data-target')
if(!selector){selector=$this.attr('href')
selector=selector&&selector.replace(/.*(?=#[^\s]*$)/,'')}
var $parent=$(selector==='#'?[]:selector)
if(e)e.preventDefault()
if(!$parent.length){$parent=$this.closest('.alert')}
$parent.trigger(e=$.Event('close.bs.alert'))
if(e.isDefaultPrevented())return
$parent.removeClass('in')
function removeElement(){$parent.detach().trigger('closed.bs.alert').remove()}
$.support.transition&&$parent.hasClass('fade')?$parent.one('bsTransitionEnd',removeElement).emulateTransitionEnd(Alert.TRANSITION_DURATION):removeElement()}
function Plugin(option){return this.each(function(){var $this=$(this)
var data=$this.data('bs.alert')
if(!data)$this.data('bs.alert',(data=new Alert(this)))
if(typeof option=='string')data[option].call($this)})}
var old=$.fn.alert
$.fn.alert=Plugin
$.fn.alert.Constructor=Alert
$.fn.alert.noConflict=function(){$.fn.alert=old
return this}
$(document).on('click.bs.alert.data-api',dismiss,Alert.prototype.close)}(jQuery);+function($){'use strict';var Button=function(element,options){this.$element=$(element)
this.options=$.extend({},Button.DEFAULTS,options)
this.isLoading=!1}
Button.VERSION='3.3.7'
Button.DEFAULTS={loadingText:'loading...'}
Button.prototype.setState=function(state){var d='disabled'
var $el=this.$element
var val=$el.is('input')?'val':'html'
var data=$el.data()
state+='Text'
if(data.resetText==null)$el.data('resetText',$el[val]())
setTimeout($.proxy(function(){$el[val](data[state]==null?this.options[state]:data[state])
if(state=='loadingText'){this.isLoading=!0
$el.addClass(d).attr(d,d).prop(d,!0)}else if(this.isLoading){this.isLoading=!1
$el.removeClass(d).removeAttr(d).prop(d,!1)}},this),0)}
Button.prototype.toggle=function(){var changed=!0
var $parent=this.$element.closest('[data-toggle="buttons"]')
if($parent.length){var $input=this.$element.find('input')
if($input.prop('type')=='radio'){if($input.prop('checked'))changed=!1
$parent.find('.active').removeClass('active')
this.$element.addClass('active')}else if($input.prop('type')=='checkbox'){if(($input.prop('checked'))!==this.$element.hasClass('active'))changed=!1
this.$element.toggleClass('active')}
$input.prop('checked',this.$element.hasClass('active'))
if(changed)$input.trigger('change')}else{this.$element.attr('aria-pressed',!this.$element.hasClass('active'))
this.$element.toggleClass('active')}}
function Plugin(option){return this.each(function(){var $this=$(this)
var data=$this.data('bs.button')
var options=typeof option=='object'&&option
if(!data)$this.data('bs.button',(data=new Button(this,options)))
if(option=='toggle')data.toggle()
else if(option)data.setState(option)})}
var old=$.fn.button
$.fn.button=Plugin
$.fn.button.Constructor=Button
$.fn.button.noConflict=function(){$.fn.button=old
return this}
$(document).on('click.bs.button.data-api','[data-toggle^="button"]',function(e){var $btn=$(e.target).closest('.btn')
Plugin.call($btn,'toggle')
if(!($(e.target).is('input[type="radio"], input[type="checkbox"]'))){e.preventDefault()
if($btn.is('input,button'))$btn.trigger('focus')
else $btn.find('input:visible,button:visible').first().trigger('focus')}}).on('focus.bs.button.data-api blur.bs.button.data-api','[data-toggle^="button"]',function(e){$(e.target).closest('.btn').toggleClass('focus',/^focus(in)?$/.test(e.type))})}(jQuery);+function($){'use strict';var Carousel=function(element,options){this.$element=$(element)
this.$indicators=this.$element.find('.carousel-indicators')
this.options=options
this.paused=null
this.sliding=null
this.interval=null
this.$active=null
this.$items=null
this.options.keyboard&&this.$element.on('keydown.bs.carousel',$.proxy(this.keydown,this))
this.options.pause=='hover'&&!('ontouchstart' in document.documentElement)&&this.$element.on('mouseenter.bs.carousel',$.proxy(this.pause,this)).on('mouseleave.bs.carousel',$.proxy(this.cycle,this))}
Carousel.VERSION='3.3.7'
Carousel.TRANSITION_DURATION=600
Carousel.DEFAULTS={interval:5000,pause:'hover',wrap:!0,keyboard:!0}
Carousel.prototype.keydown=function(e){if(/input|textarea/i.test(e.target.tagName))return
switch(e.which){case 37:this.prev();break
case 39:this.next();break
default:return}
e.preventDefault()}
Carousel.prototype.cycle=function(e){e||(this.paused=!1)
this.interval&&clearInterval(this.interval)
this.options.interval&&!this.paused&&(this.interval=setInterval($.proxy(this.next,this),this.options.interval))
return this}
Carousel.prototype.getItemIndex=function(item){this.$items=item.parent().children('.item')
return this.$items.index(item||this.$active)}
Carousel.prototype.getItemForDirection=function(direction,active){var activeIndex=this.getItemIndex(active)
var willWrap=(direction=='prev'&&activeIndex===0)||(direction=='next'&&activeIndex==(this.$items.length-1))
if(willWrap&&!this.options.wrap)return active
var delta=direction=='prev'?-1:1
var itemIndex=(activeIndex+delta)%this.$items.length
return this.$items.eq(itemIndex)}
Carousel.prototype.to=function(pos){var that=this
var activeIndex=this.getItemIndex(this.$active=this.$element.find('.item.active'))
if(pos>(this.$items.length-1)||pos<0)return
if(this.sliding)return this.$element.one('slid.bs.carousel',function(){that.to(pos)})
if(activeIndex==pos)return this.pause().cycle()
return this.slide(pos>activeIndex?'next':'prev',this.$items.eq(pos))}
Carousel.prototype.pause=function(e){e||(this.paused=!0)
if(this.$element.find('.next, .prev').length&&$.support.transition){this.$element.trigger($.support.transition.end)
this.cycle(!0)}
this.interval=clearInterval(this.interval)
return this}
Carousel.prototype.next=function(){if(this.sliding)return
return this.slide('next')}
Carousel.prototype.prev=function(){if(this.sliding)return
return this.slide('prev')}
Carousel.prototype.slide=function(type,next){var $active=this.$element.find('.item.active')
var $next=next||this.getItemForDirection(type,$active)
var isCycling=this.interval
var direction=type=='next'?'left':'right'
var that=this
if($next.hasClass('active'))return(this.sliding=!1)
var relatedTarget=$next[0]
var slideEvent=$.Event('slide.bs.carousel',{relatedTarget:relatedTarget,direction:direction})
this.$element.trigger(slideEvent)
if(slideEvent.isDefaultPrevented())return
this.sliding=!0
isCycling&&this.pause()
if(this.$indicators.length){this.$indicators.find('.active').removeClass('active')
var $nextIndicator=$(this.$indicators.children()[this.getItemIndex($next)])
$nextIndicator&&$nextIndicator.addClass('active')}
var slidEvent=$.Event('slid.bs.carousel',{relatedTarget:relatedTarget,direction:direction})
if($.support.transition&&this.$element.hasClass('slide')){$next.addClass(type)
$next[0].offsetWidth
$active.addClass(direction)
$next.addClass(direction)
$active.one('bsTransitionEnd',function(){$next.removeClass([type,direction].join(' ')).addClass('active')
$active.removeClass(['active',direction].join(' '))
that.sliding=!1
setTimeout(function(){that.$element.trigger(slidEvent)},0)}).emulateTransitionEnd(Carousel.TRANSITION_DURATION)}else{$active.removeClass('active')
$next.addClass('active')
this.sliding=!1
this.$element.trigger(slidEvent)}
isCycling&&this.cycle()
return this}
function Plugin(option){return this.each(function(){var $this=$(this)
var data=$this.data('bs.carousel')
var options=$.extend({},Carousel.DEFAULTS,$this.data(),typeof option=='object'&&option)
var action=typeof option=='string'?option:options.slide
if(!data)$this.data('bs.carousel',(data=new Carousel(this,options)))
if(typeof option=='number')data.to(option)
else if(action)data[action]()
else if(options.interval)data.pause().cycle()})}
var old=$.fn.carousel
$.fn.carousel=Plugin
$.fn.carousel.Constructor=Carousel
$.fn.carousel.noConflict=function(){$.fn.carousel=old
return this}
var clickHandler=function(e){var href
var $this=$(this)
var $target=$($this.attr('data-target')||(href=$this.attr('href'))&&href.replace(/.*(?=#[^\s]+$)/,''))
if(!$target.hasClass('carousel'))return
var options=$.extend({},$target.data(),$this.data())
var slideIndex=$this.attr('data-slide-to')
if(slideIndex)options.interval=!1
Plugin.call($target,options)
if(slideIndex){$target.data('bs.carousel').to(slideIndex)}
e.preventDefault()}
$(document).on('click.bs.carousel.data-api','[data-slide]',clickHandler).on('click.bs.carousel.data-api','[data-slide-to]',clickHandler)
$(window).on('load',function(){$('[data-ride="carousel"]').each(function(){var $carousel=$(this)
Plugin.call($carousel,$carousel.data())})})}(jQuery);+function($){'use strict';var Collapse=function(element,options){this.$element=$(element)
this.options=$.extend({},Collapse.DEFAULTS,options)
this.$trigger=$('[data-toggle="collapse"][href="#'+element.id+'"],'+'[data-toggle="collapse"][data-target="#'+element.id+'"]')
this.transitioning=null
if(this.options.parent){this.$parent=this.getParent()}else{this.addAriaAndCollapsedClass(this.$element,this.$trigger)}
if(this.options.toggle)this.toggle()}
Collapse.VERSION='3.3.7'
Collapse.TRANSITION_DURATION=350
Collapse.DEFAULTS={toggle:!0}
Collapse.prototype.dimension=function(){var hasWidth=this.$element.hasClass('width')
return hasWidth?'width':'height'}
Collapse.prototype.show=function(){if(this.transitioning||this.$element.hasClass('in'))return
var activesData
var actives=this.$parent&&this.$parent.children('.panel').children('.in, .collapsing')
if(actives&&actives.length){activesData=actives.data('bs.collapse')
if(activesData&&activesData.transitioning)return}
var startEvent=$.Event('show.bs.collapse')
this.$element.trigger(startEvent)
if(startEvent.isDefaultPrevented())return
if(actives&&actives.length){Plugin.call(actives,'hide')
activesData||actives.data('bs.collapse',null)}
var dimension=this.dimension()
this.$element.removeClass('collapse').addClass('collapsing')[dimension](0).attr('aria-expanded',!0)
this.$trigger.removeClass('collapsed').attr('aria-expanded',!0)
this.transitioning=1
var complete=function(){this.$element.removeClass('collapsing').addClass('collapse in')[dimension]('')
this.transitioning=0
this.$element.trigger('shown.bs.collapse')}
if(!$.support.transition)return complete.call(this)
var scrollSize=$.camelCase(['scroll',dimension].join('-'))
this.$element.one('bsTransitionEnd',$.proxy(complete,this)).emulateTransitionEnd(Collapse.TRANSITION_DURATION)[dimension](this.$element[0][scrollSize])}
Collapse.prototype.hide=function(){if(this.transitioning||!this.$element.hasClass('in'))return
var startEvent=$.Event('hide.bs.collapse')
this.$element.trigger(startEvent)
if(startEvent.isDefaultPrevented())return
var dimension=this.dimension()
this.$element[dimension](this.$element[dimension]())[0].offsetHeight
this.$element.addClass('collapsing').removeClass('collapse in').attr('aria-expanded',!1)
this.$trigger.addClass('collapsed').attr('aria-expanded',!1)
this.transitioning=1
var complete=function(){this.transitioning=0
this.$element.removeClass('collapsing').addClass('collapse').trigger('hidden.bs.collapse')}
if(!$.support.transition)return complete.call(this)
this.$element[dimension](0).one('bsTransitionEnd',$.proxy(complete,this)).emulateTransitionEnd(Collapse.TRANSITION_DURATION)}
Collapse.prototype.toggle=function(){this[this.$element.hasClass('in')?'hide':'show']()}
Collapse.prototype.getParent=function(){return $(this.options.parent).find('[data-toggle="collapse"][data-parent="'+this.options.parent+'"]').each($.proxy(function(i,element){var $element=$(element)
this.addAriaAndCollapsedClass(getTargetFromTrigger($element),$element)},this)).end()}
Collapse.prototype.addAriaAndCollapsedClass=function($element,$trigger){var isOpen=$element.hasClass('in')
$element.attr('aria-expanded',isOpen)
$trigger.toggleClass('collapsed',!isOpen).attr('aria-expanded',isOpen)}
function getTargetFromTrigger($trigger){var href
var target=$trigger.attr('data-target')||(href=$trigger.attr('href'))&&href.replace(/.*(?=#[^\s]+$)/,'')
return $(target)}
function Plugin(option){return this.each(function(){var $this=$(this)
var data=$this.data('bs.collapse')
var options=$.extend({},Collapse.DEFAULTS,$this.data(),typeof option=='object'&&option)
if(!data&&options.toggle&&/show|hide/.test(option))options.toggle=!1
if(!data)$this.data('bs.collapse',(data=new Collapse(this,options)))
if(typeof option=='string')data[option]()})}
var old=$.fn.collapse
$.fn.collapse=Plugin
$.fn.collapse.Constructor=Collapse
$.fn.collapse.noConflict=function(){$.fn.collapse=old
return this}
$(document).on('click.bs.collapse.data-api','[data-toggle="collapse"]',function(e){var $this=$(this)
if(!$this.attr('data-target'))e.preventDefault()
var $target=getTargetFromTrigger($this)
var data=$target.data('bs.collapse')
var option=data?'toggle':$this.data()
Plugin.call($target,option)})}(jQuery);+function($){'use strict';var backdrop='.dropdown-backdrop'
var toggle='[data-toggle="dropdown"]'
var Dropdown=function(element){$(element).on('click.bs.dropdown',this.toggle)}
Dropdown.VERSION='3.3.7'
function getParent($this){var selector=$this.attr('data-target')
if(!selector){selector=$this.attr('href')
selector=selector&&/#[A-Za-z]/.test(selector)&&selector.replace(/.*(?=#[^\s]*$)/,'')}
var $parent=selector&&$(selector)
return $parent&&$parent.length?$parent:$this.parent()}
function clearMenus(e){if(e&&e.which===3)return
$(backdrop).remove()
$(toggle).each(function(){var $this=$(this)
var $parent=getParent($this)
var relatedTarget={relatedTarget:this}
if(!$parent.hasClass('open'))return
if(e&&e.type=='click'&&/input|textarea/i.test(e.target.tagName)&&$.contains($parent[0],e.target))return
$parent.trigger(e=$.Event('hide.bs.dropdown',relatedTarget))
if(e.isDefaultPrevented())return
$this.attr('aria-expanded','false')
$parent.removeClass('open').trigger($.Event('hidden.bs.dropdown',relatedTarget))})}
Dropdown.prototype.toggle=function(e){var $this=$(this)
if($this.is('.disabled, :disabled'))return
var $parent=getParent($this)
var isActive=$parent.hasClass('open')
clearMenus()
if(!isActive){if('ontouchstart' in document.documentElement&&!$parent.closest('.navbar-nav').length){$(document.createElement('div')).addClass('dropdown-backdrop').insertAfter($(this)).on('click',clearMenus)}
var relatedTarget={relatedTarget:this}
$parent.trigger(e=$.Event('show.bs.dropdown',relatedTarget))
if(e.isDefaultPrevented())return
$this.trigger('focus').attr('aria-expanded','true')
$parent.toggleClass('open').trigger($.Event('shown.bs.dropdown',relatedTarget))}
return !1}
Dropdown.prototype.keydown=function(e){if(!/(38|40|27|32)/.test(e.which)||/input|textarea/i.test(e.target.tagName))return
var $this=$(this)
e.preventDefault()
e.stopPropagation()
if($this.is('.disabled, :disabled'))return
var $parent=getParent($this)
var isActive=$parent.hasClass('open')
if(!isActive&&e.which!=27||isActive&&e.which==27){if(e.which==27)$parent.find(toggle).trigger('focus')
return $this.trigger('click')}
var desc=' li:not(.disabled):visible a'
var $items=$parent.find('.dropdown-menu'+desc)
if(!$items.length)return
var index=$items.index(e.target)
if(e.which==38&&index>0)index--
if(e.which==40&&index<$items.length-1)index++
if(!~index)index=0
$items.eq(index).trigger('focus')}
function Plugin(option){return this.each(function(){var $this=$(this)
var data=$this.data('bs.dropdown')
if(!data)$this.data('bs.dropdown',(data=new Dropdown(this)))
if(typeof option=='string')data[option].call($this)})}
var old=$.fn.dropdown
$.fn.dropdown=Plugin
$.fn.dropdown.Constructor=Dropdown
$.fn.dropdown.noConflict=function(){$.fn.dropdown=old
return this}
$(document).on('click.bs.dropdown.data-api',clearMenus).on('click.bs.dropdown.data-api','.dropdown form',function(e){e.stopPropagation()}).on('click.bs.dropdown.data-api',toggle,Dropdown.prototype.toggle).on('keydown.bs.dropdown.data-api',toggle,Dropdown.prototype.keydown).on('keydown.bs.dropdown.data-api','.dropdown-menu',Dropdown.prototype.keydown)}(jQuery);+function($){'use strict';var Modal=function(element,options){this.options=options
this.$body=$(document.body)
this.$element=$(element)
this.$dialog=this.$element.find('.modal-dialog')
this.$backdrop=null
this.isShown=null
this.originalBodyPad=null
this.scrollbarWidth=0
this.ignoreBackdropClick=!1
if(this.options.remote){this.$element.find('.modal-content').load(this.options.remote,$.proxy(function(){this.$element.trigger('loaded.bs.modal')},this))}}
Modal.VERSION='3.3.7'
Modal.TRANSITION_DURATION=300
Modal.BACKDROP_TRANSITION_DURATION=150
Modal.DEFAULTS={backdrop:!0,keyboard:!0,show:!0}
Modal.prototype.toggle=function(_relatedTarget){return this.isShown?this.hide():this.show(_relatedTarget)}
Modal.prototype.show=function(_relatedTarget){var that=this
var e=$.Event('show.bs.modal',{relatedTarget:_relatedTarget})
this.$element.trigger(e)
if(this.isShown||e.isDefaultPrevented())return
this.isShown=!0
this.checkScrollbar()
this.setScrollbar()
this.$body.addClass('modal-open')
this.escape()
this.resize()
this.$element.on('click.dismiss.bs.modal','[data-dismiss="modal"]',$.proxy(this.hide,this))
this.$dialog.on('mousedown.dismiss.bs.modal',function(){that.$element.one('mouseup.dismiss.bs.modal',function(e){if($(e.target).is(that.$element))that.ignoreBackdropClick=!0})})
this.backdrop(function(){var transition=$.support.transition&&that.$element.hasClass('fade')
if(!that.$element.parent().length){that.$element.appendTo(that.$body)}
that.$element.show().scrollTop(0)
that.adjustDialog()
if(transition){that.$element[0].offsetWidth}
that.$element.addClass('in')
that.enforceFocus()
var e=$.Event('shown.bs.modal',{relatedTarget:_relatedTarget})
transition?that.$dialog.one('bsTransitionEnd',function(){that.$element.trigger('focus').trigger(e)}).emulateTransitionEnd(Modal.TRANSITION_DURATION):that.$element.trigger('focus').trigger(e)})}
Modal.prototype.hide=function(e){if(e)e.preventDefault()
e=$.Event('hide.bs.modal')
this.$element.trigger(e)
if(!this.isShown||e.isDefaultPrevented())return
this.isShown=!1
this.escape()
this.resize()
$(document).off('focusin.bs.modal')
this.$element.removeClass('in').off('click.dismiss.bs.modal').off('mouseup.dismiss.bs.modal')
this.$dialog.off('mousedown.dismiss.bs.modal')
$.support.transition&&this.$element.hasClass('fade')?this.$element.one('bsTransitionEnd',$.proxy(this.hideModal,this)).emulateTransitionEnd(Modal.TRANSITION_DURATION):this.hideModal()}
Modal.prototype.enforceFocus=function(){$(document).off('focusin.bs.modal').on('focusin.bs.modal',$.proxy(function(e){if(document!==e.target&&this.$element[0]!==e.target&&!this.$element.has(e.target).length){this.$element.trigger('focus')}},this))}
Modal.prototype.escape=function(){if(this.isShown&&this.options.keyboard){this.$element.on('keydown.dismiss.bs.modal',$.proxy(function(e){e.which==27&&this.hide()},this))}else if(!this.isShown){this.$element.off('keydown.dismiss.bs.modal')}}
Modal.prototype.resize=function(){if(this.isShown){$(window).on('resize.bs.modal',$.proxy(this.handleUpdate,this))}else{$(window).off('resize.bs.modal')}}
Modal.prototype.hideModal=function(){var that=this
this.$element.hide()
this.backdrop(function(){that.$body.removeClass('modal-open')
that.resetAdjustments()
that.resetScrollbar()
that.$element.trigger('hidden.bs.modal')})}
Modal.prototype.removeBackdrop=function(){this.$backdrop&&this.$backdrop.remove()
this.$backdrop=null}
Modal.prototype.backdrop=function(callback){var that=this
var animate=this.$element.hasClass('fade')?'fade':''
if(this.isShown&&this.options.backdrop){var doAnimate=$.support.transition&&animate
this.$backdrop=$(document.createElement('div')).addClass('modal-backdrop '+animate).appendTo(this.$body)
this.$element.on('click.dismiss.bs.modal',$.proxy(function(e){if(this.ignoreBackdropClick){this.ignoreBackdropClick=!1
return}
if(e.target!==e.currentTarget)return
this.options.backdrop=='static'?this.$element[0].focus():this.hide()},this))
if(doAnimate)this.$backdrop[0].offsetWidth
this.$backdrop.addClass('in')
if(!callback)return
doAnimate?this.$backdrop.one('bsTransitionEnd',callback).emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION):callback()}else if(!this.isShown&&this.$backdrop){this.$backdrop.removeClass('in')
var callbackRemove=function(){that.removeBackdrop()
callback&&callback()}
$.support.transition&&this.$element.hasClass('fade')?this.$backdrop.one('bsTransitionEnd',callbackRemove).emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION):callbackRemove()}else if(callback){callback()}}
Modal.prototype.handleUpdate=function(){this.adjustDialog()}
Modal.prototype.adjustDialog=function(){var modalIsOverflowing=this.$element[0].scrollHeight>document.documentElement.clientHeight
this.$element.css({paddingLeft:!this.bodyIsOverflowing&&modalIsOverflowing?this.scrollbarWidth:'',paddingRight:this.bodyIsOverflowing&&!modalIsOverflowing?this.scrollbarWidth:''})}
Modal.prototype.resetAdjustments=function(){this.$element.css({paddingLeft:'',paddingRight:''})}
Modal.prototype.checkScrollbar=function(){var fullWindowWidth=window.innerWidth
if(!fullWindowWidth){var documentElementRect=document.documentElement.getBoundingClientRect()
fullWindowWidth=documentElementRect.right-Math.abs(documentElementRect.left)}
this.bodyIsOverflowing=document.body.clientWidth<fullWindowWidth
this.scrollbarWidth=this.measureScrollbar()}
Modal.prototype.setScrollbar=function(){var bodyPad=parseInt((this.$body.css('padding-right')||0),10)
this.originalBodyPad=document.body.style.paddingRight||''
if(this.bodyIsOverflowing)this.$body.css('padding-right',bodyPad+this.scrollbarWidth)}
Modal.prototype.resetScrollbar=function(){this.$body.css('padding-right',this.originalBodyPad)}
Modal.prototype.measureScrollbar=function(){var scrollDiv=document.createElement('div')
scrollDiv.className='modal-scrollbar-measure'
this.$body.append(scrollDiv)
var scrollbarWidth=scrollDiv.offsetWidth-scrollDiv.clientWidth
this.$body[0].removeChild(scrollDiv)
return scrollbarWidth}
function Plugin(option,_relatedTarget){return this.each(function(){var $this=$(this)
var data=$this.data('bs.modal')
var options=$.extend({},Modal.DEFAULTS,$this.data(),typeof option=='object'&&option)
if(!data)$this.data('bs.modal',(data=new Modal(this,options)))
if(typeof option=='string')data[option](_relatedTarget)
else if(options.show)data.show(_relatedTarget)})}
var old=$.fn.modal
$.fn.modal=Plugin
$.fn.modal.Constructor=Modal
$.fn.modal.noConflict=function(){$.fn.modal=old
return this}
$(document).on('click.bs.modal.data-api','[data-toggle="modal"]',function(e){var $this=$(this)
var href=$this.attr('href')
var $target=$($this.attr('data-target')||(href&&href.replace(/.*(?=#[^\s]+$)/,'')))
var option=$target.data('bs.modal')?'toggle':$.extend({remote:!/#/.test(href)&&href},$target.data(),$this.data())
if($this.is('a'))e.preventDefault()
$target.one('show.bs.modal',function(showEvent){if(showEvent.isDefaultPrevented())return
$target.one('hidden.bs.modal',function(){$this.is(':visible')&&$this.trigger('focus')})})
Plugin.call($target,option,this)})}(jQuery);+function($){'use strict';var Tooltip=function(element,options){this.type=null
this.options=null
this.enabled=null
this.timeout=null
this.hoverState=null
this.$element=null
this.inState=null
this.init('tooltip',element,options)}
Tooltip.VERSION='3.3.7'
Tooltip.TRANSITION_DURATION=150
Tooltip.DEFAULTS={animation:!0,placement:'top',selector:!1,template:'<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',trigger:'hover focus',title:'',delay:0,html:!1,container:!1,viewport:{selector:'body',padding:0}}
Tooltip.prototype.init=function(type,element,options){this.enabled=!0
this.type=type
this.$element=$(element)
this.options=this.getOptions(options)
this.$viewport=this.options.viewport&&$($.isFunction(this.options.viewport)?this.options.viewport.call(this,this.$element):(this.options.viewport.selector||this.options.viewport))
this.inState={click:!1,hover:!1,focus:!1}
if(this.$element[0]instanceof document.constructor&&!this.options.selector){throw new Error('`selector` option must be specified when initializing '+this.type+' on the window.document object!')}
var triggers=this.options.trigger.split(' ')
for(var i=triggers.length;i--;){var trigger=triggers[i]
if(trigger=='click'){this.$element.on('click.'+this.type,this.options.selector,$.proxy(this.toggle,this))}else if(trigger!='manual'){var eventIn=trigger=='hover'?'mouseenter':'focusin'
var eventOut=trigger=='hover'?'mouseleave':'focusout'
this.$element.on(eventIn+'.'+this.type,this.options.selector,$.proxy(this.enter,this))
this.$element.on(eventOut+'.'+this.type,this.options.selector,$.proxy(this.leave,this))}}
this.options.selector?(this._options=$.extend({},this.options,{trigger:'manual',selector:''})):this.fixTitle()}
Tooltip.prototype.getDefaults=function(){return Tooltip.DEFAULTS}
Tooltip.prototype.getOptions=function(options){options=$.extend({},this.getDefaults(),this.$element.data(),options)
if(options.delay&&typeof options.delay=='number'){options.delay={show:options.delay,hide:options.delay}}
return options}
Tooltip.prototype.getDelegateOptions=function(){var options={}
var defaults=this.getDefaults()
this._options&&$.each(this._options,function(key,value){if(defaults[key]!=value)options[key]=value})
return options}
Tooltip.prototype.enter=function(obj){var self=obj instanceof this.constructor?obj:$(obj.currentTarget).data('bs.'+this.type)
if(!self){self=new this.constructor(obj.currentTarget,this.getDelegateOptions())
$(obj.currentTarget).data('bs.'+this.type,self)}
if(obj instanceof $.Event){self.inState[obj.type=='focusin'?'focus':'hover']=!0}
if(self.tip().hasClass('in')||self.hoverState=='in'){self.hoverState='in'
return}
clearTimeout(self.timeout)
self.hoverState='in'
if(!self.options.delay||!self.options.delay.show)return self.show()
self.timeout=setTimeout(function(){if(self.hoverState=='in')self.show()},self.options.delay.show)}
Tooltip.prototype.isInStateTrue=function(){for(var key in this.inState){if(this.inState[key])return !0}
return !1}
Tooltip.prototype.leave=function(obj){var self=obj instanceof this.constructor?obj:$(obj.currentTarget).data('bs.'+this.type)
if(!self){self=new this.constructor(obj.currentTarget,this.getDelegateOptions())
$(obj.currentTarget).data('bs.'+this.type,self)}
if(obj instanceof $.Event){self.inState[obj.type=='focusout'?'focus':'hover']=!1}
if(self.isInStateTrue())return
clearTimeout(self.timeout)
self.hoverState='out'
if(!self.options.delay||!self.options.delay.hide)return self.hide()
self.timeout=setTimeout(function(){if(self.hoverState=='out')self.hide()},self.options.delay.hide)}
Tooltip.prototype.show=function(){var e=$.Event('show.bs.'+this.type)
if(this.hasContent()&&this.enabled){this.$element.trigger(e)
var inDom=$.contains(this.$element[0].ownerDocument.documentElement,this.$element[0])
if(e.isDefaultPrevented()||!inDom)return
var that=this
var $tip=this.tip()
var tipId=this.getUID(this.type)
this.setContent()
$tip.attr('id',tipId)
this.$element.attr('aria-describedby',tipId)
if(this.options.animation)$tip.addClass('fade')
var placement=typeof this.options.placement=='function'?this.options.placement.call(this,$tip[0],this.$element[0]):this.options.placement
var autoToken=/\s?auto?\s?/i
      var autoPlace=autoToken.test(placement)
if(autoPlace)placement=placement.replace(autoToken,'')||'top'
$tip.detach().css({top:0,left:0,display:'block'}).addClass(placement).data('bs.'+this.type,this)
this.options.container?$tip.appendTo(this.options.container):$tip.insertAfter(this.$element)
this.$element.trigger('inserted.bs.'+this.type)
var pos=this.getPosition()
var actualWidth=$tip[0].offsetWidth
var actualHeight=$tip[0].offsetHeight
if(autoPlace){var orgPlacement=placement
var viewportDim=this.getPosition(this.$viewport)
placement=placement=='bottom'&&pos.bottom+actualHeight>viewportDim.bottom?'top':placement=='top'&&pos.top-actualHeight<viewportDim.top?'bottom':placement=='right'&&pos.right+actualWidth>viewportDim.width?'left':placement=='left'&&pos.left-actualWidth<viewportDim.left?'right':placement
$tip.removeClass(orgPlacement).addClass(placement)}
var calculatedOffset=this.getCalculatedOffset(placement,pos,actualWidth,actualHeight)
this.applyPlacement(calculatedOffset,placement)
var complete=function(){var prevHoverState=that.hoverState
that.$element.trigger('shown.bs.'+that.type)
that.hoverState=null
if(prevHoverState=='out')that.leave(that)}
$.support.transition&&this.$tip.hasClass('fade')?$tip.one('bsTransitionEnd',complete).emulateTransitionEnd(Tooltip.TRANSITION_DURATION):complete()}}
Tooltip.prototype.applyPlacement=function(offset,placement){var $tip=this.tip()
var width=$tip[0].offsetWidth
var height=$tip[0].offsetHeight
var marginTop=parseInt($tip.css('margin-top'),10)
var marginLeft=parseInt($tip.css('margin-left'),10)
if(isNaN(marginTop))marginTop=0
if(isNaN(marginLeft))marginLeft=0
offset.top+=marginTop
offset.left+=marginLeft
$.offset.setOffset($tip[0],$.extend({using:function(props){$tip.css({top:Math.round(props.top),left:Math.round(props.left)})}},offset),0)
$tip.addClass('in')
var actualWidth=$tip[0].offsetWidth
var actualHeight=$tip[0].offsetHeight
if(placement=='top'&&actualHeight!=height){offset.top=offset.top+height-actualHeight}
var delta=this.getViewportAdjustedDelta(placement,offset,actualWidth,actualHeight)
if(delta.left)offset.left+=delta.left
else offset.top+=delta.top
var isVertical=/top|bottom/.test(placement)
var arrowDelta=isVertical?delta.left*2-width+actualWidth:delta.top*2-height+actualHeight
var arrowOffsetPosition=isVertical?'offsetWidth':'offsetHeight'
$tip.offset(offset)
this.replaceArrow(arrowDelta,$tip[0][arrowOffsetPosition],isVertical)}
Tooltip.prototype.replaceArrow=function(delta,dimension,isVertical){this.arrow().css(isVertical?'left':'top',50*(1-delta/dimension)+'%').css(isVertical?'top':'left','')}
Tooltip.prototype.setContent=function(){var $tip=this.tip()
var title=this.getTitle()
$tip.find('.tooltip-inner')[this.options.html?'html':'text'](title)
$tip.removeClass('fade in top bottom left right')}
Tooltip.prototype.hide=function(callback){var that=this
var $tip=$(this.$tip)
var e=$.Event('hide.bs.'+this.type)
function complete(){if(that.hoverState!='in')$tip.detach()
if(that.$element){that.$element.removeAttr('aria-describedby').trigger('hidden.bs.'+that.type)}
callback&&callback()}
this.$element.trigger(e)
if(e.isDefaultPrevented())return
$tip.removeClass('in')
$.support.transition&&$tip.hasClass('fade')?$tip.one('bsTransitionEnd',complete).emulateTransitionEnd(Tooltip.TRANSITION_DURATION):complete()
this.hoverState=null
return this}
Tooltip.prototype.fixTitle=function(){var $e=this.$element
if($e.attr('title')||typeof $e.attr('data-original-title')!='string'){$e.attr('data-original-title',$e.attr('title')||'').attr('title','')}}
Tooltip.prototype.hasContent=function(){return this.getTitle()}
Tooltip.prototype.getPosition=function($element){$element=$element||this.$element
var el=$element[0]
var isBody=el.tagName=='BODY'
var elRect=el.getBoundingClientRect()
if(elRect.width==null){elRect=$.extend({},elRect,{width:elRect.right-elRect.left,height:elRect.bottom-elRect.top})}
var isSvg=window.SVGElement&&el instanceof window.SVGElement
var elOffset=isBody?{top:0,left:0}:(isSvg?null:$element.offset())
var scroll={scroll:isBody?document.documentElement.scrollTop||document.body.scrollTop:$element.scrollTop()}
var outerDims=isBody?{width:$(window).width(),height:$(window).height()}:null
return $.extend({},elRect,scroll,outerDims,elOffset)}
Tooltip.prototype.getCalculatedOffset=function(placement,pos,actualWidth,actualHeight){return placement=='bottom'?{top:pos.top+pos.height,left:pos.left+pos.width/2-actualWidth/2}:placement=='top'?{top:pos.top-actualHeight,left:pos.left+pos.width/2-actualWidth/2}:placement=='left'?{top:pos.top+pos.height/2-actualHeight/2,left:pos.left-actualWidth}:{top:pos.top+pos.height/2-actualHeight/2,left:pos.left+pos.width}}
Tooltip.prototype.getViewportAdjustedDelta=function(placement,pos,actualWidth,actualHeight){var delta={top:0,left:0}
if(!this.$viewport)return delta
var viewportPadding=this.options.viewport&&this.options.viewport.padding||0
var viewportDimensions=this.getPosition(this.$viewport)
if(/right|left/.test(placement)){var topEdgeOffset=pos.top-viewportPadding-viewportDimensions.scroll
var bottomEdgeOffset=pos.top+viewportPadding-viewportDimensions.scroll+actualHeight
if(topEdgeOffset<viewportDimensions.top){delta.top=viewportDimensions.top-topEdgeOffset}else if(bottomEdgeOffset>viewportDimensions.top+viewportDimensions.height){delta.top=viewportDimensions.top+viewportDimensions.height-bottomEdgeOffset}}else{var leftEdgeOffset=pos.left-viewportPadding
var rightEdgeOffset=pos.left+viewportPadding+actualWidth
if(leftEdgeOffset<viewportDimensions.left){delta.left=viewportDimensions.left-leftEdgeOffset}else if(rightEdgeOffset>viewportDimensions.right){delta.left=viewportDimensions.left+viewportDimensions.width-rightEdgeOffset}}
return delta}
Tooltip.prototype.getTitle=function(){var title
var $e=this.$element
var o=this.options
title=$e.attr('data-original-title')||(typeof o.title=='function'?o.title.call($e[0]):o.title)
return title}
Tooltip.prototype.getUID=function(prefix){do prefix+=~~(Math.random()*1000000)
while(document.getElementById(prefix))
return prefix}
Tooltip.prototype.tip=function(){if(!this.$tip){this.$tip=$(this.options.template)
if(this.$tip.length!=1){throw new Error(this.type+' `template` option must consist of exactly 1 top-level element!')}}
return this.$tip}
Tooltip.prototype.arrow=function(){return(this.$arrow=this.$arrow||this.tip().find('.tooltip-arrow'))}
Tooltip.prototype.enable=function(){this.enabled=!0}
Tooltip.prototype.disable=function(){this.enabled=!1}
Tooltip.prototype.toggleEnabled=function(){this.enabled=!this.enabled}
Tooltip.prototype.toggle=function(e){var self=this
if(e){self=$(e.currentTarget).data('bs.'+this.type)
if(!self){self=new this.constructor(e.currentTarget,this.getDelegateOptions())
$(e.currentTarget).data('bs.'+this.type,self)}}
if(e){self.inState.click=!self.inState.click
if(self.isInStateTrue())self.enter(self)
else self.leave(self)}else{self.tip().hasClass('in')?self.leave(self):self.enter(self)}}
Tooltip.prototype.destroy=function(){var that=this
clearTimeout(this.timeout)
this.hide(function(){that.$element.off('.'+that.type).removeData('bs.'+that.type)
if(that.$tip){that.$tip.detach()}
that.$tip=null
that.$arrow=null
that.$viewport=null
that.$element=null})}
function Plugin(option){return this.each(function(){var $this=$(this)
var data=$this.data('bs.tooltip')
var options=typeof option=='object'&&option
if(!data&&/destroy|hide/.test(option))return
if(!data)$this.data('bs.tooltip',(data=new Tooltip(this,options)))
if(typeof option=='string')data[option]()})}
var old=$.fn.tooltip
$.fn.tooltip=Plugin
$.fn.tooltip.Constructor=Tooltip
$.fn.tooltip.noConflict=function(){$.fn.tooltip=old
return this}}(jQuery);+function($){'use strict';var Popover=function(element,options){this.init('popover',element,options)}
if(!$.fn.tooltip)throw new Error('Popover requires tooltip.js')
Popover.VERSION='3.3.7'
Popover.DEFAULTS=$.extend({},$.fn.tooltip.Constructor.DEFAULTS,{placement:'right',trigger:'click',content:'',template:'<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'})
Popover.prototype=$.extend({},$.fn.tooltip.Constructor.prototype)
Popover.prototype.constructor=Popover
Popover.prototype.getDefaults=function(){return Popover.DEFAULTS}
Popover.prototype.setContent=function(){var $tip=this.tip()
var title=this.getTitle()
var content=this.getContent()
$tip.find('.popover-title')[this.options.html?'html':'text'](title)
$tip.find('.popover-content').children().detach().end()[this.options.html?(typeof content=='string'?'html':'append'):'text'](content)
$tip.removeClass('fade top bottom left right in')
if(!$tip.find('.popover-title').html())$tip.find('.popover-title').hide()}
Popover.prototype.hasContent=function(){return this.getTitle()||this.getContent()}
Popover.prototype.getContent=function(){var $e=this.$element
var o=this.options
return $e.attr('data-content')||(typeof o.content=='function'?o.content.call($e[0]):o.content)}
Popover.prototype.arrow=function(){return(this.$arrow=this.$arrow||this.tip().find('.arrow'))}
function Plugin(option){return this.each(function(){var $this=$(this)
var data=$this.data('bs.popover')
var options=typeof option=='object'&&option
if(!data&&/destroy|hide/.test(option))return
if(!data)$this.data('bs.popover',(data=new Popover(this,options)))
if(typeof option=='string')data[option]()})}
var old=$.fn.popover
$.fn.popover=Plugin
$.fn.popover.Constructor=Popover
$.fn.popover.noConflict=function(){$.fn.popover=old
return this}}(jQuery);+function($){'use strict';function ScrollSpy(element,options){this.$body=$(document.body)
this.$scrollElement=$(element).is(document.body)?$(window):$(element)
this.options=$.extend({},ScrollSpy.DEFAULTS,options)
this.selector=(this.options.target||'')+' .nav li > a'
this.offsets=[]
this.targets=[]
this.activeTarget=null
this.scrollHeight=0
this.$scrollElement.on('scroll.bs.scrollspy',$.proxy(this.process,this))
this.refresh()
this.process()}
ScrollSpy.VERSION='3.3.7'
ScrollSpy.DEFAULTS={offset:10}
ScrollSpy.prototype.getScrollHeight=function(){return this.$scrollElement[0].scrollHeight||Math.max(this.$body[0].scrollHeight,document.documentElement.scrollHeight)}
ScrollSpy.prototype.refresh=function(){var that=this
var offsetMethod='offset'
var offsetBase=0
this.offsets=[]
this.targets=[]
this.scrollHeight=this.getScrollHeight()
if(!$.isWindow(this.$scrollElement[0])){offsetMethod='position'
offsetBase=this.$scrollElement.scrollTop()}
this.$body.find(this.selector).map(function(){var $el=$(this)
var href=$el.data('target')||$el.attr('href')
var $href=/^#./.test(href)&&$(href)
return($href&&$href.length&&$href.is(':visible')&&[[$href[offsetMethod]().top+offsetBase,href]])||null}).sort(function(a,b){return a[0]-b[0]}).each(function(){that.offsets.push(this[0])
that.targets.push(this[1])})}
ScrollSpy.prototype.process=function(){var scrollTop=this.$scrollElement.scrollTop()+this.options.offset
var scrollHeight=this.getScrollHeight()
var maxScroll=this.options.offset+scrollHeight-this.$scrollElement.height()
var offsets=this.offsets
var targets=this.targets
var activeTarget=this.activeTarget
var i
if(this.scrollHeight!=scrollHeight){this.refresh()}
if(scrollTop>=maxScroll){return activeTarget!=(i=targets[targets.length-1])&&this.activate(i)}
if(activeTarget&&scrollTop<offsets[0]){this.activeTarget=null
return this.clear()}
for(i=offsets.length;i--;){activeTarget!=targets[i]&&scrollTop>=offsets[i]&&(offsets[i+1]===undefined||scrollTop<offsets[i+1])&&this.activate(targets[i])}}
ScrollSpy.prototype.activate=function(target){this.activeTarget=target
this.clear()
var selector=this.selector+'[data-target="'+target+'"],'+this.selector+'[href="'+target+'"]'
var active=$(selector).parents('li').addClass('active')
if(active.parent('.dropdown-menu').length){active=active.closest('li.dropdown').addClass('active')}
active.trigger('activate.bs.scrollspy')}
ScrollSpy.prototype.clear=function(){$(this.selector).parentsUntil(this.options.target,'.active').removeClass('active')}
function Plugin(option){return this.each(function(){var $this=$(this)
var data=$this.data('bs.scrollspy')
var options=typeof option=='object'&&option
if(!data)$this.data('bs.scrollspy',(data=new ScrollSpy(this,options)))
if(typeof option=='string')data[option]()})}
var old=$.fn.scrollspy
$.fn.scrollspy=Plugin
$.fn.scrollspy.Constructor=ScrollSpy
$.fn.scrollspy.noConflict=function(){$.fn.scrollspy=old
return this}
$(window).on('load.bs.scrollspy.data-api',function(){$('[data-spy="scroll"]').each(function(){var $spy=$(this)
Plugin.call($spy,$spy.data())})})}(jQuery);+function($){'use strict';var Tab=function(element){this.element=$(element)}
Tab.VERSION='3.3.7'
Tab.TRANSITION_DURATION=150
Tab.prototype.show=function(){var $this=this.element
var $ul=$this.closest('ul:not(.dropdown-menu)')
var selector=$this.data('target')
if(!selector){selector=$this.attr('href')
selector=selector&&selector.replace(/.*(?=#[^\s]*$)/,'')}
if($this.parent('li').hasClass('active'))return
var $previous=$ul.find('.active:last a')
var hideEvent=$.Event('hide.bs.tab',{relatedTarget:$this[0]})
var showEvent=$.Event('show.bs.tab',{relatedTarget:$previous[0]})
$previous.trigger(hideEvent)
$this.trigger(showEvent)
if(showEvent.isDefaultPrevented()||hideEvent.isDefaultPrevented())return
var $target=$(selector)
this.activate($this.closest('li'),$ul)
this.activate($target,$target.parent(),function(){$previous.trigger({type:'hidden.bs.tab',relatedTarget:$this[0]})
$this.trigger({type:'shown.bs.tab',relatedTarget:$previous[0]})})}
Tab.prototype.activate=function(element,container,callback){var $active=container.find('> .active')
var transition=callback&&$.support.transition&&($active.length&&$active.hasClass('fade')||!!container.find('> .fade').length)
function next(){$active.removeClass('active').find('> .dropdown-menu > .active').removeClass('active').end().find('[data-toggle="tab"]').attr('aria-expanded',!1)
element.addClass('active').find('[data-toggle="tab"]').attr('aria-expanded',!0)
if(transition){element[0].offsetWidth
element.addClass('in')}else{element.removeClass('fade')}
if(element.parent('.dropdown-menu').length){element.closest('li.dropdown').addClass('active').end().find('[data-toggle="tab"]').attr('aria-expanded',!0)}
callback&&callback()}
$active.length&&transition?$active.one('bsTransitionEnd',next).emulateTransitionEnd(Tab.TRANSITION_DURATION):next()
$active.removeClass('in')}
function Plugin(option){return this.each(function(){var $this=$(this)
var data=$this.data('bs.tab')
if(!data)$this.data('bs.tab',(data=new Tab(this)))
if(typeof option=='string')data[option]()})}
var old=$.fn.tab
$.fn.tab=Plugin
$.fn.tab.Constructor=Tab
$.fn.tab.noConflict=function(){$.fn.tab=old
return this}
var clickHandler=function(e){e.preventDefault()
Plugin.call($(this),'show')}
$(document).on('click.bs.tab.data-api','[data-toggle="tab"]',clickHandler).on('click.bs.tab.data-api','[data-toggle="pill"]',clickHandler)}(jQuery);+function($){'use strict';var Affix=function(element,options){this.options=$.extend({},Affix.DEFAULTS,options)
this.$target=$(this.options.target).on('scroll.bs.affix.data-api',$.proxy(this.checkPosition,this)).on('click.bs.affix.data-api',$.proxy(this.checkPositionWithEventLoop,this))
this.$element=$(element)
this.affixed=null
this.unpin=null
this.pinnedOffset=null
this.checkPosition()}
Affix.VERSION='3.3.7'
Affix.RESET='affix affix-top affix-bottom'
Affix.DEFAULTS={offset:0,target:window}
Affix.prototype.getState=function(scrollHeight,height,offsetTop,offsetBottom){var scrollTop=this.$target.scrollTop()
var position=this.$element.offset()
var targetHeight=this.$target.height()
if(offsetTop!=null&&this.affixed=='top')return scrollTop<offsetTop?'top':!1
if(this.affixed=='bottom'){if(offsetTop!=null)return(scrollTop+this.unpin<=position.top)?!1:'bottom'
return(scrollTop+targetHeight<=scrollHeight-offsetBottom)?!1:'bottom'}
var initializing=this.affixed==null
var colliderTop=initializing?scrollTop:position.top
var colliderHeight=initializing?targetHeight:height
if(offsetTop!=null&&scrollTop<=offsetTop)return 'top'
if(offsetBottom!=null&&(colliderTop+colliderHeight>=scrollHeight-offsetBottom))return 'bottom'
return !1}
Affix.prototype.getPinnedOffset=function(){if(this.pinnedOffset)return this.pinnedOffset
this.$element.removeClass(Affix.RESET).addClass('affix')
var scrollTop=this.$target.scrollTop()
var position=this.$element.offset()
return(this.pinnedOffset=position.top-scrollTop)}
Affix.prototype.checkPositionWithEventLoop=function(){setTimeout($.proxy(this.checkPosition,this),1)}
Affix.prototype.checkPosition=function(){if(!this.$element.is(':visible'))return
var height=this.$element.height()
var offset=this.options.offset
var offsetTop=offset.top
var offsetBottom=offset.bottom
var scrollHeight=Math.max($(document).height(),$(document.body).height())
if(typeof offset!='object')offsetBottom=offsetTop=offset
if(typeof offsetTop=='function')offsetTop=offset.top(this.$element)
if(typeof offsetBottom=='function')offsetBottom=offset.bottom(this.$element)
var affix=this.getState(scrollHeight,height,offsetTop,offsetBottom)
if(this.affixed!=affix){if(this.unpin!=null)this.$element.css('top','')
var affixType='affix'+(affix?'-'+affix:'')
var e=$.Event(affixType+'.bs.affix')
this.$element.trigger(e)
if(e.isDefaultPrevented())return
this.affixed=affix
this.unpin=affix=='bottom'?this.getPinnedOffset():null
this.$element.removeClass(Affix.RESET).addClass(affixType).trigger(affixType.replace('affix','affixed')+'.bs.affix')}
if(affix=='bottom'){this.$element.offset({top:scrollHeight-height-offsetBottom})}}
function Plugin(option){return this.each(function(){var $this=$(this)
var data=$this.data('bs.affix')
var options=typeof option=='object'&&option
if(!data)$this.data('bs.affix',(data=new Affix(this,options)))
if(typeof option=='string')data[option]()})}
var old=$.fn.affix
$.fn.affix=Plugin
$.fn.affix.Constructor=Affix
$.fn.affix.noConflict=function(){$.fn.affix=old
return this}
$(window).on('load',function(){$('[data-spy="affix"]').each(function(){var $spy=$(this)
var data=$spy.data()
data.offset=data.offset||{}
if(data.offsetBottom!=null)data.offset.bottom=data.offsetBottom
if(data.offsetTop!=null)data.offset.top=data.offsetTop
Plugin.call($spy,data)})})}(jQuery);!function(t,i,e,s){function o(i,e){var h=this;"object"==typeof e&&(delete e.refresh,delete e.render,t.extend(this,e)),this.$element=t(i),!this.imageSrc&&this.$element.is("img")&&(this.imageSrc=this.$element.attr("src"));var r=(this.position+"").toLowerCase().match(/\S+/g)||[];if(r.length<1&&r.push("center"),1==r.length&&r.push(r[0]),("top"==r[0]||"bottom"==r[0]||"left"==r[1]||"right"==r[1])&&(r=[r[1],r[0]]),this.positionX!=s&&(r[0]=this.positionX.toLowerCase()),this.positionY!=s&&(r[1]=this.positionY.toLowerCase()),h.positionX=r[0],h.positionY=r[1],"left"!=this.positionX&&"right"!=this.positionX&&(this.positionX=isNaN(parseInt(this.positionX))?"center":parseInt(this.positionX)),"top"!=this.positionY&&"bottom"!=this.positionY&&(this.positionY=isNaN(parseInt(this.positionY))?"center":parseInt(this.positionY)),this.position=this.positionX+(isNaN(this.positionX)?"":"px")+" "+this.positionY+(isNaN(this.positionY)?"":"px"),navigator.userAgent.match(/(iPod|iPhone|iPad)/))return this.imageSrc&&this.iosFix&&!this.$element.is("img")&&this.$element.css({backgroundImage:"url("+this.imageSrc+")",backgroundSize:"cover",backgroundPosition:this.position}),this;if(navigator.userAgent.match(/(Android)/))return this.imageSrc&&this.androidFix&&!this.$element.is("img")&&this.$element.css({backgroundImage:"url("+this.imageSrc+")",backgroundSize:"cover",backgroundPosition:this.position}),this;this.$mirror=t("<div />").prependTo("body");var a=this.$element.find(">.parallax-slider"),n=!1;0==a.length?this.$slider=t("<img />").prependTo(this.$mirror):(this.$slider=a.prependTo(this.$mirror),n=!0),this.$mirror.addClass("parallax-mirror").css({visibility:"hidden",zIndex:this.zIndex,position:"fixed",top:0,left:0,overflow:"hidden"}),this.$slider.addClass("parallax-slider").one("load",function(){h.naturalHeight&&h.naturalWidth||(h.naturalHeight=this.naturalHeight||this.height||1,h.naturalWidth=this.naturalWidth||this.width||1),h.aspectRatio=h.naturalWidth/h.naturalHeight,o.isSetup||o.setup(),o.sliders.push(h),o.isFresh=!1,o.requestRender()}),n||(this.$slider[0].src=this.imageSrc),(this.naturalHeight&&this.naturalWidth||this.$slider[0].complete||a.length>0)&&this.$slider.trigger("load")}function h(s){return this.each(function(){var h=t(this),r="object"==typeof s&&s;this==i||this==e||h.is("body")?o.configure(r):h.data("px.parallax")?"object"==typeof s&&t.extend(h.data("px.parallax"),r):(r=t.extend({},h.data(),r),h.data("px.parallax",new o(this,r))),"string"==typeof s&&("destroy"==s?o.destroy(this):o[s]())})}!function(){for(var t=0,e=["ms","moz","webkit","o"],s=0;s<e.length&&!i.requestAnimationFrame;++s)i.requestAnimationFrame=i[e[s]+"RequestAnimationFrame"],i.cancelAnimationFrame=i[e[s]+"CancelAnimationFrame"]||i[e[s]+"CancelRequestAnimationFrame"];i.requestAnimationFrame||(i.requestAnimationFrame=function(e){var s=(new Date).getTime(),o=Math.max(0,16-(s-t)),h=i.setTimeout(function(){e(s+o)},o);return t=s+o,h}),i.cancelAnimationFrame||(i.cancelAnimationFrame=function(t){clearTimeout(t)})}(),t.extend(o.prototype,{speed:.2,bleed:0,zIndex:-100,iosFix:!0,androidFix:!0,position:"center",overScrollFix:!1,refresh:function(){this.boxWidth=this.$element.outerWidth(),this.boxHeight=this.$element.outerHeight()+2*this.bleed,this.boxOffsetTop=this.$element.offset().top-this.bleed,this.boxOffsetLeft=this.$element.offset().left,this.boxOffsetBottom=this.boxOffsetTop+this.boxHeight;var t=o.winHeight,i=o.docHeight,e=Math.min(this.boxOffsetTop,i-t),s=Math.max(this.boxOffsetTop+this.boxHeight-t,0),h=this.boxHeight+(e-s)*(1-this.speed)|0,r=(this.boxOffsetTop-e)*(1-this.speed)|0;if(h*this.aspectRatio>=this.boxWidth){this.imageWidth=h*this.aspectRatio|0,this.imageHeight=h,this.offsetBaseTop=r;var a=this.imageWidth-this.boxWidth;this.offsetLeft="left"==this.positionX?0:"right"==this.positionX?-a:isNaN(this.positionX)?-a/2|0:Math.max(this.positionX,-a)}else{this.imageWidth=this.boxWidth,this.imageHeight=this.boxWidth/this.aspectRatio|0,this.offsetLeft=0;var a=this.imageHeight-h;this.offsetBaseTop="top"==this.positionY?r:"bottom"==this.positionY?r-a:isNaN(this.positionY)?r-a/2|0:r+Math.max(this.positionY,-a)}},render:function(){var t=o.scrollTop,i=o.scrollLeft,e=this.overScrollFix?o.overScroll:0,s=t+o.winHeight;this.boxOffsetBottom>t&&this.boxOffsetTop<=s?(this.visibility="visible",this.mirrorTop=this.boxOffsetTop-t,this.mirrorLeft=this.boxOffsetLeft-i,this.offsetTop=this.offsetBaseTop-this.mirrorTop*(1-this.speed)):this.visibility="hidden",this.$mirror.css({transform:"translate3d(0px, 0px, 0px)",visibility:this.visibility,top:this.mirrorTop-e,left:this.mirrorLeft,height:this.boxHeight,width:this.boxWidth}),this.$slider.css({transform:"translate3d(0px, 0px, 0px)",position:"absolute",top:this.offsetTop,left:this.offsetLeft,height:this.imageHeight,width:this.imageWidth,maxWidth:"none"})}}),t.extend(o,{scrollTop:0,scrollLeft:0,winHeight:0,winWidth:0,docHeight:1<<30,docWidth:1<<30,sliders:[],isReady:!1,isFresh:!1,isBusy:!1,setup:function(){if(!this.isReady){var s=t(e),h=t(i),r=function(){o.winHeight=h.height(),o.winWidth=h.width(),o.docHeight=s.height(),o.docWidth=s.width()},a=function(){var t=h.scrollTop(),i=o.docHeight-o.winHeight,e=o.docWidth-o.winWidth;o.scrollTop=Math.max(0,Math.min(i,t)),o.scrollLeft=Math.max(0,Math.min(e,h.scrollLeft())),o.overScroll=Math.max(t-i,Math.min(t,0))};h.on("resize.px.parallax load.px.parallax",function(){r(),o.isFresh=!1,o.requestRender()}).on("scroll.px.parallax load.px.parallax",function(){a(),o.requestRender()}),r(),a(),this.isReady=!0}},configure:function(i){"object"==typeof i&&(delete i.refresh,delete i.render,t.extend(this.prototype,i))},refresh:function(){t.each(this.sliders,function(){this.refresh()}),this.isFresh=!0},render:function(){this.isFresh||this.refresh(),t.each(this.sliders,function(){this.render()})},requestRender:function(){var t=this;this.isBusy||(this.isBusy=!0,i.requestAnimationFrame(function(){t.render(),t.isBusy=!1}))},destroy:function(e){var s,h=t(e).data("px.parallax");for(h.$mirror.remove(),s=0;s<this.sliders.length;s+=1)this.sliders[s]==h&&this.sliders.splice(s,1);t(e).data("px.parallax",!1),0===this.sliders.length&&(t(i).off("scroll.px.parallax resize.px.parallax load.px.parallax"),this.isReady=!1,o.isSetup=!1)}});var r=t.fn.parallax;t.fn.parallax=h,t.fn.parallax.Constructor=o,t.fn.parallax.noConflict=function(){return t.fn.parallax=r,this},t(e).on("ready.px.parallax.data-api",function(){t('[data-parallax="scroll"]').parallax()})}(jQuery,window,document);(function($){function notification(param){this.animation=param.animation,this.toggler=param.toggler,this.exceptions=param.exceptions;this.init=function(){var that=this;var stopToggler=this.implode(this.exceptions);if(typeof stopToggler!=='undefined'){$(document).on('click',stopToggler,function(e){e.stopPropagation()})}
var toggler=this.implode(this.toggler);if(typeof toggler!=='undefined'){$(document).on('click touchstart',toggler,function(e){e.stopPropagation();e.preventDefault();that.toggle()})}}
this.toggle=function(){var selectors=this.implode(this.animation);if(typeof selectors!=='undefined'){$(selectors).toggleClass('open')}}
this.implode=function(arr,imploder){if(!(arr instanceof Array)){return arr}
if(typeof imploder=='undefined'){imploder=','}
var data=arr;var ele='';for(var j=0;j<arr.length;j++){ele+=arr[j];if(j!==arr.length-1){ele+=imploder}}
data=ele;return data}}
var notificationConfig={animation:['.notification-overlay','.notification-board','body'],exceptions:['.notification-board'],toggler:['#menu','.notification-overlay','#close']};new notification(notificationConfig).init()})(jQuery);(function(window){'use strict';if(!Date.now){Date.now=function(){return new Date().getTime()}}
if(!window.requestAnimationFrame){(function(){var vendors=['webkit','moz'];for(var i=0;i<vendors.length&&!window.requestAnimationFrame;++i){var vp=vendors[i];window.requestAnimationFrame=window[vp+'RequestAnimationFrame'];window.cancelAnimationFrame=window[vp+'CancelAnimationFrame']||window[vp+'CancelRequestAnimationFrame']}
if(/iP(ad|hone|od).*OS 6/.test(window.navigator.userAgent)||!window.requestAnimationFrame||!window.cancelAnimationFrame){var lastTime=0;window.requestAnimationFrame=function(callback){var now=Date.now();var nextTime=Math.max(lastTime+16,now);return setTimeout(function(){callback(lastTime=nextTime)},nextTime-now)};window.cancelAnimationFrame=clearTimeout}}())}
var supportTransform=(function(){if(!window.getComputedStyle){return !1}
var el=document.createElement('p'),has3d,transforms={'webkitTransform':'-webkit-transform','OTransform':'-o-transform','msTransform':'-ms-transform','MozTransform':'-moz-transform','transform':'transform'};(document.body||document.documentElement).insertBefore(el,null);for(var t in transforms){if(typeof el.style[t]!=='undefined'){el.style[t]="translate3d(1px,1px,1px)";has3d=window.getComputedStyle(el).getPropertyValue(transforms[t])}}(document.body||document.documentElement).removeChild(el);return typeof has3d!=='undefined'&&has3d.length>0&&has3d!=="none"}());var isAndroid=navigator.userAgent.toLowerCase().indexOf('android')>-1;var isIOs=/iPad|iPhone|iPod/.test(navigator.userAgent)&&!window.MSStream;var isOperaOld=!!window.opera;var isEdge=/Edge\/\d+/.test(navigator.userAgent);var isIE11=/Trident.*rv[ :]*11\./.test(navigator.userAgent);var isIE10=!!Function('/*@cc_on return document.documentMode===10@*/')();var isIElt10=document.all&&!window.atob;var wndW;var wndH;function updateWndVars(){wndW=window.innerWidth||document.documentElement.clientWidth;wndH=window.innerHeight||document.documentElement.clientHeight}
updateWndVars();var jarallaxList=[];var Jarallax=(function(){var instanceID=0;function Jarallax_inner(item,userOptions){var _this=this,dataOptions;_this.$item=item;_this.defaults={type:'scroll',speed:0.5,imgSrc:null,imgWidth:null,imgHeight:null,enableTransform:!0,elementInViewport:null,zIndex:-100,noAndroid:!1,noIos:!0,onScroll:null,onInit:null,onDestroy:null,onCoverImage:null};dataOptions=JSON.parse(_this.$item.getAttribute('data-jarallax')||'{}');_this.options=_this.extend({},_this.defaults,dataOptions,userOptions);if(isAndroid&&_this.options.noAndroid||isIOs&&_this.options.noIos){return}
_this.options.speed=Math.min(2,Math.max(-1,parseFloat(_this.options.speed)));var elementInVP=_this.options.elementInViewport;if(elementInVP&&typeof elementInVP==='object'&&typeof elementInVP.length!=='undefined'){elementInVP=elementInVP[0]}
if(!elementInVP instanceof Element){elementInVP=null}
_this.options.elementInViewport=elementInVP;_this.instanceID=instanceID++;_this.image={src:_this.options.imgSrc||null,$container:null,$item:null,width:_this.options.imgWidth||null,height:_this.options.imgHeight||null,useImgTag:isIOs||isAndroid||isOperaOld||isIE11||isIE10||isEdge};if(_this.initImg()){_this.init()}}
return Jarallax_inner}());Jarallax.prototype.css=function(el,styles){if(typeof styles==='string'){if(window.getComputedStyle){return window.getComputedStyle(el).getPropertyValue(styles)}
return el.style[styles]}
if(styles.transform){styles.WebkitTransform=styles.MozTransform=styles.transform}
for(var k in styles){el.style[k]=styles[k]}
return el};Jarallax.prototype.extend=function(out){out=out||{};for(var i=1;i<arguments.length;i++){if(!arguments[i]){continue}
for(var key in arguments[i]){if(arguments[i].hasOwnProperty(key)){out[key]=arguments[i][key]}}}
return out};Jarallax.prototype.initImg=function(){var _this=this;if(_this.image.src===null){_this.image.src=_this.css(_this.$item,'background-image').replace(/^url\(['"]?/g,'').replace(/['"]?\)$/g,'')}
return !(!_this.image.src||_this.image.src==='none')};Jarallax.prototype.init=function(){var _this=this,containerStyles={position:'absolute',top:0,left:0,width:'100%',height:'100%',overflow:'hidden',pointerEvents:'none'},imageStyles={position:'fixed'};_this.image.$container=document.createElement('div');_this.css(_this.image.$container,containerStyles);_this.css(_this.image.$container,{visibility:'hidden','z-index':_this.options.zIndex});_this.image.$container.setAttribute('id','jarallax-container-'+_this.instanceID);_this.$item.appendChild(_this.image.$container);if(_this.image.useImgTag&&supportTransform&&_this.options.enableTransform){_this.image.$item=document.createElement('img');_this.image.$item.setAttribute('src',_this.image.src);imageStyles=_this.extend({'max-width':'none'},containerStyles,imageStyles)}
else{_this.image.$item=document.createElement('div');imageStyles=_this.extend({'background-position':'50% 50%','background-size':'100% auto','background-repeat':'no-repeat no-repeat','background-image':'url("'+_this.image.src+'")'},containerStyles,imageStyles)}
if(isIElt10){imageStyles.backgroundAttachment='fixed'}
_this.parentWithTransform=0;var $itemParents=_this.$item;while($itemParents!==null&&$itemParents!==document&&_this.parentWithTransform===0){var parent_transform=_this.css($itemParents,'-webkit-transform')||_this.css($itemParents,'-moz-transform')||_this.css($itemParents,'transform');if(parent_transform&&parent_transform!=='none'){_this.parentWithTransform=1;_this.css(_this.image.$container,{transform:'translateX(0) translateY(0)'})}
$itemParents=$itemParents.parentNode}
_this.css(_this.image.$item,imageStyles);_this.image.$container.appendChild(_this.image.$item);function initAfterReady(){_this.coverImage();_this.clipContainer();_this.onScroll(!0);_this.$item.setAttribute('data-jarallax-original-styles',_this.$item.getAttribute('style'));if(_this.options.onInit){_this.options.onInit.call(_this)}
setTimeout(function(){if(_this.$item){_this.css(_this.$item,{'background-image':'none','background-attachment':'scroll','background-size':'auto'})}},0)}
if(_this.image.width&&_this.image.height){initAfterReady()}else{_this.getImageSize(_this.image.src,function(width,height){_this.image.width=width;_this.image.height=height;initAfterReady()})}
jarallaxList.push(_this)};Jarallax.prototype.destroy=function(){var _this=this;for(var k=0,len=jarallaxList.length;k<len;k++){if(jarallaxList[k].instanceID===_this.instanceID){jarallaxList.splice(k,1);break}}
var originalStylesTag=_this.$item.getAttribute('data-jarallax-original-styles');_this.$item.removeAttribute('data-jarallax-original-styles');if(originalStylesTag==='null'){_this.$item.removeAttribute('style')}else{_this.$item.setAttribute('style',originalStylesTag)}
if(_this.$clipStyles){_this.$clipStyles.parentNode.removeChild(_this.$clipStyles)}
_this.image.$container.parentNode.removeChild(_this.image.$container);if(_this.options.onDestroy){_this.options.onDestroy.call(_this)}
delete _this.$item.jarallax;for(var n in _this){delete _this[n]}};Jarallax.prototype.getImageSize=function(src,callback){if(!src||!callback){return}
var tempImg=new Image();tempImg.onload=function(){callback(tempImg.width,tempImg.height)};tempImg.src=src};Jarallax.prototype.clipContainer=function(){if(isIElt10){return}
var _this=this,rect=_this.image.$container.getBoundingClientRect(),width=rect.width,height=rect.height;if(!_this.$clipStyles){_this.$clipStyles=document.createElement('style');_this.$clipStyles.setAttribute('type','text/css');_this.$clipStyles.setAttribute('id','#jarallax-clip-'+_this.instanceID);var head=document.head||document.getElementsByTagName('head')[0];head.appendChild(_this.$clipStyles)}
var styles=['#jarallax-container-'+_this.instanceID+' {','   clip: rect(0 '+width+'px '+height+'px 0);','   clip: rect(0, '+width+'px, '+height+'px, 0);','}'].join('\n');if(_this.$clipStyles.styleSheet){_this.$clipStyles.styleSheet.cssText=styles}else{_this.$clipStyles.innerHTML=styles}};Jarallax.prototype.coverImage=function(){var _this=this;if(!_this.image.width||!_this.image.height){return}
var rect=_this.image.$container.getBoundingClientRect(),contW=rect.width,contH=rect.height,contL=rect.left,imgW=_this.image.width,imgH=_this.image.height,speed=_this.options.speed,isScroll=_this.options.type==='scroll'||_this.options.type==='scroll-opacity',scrollDist=0,resultW=0,resultH=contH,resultML=0,resultMT=0;if(isScroll){scrollDist=speed*(contH+wndH)/2;if(speed<0||speed>1){scrollDist=speed*Math.max(contH,wndH)/2}
if(speed<0||speed>1){resultH=Math.max(contH,wndH)+Math.abs(scrollDist)*2}else{resultH+=Math.abs(wndH-contH)*(1-speed)}}
resultW=resultH*imgW/imgH;if(resultW<contW){resultW=contW;resultH=resultW*imgH/imgW}
_this.bgPosVerticalCenter=0;if(isScroll&&resultH<wndH&&(!supportTransform||!_this.options.enableTransform)){_this.bgPosVerticalCenter=(wndH-resultH)/2;resultH=wndH}
if(isScroll){resultML=contL+(contW-resultW)/2;resultMT=(wndH-resultH)/2}else{resultML=(contW-resultW)/2;resultMT=(contH-resultH)/2}
if(supportTransform&&_this.options.enableTransform&&_this.parentWithTransform){resultML-=contL}
_this.parallaxScrollDistance=scrollDist;_this.css(_this.image.$item,{width:resultW+'px',height:resultH+'px',marginLeft:resultML+'px',marginTop:resultMT+'px'});if(_this.options.onCoverImage){_this.options.onCoverImage.call(_this)}};Jarallax.prototype.isVisible=function(){return this.isElementInViewport||!1};Jarallax.prototype.onScroll=function(force){var _this=this;if(!_this.image.width||!_this.image.height){return}
var rect=_this.$item.getBoundingClientRect(),contT=rect.top,contH=rect.height,styles={position:'absolute',visibility:'visible',backgroundPosition:'50% 50%'};var viewportRect=rect;if(_this.options.elementInViewport){viewportRect=_this.options.elementInViewport.getBoundingClientRect()}
_this.isElementInViewport=viewportRect.bottom>=0&&viewportRect.right>=0&&viewportRect.top<=wndH&&viewportRect.left<=wndW;if(force?!1:!_this.isElementInViewport){return}
var beforeTop=Math.max(0,contT),beforeTopEnd=Math.max(0,contH+contT),afterTop=Math.max(0,-contT),beforeBottom=Math.max(0,contT+contH-wndH),beforeBottomEnd=Math.max(0,contH-(contT+contH-wndH)),afterBottom=Math.max(0,-contT+wndH-contH),fromViewportCenter=1-2*(wndH-contT)/(wndH+contH);var visiblePercent=1;if(contH<wndH){visiblePercent=1-(afterTop||beforeBottom)/contH}else{if(beforeTopEnd<=wndH){visiblePercent=beforeTopEnd/wndH}else if(beforeBottomEnd<=wndH){visiblePercent=beforeBottomEnd/wndH}}
if(_this.options.type==='opacity'||_this.options.type==='scale-opacity'||_this.options.type==='scroll-opacity'){styles.transform='translate3d(0, 0, 0)';styles.opacity=visiblePercent}
if(_this.options.type==='scale'||_this.options.type==='scale-opacity'){var scale=1;if(_this.options.speed<0){scale-=_this.options.speed*visiblePercent}else{scale+=_this.options.speed*(1-visiblePercent)}
styles.transform='scale('+scale+') translate3d(0, 0, 0)'}
if(_this.options.type==='scroll'||_this.options.type==='scroll-opacity'){var positionY=_this.parallaxScrollDistance*fromViewportCenter;if(supportTransform&&_this.options.enableTransform){if(_this.parentWithTransform){positionY-=contT}
styles.transform='translate3d(0, '+positionY+'px, 0)'}else if(_this.image.useImgTag){styles.top=positionY+'px'}else{if(_this.bgPosVerticalCenter){positionY+=_this.bgPosVerticalCenter}
styles.backgroundPosition='50% '+positionY+'px'}
styles.position=isIElt10?'absolute':'fixed'}
_this.css(_this.image.$item,styles);if(_this.options.onScroll){_this.options.onScroll.call(_this,{section:rect,beforeTop:beforeTop,beforeTopEnd:beforeTopEnd,afterTop:afterTop,beforeBottom:beforeBottom,beforeBottomEnd:beforeBottomEnd,afterBottom:afterBottom,visiblePercent:visiblePercent,fromViewportCenter:fromViewportCenter})}};function addEventListener(el,eventName,handler){if(el.addEventListener){el.addEventListener(eventName,handler)}else{el.attachEvent('on'+eventName,function(){handler.call(el)})}}
function update(e){window.requestAnimationFrame(function(){if(e.type!=='scroll'){updateWndVars()}
for(var k=0,len=jarallaxList.length;k<len;k++){if(e.type!=='scroll'){jarallaxList[k].coverImage();jarallaxList[k].clipContainer()}
jarallaxList[k].onScroll()}})}
addEventListener(window,'scroll',update);addEventListener(window,'resize',update);addEventListener(window,'orientationchange',update);addEventListener(window,'load',update);var plugin=function(items){if(typeof HTMLElement==="object"?items instanceof HTMLElement:items&&typeof items==="object"&&items!==null&&items.nodeType===1&&typeof items.nodeName==="string"){items=[items]}
var options=arguments[1],args=Array.prototype.slice.call(arguments,2),len=items.length,k=0,ret;for(k;k<len;k++){if(typeof options==='object'||typeof options==='undefined'){if(!items[k].jarallax){items[k].jarallax=new Jarallax(items[k],options)}}
else if(items[k].jarallax){ret=items[k].jarallax[options].apply(items[k].jarallax,args)}
if(typeof ret!=='undefined'){return ret}}
return items};plugin.constructor=Jarallax;var oldPlugin=window.jarallax;window.jarallax=plugin;window.jarallax.noConflict=function(){window.jarallax=oldPlugin;return this};if(typeof jQuery!=='undefined'){var jQueryPlugin=function(){var args=arguments||[];Array.prototype.unshift.call(args,this);var res=plugin.apply(window,args);return typeof res!=='object'?res:this};jQueryPlugin.constructor=Jarallax;var oldJqPlugin=jQuery.fn.jarallax;jQuery.fn.jarallax=jQueryPlugin;jQuery.fn.jarallax.noConflict=function(){jQuery.fn.jarallax=oldJqPlugin;return this}}
addEventListener(window,'DOMContentLoaded',function(){plugin(document.querySelectorAll('[data-jarallax], [data-jarallax-video]'))})}(window));(function(window){'use strict';function extend(out){out=out||{};for(var i=1;i<arguments.length;i++){if(!arguments[i]){continue}
for(var key in arguments[i]){if(arguments[i].hasOwnProperty(key)){out[key]=arguments[i][key]}}}
return out}
function Deferred(){this._done=[];this._fail=[]}
Deferred.prototype={execute:function(list,args){var i=list.length;args=Array.prototype.slice.call(args);while(i--){list[i].apply(null,args)}},resolve:function(){this.execute(this._done,arguments)},reject:function(){this.execute(this._fail,arguments)},done:function(callback){this._done.push(callback)},fail:function(callback){this._fail.push(callback)}};function addEventListener(el,eventName,handler){if(el.addEventListener){el.addEventListener(eventName,handler)}else{el.attachEvent('on'+eventName,function(){handler.call(el)})}}
var VideoWorker=(function(){var ID=0;function VideoWorker_inner(url,options){var _this=this;_this.url=url;_this.options_default={autoplay:1,loop:1,mute:1,controls:0,startTime:0,endTime:0};_this.options=extend({},_this.options_default,options);_this.videoID=_this.parseURL(url);if(_this.videoID){_this.ID=ID++;_this.loadAPI();_this.init()}}
return VideoWorker_inner}());VideoWorker.prototype.parseURL=function(url){function getYoutubeID(ytUrl){var regExp=/.*(?:youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=)([^#\&\?]*).*/;var match=ytUrl.match(regExp);return match&&match[1].length===11?match[1]:!1}
function getVimeoID(vmUrl){var regExp=/https?:\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)/;var match=vmUrl.match(regExp);return match&&match[3]?match[3]:!1}
function getLocalVideos(locUrl){var videoFormats=locUrl.split(/,(?=mp4\:|webm\:|ogv\:|ogg\:)/);var result={};var ready=0;for(var k=0;k<videoFormats.length;k++){var match=videoFormats[k].match(/^(mp4|webm|ogv|ogg)\:(.*)/);if(match&&match[1]&&match[2]){result[match[1]==='ogv'?'ogg':match[1]]=match[2];ready=1}}
return ready?result:!1}
var Youtube=getYoutubeID(url);var Vimeo=getVimeoID(url);var Local=getLocalVideos(url);if(Youtube){this.type='youtube';return Youtube}else if(Vimeo){this.type='vimeo';return Vimeo}else if(Local){this.type='local';return Local}
return !1};VideoWorker.prototype.isValid=function(){return !!this.videoID};VideoWorker.prototype.on=function(name,callback){this.userEventsList=this.userEventsList||[];(this.userEventsList[name]||(this.userEventsList[name]=[])).push(callback)};VideoWorker.prototype.off=function(name,callback){if(!this.userEventsList||!this.userEventsList[name]){return}
if(!callback){delete this.userEventsList[name]}else{for(var k=0;k<this.userEventsList[name].length;k++){if(this.userEventsList[name][k]===callback){this.userEventsList[name][k]=!1}}}};VideoWorker.prototype.fire=function(name){var args=[].slice.call(arguments,1);if(this.userEventsList&&typeof this.userEventsList[name]!=='undefined'){for(var k in this.userEventsList[name]){if(this.userEventsList[name][k]){this.userEventsList[name][k].apply(this,args)}}}};var vimeoPlayBusy=0;VideoWorker.prototype.play=function(start){var _this=this;if(!_this.player){return}
if(_this.type==='youtube'&&_this.player.playVideo){if(typeof start!=='undefined'){_this.player.seekTo(start||0)}
_this.player.playVideo()}
if(_this.type==='vimeo'&&!vimeoPlayBusy){if(typeof start!=='undefined'){vimeoPlayBusy=1;_this.player.setCurrentTime(start||0).then(function(){_this.player.play();vimeoPlayBusy=0})}else{_this.player.play()}}
if(_this.type==='local'){if(typeof start!=='undefined'){_this.player.currentTime=start}
_this.player.play()}};VideoWorker.prototype.pause=function(){if(!this.player){return}
if(this.type==='youtube'&&this.player.pauseVideo){this.player.pauseVideo()}
if(this.type==='vimeo'){this.player.pause()}
if(this.type==='local'){this.player.pause()}};VideoWorker.prototype.getImageURL=function(callback){var _this=this;if(_this.videoImage){callback(_this.videoImage);return}
if(_this.type==='youtube'){var availableSizes=['maxresdefault','sddefault','hqdefault','0'];var step=0;var tempImg=new Image();tempImg.onload=function(){if((this.naturalWidth||this.width)!==120||step===availableSizes.length-1){_this.videoImage='https://i.ytimg.com/vi/'+_this.videoID+'/'+availableSizes[step]+'.jpg';callback(_this.videoImage)}else{step++;this.src='https://i.ytimg.com/vi/'+_this.videoID+'/'+availableSizes[step]+'.jpg'}};tempImg.src='https://i.ytimg.com/vi/'+_this.videoID+'/'+availableSizes[step]+'.jpg'}
if(_this.type==='vimeo'){var request=new XMLHttpRequest();request.open('GET','https://vimeo.com/api/v2/video/'+_this.videoID+'.json',!0);request.onreadystatechange=function(){if(this.readyState===4){if(this.status>=200&&this.status<400){var response=JSON.parse(this.responseText);_this.videoImage=response[0].thumbnail_large;callback(_this.videoImage)}else{}}};request.send();request=null}};VideoWorker.prototype.getIframe=function(callback){var _this=this;if(_this.$iframe){callback(_this.$iframe);return}
_this.onAPIready(function(){var hiddenDiv;if(!_this.$iframe){hiddenDiv=document.createElement('div');hiddenDiv.style.display='none'}
if(_this.type==='youtube'){_this.playerOptions={};_this.playerOptions.videoId=_this.videoID;_this.playerOptions.width=window.innerWidth||document.documentElement.clientWidth;_this.playerOptions.playerVars={autohide:1,rel:0,autoplay:0};if(!_this.options.controls){_this.playerOptions.playerVars.iv_load_policy=3;_this.playerOptions.playerVars.modestbranding=1;_this.playerOptions.playerVars.controls=0;_this.playerOptions.playerVars.showinfo=0;_this.playerOptions.playerVars.disablekb=1}
var ytStarted;var ytProgressInterval;_this.playerOptions.events={onReady:function(e){if(_this.options.mute){e.target.mute()}
if(_this.options.autoplay){_this.play(_this.options.startTime)}
_this.fire('ready',e)},onStateChange:function(e){if(_this.options.loop&&e.data===YT.PlayerState.ENDED){_this.play(_this.options.startTime)}
if(!ytStarted&&e.data===YT.PlayerState.PLAYING){ytStarted=1;_this.fire('started',e)}
if(e.data===YT.PlayerState.PLAYING){_this.fire('play',e)}
if(e.data===YT.PlayerState.PAUSED){_this.fire('pause',e)}
if(e.data===YT.PlayerState.ENDED){_this.fire('end',e)}
if(_this.options.endTime){if(e.data===YT.PlayerState.PLAYING){ytProgressInterval=setInterval(function(){if(_this.options.endTime&&_this.player.getCurrentTime()>=_this.options.endTime){if(_this.options.loop){_this.play(_this.options.startTime)}else{_this.pause()}}},150)}else{clearInterval(ytProgressInterval)}}}};var firstInit=!_this.$iframe;if(firstInit){var div=document.createElement('div');div.setAttribute('id',_this.playerID);hiddenDiv.appendChild(div);document.body.appendChild(hiddenDiv)}
_this.player=_this.player||new window.YT.Player(_this.playerID,_this.playerOptions);if(firstInit){_this.$iframe=document.getElementById(_this.playerID)}}
if(_this.type==='vimeo'){_this.playerOptions='';_this.playerOptions+='player_id='+_this.playerID;_this.playerOptions+='&autopause=0';if(!_this.options.controls){_this.playerOptions+='&badge=0&byline=0&portrait=0&title=0'}
_this.playerOptions+='&autoplay=0';_this.playerOptions+='&loop='+(_this.options.loop?1:0);if(!_this.$iframe){_this.$iframe=document.createElement('iframe');_this.$iframe.setAttribute('id',_this.playerID);_this.$iframe.setAttribute('src','https://player.vimeo.com/video/'+_this.videoID+'?'+_this.playerOptions);_this.$iframe.setAttribute('frameborder','0');hiddenDiv.appendChild(_this.$iframe);document.body.appendChild(hiddenDiv)}
_this.player=_this.player||new Vimeo.Player(_this.$iframe);_this.player.setVolume(_this.options.mute?0:100);if(_this.options.autoplay){_this.play(_this.options.startTime)}
var vmStarted;_this.player.on('timeupdate',function(e){if(!vmStarted){_this.fire('started',e)}
vmStarted=1;if(_this.options.endTime){if(_this.options.endTime&&e.seconds>=_this.options.endTime){if(_this.options.loop){_this.play(_this.options.startTime)}else{_this.pause()}}}});_this.player.on('play',function(e){_this.fire('play',e)});_this.player.on('pause',function(e){_this.fire('pause',e)});_this.player.on('ended',function(e){_this.fire('end',e)});_this.player.on('loaded',function(e){_this.fire('ready',e)})}
function addSourceToLocal(element,src,type){var source=document.createElement('source');source.src=src;source.type=type;element.appendChild(source)}
if(_this.type==='local'){if(!_this.$iframe){_this.$iframe=document.createElement('video');if(_this.options.mute){_this.$iframe.setAttribute('muted','on')}
if(_this.options.loop){_this.$iframe.setAttribute('loop','on')}
_this.$iframe.setAttribute('id',_this.playerID);hiddenDiv.appendChild(_this.$iframe);document.body.appendChild(hiddenDiv);for(var k in _this.videoID){addSourceToLocal(_this.$iframe,_this.videoID[k],'video/'+k)}}
_this.player=_this.player||_this.$iframe;var locStarted;addEventListener(_this.player,'playing',function(e){if(!locStarted){_this.fire('started',e)}
locStarted=1});addEventListener(_this.player,'timeupdate',function(){if(_this.options.endTime){if(_this.options.endTime&&this.currentTime>=_this.options.endTime){if(_this.options.loop){_this.play(_this.options.startTime)}else{_this.pause()}}}});addEventListener(_this.player,'play',function(e){_this.fire('play',e)});addEventListener(_this.player,'pause',function(e){_this.fire('pause',e)});addEventListener(_this.player,'ended',function(e){_this.fire('end',e)});addEventListener(_this.player,'loadedmetadata',function(){_this.fire('ready');if(_this.options.autoplay){_this.play(_this.options.startTime)}})}
callback(_this.$iframe)})};VideoWorker.prototype.init=function(){var _this=this;_this.playerID='VideoWorker-'+_this.ID};var YoutubeAPIadded=0;var VimeoAPIadded=0;VideoWorker.prototype.loadAPI=function(){var _this=this;if(YoutubeAPIadded&&VimeoAPIadded){return}
var src='';if(_this.type==='youtube'&&!YoutubeAPIadded){YoutubeAPIadded=1;src='//www.youtube.com/iframe_api'}
if(_this.type==='vimeo'&&!VimeoAPIadded){VimeoAPIadded=1;src='//player.vimeo.com/api/player.js'}
if(!src){return}
if(window.location.origin==='file://'){src='http:'+src}
var tag=document.createElement('script');var head=document.getElementsByTagName('head')[0];tag.src=src;head.appendChild(tag);head=null;tag=null};var loadingYoutubePlayer=0;var loadingVimeoPlayer=0;var loadingYoutubeDeffer=new Deferred();var loadingVimeoDeffer=new Deferred();VideoWorker.prototype.onAPIready=function(callback){var _this=this;if(_this.type==='youtube'){if((typeof YT==='undefined'||YT.loaded===0)&&!loadingYoutubePlayer){loadingYoutubePlayer=1;window.onYouTubeIframeAPIReady=function(){window.onYouTubeIframeAPIReady=null;loadingYoutubeDeffer.resolve('done');callback()}}else if(typeof YT==='object'&&YT.loaded===1){callback()}else{loadingYoutubeDeffer.done(function(){callback()})}}
if(_this.type==='vimeo'){if(typeof Vimeo==='undefined'&&!loadingVimeoPlayer){loadingVimeoPlayer=1;var vimeo_interval=setInterval(function(){if(typeof Vimeo!=='undefined'){clearInterval(vimeo_interval);loadingVimeoDeffer.resolve('done');callback()}},20)}else if(typeof Vimeo!=='undefined'){callback()}else{loadingVimeoDeffer.done(function(){callback()})}}
if(_this.type==='local'){callback()}};window.VideoWorker=VideoWorker}(window));(function(){'use strict';if(typeof jarallax==='undefined'){return}
var Jarallax=jarallax.constructor;var def_init=Jarallax.prototype.init;Jarallax.prototype.init=function(){var _this=this;def_init.apply(_this);if(_this.video){_this.video.getIframe(function(iframe){var $parent=iframe.parentNode;_this.css(iframe,{position:'fixed',top:'0px',left:'0px',right:'0px',bottom:'0px',width:'100%',height:'100%',visibility:'visible',zIndex:-1});_this.$video=iframe;_this.image.$container.appendChild(iframe);$parent.parentNode.removeChild($parent)})}};var def_coverImage=Jarallax.prototype.coverImage;Jarallax.prototype.coverImage=function(){var _this=this;def_coverImage.apply(_this);if(_this.video&&_this.image.$item.nodeName==='IFRAME'){_this.css(_this.image.$item,{height:_this.image.$item.getBoundingClientRect().height+400+'px',marginTop:(-200+parseFloat(_this.css(_this.image.$item,'margin-top')))+'px'})}};var def_initImg=Jarallax.prototype.initImg;Jarallax.prototype.initImg=function(){var _this=this;if(!_this.options.videoSrc){_this.options.videoSrc=_this.$item.getAttribute('data-jarallax-video')||!1}
if(_this.options.videoSrc){var video=new VideoWorker(_this.options.videoSrc,{startTime:_this.options.videoStartTime||0,endTime:_this.options.videoEndTime||0});if(video.isValid()){_this.image.useImgTag=!0;video.on('ready',function(){var oldOnScroll=_this.onScroll;_this.onScroll=function(){oldOnScroll.apply(_this);if(_this.isVisible()){video.play()}else{video.pause()}}});video.on('started',function(){_this.image.$default_item=_this.image.$item;_this.image.$item=_this.$video;_this.image.width=_this.options.imgWidth=_this.image.width||1280;_this.image.height=_this.options.imgHeight=_this.image.height||720;_this.coverImage();_this.clipContainer();_this.onScroll();if(_this.image.$default_item){_this.image.$default_item.style.display='none'}});_this.video=video;if(video.type!=='local'){video.getImageURL(function(url){_this.image.src=url;_this.init()})}}
if(video.type!=='local'){return !1}}
return def_initImg.apply(_this)};var def_destroy=Jarallax.prototype.destroy;Jarallax.prototype.destroy=function(){var _this=this;def_destroy.apply(_this)}}());function preloader(param){this.time=param.time;this.init=function(){var time=this.time;jQuery(window).load(function(){jQuery('body').animate({opacity:1},time)})}}
function lookNfill(){var that=this;this.init=function(){this.headerFix();jQuery(window).load(function(){that.bannerTextAnimation();that.isAppleProduct()})};this.headerFix=function(){var $header=jQuery('header#masthead'),$window=jQuery(window),$document=jQuery(document),fixHeaderClass='fix-header',headerHeight=30;$document.ready(fixHeader);$window.scroll(fixHeader);function fixHeader(){var scrollPosition=$window.scrollTop();if(scrollPosition>=headerHeight){if(!$header.hasClass(fixHeaderClass)){$header.addClass(fixHeaderClass)}}else{$header.removeClass(fixHeaderClass)}}};this.isAppleProduct=function(){var isiOSSafari=(navigator.userAgent.match(/like Mac OS X/i))?!0:!1,$window=jQuery(window).width(),$bannerText=jQuery('.banner-text'),$imageWrapper=jQuery('.img-wrapper');if(isiOSSafari&&$window<1370&&$bannerText.length>0){$bannerText.addClass('ani')}else if(isiOSSafari&&$window<1370&&$imageWrapper.length>0){$imageWrapper.parent().addClass('animation')}};this.bannerTextAnimation=function(){var $bannerText=jQuery('.banner-text')
$imageCover=jQuery('.image-cover'),className='ani';if($bannerText.length>0){$bannerText.addClass(className);setTimeout(function(){$bannerText.removeClass(className)},5000)}
if($imageCover.length>0){console.log('ok here');$imageCover.addClass(className)}}}
jQuery(document).ready(function(){new lookNfill().init()});jQuery(window).load(function(){var bokaSrc=jQuery('#boka-board').attr('data-src');jQuery('#boka-board').removeAttr('data-src');jQuery('#boka-board').attr('src',bokaSrc)});new preloader({time:0}).init();jQuery.fn.scrollDown=function(){this.click(function(e){e.preventDefault();var $this=jQuery(this),$element=jQuery('body,html'),target=$this.attr('data-target'),reduceHeightAttr=$this.attr('data-reduce-offset'),animationDuration=$this.attr('data-animation-duration'),targetOffset=jQuery(target).offset().top,animationDurationValue=(typeof animationDuration!='undefined')?parseInt(animationDuration):500,actualOffset=targetOffset-reduceHeight(reduceHeightAttr);function reduceHeight(param){var reduceHeight;if(jQuery(param).length>0){reduceHeight=jQuery(param).outerHeight();return reduceHeight}else if(jQuery.isNumeric(param)){return parseInt(param)}
return 0}
$element.stop().animate({scrollTop:actualOffset},animationDurationValue)})}
jQuery(document).ready(function(){$("#fs-down-arrow").scrollDown()});jQuery(document).ready(function($){jQuery('.daily-lunch-pdf .week-pdf .title a').on('click',function(e){e.preventDefault();var date=jQuery(this).data('date');var week=jQuery(this).data('week');var _that=jQuery(this);if(date){jQuery.ajax({type:'post',url:hugoAjax.ajaxurl,data:{action:'get_daily_lunch_menu_items',menu_date:date},dataType:'json',beforeSend:function(){_that.addClass('loading');jQuery('#daily-lunch-menu').css('visibility','hidden')},success:function(response){_that.removeClass('loading');jQuery('#daily-lunch-menu').css('visibility','visible');jQuery('.daily-lunch-menu .current-week').html('Lunchmeny vecka '+week);jQuery('.daily-lunch-menu .row').remove();jQuery('.daily-lunch-menu').append(response)}})}})})})(jQuery)