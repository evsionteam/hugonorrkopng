/** handles (scripts) :wp-embed  */(function(window,document){'use strict';var supportedBrowser=!1,loaded=!1;if(document.querySelector){if(window.addEventListener){supportedBrowser=!0}}
window.wp=window.wp||{};if(!!window.wp.receiveEmbedMessage){return}
window.wp.receiveEmbedMessage=function(e){var data=e.data;if(!(data.secret||data.message||data.value)){return}
if(/[^a-zA-Z0-9]/.test(data.secret)){return}
var iframes=document.querySelectorAll('iframe[data-secret="'+data.secret+'"]'),blockquotes=document.querySelectorAll('blockquote[data-secret="'+data.secret+'"]'),i,source,height,sourceURL,targetURL;for(i=0;i<blockquotes.length;i++){blockquotes[i].style.display='none'}
for(i=0;i<iframes.length;i++){source=iframes[i];if(e.source!==source.contentWindow){continue}
source.removeAttribute('style');if('height'===data.message){height=parseInt(data.value,10);if(height>1000){height=1000}else if(~~height<200){height=200}
source.height=height}
if('link'===data.message){sourceURL=document.createElement('a');targetURL=document.createElement('a');sourceURL.href=source.getAttribute('src');targetURL.href=data.value;if(targetURL.host===sourceURL.host){if(document.activeElement===source){window.top.location.href=data.value}}}}};function onLoad(){if(loaded){return}
loaded=!0;var isIE10=-1!==navigator.appVersion.indexOf('MSIE 10'),isIE11=!!navigator.userAgent.match(/Trident.*rv:11\./ ),iframes=document.querySelectorAll('iframe.wp-embedded-content'),iframeClone,i,source,secret;for(i=0;i<iframes.length;i++){source=iframes[i];if(!source.getAttribute('data-secret')){secret=Math.random().toString(36).substr(2,10);source.src+='#?secret='+secret;source.setAttribute('data-secret',secret)}
if((isIE10||isIE11)){iframeClone=source.cloneNode(!0);iframeClone.removeAttribute('security');source.parentNode.replaceChild(iframeClone,source)}}}
if(supportedBrowser){window.addEventListener('message',window.wp.receiveEmbedMessage,!1);document.addEventListener('DOMContentLoaded',onLoad,!1);window.addEventListener('load',onLoad,!1)}})(window,document)