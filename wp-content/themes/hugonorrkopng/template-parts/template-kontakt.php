<?php
/* Template Name: Kontakt */
?>

<?php
get_header();
global $hugo_opt;
while (have_posts()) : the_post();
    ?>
    <div class="contact">

        <?php
        $enable_map = get_post_meta( $post->ID, 'enable_map', true);
        if('yes' == $enable_map){
            ?>
            <!--Google Map-->
            <div id="map" style="width: 100%;height: 500px;background-color: grey;"></div>
            <!---->
        <?php
        }
        ?>

        <!-- DESCRIPTION -->
        <?php
        $container_class = '';
        if(is_multisite()){
            $blog_details = get_blog_details();
            if (strpos($blog_details->path, 'nojen') !== false) {
                $container_class = 'night-contact';
            }
        }
        ?>
        <div class="<?php echo $container_class;?> contact-detail">
            <div class="container">
                <div class="description">
                    <div class="entry-content">
                        <div class="text-left">
                            <?php the_content(); ?>
                        </div>
                    </div>

                    <div id="contact-page-data" class="c-form-n-op-hours">
                        <div class="row">
                            <!-- .entry-content -->
                            <div class="col-sm-6 opening-hours text-left">
                                <?php echo do_shortcode('[opening_hours]'); ?>
                            </div>
                            <div class="col-sm-6 contact-form text-left">
                                <?php if(isset($hugo_opt['contact-form']) && !empty($hugo_opt['contact-form'])):?>
                                    <h2 class="modal-sub-title">KONTAKTA OSS</h2>
                                    <?php echo do_shortcode($hugo_opt['contact-form']); ?>
                                <?php endif;?>
                            </div>
                        </div>

                    </div>
                </div><!-- /.description -->
            </div><!-- /.container -->
        </div>

        <iframe src="https://www.google.com/maps/embed?pb=!1m0!3m2!1ssv!2sse!4v1484830189953!6m8!1m7!1sQZsJaltUN10AAAQvxXdspQ!2m2!1d58.59517531078579!2d16.18201562362401!3f182.69721547122643!4f-10.524834627590849!5f0.7820865974627469" width="100%" height="450" frameborder="0" style="border:0;display:block;" allowfullscreen></iframe>
    </div><!-- /.contact -->

    <?php
endwhile;
wp_reset_query();
get_footer();
?>