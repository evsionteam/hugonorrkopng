<?php
/* Template Name: Bowling */
get_header();
while (have_posts()) : the_post();
    $overlay_class = '';
    $overlay_value = get_post_meta(get_the_ID(), 'page_overlay', true);
    if ('yes' == $overlay_value) {
        $overlay_class = ' overlay ';
    }
    ?>

    <div class="inner-page bowling">
        <!-- IMG WRAPPER -->
        <div class="image-cover">
            <div class="img-wrapper jarallax <?php echo $overlay_class; ?>" data-jarallax='{"speed": 0.2}' style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id(get_the_ID())); ?>');">
                <!-- Caption -->

                <div class="caption">
                    <header class="entry-header"><meta http-equiv="Content-Type" content="text/html; charset=euc-kr"><?php the_title(); ?></header>
                    <!-- .entry-header -->
                    <?php $button_text = get_post_meta(get_the_ID(), 'button_text', true); ?>
                    <?php $button_link = get_post_meta(get_the_ID(), 'button_link', true); ?>
                    <?php if (!empty($button_link) && !empty($button_text)) { ?>
                        <div class="btn-link"><a href="<?php echo get_post_meta(get_the_ID(), 'button_link', true); ?>" class="btn-bordered"><?php echo get_post_meta(get_the_ID(), 'button_text', true); ?></a></div>
                    <?php } ?>
                </div>
            </div><!-- /.img-wrapper -->
        </div><!-- /.img-wrapper -->

        <!-- DESCRIPTION -->
        <div class="container">
            <div class="description">
                <div class="entry-content">
                    <div class="text-left">
                        <p><?php the_content(); ?></p>

                    </div>
                </div><!-- /.entry-content -->
            </div><!-- /.description -->
        </div>
         
        <!-- OPENING HOURS AND BOOKING IFRAME -->
        <div class="o-hours-n-booking">
            <div class="container">
                <div class="blowing-description">
                    <div class="row">
                        <div class="col-sm-4">
                            <?php
                            $time_n_prices = get_post_meta(get_the_ID(), 'time_n_price', true);
                            if (!empty($time_n_prices)) {
                                echo '<h4 class="modal-sub-title">Våra priser &amp; tider</h4>';
                                echo '<div class="our-price-n-time">';
                                foreach ($time_n_prices as $time_n_price) {
                                    $time_n_price_arr = explode('|', $time_n_price['name_prices']);
                                    $time = $time_n_price_arr[0];
                                    $price = $time_n_price_arr[1];
                                    ?>
                                    <p class="time"><?php echo $time; ?></p>
                                    <p  class="price"><?php echo $price; ?></p>
                                    <?php
                                }
                                echo '</div>';
                                echo 'Skohyra är inkluderat i priset!';
                            }
                            ?>
                        </div><!-- /.col-sm-4 -->

                        <div class="col-sm-8">
                            <iframe id="boka-board" height="600" width="300" data-src="//webbokning.bokad.se/bokning/hugo" ></iframe>
                            <?php /*
                            $booking_form = get_post_meta(get_the_ID(), 'booking_form', true);
                            if (!empty($booking_form)) {
                                echo $booking_form;
                            } */
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- o-hours-n-booking -->
        
        
        <?php $menu_select = get_post_meta(get_the_ID(), 'slider_design_select_meta_box1', true);
        ?>
        <!-- MODAL RESTAURANT BLOCK -->
        <div id="modal-restaurant-block" class="restaurant-block">
            <div class="container">
                <div class="row">
                    <?php
                    $args = array('post__in' => $menu_select, 'post_type' => 'menu');
                    $query = new WP_Query($args);
                    ?>
                    <?php while ($query->have_posts()) : $query->the_post(); ?>
                        <?php
                        $attach_pdf_id = '';
                        $pdf_link = '';
                        $attach_pdf_id = get_post_meta(get_the_ID(), '_image_id', true);
                        if (!empty($attach_pdf_id)) {
                            $pdf_link = wp_get_attachment_url($attach_pdf_id);
                        }
                        ?>
                        <div class="col-sm-4 col-md-4 col-xs-6 restro-block">
                            <a href="<?php echo $pdf_link; ?>">
                                <div class="resturant-img">
                                    <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id(get_the_ID())); ?>" alt="image">
                                </div>
                            </a>
                            <a href="<?php echo $pdf_link; ?>">
                                <div class="resturant-title"><?php the_title(); ?></div>
                            </a>
                            <p></p><?php the_content(); ?>
                            <p></p>
                            <?php if (!empty($pdf_link)): ?>
                                <a href="<?php echo $pdf_link; ?>" class="see-more">
                                    Se meny
                                </a>
                            <?php endif; ?>
                        </div>

                    <?php endwhile; ?> 
                    <?php wp_reset_postdata();
                    ?>
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.restaurant-block -->


    </div><!-- /.inner-page bowling -->

    <?php
endwhile;
wp_reset_query();
get_footer();
?>