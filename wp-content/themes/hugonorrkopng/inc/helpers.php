<?php

add_filter( 'body_class', 'hugonorrkopng_body_classes' );
function hugonorrkopng_body_classes( $classes ) {
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

    if (is_multisite()) {
        $blog_details = get_blog_details();
        if (strpos($blog_details->path, 'nojen') !== false) {
            $classes[] = 'nojen';
        }
    }

	return $classes;
}


add_action( 'wp_head', 'hugonorrkopng_pingback_header' );
function hugonorrkopng_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', bloginfo( 'pingback_url' ), '">';
	}
}

add_filter('nav_menu_link_attributes', 'hugo_menu_attribute_add', 10, 3);
function hugo_menu_attribute_add($atts, $item, $args) {
    $atts['page-id'] = $item->object_id;
    return $atts;
}

/*

add_filter( 'wp_nav_menu_objects', 'page_menu_link', 10, 3 );
function page_menu_link($sorted_menu_items, $args) {
    foreach ($sorted_menu_items as $sorted_menu_item) {
        if ($sorted_menu_item->object == 'page') {
            $sorted_menu_item->url = '#';
        }
    }
    return $sorted_menu_items;
}
*/


add_filter('wpcf7_form_action_url', 'alter_form_action');
function alter_form_action($url) {
    global $wp;
    $url_frag = explode('#', $url);

    if (isset($url_frag[1])) {
        $url = home_url(add_query_arg(array(), $wp->request)) . '/#' . $url_frag[1];
    }

    return $url;
}

add_filter('the_content', 'remove_empty_p', 20, 1);
function remove_empty_p($content) {
    $content = force_balance_tags($content);
    $content = preg_replace('#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content);
    $content = preg_replace('~\s?<p>(\s|&nbsp;)+</p>\s?~', '', $content);
    return $content;
}

add_filter('the_content', 'shortcode_empty_paragraph_fix');
function shortcode_empty_paragraph_fix($content) {

    // define your shortcodes to filter, '' filters all shortcodes
    $shortcodes = array('col', 'wrapper');

    foreach ($shortcodes as $shortcode) {

        $array = array(
            '<p>[' . $shortcode => '[' . $shortcode,
            '<p>[/' . $shortcode => '[/' . $shortcode,
            $shortcode . ']</p>' => $shortcode . ']',
            $shortcode . ']<br />' => $shortcode . ']'
        );

        $content = strtr($content, $array);
    }

    return $content;
}

add_filter('tiny_mce_before_init', 'fb_change_mce_options');
function fb_change_mce_options($initArray) {
    $ext = 'pre[id|name|class|style],iframe[align|longdesc|name|width|height|frameborder|scrolling|marginheight|marginwidth|src]';

    if (isset($initArray['extended_valid_elements'])) {
        $initArray['extended_valid_elements'] .= ',' . $ext;
    } else {
        $initArray['extended_valid_elements'] = $ext;
    }
    return $initArray;
}

/* Favicons for the head */
add_filter( 'get_site_icon_url', 'hugo_provide_site_icon',10,2);
function hugo_provide_site_icon($url, $size){
    global $hugo_opt;

    $fav_sizes = array(16,32,96);
    $apple_sizes = array(57,60,72,76,114,120,144,152,180,192);

    if( in_array($size, $fav_sizes) ){
        $url = ( isset($hugo_opt['favicon-'.$size]['url']) && !empty($hugo_opt['favicon-'.$size]['url']) )? $hugo_opt['favicon-'.$size]['url']:'';
    }else if( in_array($size, $apple_sizes) ){
        $url = ( isset($hugo_opt['apple-icon-'.$size]['url']) && !empty($hugo_opt['apple-icon-'.$size]['url']) )? $hugo_opt['apple-icon-'.$size]['url']: '';
    }

    return $url;
}


add_filter( 'site_icon_image_sizes', 'hugo_site_icon_size' );
function hugo_site_icon_size( $sizes ) {    
    $fav_sizes = array(16,32,57,60,72,76,96,114,120,144,152,180,192);
    $sizes = array_merge($sizes, $fav_sizes);
    return $sizes;
}

add_action('wp_head','hugo_site_icon_tag');
/*add_filter( 'site_icon_meta_tags', 'hugo_site_icon_tag' );
function hugo_site_icon_tag( $meta_tags ) {*/
function hugo_site_icon_tag() {
    global $hugo_opt;
    
    $meta_tags[] = sprintf( '<link rel="icon" type="image/png" sizes="16x16" href="%s"/>', esc_url( get_site_icon_url( 16 ) ) );
    $meta_tags[] = sprintf( '<link rel="icon" type="image/png" sizes="32x32" href="%s"/>', esc_url( get_site_icon_url( 32 ) ) );
    $meta_tags[] = sprintf( '<link rel="icon" type="image/png" sizes="96x96" href="%s"/>', esc_url( get_site_icon_url( 96 ) ) );
   
    $meta_tags[] = sprintf( '<link rel="apple-touch-icon" sizes="57x57" href="%s" />', get_site_icon_url( 57 ) );
    $meta_tags[] = sprintf( '<link rel="apple-touch-icon" sizes="60x60" href="%s" />', get_site_icon_url( 60 ) );
    $meta_tags[] = sprintf( '<link rel="apple-touch-icon" sizes="72x72" href="%s" />', get_site_icon_url( 72 ) );
    $meta_tags[] = sprintf( '<link rel="apple-touch-icon" sizes="76x76" href="%s" />', get_site_icon_url( 76 ) );
    $meta_tags[] = sprintf( '<link rel="apple-touch-icon" sizes="114x114" href="%s" />', get_site_icon_url( 114 ) );
    $meta_tags[] = sprintf( '<link rel="apple-touch-icon" sizes="120x120" href="%s" />', get_site_icon_url( 120 ) );
    $meta_tags[] = sprintf( '<link rel="apple-touch-icon" sizes="144x144" href="%s" />', get_site_icon_url( 144 ) );
    $meta_tags[] = sprintf( '<link rel="apple-touch-icon" sizes="152x152" href="%s" />', get_site_icon_url( 152 ) );
    $meta_tags[] = sprintf( '<link rel="apple-touch-icon" sizes="180x180" href="%s" />', get_site_icon_url( 180 ) );
    $meta_tags[] = sprintf( '<link rel="apple-touch-icon" sizes="192x192" href="%s" />', get_site_icon_url( 192 ) );   
   
    $meta_tags[] = sprintf( '<meta name="msapplication-TileImage" content="%2s">', esc_url( get_site_icon_url( 144 ) ) ); 
        
    if( isset($hugo_opt['favicon-theme-color']) && !empty($hugo_opt['favicon-theme-color']) ){
        $meta_tags[] = '<meta name="theme-color" content="'.$hugo_opt['favicon-theme-color'].'">';
    }

    if( isset($hugo_opt['favicon-title-color']) && !empty($hugo_opt['favicon-title-color']) ){
        $meta_tags[] = '<meta name="msapplication-TileColor" content="'.$hugo_opt['favicon-title-color'].'">';
    }

    echo implode("\n",$meta_tags);

    //return $meta_tags;
}