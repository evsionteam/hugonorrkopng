<?php
// Register Custom Post Type
function hugo_register_activities_post_type() {
	$labels = array(
		'name'               => esc_html__( 'Activities' , 'hugonorrkopng' ),
		'singular_name'      => esc_html__( 'Activities', 'hugonorrkopng' ),
		'menu_name'          => esc_html__( 'Activities', 'hugonorrkopng' ),
		'name_admin_bar'     => esc_html__( 'Activities', 'hugonorrkopng' ),
		'add_new'            => esc_html__( 'Add New', 'hugonorrkopng' ),
		'add_new_item'       => esc_html__( 'Add New Activities', 'hugonorrkopng' ),
		'new_item'           => esc_html__( 'New Activities', 'hugonorrkopng' ),
		'edit_item'          => esc_html__( 'Edit Activities', 'hugonorrkopng' ),
		'view_item'          => esc_html__( 'View Activities', 'hugonorrkopng' ),
		'all_items'          => esc_html__( 'All Activities', 'hugonorrkopng' ),
		'search_items'       => esc_html__( 'Search Activities', 'hugonorrkopng' ),
		'parent_item_colon'  => esc_html__( 'Parent Activities:', 'hugonorrkopng' ),
		'not_found'          => esc_html__( 'No Activities found.', 'hugonorrkopng' ),
		'not_found_in_trash' => esc_html__( 'No Activities found in Trash.', 'hugonorrkopng' )
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'activities' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt' )
	);

	register_post_type( 'activities', $args );
}
add_action( 'init', 'hugo_register_activities_post_type' );

# adding metabox to enter link
add_action( 'add_meta_boxes', 'hugo_activities_meta_box' );
function hugo_activities_meta_box(){
    add_meta_box( 
        'hugo-activities-link',
        esc_html__( 'Link', 'hugonorrkopng' ),
        'hugo_link_meta_box',
        'activities',
    );   
}

function hugo_link_meta_box( $post ){ ?>
    <?php 
    	// Add an nonce field so we can check for it later.
    	wp_nonce_field( 'hugo_post_type_activities', 'hugo_post_type_activities_nonce' );

    	$link = get_post_meta( $post->ID, 'hugo_activities_link', true );
    ?>
    <input type="text" name="hugo-activities-link" value="<?php echo esc_attr( $link ) ?>" style="width:100%" >
<?php }

function hugo_activities_save( $post ){
	// Verify that the nonce is valid.
	$nonce = $_POST['hugo_post_type_activities_nonce'];
	if ( ! wp_verify_nonce( $nonce, 'hugo_post_type_activities' ) ) {
	    return;
	}

    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ){
       return;
    }

    if( !current_user_can( 'edit_post', $post ) ){
       return;
    }

    if( !isset( $_POST[ 'hugo-activities-link' ] ) ){
       return;
    }

    // Sanitize the user input.
    $link = sanitize_url( $_POST[ 'hugo-activities-link' ] );
    
    // Update the meta field.
    update_post_meta( $post, 'hugo_activities_link', $link );
}

add_action( 'save_post', 'hugo_activities_save' );