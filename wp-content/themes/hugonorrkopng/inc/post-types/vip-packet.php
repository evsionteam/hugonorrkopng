<?php
// Register Custom Post Type
function hugo_register_vip_package_post() {

    $labels = array(
        'name'               => _x( 'VIP Paket', 'post type general name', 'hugonorrkopng' ),
        'singular_name'      => _x( 'VIP Paket', 'post type singular name', 'hugonorrkopng' ),
        'menu_name'          => _x( 'VIP Paket', 'admin menu', 'hugonorrkopng' ),
        'name_admin_bar'     => _x( 'VIP Paket', 'add new on admin bar', 'hugonorrkopng' ),
        'add_new'            => _x( 'Add New', 'VIP Package', 'hugonorrkopng' ),
        'add_new_item'       => __( 'Add New VIP Package', 'hugonorrkopng' ),
        'new_item'           => __( 'New VIP Package', 'hugonorrkopng' ),
        'edit_item'          => __( 'Edit VIP Package', 'hugonorrkopng' ),
        'view_item'          => __( 'View VIP Package', 'hugonorrkopng' ),
        'all_items'          => __( 'All VIP Packages', 'hugonorrkopng' ),
        'search_items'       => __( 'Search VIP Packages', 'hugonorrkopng' ),
        'parent_item_colon'  => __( 'Parent VIP Packages:', 'hugonorrkopng' ),
        'not_found'          => __( 'No VIP Packages found.', 'hugonorrkopng' ),
        'not_found_in_trash' => __( 'No VIP Packages found in Trash.', 'hugonorrkopng' )
    );

    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'menu_icon'          => 'dashicons-businessman',
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'vip-packet' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor' )
    );

    register_post_type( 'vip_package', $args );
}
add_action( 'init', 'hugo_register_vip_package_post', 0 );