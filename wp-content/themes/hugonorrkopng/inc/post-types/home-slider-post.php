<?php
// Register Custom Post Type
function custom_post_type_slider() {

    $labels = array(
        'name'                  => _x( 'Home Slider', 'Post Type General Name', 'text_domain' ),
        'singular_name'         => _x( 'Home Slider', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'             => __( 'Home Slider', 'text_domain' ),
        'name_admin_bar'        => __( 'Home Slider', 'text_domain' ),
        'archives'              => __( 'Item Archives', 'text_domain' ),
        'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
        'all_items'             => __( 'All Items', 'text_domain' ),
        'add_new_item'          => __( 'Add New Item', 'text_domain' ),
        'add_new'               => __( 'Add New', 'text_domain' ),
        'new_item'              => __( 'New Item', 'text_domain' ),
        'edit_item'             => __( 'Edit Item', 'text_domain' ),
        'update_item'           => __( 'Update Item', 'text_domain' ),
        'view_item'             => __( 'View Item', 'text_domain' ),
        'search_items'          => __( 'Search Item', 'text_domain' ),
        'not_found'             => __( 'Not found', 'text_domain' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
        'featured_image'        => __( 'Featured Image', 'text_domain' ),
        'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
        'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
        'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
        'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
        'items_list'            => __( 'Items list', 'text_domain' ),
        'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
        'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
    );
    $args = array(
        'label'                 => __( 'Slider', 'text_domain' ),
        'labels'                => $labels,
        'supports'              => array( 'title','editor','thumbnail' ),
        'hierarchical'          => false,
        'public'                => true,
        'menu_icon'             => 'dashicons-admin-page',
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'home_slider', $args );

}
add_action( 'init', 'custom_post_type_slider', 0 );



//Custom Link

add_action( 'add_meta_boxes', 'meta_slider_custom_link' );
function meta_slider_custom_link()
{
    add_meta_box( 'meta-slider-custom-link', 'Custom Link(Only If Required)', 'meta_slider_callback', 'home_slider', 'normal', 'high' );

}

function meta_slider_callback( $post )
{
    $values1 = get_post_custom( $post->ID );
    $selected1 = isset( $values1['custom_link_slider_meta'] ) ? $values1['custom_link_slider_meta'][0] : '';

    wp_nonce_field( 'custom_link_slider_meta_nonce', 'my_custom_link_slider_meta_nonce' );
    ?>
    <p>
        <label for="custom_link_slider_meta"><p>Insert only if it's redirected to different link than it's own </p></label>
        <input type="text" name="custom_link_slider_meta" id="custom_link_slider_meta" size="90" value="<?php echo $selected1; ?>"></textarea>
    </p>
    <?php   
}

add_action( 'save_post', 'meta_slider_custom_link_save' );
function meta_slider_custom_link_save( $post_id )
{
    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

    // if our nonce isn't there, or we can't verify it, bail
    if( !isset( $_POST['my_custom_link_slider_meta_nonce'] ) || !wp_verify_nonce( $_POST['my_custom_link_slider_meta_nonce'], 'custom_link_slider_meta_nonce' ) ) return;

    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post',$post_id ) ) return;

    // now we can actually save the data
    $allowed = array( 
        'a' => array( // on allow a tags
            'href' => array() // and those anchords can only have href attribute
        )
    );

    // Probably a good idea to make sure your data is set

    if( isset( $_POST['custom_link_slider_meta'] ) )
        update_post_meta( $post_id, 'custom_link_slider_meta', $_POST['custom_link_slider_meta'] );

}


//Button name

add_action( 'add_meta_boxes', 'meta_slider_slider_button' );
function meta_slider_slider_button()
{
    add_meta_box( 'meta-slider-button', 'Button Text', 'meta_slider_callback2', 'home_slider', 'normal', 'high' );
}

function meta_slider_callback2( $post )
{
    $values1 = get_post_custom( $post->ID );
    $selected1 = isset( $values1['slider_button_slider_meta'] ) ? $values1['slider_button_slider_meta'][0] : '';

    wp_nonce_field( 'slider_button_slider_meta_nonce', 'my_slider_button_slider_meta_nonce' );
    ?>
    <p>
        <label for="slider_button_slider_meta"><p>Enter Button Text </p></label>
        <input type="text" name="slider_button_slider_meta" id="slider_button_slider_meta" size="90" value="<?php echo $selected1; ?>"></textarea>
    </p>
    <?php   
}

add_action( 'save_post', 'meta_slider_slider_button_save' );
function meta_slider_slider_button_save( $post_id )
{
    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

    // if our nonce isn't there, or we can't verify it, bail
    if( !isset( $_POST['my_slider_button_slider_meta_nonce'] ) || !wp_verify_nonce( $_POST['my_slider_button_slider_meta_nonce'], 'slider_button_slider_meta_nonce' ) ) return;

    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post', $post_id ) ) return;

    // now we can actually save the data
    $allowed = array( 
        'a' => array( // on allow a tags
            'href' => array() // and those anchords can only have href attribute
        )
    );

    // Probably a good idea to make sure your data is set

    if( isset( $_POST['slider_button_slider_meta'] ) )
        update_post_meta( $post_id, 'slider_button_slider_meta', $_POST['slider_button_slider_meta'] );

}


//Design Select

add_action( 'add_meta_boxes', 'so_custom_meta_box' );

function so_custom_meta_box($post){
    add_meta_box('so_meta_box', 'Slider Design Type', 'slider_design_select_meta_box', 'home_slider', 'normal' , 'high');
}

add_action('save_post', 'so_save_metabox');

function so_save_metabox(){ 
    global $post;
    if(isset($_POST["custom_element_grid_class"])){
         //UPDATE: 
        $meta_element_class = $_POST['custom_element_grid_class'];
        //END OF UPDATE

        update_post_meta($post->ID, 'slider_design_select_meta_box', $meta_element_class);
        //print_r($_POST);
    }
}

function slider_design_select_meta_box($post){
    $meta_element_class = get_post_meta($post->ID, 'slider_design_select_meta_box', true); //true ensures you get just one value instead of an array
    ?>   
    <label>Choose the Design Type of tthe slider :  </label>

    <select name="custom_element_grid_class" id="custom_element_grid_class">
      <option value="design_1" <?php selected( $meta_element_class, 'design_1' ); ?>>Design 1</option>
      <option value="design_2" <?php selected( $meta_element_class, 'design_2' ); ?>>Design 2</option>
    </select>
    <?php
}