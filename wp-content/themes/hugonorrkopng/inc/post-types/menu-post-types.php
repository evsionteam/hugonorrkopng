<?php
// Register Custom Post Type
function custom_post_type_menu() {

    $labels = array(
        'name'                  => _x( 'Menu', 'Post Type General Name', 'text_domain' ),
        'singular_name'         => _x( 'Menu', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'             => __( 'Menu', 'text_domain' ),
        'name_admin_bar'        => __( 'Menu', 'text_domain' ),
        'archives'              => __( 'Item Archives', 'text_domain' ),
        'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
        'all_items'             => __( 'All Items', 'text_domain' ),
        'add_new_item'          => __( 'Add New Item', 'text_domain' ),
        'add_new'               => __( 'Add New', 'text_domain' ),
        'new_item'              => __( 'New Item', 'text_domain' ),
        'edit_item'             => __( 'Edit Item', 'text_domain' ),
        'update_item'           => __( 'Update Item', 'text_domain' ),
        'view_item'             => __( 'View Item', 'text_domain' ),
        'search_items'          => __( 'Search Item', 'text_domain' ),
        'not_found'             => __( 'Not found', 'text_domain' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
        'featured_image'        => __( 'Featured Image', 'text_domain' ),
        'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
        'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
        'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
        'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
        'items_list'            => __( 'Items list', 'text_domain' ),
        'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
        'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
    );
    $args = array(
        'label'                 => __( 'Menu', 'text_domain' ),
        'labels'                => $labels,
        'supports'              => array( 'title','editor','thumbnail','excerpt' ),
        'hierarchical'          => false,
        'public'                => true,
        'menu_icon'             => 'dashicons-clipboard',
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'menu', $args );

}
add_action( 'init', 'custom_post_type_menu', 0 );


class Custom_Post_Type_Image_Upload {


    public function __construct() {

        if ( is_admin() ) {
            add_action( 'admin_init', array( &$this, 'admin_init' ) );
        }
        add_action( 'admin_enqueue_scripts', array($this,'load_admin_things' ) );

    }

    function load_admin_things() {
        wp_enqueue_media();
        /*wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');
        wp_enqueue_style('thickbox');*/
    }



    /** Admin methods ******************************************************/


    /**
     * Initialize the admin, adding actions to properly display and handle
     * the menu custom post type add/edit page
     */
    public function admin_init() {
        global $pagenow;

        if ( $pagenow == 'post-new.php' || $pagenow == 'post.php' || $pagenow == 'edit.php' ) {

            add_action( 'add_meta_boxes', array( &$this, 'meta_boxes' ) );
            add_filter( 'enter_title_here', array( &$this, 'enter_title_here' ), 1, 2 );

            add_action( 'save_post', array( &$this, 'meta_boxes_save' ), 1, 2 );
        }
    }


    /**
     * Save meta boxes
     *
     * Runs when a post is saved and does an action which the write panel save scripts can hook into.
     */
    public function meta_boxes_save( $post_id, $post ) {
        if ( empty( $post_id ) || empty( $post ) || empty( $_POST ) ) return;
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
        if ( is_int( wp_is_post_revision( $post ) ) ) return;
        if ( is_int( wp_is_post_autosave( $post ) ) ) return;
        if ( ! current_user_can( 'edit_post', $post_id ) ) return;
        if ( $post->post_type != 'menu' ) return;

        $this->process_menu_meta( $post_id, $post );
    }


    /**
     * Function for processing and storing all menu data.
     */
    private function process_menu_meta( $post_id, $post ) {
        update_post_meta( $post_id, '_image_id', $_POST['upload_image_id'] );
        if( isset($_POST['menu_page_link']) && !empty($_POST['menu_page_link'])){
            update_post_meta( $post_id, 'menu_page_link', $_POST['menu_page_link'] );
        }
    }


    /**
     * Set a more appropriate placeholder text for the New menu title field
     */
    public function enter_title_here( $text, $post ) {
        if ( $post->post_type == 'menu' ) return __( 'menu Title' );
        return $text;
    }


    /**
     * Add and remove meta boxes from the edit page
     */
    public function meta_boxes() {
        add_meta_box( 'menu-image', __( 'Menu Settings' ), array( &$this, 'menu_image_meta_box' ), 'menu', 'normal', 'high' );
    }


    /**
     * Display the image meta box
     */
    public function menu_image_meta_box() {
        global $post;

        $menu_page_link = get_post_meta( $post->ID,'menu_page_link',true );

        $image_src = '';

        $image_id = get_post_meta( $post->ID, '_image_id', true );
        $image_src = wp_get_attachment_url( $image_id );

        $image = wp_get_attachment_url( $image_id );
        if( $image) : 


        ?>
       <!--  <img id="menu_image" src="<?php echo $image_src ?>" style="max-width:100%;" /> -->

        <p><?php _e('File Name');?>: <span id="pdf-name"><?php echo basename($image);?></span></p>
        <p><?php _e('File URL');?>: <span id="pdf-url"><?php echo $image;?></span></p>
        <?php endif;?>
        <input type="hidden" name="upload_image_id" id="upload_image_id" value="<?php echo ($image_id)? $image_id:''; ?>" />
        <p>            
            <a class="upload-custom-img " title="<?php esc_attr_e( 'Choose PDF To Link:' ) ?>" href="#" id="set-menu-image"><?php _e( 'Set menu pdf' ) ?></a>
            <a class="delete-custom-img" title="<?php esc_attr_e( 'Remove menu pdf' ) ?>" href="#" id="remove-menu-image" style="<?php echo ( ! $image_id ? 'display:none;' : '' ); ?>"><?php _e( 'Remove menu pdf' ) ?></a>
        </p>

        <p>
            <label for="meta-select">
                <?php _e( 'Choose Page To Link:', 'hugonorrkopng' )?>
            </label><br/>
            <?php
            $args = array(
                'name'                  => 'menu_page_link',
                'id'                    => 'menu-page-link',
                'show_option_none'      => '&mdash;&mdash;  Select &mdash;&mdash;',
                'selected'              => $menu_page_link
            );
            wp_dropdown_pages($args);
            ?>
        </p>
        
        

        <script type="text/javascript">
            jQuery(document).ready(function($) {
                // Set all variables to be used in scope
                 var frame,
                     metaBox = $('.postbox-container'), // Your meta box id here
                     addImgLink = metaBox.find('.upload-custom-img'),
                     delImgLink = metaBox.find( '.delete-custom-img'),
                     // imgContainer = metaBox.find( '#menu_image'),
                     imgIdInput = metaBox.find( '#upload_image_id' ),
                     pdfName = metaBox.find('#pdf-name'),
                     pdfUrl = metaBox.find('#pdf-url');

                 // ADD IMAGE LINK
                 addImgLink.on( 'click', function( event ){                   
                   event.preventDefault();                   
                   // If the media frame already exists, reopen it.
                   if ( frame ) {
                     frame.open();
                     return;
                   }
                   
                   // Create a new media frame
                   frame = wp.media({
                     title: 'Select or Upload Media Of Your Chosen Persuasion',
                     button: {
                       text: 'Use this media'
                     },
                     multiple: false  // Set to true to allow multiple files to be selected
                   });

                   
                   // When an image is selected in the media frame...
                   frame.on( 'select', function() {                     
                     // Get media attachment details from the frame state
                     var attachment = frame.state().get('selection').first().toJSON();

                     // Send the attachment URL to our custom image input field.
                     // imgContainer.append( '<img src="'+attachment.url+'" alt="" style="max-width:100%;"/>' );

                     // Send the attachment id to our hidden input
                     imgIdInput.val( attachment.id );

                     pdfName.html(attachment.filename);
                     pdfUrl.html( attachment.url);
                     // pdfLink.html( attachment.link);

                     // Hide the add image link
                     // addImgLink.addClass( 'hidden' );

                     // Unhide the remove image link
                     delImgLink.removeClass( 'hidden' );
                   });

                   // Finally, open the modal on click
                   frame.open();
                 });
                 
                 
                 // DELETE IMAGE LINK
                 delImgLink.on( 'click', function( event ){

                   event.preventDefault();

                   // Clear out the preview image
                   // imgContainer.html( '' );
                       pdfName.html('');
                       pdfUrl.html('');

                   // Un-hide the add image link
                   addImgLink.removeClass( 'hidden' );

                   // Hide the delete image link
                   delImgLink.addClass( 'hidden' );

                   // Delete the image id from the hidden input
                   imgIdInput.val( '' );

                 });
               

            });
        </script>
    <?php
    }
}

// finally instantiate our plugin class and add it to the set of globals
$GLOBALS['custom_post_type_image_upload'] = new Custom_Post_Type_Image_Upload();



function sk_add_menu_meta_box(){
    add_meta_box(
        'featured-menu-meta-select',
        __( 'Feautred Menu' ),
        'featured_menu_meta_select',
        'menu',
        'normal',
        'default'
    );
}
add_action( 'add_meta_boxes_menu', 'sk_add_menu_meta_box' );

function featured_menu_meta_select($post){
    wp_nonce_field( basename( __FILE__ ), 'featured_menu_meta_select' );
    $featured_menu_value = get_post_meta($post->ID,'featured_menu',true);
    ?>
    <div class="featured-menu">
        <label for="featured-menu">
            <input type="checkbox" name="featured_menu" id="featured-menu" value="disabled" <?php if ( isset ( $featured_menu_value ) ) checked( $featured_menu_value, 'yes' ); ?> />
            <?php _e( 'Is featured Menu?', 'hugonorrkopng' )?>
        </label>
    </div>
<?php
}

/**
 * Saves the custom meta input
 */
function featured_menu_meta_save( $post_id ) {
    // Checks save status
    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'featured_menu_meta_select' ] ) && wp_verify_nonce( $_POST[ 'featured_menu_meta_select' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
    // Exits script depending on save status
    if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
        return;
    }

    if( isset( $_POST[ 'featured_menu' ] ) ) {
        update_post_meta( $post_id, 'featured_menu', 'yes' );
    } else {
        update_post_meta( $post_id, 'featured_menu', 'no' );
    }

}
add_action( 'save_post_menu', 'featured_menu_meta_save' ); 


/*
        <?php   $args = array( 'post_type' => 'menu' );
                $query = new WP_Query($args); 
                 while ($query->have_posts()) {
                $query->the_post();?>
        
         <?php } wp_reset_query(); ?>

 */


//Design Select

add_action( 'add_meta_boxes', 'so_custom_meta_box1' );

function so_custom_meta_box1($post){
    add_meta_box('so_meta_box', 'Slider Design Type', 'slider_design_select_meta_box1', 'page', 'normal' , 'high');
    add_meta_box('hugo_button_meta_box', 'Page Button Settings', 'hugo_button_meta_callback', 'page', 'normal' , 'high');
    add_meta_box('hugo_overlay_meta_box', 'Overlay Settings', 'hugo_overlay_meta_callback', 'page', 'normal' , 'high');
}
function slider_design_select_meta_box1($post){
    $meta_menu_selected = get_post_meta($post->ID, 'slider_design_select_meta_box1', true); //true ensures you get just one value instead of an array
    ?>
    <label>Choose the Design Type of the slider :  </label>
    <select name="slider_design_select_meta_box1[]" id="slider_design_select_meta_box1" multiple>
        <option value=""><?php _e('&mdash; Select for Empty &mdash;');?></option>
        <?php   $args = array( 'post_type' => 'menu', 'posts_per_page' => -1 );
        $query = new WP_Query($args);
        if($query->have_posts()){
            while ($query->have_posts()) {
                $query->the_post();?>

                <?php $selected = in_array( get_the_ID(), $meta_menu_selected ) ? ' selected="selected" ' : ''; ?>

                <option value="<?php echo get_the_ID();?>" <?php echo $selected; ?>><?php the_title(); ?></option>

            <?php } } wp_reset_query(); ?>
    </select>
<?php
}

function hugo_button_meta_callback($post){
    $btn_text = get_post_meta($post->ID, 'button_text', true);
    $btn_link = get_post_meta($post->ID, 'button_link', true);
    ?>
    <p>
        <label for="button-text" class="">
            <?php _e( 'Button Text: ', 'hugonorrkopng' )?>
        </label>
        <input type="text" name="button_text" id="button-text" value="<?php echo $btn_text;?>" size="15" />
    </p>
    <p>
        <label for="button-link" class="">
            <?php _e( 'Button Link: ', 'hugonorrkopng' )?>
        </label>
        <input type="text" name="button_link" id="button-link" value="<?php echo $btn_link;?>" size="50" />
    </p>
<?php
}

function hugo_overlay_meta_callback($post){
    $page_overlay = get_post_meta($post->ID, 'page_overlay', true);
    ?>
    <p>
        <input type="checkbox" name="page_overlay" id="page-overlay" value="yes" <?php if ( isset ( $page_overlay ) ) checked( $page_overlay, 'yes' ); ?> />
        <label for="page-overlay"> <?php _e( 'Show Page Overlay', 'hugonorrkopng' )?></label>
    </p>
<?php
}

add_action('save_post', 'so_save_metabox123');
function so_save_metabox123($post_id){

    if(isset($_POST["slider_design_select_meta_box1"])){
        $meta_menu_selected = $_POST['slider_design_select_meta_box1'];
        update_post_meta($post_id, 'slider_design_select_meta_box1', $meta_menu_selected);
    }

    if( isset( $_POST[ 'button_text' ] ) ) {
        update_post_meta( $post_id, 'button_text', sanitize_text_field( $_POST[ 'button_text' ] ) );
    }

    if( isset( $_POST[ 'button_link' ] ) ) {
        update_post_meta( $post_id, 'button_link', sanitize_text_field( $_POST[ 'button_link' ] ) );
    }

    if( isset( $_POST[ 'page_overlay' ] ) ) {
        update_post_meta( $post_id, 'page_overlay', 'yes' );
    } else {
        update_post_meta( $post_id, 'page_overlay', '' );
    }
}

