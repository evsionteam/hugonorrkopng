<?php

function arrangement_js_css_enqueue() {
    global $typenow;
    if( $typenow == 'arrangements' ) {
       //For image upload
        wp_enqueue_media();
        wp_register_script( 'meta-box-image', get_template_directory_uri() . '/js/meta-image.js', array( 'jquery' ) );
        wp_localize_script( 'meta-box-image', 'meta_image',
            array(
                'title' => __( 'Choose or Upload an Image', 'hugonorrkopng' ),
                'button' => __( 'Use this image', 'hugonorrkopng' ),
            )
        );
        wp_enqueue_script( 'meta-box-image' );
    }
}
add_action( 'admin_enqueue_scripts', 'arrangement_js_css_enqueue' );


add_action('add_meta_boxes', 'arrangement_post_meta_boxes', 1);
function arrangement_post_meta_boxes() {
    add_meta_box( 'arrangement-post-meta', __( 'Arrangement Options' ), 'arrangements_settings_display', 'arrangements', 'normal', 'default');
}

function arrangements_settings_display($post) {

    $fb_link = get_post_meta( $post->ID, 'fb_link', true);
    $header_image = get_post_meta( $post->ID, 'header_image', true);
    wp_nonce_field( 'arrangement_meta_box_nonce', 'arrangement_meta_box' );
    ?>
    <p>
        <label for="fb-link"><?php _e( 'Facebook Link: ', 'hugonorrkopng' )?></label><br/>
        <input type="text" name="fb_link" id="fb-link" value="<?php echo $fb_link;?>" size="80" />
    </p>
    <div id="image_field">
        <label for="header-image"><?php _e( 'Header Image: ', 'hugonorrkopng' )?></label><br/>
        <input type="hidden" id="header-image" name="header_image" value="<?php echo $header_image; ?>" />
        <input id="upload_image" type="button" class="button" value="<?php _e( 'Upload Image', 'hugonorrkopng' ); ?>" />
        <?php if ( '' != $header_image): ?>
            <input id="delete_image" type="button" class="button" value="<?php _e( 'Delete Image', 'hugonorrkopng' ); ?>" />
        <?php endif; ?>
    </div>
    <div id="image_preview">
        <p class="description">
            <img style="max-width:300px;" src="<?php echo $header_image; ?>" />
        </p>
    </div>
<?php
}


add_action('save_post', 'arrangement_meta_box_save');
function arrangement_meta_box_save($post_id) {

    if ( ! isset( $_POST['arrangement_meta_box'] ) || ! wp_verify_nonce( $_POST['arrangement_meta_box'], 'arrangement_meta_box_nonce' ) )
        return;

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return;

    if (!current_user_can('edit_post', $post_id))
        return;

    if( isset( $_POST[ 'fb_link' ] ) ) {
        update_post_meta( $post_id, 'fb_link', $_POST[ 'fb_link' ] );
    }

    if( isset( $_POST[ 'header_image' ] ) ) {
        update_post_meta( $post_id, 'header_image', $_POST[ 'header_image' ] );
    }
}
