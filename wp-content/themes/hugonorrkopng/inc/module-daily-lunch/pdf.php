<?php
	add_action('init','print_daily_lunch' );
	function print_daily_lunch(){

		if( isset($_GET['action']) && ( $_GET['action'] == 'print-menu-this-week' || $_GET['action'] == 'print-menu-next-week') ):

            global $hugo_opt;

			$heading = __('LUNCHBUFFE','supporterskalet');

			require_once __DIR__ . '/../tcpdf/config/tcpdf_config.php';
			require_once __DIR__ . '/../tcpdf/tcpdf.php';

            // Extend the TCPDF class to create custom Header and Footer
            class HUGO_PDF extends TCPDF {
                //Page header
                public function Header() {
                    // get the current page break margin
                    $bMargin = $this->getBreakMargin();
                    // get current auto-page-break mode
                    $auto_page_break = $this->AutoPageBreak;
                    // disable auto-page-break
                    $this->SetAutoPageBreak(false, 0);
                    // set background image
                    //$img_file = get_template_directory_uri().'/assets/src/img/menupdf.jpg';
                    //$this->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
                    // restore auto-page-break status
                    $this->SetAutoPageBreak($auto_page_break, $bMargin);
                    // set the starting point for the page content
                    $this->setPageMark();
                }

                /*Page footer*/
                public function Footer() {
                    global $hugo_opt;
                    if(isset($hugo_opt['menu-pdf-footer-image']) && !empty($hugo_opt['menu-pdf-footer-image'])){
                        //$footer_img = wp_make_link_relative($hugo_opt['menu-pdf-footer-image']['url']);
                        $footer_img = $hugo_opt['menu-pdf-footer-image']['url'];
                        $this->Image($footer_img, 0, 250, 210 , 0, 'PNG', '', '', false, 300, '', false, false, 0, 'LB', false, false);
                    }
                }
            }

			$pdf = new HUGO_PDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

			$pdf->SetCreator(get_bloginfo('name'));
			$pdf->SetAuthor(get_bloginfo('name'));
			$pdf->SetTitle($heading);
			$pdf->SetSubject($heading);

			//$pdf->setPrintHeader(false);
			//$pdf->setPrintFooter(false);

			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

			$pdf->SetMargins(0, 0, 0);
			//$pdf->SetMargins(PDF_MARGIN_LEFT, 0, PDF_MARGIN_RIGHT);

			$pdf->SetAutoPageBreak(FALSE, 0);

			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

			if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			    require_once(dirname(__FILE__).'/lang/eng.php');
			    $pdf->setLanguageArray($l);
			}

			$pdf->setFontSubsetting(true);

			$pdf->setTextShadow(array('enabled'=>false, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

			$header = file_get_contents( __DIR__.'/parts/header.html' );


            $body = file_get_contents( __DIR__.'/parts/body.html' );

            $menu_description = $menu_price_info = $menu_footer_info =  $menu_logo = '';
            if(isset($hugo_opt['menu-pdf-desc'])){
                $menu_description = $hugo_opt['menu-pdf-desc'];
            }
            if(isset($hugo_opt['menu-pdf-price-info'])){
                $menu_price_info = $hugo_opt['menu-pdf-price-info'];
            }

            if(isset($hugo_opt['menu-pdf-footer-info'])){
                $menu_footer_info = $hugo_opt['menu-pdf-footer-info'];
            }

            if($_GET['action'] == 'print-menu-this-week'){
                $date = strtotime(date('F j, Y'));
                $week = date("W");
            }else{
                $date = strtotime('next monday');
                $week = date('W', $date);
            }

            $menu_items = get_daily_lunch_menu_items($date);
            $menu_items = str_replace('<div class="clearfix"></div>','',$menu_items);

            if(isset($hugo_opt['menu-pdf-logo-image']) && !empty($hugo_opt['menu-pdf-logo-image'])){
                //$logo_img = wp_make_link_relative($hugo_opt['menu-pdf-logo-image']['url']);
                $logo_img = $hugo_opt['menu-pdf-logo-image']['url'];
                $menu_logo = '<img width="180" src="'.$logo_img.'">';
            }

            $menu_week = 'VECKANS LUNCH V '.$week;

            $body = str_replace('@menu_logo@', $menu_logo, $body);
            $body = str_replace('@menu_week@', $menu_week, $body);
            $body = str_replace('@menu_description@', $menu_description, $body);
            $body = str_replace('@menu_price_info@', $menu_price_info, $body);
            $body = str_replace('@menu_footer_info@', $menu_footer_info, $body);
            $body = str_replace('@menu_items@', $menu_items, $body);

            $footer = file_get_contents( __DIR__.'/parts/footer.html' );

            $html = $header.$body.$footer;
            $html = str_replace("background-color:transparent","background-color:white", $html);

            $pdf->AddPage();
            $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

			$output_type = 'I';
			if(isset($_GET['mobile'])){
				$output_type = 'D';
			}
			$title = 'vecka-'.$week.'.pdf';
            //print_r($html);
            //ob_end_clean();
			$pdf->Output($title, $output_type);
            exit();

		endif;
	}

