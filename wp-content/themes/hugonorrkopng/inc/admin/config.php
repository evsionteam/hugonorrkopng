<?php
    
    require_once __DIR__.'/ReduxCore/framework.php';

    /**
     * ReduxFramework Barebones Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }

    // This is your option name where all the Redux data is stored.
    $opt_name = "hugo_opt";

    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'           => __( 'Hugonorrkopng', 'hugonorrkopng' ),
        'page_title'           => __( 'Hugonorrkopng', 'hugonorrkopng' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => '',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => true,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => true,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-portfolio',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => '',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => true,
        // Show the time the page took to load, etc
        'update_notice'        => true,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => true,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => '',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '_options',
        // Page slug used to denote the panel
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
        // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!

        'use_cdn'              => true,
        // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

        //'compiler'             => true,

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'light',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

    // Panel Intro text -> before the form
    if ( ! isset( $args['global_variable'] ) || $args['global_variable'] !== false ) {
        if ( ! empty( $args['global_variable'] ) ) {
            $v = $args['global_variable'];
        } else {
            $v = str_replace( '-', '_', $args['opt_name'] );
        }
        $args['intro_text'] = sprintf( __( '<p>Did you know that Redux sets a global variable for you? To access any of your saved options from within your code you can use your global variable: <strong>$%1$s</strong></p>', 'hugonorrkopng' ), $v );
    } else {
        $args['intro_text'] = __( '<p>This text is displayed above the options panel. It isn\'t required, but more info is always better! The intro_text field accepts all HTML.</p>', 'hugonorrkopng' );
    }

    // Add content after the form.
    $args['footer_text'] = __( '<p>This text is displayed below the options panel. It isn\'t required, but more info is always better! The footer_text field accepts all HTML.</p>', 'hugonorrkopng' );

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */

    /*
     *
     * ---> START SECTIONS
     *
     */

    /*

        As of Redux 3.5+, there is an extensive API. This API can be used in a mix/match mode allowing for


     */

    // -> START Basic Fields
    Redux::setSection( $opt_name, array(
        'title'  => __( 'Header Section', 'hugonorrkopng' ),
        'id'     => 'basic',
        'desc'   => __( 'Basic field with no subsections.', 'hugonorrkopng' ),
        'icon'   => 'el el-home',
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Fav Icons', 'hugonorrkopng' ),
        'id'         => 'opt-favicon-subsection',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'favicon-16',
                'type'     => 'media',
                'title'    => __( 'Favicon (16x16)', 'hugonorrkopng' ),
            ),
            array(
                'id'       => 'favicon-32',
                'type'     => 'media',
                'title'    => __( 'Favicon (32x32)', 'hugonorrkopng' ),
            ),
            array(
                'id'       => 'favicon-96',
                'type'     => 'media',
                'title'    => __( 'Favicon (96x96)', 'hugonorrkopng' ),
            ),

            array(
                'id'       => 'apple-icon-57',
                'type'     => 'media',
                'title'    => __( 'Apple Icon (57x57)', 'hugonorrkopng' ),
            ),
            array(
                'id'       => 'apple-icon-60',
                'type'     => 'media',
                'title'    => __( 'Apple Icon (60x60)', 'hugonorrkopng' ),
            ),
            array(
                'id'       => 'apple-icon-72',
                'type'     => 'media',
                'title'    => __( 'Apple Icon (72x72)', 'hugonorrkopng' ),
            ),
            array(
                'id'       => 'apple-icon-76',
                'type'     => 'media',
                'title'    => __( 'Apple Icon (76x76)', 'hugonorrkopng' ),
            ),
            array(
                'id'       => 'apple-icon-114',
                'type'     => 'media',
                'title'    => __( 'Apple Icon (114x114)', 'hugonorrkopng' ),
            ),
            array(
                'id'       => 'apple-icon-120',
                'type'     => 'media',
                'title'    => __( 'Apple Icon (120x120)', 'hugonorrkopng' ),
            ),
            array(
                'id'       => 'apple-icon-144',
                'type'     => 'media',
                'title'    => __( 'Apple Icon (144x144)', 'hugonorrkopng' ),
            ),
            array(
                'id'       => 'apple-icon-152',
                'type'     => 'media',
                'title'    => __( 'Apple Icon (152x152)', 'hugonorrkopng' ),
            ),
            array(
                'id'       => 'apple-icon-180',
                'type'     => 'media',
                'title'    => __( 'Apple Icon (180x180)', 'hugonorrkopng' ),
            ),
            array(
                'id'       => 'android-icon-192',
                'type'     => 'media',
                'title'    => __( 'Android Icon (192x192)', 'hugonorrkopng' ),
            ),
            array(
                'id'       => 'favicon-theme-color',
                'type'     => 'color',
                'title'    => __('Favicon Background Color', 'hugonorrkopng'),
                'subtitle' => __('Pick a background color for Windows shortcut', 'hugonorrkopng'),
                'default'  => '#FFFFFF',
            ),
            array(
                'id'       => 'favicon-title-color',
                'type'     => 'color',
                'title'    => __('Favicon Title Color', 'hugonorrkopng'), 
                'subtitle' => __('Pick a title color for Windows shortcut', 'hugonorrkopng'),
                'default'  => '#333333',
            )
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Banner Section', 'hugonorrkopng' ),
        /*'desc'       => __( 'Enter Details of the banner section'),*/
        'id'         => 'opt-text-subsection',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'top-header-image',
                'type'     => 'media',
                'title'    => __( 'Top Header Image', 'hugonorrkopng' ),
                'desc'     => __( 'Image shown at the top of page', 'hugonorrkopng' ),
            ),

             array(
                'id'       => 'mobile-top-header-image',
                'type'     => 'media',
                'title'    => __( 'Top Header Image (Mobile)', 'hugonorrkopng' ),
                'desc'     => __( 'Image shown at the top of page in mobile', 'hugonorrkopng' ),
            ),

            array(
                'id'       => 'logo-image',
                'type'     => 'media',
                'title'    => __( 'Site Logo', 'hugonorrkopng' ),
                /*'subtitle' => __( 'Subtitle', 'hugonorrkopng' ),*/
                'desc'     => __( 'Insert Site Logo', 'hugonorrkopng' ),
            ),
            array(
                'id'       => 'banner-title',
                'type'     => 'text',
                'title'    => __( 'Banner Title', 'hugonorrkopng' ),
                /*'subtitle' => __( 'Subtitle', 'hugonorrkopng' ),*/
                'desc'     => __( 'Enter Title Text for Banner Image', 'hugonorrkopng' ),
            ),
            array(
                'id'       => 'banner-sub-title',
                'type'     => 'text',
                'title'    => __( 'Banner Sub Title', 'hugonorrkopng' ),
                /*'subtitle' => __( 'Subtitle', 'hugonorrkopng' ),*/
                'desc'     => __( 'Enter Sub Title Text for Banner Image', 'hugonorrkopng' ),
            ),
            array(
                'id'       => 'banner-image',
                'type'     => 'media',
                'title'    => __( 'Banner Image', 'hugonorrkopng' ),
                /*'subtitle' => __( 'Subtitle', 'hugonorrkopng' ),*/
                'desc'     => __( 'Insert Banner Image', 'hugonorrkopng' ),
            ),
            array(
                'id'       => 'banner-video',
                'type'     => 'text',
                'title'    => __( 'Banner Video', 'hugonorrkopng' ),
                /*'subtitle' => __( 'Subtitle', 'hugonorrkopng' ),*/
                'desc'     => __( 'Insert Banner Video', 'hugonorrkopng' ),
            ),
            array(
                'id'       => 'loader-image',
                'type'     => 'media',
                'title'    => __( 'Loader Image', 'hugonorrkopng' ),
                /*'subtitle' => __( 'Subtitle', 'hugonorrkopng' ),*/
                'desc'     => __( 'Insert Loader Image', 'hugonorrkopng' ),
                'mode'    => false,
            ),
            array(
                'id'       => 'video-overlay',
                'type'     => 'checkbox',
                'title'    => __( 'Show video Overlay?', 'hugonorrkopng' ),
            ),
        )
    ) );

    /*
     * <--- END SECTIONS
     */

    /*Opening Hours*/
    Redux::setSection( $opt_name, array(
        'title'  => __( 'Bordsbokning Section', 'hugonorrkopng' ),
        'id'     => 'bordsbokning-section',
        'icon'   => 'el el-home',
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'BORDSBOKNING', 'hugonorrkopng' ),
        'id'         => 'bordsbokning',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'bordsbokning-title',
                'type'     => 'text',
                'title'    => __( 'Bordsbokning Title', 'hugonorrkopng' ),
            ),
            array(
                'id'       => 'bordsbokning-description',
                'type'     => 'textarea',
                'title'    => __( 'Bordsbokning Description', 'hugonorrkopng' ),
            ),
        )
    ) );
    /**/

    /*Slider settings*/
    Redux::setSection( $opt_name, array(
        'title'  => __( 'Slider Settings', 'hugonorrkopng' ),
        'id'     => 'slider-settings',
        'icon'   => 'el el-home',
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Slider', 'hugonorrkopng' ),
        'id'         => 'slider',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'slider-overlay',
                'type'     => 'checkbox',
                'title'    => __( 'Show slider Overlay?', 'hugonorrkopng' ),
            ),
        )
    ) );
    /**/

    /*fornoles skull*/
    Redux::setSection( $opt_name, array(
        'title'  => __( 'Om Restaurangen Section', 'hugonorrkopng' ),
        'id'     => 'fornoles-skull-section',
        'icon'   => 'el el-home',
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Om Restaurangen', 'hugonorrkopng' ),
        'id'         => 'fornoles-skull',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'fornoles-skull-title',
                'type'     => 'text',
                'title'    => __( 'Om Restaurangen Title', 'hugonorrkopng' ),
            ),
            array(
                'id'       => 'fornoles-skull-description',
                'type'     => 'textarea',
                'title'    => __( 'Om Restaurangen Description', 'hugonorrkopng' ),
            ),
        )
    ) );
    /**/

    /**/
    Redux::setSection( $opt_name, array(
        'title'  => __( 'Restaurang Section', 'hugonorrkopng' ),
        'id'     => 'restaurang-section',
        'icon'   => 'el el-home',
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'RESTAURANG', 'hugonorrkopng' ),
        'id'         => 'restaurang',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'restaurang-title',
                'type'     => 'text',
                'title'    => __( 'RESTAURANG Title', 'hugonorrkopng' ),
            ),
        )
    ) );
    /**/

    /*Arrangemang section*/
    Redux::setSection( $opt_name, array(
        'title'  => __( 'Arrangemang Section', 'hugonorrkopng' ),
        'id'     => 'arrangemang-section',
        'icon'   => 'el el-home',
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Arrangemang', 'hugonorrkopng' ),
        'id'         => 'arrangemang',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'arrangemang-title',
                'type'     => 'text',
                'title'    => __( 'Arrangemang Title', 'hugonorrkopng' ),
            ),
            array(
                'id'       => 'arrangemang-image',
                'type'     => 'media',
                'title'    => __( 'Arrangemang Image', 'hugonorrkopng' ),
            ),
            array(
                'id'       => 'arrangemang-img-overlay',
                'type'     => 'checkbox',
                'title'    => __( 'Show Image Overlay?', 'hugonorrkopng' ),
            ),
            array(
                'id'       => 'arrangemang-archive-desc',
                'type'     => 'textarea',
                'title'    => __( 'Arrangemang archive page description', 'hugonorrkopng' ),
            ),
        )
    ) );
    /**/


    /*VIP Packets section*/
    Redux::setSection( $opt_name, array(
        'title'  => __( 'VIP Packets Section', 'hugonorrkopng' ),
        'id'     => 'vip-packets-section',
        'icon'   => 'el el-home',
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'VIP Packets', 'hugonorrkopng' ),
        'id'         => 'vip-packets',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'vip-packets-title',
                'type'     => 'text',
                'title'    => __( 'VIP Packets Title', 'hugonorrkopng' ),
            ),
            array(
                'id'       => 'vip-packets-image',
                'type'     => 'media',
                'title'    => __( 'VIP Packets Image', 'hugonorrkopng' ),
            ),
            array(
                'id'       => 'vip-packets-img-overlay',
                'type'     => 'checkbox',
                'title'    => __( 'Show Image Overlay?', 'hugonorrkopng' ),
            ),
            array(
                'id'       => 'vip-packets-archive-desc',
                'type'     => 'textarea',
                'title'    => __( 'VIP Packets archive page description', 'hugonorrkopng' ),
            ),
        )
    ) );
    /**/

     // -> START Basic Fields
    Redux::setSection( $opt_name, array(
        'title'  => __( 'Homepage Blocks', 'hugonorrkopng' ),
        'id'     => 'homepag-blocks',
        'desc'   => __( 'Basic field with no subsections.', 'hugonorrkopng' ),
        'icon'   => 'el el-home',
/*        'fields' => array(
            array(
                'id'       => 'opt-text',
                'type'     => 'text',
                'title'    => __( 'Example Text', 'hugonorrkopng' ),
                'desc'     => __( 'Example description.', 'hugonorrkopng' ),
                'subtitle' => __( 'Example subtitle.', 'hugonorrkopng' ),
                'hint'     => array(
                    'content' => 'This is a <b>hint</b> tool-tip for the text field.<br/><br/>Add any HTML based text you like here.',
                )
            )
        )*/
    ) );

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Homepage Blocks', 'hugonorrkopng' ),
        /*'desc'       => __( 'Enter Details of the banner section'),*/
        'id'         => 'opt-text-subsection9',
        'subsection' => true,
        'fields'     => array(
            array(
               'id'       => 'homepage-block-1',
                'type'     => 'select',
                'title'    => __('Select Block One Page', 'hugonorrkopng'), 
                'data'     => 'pages'
            ),
            array(
               'id'       => 'homepage-block-2',
                'type'     => 'select',
                'title'    => __('Select Block Two Page', 'hugonorrkopng'), 
                'data'     => 'pages'
            ),
            array(
               'id'       => 'homepage-block-3',
                'type'     => 'select',
                'title'    => __('Select Block Third Page', 'hugonorrkopng'), 
                'data'     => 'pages'
            ),
            array(
               'id'       => 'homepage-block-4',
                'type'     => 'select',
                'title'    => __('Select Block Four Page', 'hugonorrkopng'), 
                'data'     => 'pages'
            ),

        )
    ) );

    /*
     * <--- END SECTIONS
     */


/*Opening Hours*/
Redux::setSection( $opt_name, array(
    'title'  => __( 'Opening Hours', 'hugonorrkopng' ),
    'id'     => 'opening-hours',
    'desc'   => __( 'Opening Hours.', 'hugonorrkopng' ),
    'icon'   => 'el el-home',
) );

Redux::setSection( $opt_name, array(
    'title'      => __( 'Restaurang Hours', 'hugonorrkopng' ),
    'id'         => 'restaurang-hours',
    'subsection' => true,
    'fields'     => array(
         array(
            'id'=>'restaurang-hours-val',
            'type' => 'multi_text',
            'title' => __('Day and Hours', 'hugonorrkopng'),
            'subtitle' => __('Format: Day | Time ( Eg : Sunday | 11.00-23.00 )', 'hugonorrkopng'),
        ),
    )
) );
Redux::setSection( $opt_name, array(
    'title'      => __( 'Bowling Hours', 'hugonorrkopng' ),
    'id'         => 'bowling-hours',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'=>'bowling-hours-val',
            'type' => 'multi_text',
            'title' => __('Day and Hours', 'hugonorrkopng'),
            'subtitle' => __('Format: Day | Time ( Eg : Sunday | 11.00-23.00 )', 'hugonorrkopng'),
        ),
    )
) );
Redux::setSection( $opt_name, array(
    'title'      => __( 'Nattklubb Hours', 'hugonorrkopng' ),
    'id'         => 'nightclub-hours',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'=>'nightclub-hours-val',
            'type' => 'multi_text',
            'title' => __('Day and Hours', 'hugonorrkopng'),
            'subtitle' => __('Format: Day | Time ( Eg : Sunday | 11.00-23.00 )', 'hugonorrkopng'),
        ),
    )
) );
/**/

/*Contact Section*/
Redux::setSection( $opt_name, array(
    'title'  => __( 'Contact Section', 'hugonorrkopng' ),
    'id'     => 'contact-section',
    'icon'   => 'el el-home',
) );

Redux::setSection( $opt_name, array(
    'title'      => __( 'Contact', 'hugonorrkopng' ),
    'id'         => 'contact',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'contact-form',
            'type'     => 'text',
            'title'    => __( 'Contact form shortcode', 'hugonorrkopng' ),
        ),
    )
) );
/**/

/*Daily Lunch Section*/
Redux::setSection( $opt_name, array(
    'title'  => __( 'Dagens lunch Section', 'hugonorrkopng' ),
    'id'     => 'daily-lunch-section',
    'icon'   => 'el el-home',
) );

Redux::setSection( $opt_name, array(
    'title'      => __( 'Dagens lunch Page', 'hugonorrkopng' ),
    'id'         => 'daily-lunch',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'daily-lunch-image',
            'type'     => 'media',
            'title'    => __( 'Dagens lunch Homepage Image', 'hugonorrkopng' ),
        ),
        array(
            'id'       => 'daily-lunch-title',
            'type'     => 'text',
            'title'    => __( 'Dagens lunch Homepage Title', 'hugonorrkopng' ),
        ),
        array(
            'id'       => 'daily-lunch-desc',
            'type'     => 'textarea',
            'title'    => __( 'Dagens lunch Homepage Description', 'hugonorrkopng' ),
        ),
        array(
            'id'       => 'daily-lunch-btn-text',
            'type'     => 'text',
            'title'    => __( 'Dagens lunch Homepage Button Text', 'hugonorrkopng' ),
        ),
        array(
            'id'       => 'daily-lunch-btn-link',
            'type'     => 'text',
            'title'    => __( 'Dagens lunch Homepage Button Link', 'hugonorrkopng' ),
        ),
        array(
            'id'       => 'daily-lunch-pdf-desc',
            'type'     => 'textarea',
            'title'    => __( 'Dagens lunch main page PDF Download Description', 'hugonorrkopng' ),
        ),
    )
) );
/**/

/**/
/*Four Column Image Section*/
Redux::setSection( $opt_name, array(
    'title'  => __( 'Four Column Image', 'hugonorrkopng' ),
    'id'     => 'four-col-img-section',
    'icon'   => 'el el-home',
) );

Redux::setSection( $opt_name, array(
    'title'      => __( 'First Column Section', 'hugonorrkopng' ),
    'id'         => 'first-col',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'first-col-image',
            'type'     => 'media',
            'title'    => __( 'Image', 'hugonorrkopng' ),
        ),
        array(
            'id'       => 'first-col-img-title',
            'type'     => 'text',
            'title'    => __( 'Title', 'hugonorrkopng' ),
        ),
        array(
            'id'       => 'first-col-img-desc',
            'type'     => 'textarea',
            'title'    => __( 'Description', 'hugonorrkopng' ),
        ),
        array(
            'id'       => 'first-col-btn-text',
            'type'     => 'text',
            'title'    => __( 'Button Text', 'hugonorrkopng' ),
        ),
        array(
            'id'       => 'first-col-btn-link',
            'type'     => 'text',
            'title'    => __( 'Button Link', 'hugonorrkopng' ),
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'      => __( 'Second Column Section', 'hugonorrkopng' ),
    'id'         => 'second-col',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'second-col-image',
            'type'     => 'media',
            'title'    => __( 'Image', 'hugonorrkopng' ),
        ),
        array(
            'id'       => 'second-col-img-title',
            'type'     => 'text',
            'title'    => __( 'Title', 'hugonorrkopng' ),
        ),
        array(
            'id'       => 'second-col-img-desc',
            'type'     => 'textarea',
            'title'    => __( 'Description', 'hugonorrkopng' ),
        ),
        array(
            'id'       => 'second-col-btn-text',
            'type'     => 'text',
            'title'    => __( 'Button Text', 'hugonorrkopng' ),
        ),
        array(
            'id'       => 'second-col-btn-link',
            'type'     => 'text',
            'title'    => __( 'Button Link', 'hugonorrkopng' ),
        ),
    )
) );
Redux::setSection( $opt_name, array(
    'title'      => __( 'Third Column Section', 'hugonorrkopng' ),
    'id'         => 'third-col',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'third-col-image',
            'type'     => 'media',
            'title'    => __( 'Image', 'hugonorrkopng' ),
        ),
        array(
            'id'       => 'third-col-img-title',
            'type'     => 'text',
            'title'    => __( 'Title', 'hugonorrkopng' ),
        ),
        array(
            'id'       => 'third-col-img-desc',
            'type'     => 'textarea',
            'title'    => __( 'Description', 'hugonorrkopng' ),
        ),
        array(
            'id'       => 'third-col-btn-text',
            'type'     => 'text',
            'title'    => __( 'Button Text', 'hugonorrkopng' ),
        ),
        array(
            'id'       => 'third-col-btn-link',
            'type'     => 'text',
            'title'    => __( 'Button Link', 'hugonorrkopng' ),
        ),
    )
) );
Redux::setSection( $opt_name, array(
    'title'      => __( 'Fourth Column Section', 'hugonorrkopng' ),
    'id'         => 'fourth-col',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'fourth-col-image',
            'type'     => 'media',
            'title'    => __( 'Image', 'hugonorrkopng' ),
        ),
        array(
            'id'       => 'fourth-col-img-title',
            'type'     => 'text',
            'title'    => __( 'Title', 'hugonorrkopng' ),
        ),
        array(
            'id'       => 'fourth-col-img-desc',
            'type'     => 'textarea',
            'title'    => __( 'Description', 'hugonorrkopng' ),
        ),
        array(
            'id'       => 'fourth-col-btn-text',
            'type'     => 'text',
            'title'    => __( 'Button Text', 'hugonorrkopng' ),
        ),
        array(
            'id'       => 'fourth-col-btn-link',
            'type'     => 'text',
            'title'    => __( 'Button Link', 'hugonorrkopng' ),
        ),
    )
) );
/**/
/**/

/*Bottom Image Section*/
Redux::setSection( $opt_name, array(
    'title'  => __( 'Bottom image Section', 'hugonorrkopng' ),
    'id'     => 'partybowling-section',
    'icon'   => 'el el-home',
) );

Redux::setSection( $opt_name, array(
    'title'      => __( 'Bottom Image Section', 'hugonorrkopng' ),
    'id'         => 'party-bowling',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'party-bowling-image',
            'type'     => 'media',
            'title'    => __( 'Image', 'hugonorrkopng' ),
        ),
        array(
            'id'       => 'bowling-img-overlay',
            'type'     => 'checkbox',
            'title'    => __( 'Show Image Overlay?', 'hugonorrkopng' ),
        ),
        array(
            'id'       => 'party-bowling-title',
            'type'     => 'text',
            'title'    => __( 'Title', 'hugonorrkopng' ),
        ),
        array(
            'id'       => 'party-bowling-desc',
            'type'     => 'textarea',
            'title'    => __( 'Description', 'hugonorrkopng' ),
        ),
        array(
            'id'       => 'party-bowling-btn-text',
            'type'     => 'text',
            'title'    => __( 'Button Text', 'hugonorrkopng' ),
        ),
        array(
            'id'       => 'party-bowling-btn-link',
            'type'     => 'text',
            'title'    => __( 'Button Link', 'hugonorrkopng' ),
        ),
    )
) );
/**/

/*Menu PDF Section*/
Redux::setSection( $opt_name, array(
    'title'  => __( 'Menu PDF Section', 'hugonorrkopng' ),
    'id'     => 'menu-pdf-section',
    'icon'   => 'el el-home',
) );

Redux::setSection( $opt_name, array(
    'title'      => __( 'Daily Lunch Menu PDF', 'hugonorrkopng' ),
    'id'         => 'daily-lunch-menu-pdf',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'menu-pdf-logo-image',
            'type'     => 'media',
            'title'    => __( 'Menu PDF Logo Image', 'hugonorrkopng' ),
        ),
        array(
            'id'       => 'menu-pdf-desc',
            'type'     => 'textarea',
            'title'    => __( 'Menu PDF Description', 'hugonorrkopng' ),
        ),
        array(
            'id'       => 'menu-pdf-price-info',
            'type'     => 'textarea',
            'title'    => __( 'Menu PDF Price Info', 'hugonorrkopng' ),
        ),
        array(
            'id'       => 'menu-pdf-footer-info',
            'type'     => 'textarea',
            'title'    => __( 'Menu PDF Footer Info', 'hugonorrkopng' ),
        ),
        array(
            'id'       => 'menu-pdf-footer-image',
            'type'     => 'media',
            'title'    => __( 'Menu PDF Footer Image', 'hugonorrkopng' ),
        ),
    )
) );
/**/

/*Social Section*/
Redux::setSection( $opt_name, array(
    'title'  => __( 'Social Section', 'hugonorrkopng' ),
    'id'     => 'social-section',
    'icon'   => 'el el-home',
) );

Redux::setSection( $opt_name, array(
    'title'      => __( 'Social Section', 'hugonorrkopng' ),
    'id'         => 'site-social-section',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'fb-link',
            'type'     => 'text',
            'title'    => __( 'FaceBook Link', 'hugonorrkopng' ),
        ),
        array(
            'id'       => 'insta-link',
            'type'     => 'text',
            'title'    => __( 'Instagram Link', 'hugonorrkopng' ),
        ),
    )
) );
/**/