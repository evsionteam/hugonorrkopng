<?php

add_action('wp_ajax_nopriv_load_page_results', 'hugo_load_page_results');
add_action('wp_ajax_load_page_results', 'hugo_load_page_results');
function hugo_load_page_results() {

    global $hugo_opt;

    $output = [
        'status' => esc_html__('404', 'hugonorrkopng'),
        'message' => esc_html__('Post Not Found', 'hugonorrkopng'),
        'data' => ''
    ];

    $page_id = $_POST['page_id'];

    $temp = get_post($page_id);
    if ($temp) {

        $menu_output = array();

        $output['data']['post_title'] = $temp->post_title;
        $output['data']['post_content'] = apply_filters('the_content', $temp->post_content);
        $output['data']['post_image_url'] = wp_get_attachment_url(get_post_thumbnail_id($page_id));
        $output['page_id'] = $page_id;
        $output['status'] = esc_html__('200', 'hugonorrkopng');
        $output['message'] = esc_html__('Post Found', 'hugonorrkopng');

        $btn_text = get_post_meta($page_id, 'button_text', true);
        $btn_link = get_post_meta($page_id, 'button_link', true);

        if (!empty($btn_link) && !empty($btn_text)) {
            $output['data']['btn_html'] = '<a href="' . $btn_link . '" class="restro-btn">' . $btn_text . '</a>';
        }

        $selected_menu = get_post_meta($page_id, 'slider_design_select_meta_box1', true);
        if (!empty($selected_menu)):
            $args = array('post__in' => $selected_menu, 'post_type' => 'menu');
            $query = new WP_Query($args);
            if ($query->have_posts()):

                while ($query->have_posts()): $query->the_post();
                    global $post;
                    $temp_array = array();
                    $pdf_link = '';
                    $attach_pdf_id = get_post_meta(get_the_ID(), '_image_id', true);
                    if (!empty($attach_pdf_id)) {
                        $pdf_link = wp_get_attachment_url($attach_pdf_id);
                    }

                    $temp_array[] = '
                                            <a href="' . $pdf_link . '">
                                                <div class="resturant-img">
                                                    <img src="' . wp_get_attachment_url(get_post_thumbnail_id(get_the_ID())) . '" alt="image">
                                                </div>
                                            </a>
                                            <a href="' . $pdf_link . '">
                                                <div class="resturant-title">' . get_the_title() . '</div>
                                            </a>
                                            <p>' . apply_filters('the_content', $post->post_content) . '</p>
                                            <a href="' . $pdf_link . '" class="see-more">
                                                Se meny
                                            </a>
                                            ';
                    $menu_output[] = $temp_array;
                endwhile;
                wp_reset_query();
            endif;

            $output['menu_data'] = $menu_output;
        endif;

        /* check if is contact page */
        if ('kontakt' == $temp->post_name) {
            $output['contact_page_data']['opening_hours'] = do_shortcode('[opening_hours]');
            if (isset($hugo_opt['contact-form'])) {
                $output['contact_page_data']['contact_form'] = do_shortcode('[contact-form-7 id="229" title="Contact form"]');
            }
        }
        /**/

        /* check if is daily lunch page */
        if ('dagens-lunch' == $temp->post_name) {

            $heading = $daily_lunch_pdf_output = '';

            $current_weekNumber = date("W");
            $current_date = strtotime(date('F j, Y'));

            $next_monday = strtotime('next monday');
            $next_monday_weekNumber = date('W', $next_monday);

            $current_week_menu_items = get_daily_lunch_menu_items($current_date);
            $next_week_menu_items = get_daily_lunch_menu_items($next_monday);

            $daily_lunch_pdf_output .= '<div class="daily-lunch-pdf-wrapper">';

            if (!empty($current_week_menu_items)) {
                $heading = '<div class="current-week">Lunchmeny vecka ' . $current_weekNumber . '</div>';
                $daily_lunch_pdf_output .= '<div class="current-week-pdf week-pdf">';
                $daily_lunch_pdf_output .= '<div class="title">Lunchmeny vecka ' . $current_weekNumber . '</div>';
                $daily_lunch_pdf_output .= '<div class="download-info">Download Info Here</div>';
                $daily_lunch_pdf_output .= '<a href="' . get_site_url('', '?action=print-menu-this-week') . '" class="download-link" target="_blank">Ladda ner PDF</a>';
                $daily_lunch_pdf_output .= '</div>';
            }

            if (!empty($next_week_menu_items)) {
                $daily_lunch_pdf_output .= '<div class="next-week-pdf week-pdf">';
                $daily_lunch_pdf_output .= '<div class="title">Lunchmeny vecka ' . $next_monday_weekNumber . '</div>';
                $daily_lunch_pdf_output .= '<div class="download-info">Download Info Here</div>';
                $daily_lunch_pdf_output .= '<a href="' . get_site_url('', '?action=print-menu-next-week') . '" class="download-link" target="_blank">Ladda ner PDF</a>';
                $daily_lunch_pdf_output .= '</div>';
            }

            $daily_lunch_pdf_output .= '</div>';

            /* daily lunch menu */
            $output['lunch_page_data']['daily_menu'] = $heading . $current_week_menu_items;
            /**/

            /* daily lunch desc */
            if (isset($hugo_opt['daily-lunch-desc'])) {
                $output['lunch_page_data']['daily_lunch_desc'] = wpautop($hugo_opt['daily-lunch-desc']);
            }

            /* daily lunch pdf */
            $output['lunch_page_data']['daily_lunch_pdf'] = $daily_lunch_pdf_output;
            /**/
        }
        /**/

        echo json_encode($output);
        exit;
    }
}


/* Get daily lunch menu items of the week as per date */
add_action('wp_ajax_nopriv_get_daily_lunch_menu_items', 'get_daily_lunch_menu_items');
add_action('wp_ajax_get_daily_lunch_menu_items', 'get_daily_lunch_menu_items');

function get_daily_lunch_menu_items($date) {

    if (empty($date)) {
        /* if from ajax call */
        $date = $_POST['menu_date'];
        /**/
        if (empty($date)) {
            return '';
        }
    }

    $menu_items = '';
    $args = array(
        'post_type' => 'daily_lunch',
        'posts_per_page' => 1,
        'meta_query' => array(
            'relation' => 'AND',
            array(
                'key' => 'daily_lunch_start_date',
                'value' => $date,
                'compare' => '<=',
            ),
            array(
                'key' => 'daily_lunch_end_date',
                'value' => $date,
                'compare' => '>=',
            ),
        ),
    );
    $query = new WP_Query($args);
    if ($query->have_posts()):
        while ($query->have_posts()):$query->the_post();

            $temp = '';

            $day_array = array(
                'monday' => 'MÅNDAG',
                'tuesday' => 'TISDAG',
                'wednesday' => 'ONSDAG',
                'thursday' => 'TORSDAG',
                'friday' => 'FREDAG',
                'saturday' => 'LÖRDAG',
                'sunday' => 'SÖNDAG',
                'other_info' => 'ÖVRIG INFORMATION'
            );
             $count = 1;
            foreach ($day_array as $day => $value) {
                $each_day_menus = get_post_meta(get_the_ID(), $day, true);
                if (!empty($each_day_menus)) {
                    $temp .= '<div class="daily-menu-items col-sm-4">';
                    $temp .= '<div class="day">' . $value . '</div>';
                    foreach ($each_day_menus as $menu) {
                        $temp .= '<div class="menu-items">' . $menu["name_$day"] . '</div>';
                    }
                    $temp .= '</div>';
                
                    if( $count%3 == 0 ){
                        $temp .= '<div class="clearfix"></div>';
                    }
                    $count++;
                }
            }

            if (!empty($temp)) {
                $temp = '<div class="row">' . $temp . '</div>';
            }
            $menu_items = $temp;

        endwhile;
        wp_reset_postdata();

    endif;

    /* if from ajax call */
    if (isset($_POST['menu_date'])) {
        echo json_encode($menu_items);
        die;
    }
    /**/
    return $menu_items;
}