<?php

function hugonorrkopng_scripts() {
    $map_script = '';
    $assets_url = get_stylesheet_directory_uri() . '/assets/build';
    $suffix = '.min';

    if (defined('SCRIPT_DEBUG') && SCRIPT_DEBUG) {
        $suffix = '';
    }

    wp_enqueue_style('hugonorrkopng-above-fold', $assets_url.'/css/above-fold.css');
   // wp_enqueue_style('jquery-ui', 'http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css');

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }

    wp_add_inline_style( 'hugonorrkopng-above-fold','body{
        opacity: 0
    }');

    wp_enqueue_script('hugo-script', $assets_url . '/js/script' . $suffix . '.js', array('jquery'), null, false);
    
    $main_css = $assets_url . '/css/main' . $suffix . '.css';

    $script = "
        var head  = document.getElementsByTagName('head')[0];
        var link  = document.createElement('link');
        link.rel  = 'stylesheet';
        link.type = 'text/css';
        link.href = '" . $main_css . "';
        link.media = 'all';
        head.appendChild(link);
        
        WebFontConfig = {
            google: { families: [ 'Raleway:200,300,400,400i,500,600,700:latin' ] }
        };
        (function() {
           var wf = document.createElement('script');
           wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
           wf.type = 'text/javascript';
           wf.async = 'true';
           var s = document.getElementsByTagName('script')[0];
           s.parentNode.insertBefore(wf, s);
        })();
   ";

    if (is_page_template('template-parts/template-kontakt.php')) {
        global $post;

        $enable_map = get_post_meta($post->ID, 'enable_map', true);

        if ('yes' == $enable_map) {

            $latitude = floatval(get_post_meta($post->ID, 'latitude', true));
            $longitude = floatval(get_post_meta($post->ID, 'longitude', true));
            $map_zoom = intval(get_post_meta($post->ID, 'map_zoom', true));


            $map_script = "function initMap() {
                var uluru = {lat: ".$latitude.", lng:".$longitude."};
                var map = new google.maps.Map(document.getElementById('map'), {
                  zoom: ".$map_zoom.",
                  scrollwheel: false,
                  center: uluru
                });
                var marker = new google.maps.Marker({
                  position: uluru,
                  map: map
                });
              }

            var gmapScript = document.createElement('script');
            gmapScript.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyC0UFKb0FfidiD_2n4zolGgpeH47C0VMlQ&callback=initMap';
            gmapScript.type = 'text/javascript';
            gmapScript.async = 'true';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(gmapScript, s); ";
        }
    }

    wp_add_inline_script('hugo-script', $script.$map_script);
    wp_localize_script('hugo-script', 'hugoAjax', array('ajaxurl' => admin_url('admin-ajax.php')));
}

add_action('wp_enqueue_scripts', 'hugonorrkopng_scripts');


add_action('admin_enqueue_scripts', 'hugo_load_admin_script');

function hugo_load_admin_script() {
    $assets_url = get_stylesheet_directory_uri() . '/assets/build';
    $suffix = '.min';
    if (defined('SCRIPT_DEBUG') && SCRIPT_DEBUG) {
        $suffix = '';
    }

    wp_enqueue_style('jquery-ui', 'http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui'.$suffix.'.css');
    wp_enqueue_script('hugo-admin', $assets_url . '/js/admin'.$suffix.'.js', array('jquery-ui-datepicker','jquery'), '', true);
}