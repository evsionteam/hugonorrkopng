<?php
get_header();
$overlay_class = '';
$overlay_value = get_post_meta(get_the_ID(),'page_overlay',true);
if('yes' == $overlay_value){
    $overlay_class = ' overlay ';
}
?>
<div class="inner-page">
    <!-- IMG WRAPPER -->
    <div class="image-cover">
        <div class="img-wrapper jarallax <?php echo $overlay_class;?>" data-jarallax='{"speed": 0.2}' style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id(get_the_ID())); ?>');">
            <!-- Caption -->
            <div class="caption">
                <header class="entry-header"><?php the_title(); ?></header>
                <!-- .entry-header -->
                <?php $button_text = get_post_meta( get_the_ID(), 'button_text', true ); ?>
                <?php $button_link = get_post_meta( get_the_ID(), 'button_link', true ); ?>
                <?php if ( !empty( $button_link ) && !empty( $button_text ) ) { ?>
                    <div class="btn-link"><a href="<?php echo get_post_meta( get_the_ID(), 'button_link', true ); ?>" class="restro-btn"><?php echo get_post_meta( get_the_ID(), 'button_text', true ); ?></a></div>
                <?php } ?>
            </div>
        </div><!-- /.img-wrapper -->
    </div><!-- /.img-wrapper -->
	<div class="container">
		<div class="description">
			<div class="entry-content">
			    <div class="text-left">
					<?php
						while ( have_posts() ) : the_post();
							the_content();
						endwhile;
					?>
			    </div>
			</div><!-- /.entry-content -->
		</div>
	<?php $loop = new WP_Query( array( 'post_type' => 'activities', 'posts_per_page' => -1 ) ); ?>
   	<?php if( $loop->have_posts() ) { ?>
   		<div class="restaurant-block">
   			
	        <?php
	        $count = 1;
	        
	        while ( $loop->have_posts() ) : $loop->the_post();
	            ?>
	            
	            <?php if( 1 == $count % 3 ) { ?>
	            <div class="row">
	            <?php } ?>
	            
			   	<div class="col-sm-4 col-md-4 col-xs-6 restro-block">
		            <?php if(has_post_thumbnail()):?>
                        <div class="resturant-img">
                            <img src="<?php the_post_thumbnail_url();?>" class="img-responsive" alt="img"/>
                        </div>
                    <?php endif;?>
                     <div class="resturant-title">
			            	<?php the_title();?>
			        </div>
	            <div class="desc new-design">
	            	<?php the_content();?>	            
	            </div>

	            <?php $link = get_post_meta( get_the_ID(), 'hugo_activities_link', true );
	            if( '' != $link ){ ?>
	            	<a href="<?php echo esc_url( $link ) ?>" class="hugo-activity-link see-more" target="_blank"> <?php echo esc_html( 'Se menyn här' ) ?></a>
	            <?php } ?>

	            <a href="<?php echo esc_url( 'https://www.hugonorrkoping.se/boka-online/' ) ?>" class="see-more"> <?php echo esc_html( 'Boka här' ) ?></a>
			   	</div><!-- /.container -->
			   	
			   	<?php if( 0 == $count % 3 ) { ?>
	           </div>
	            <?php } ?>
	            
	            <?php $count++; ?>
	            
	        <?php endwhile;  wp_reset_query(); ?>
	        
	        	<?php if( 0 != $count % 3 ) { ?>
	           </div>
	            <?php } ?>
	    
	    </div>
   	<?php } ?>
    </div>
    </div>
<?php
get_footer();