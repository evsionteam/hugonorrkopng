<?php
/* Template Name: Night Homepage Template */
get_header();
global $hugo_opt;
while (have_posts()):the_post();
    $header_image = get_post_meta(get_the_ID(),'header_image',true);
    ?>

    <?php if (!empty($header_image)): ?>
        <div class="image-cover">
            <div class="jarallax img-wrapper  <?php //echo $overlay_class;     ?>" data-jarallax='{"speed": 0.2}' style="background-image:url('<?php echo $header_image; ?>')">
                <!-- Caption -->
                <div class="caption">
                    <header class="entry-header"><?php the_title(); ?></header>
                </div>
            </div>
        </div>
    <?php else: ?>
        <div class="image-cover">
            <div class="jarallax img-wrapper <?php //echo $overlay_class;   ?>" data-jarallax='{"speed": 0.2}' style="background-image:url('<?php echo get_template_directory_uri() . '/assets/src/img/inner-banner.jpg' ?>')">
                <!-- Caption -->
                <div class="caption">
                    <header class="entry-header"><?php the_title(); ?></header>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <!-- FORNOLES SKULL -->
    <div class="fornoles-skull arr-fornoles-skull">
        <h2><?php the_title(); ?>
        </h2>
        <div class="desciption">
            <?php the_content(); ?>
        </div>
    </div><!-- /.fornoles-skull -->

    <?php
endwhile;
wp_reset_postdata();
get_footer();
