jQuery(document).ready(function($) {

    //For image upload
    // Instantiates the variable that holds the media library frame.
    var hugo_image_frame;

    // Runs when the image button is clicked.
    $('#upload_image').on('click',function(e){

        // Prevents the default action from occuring.
        e.preventDefault();

        // If the frame already exists, re-open it.
        if ( hugo_image_frame ) {
            hugo_image_frame.open();
            return;
        }

        // Sets up the media library frame
        hugo_image_frame = wp.media.frames.hugo_image_frame = wp.media({
            title: meta_image.title,
            button: { text:  meta_image.button },
            library: { type: 'image' }
        });

        // Runs when an image is selected.
        hugo_image_frame.on('select', function(){

            // Grabs the attachment selection and creates a JSON representation of the model.
            var media_attachment = hugo_image_frame.state().get('selection').first().toJSON();

            // Sends the attachment URL to our custom image input field.
            $('#header-image').val(media_attachment.url);
            $('#image_preview p').html('<img style="max-width:300px;" src="'+media_attachment.url+'">');
        });

        // Opens the media library frame.
        hugo_image_frame.open();
    });
    
    $('#delete_image').on('click',function(){
        $('#header-image').attr('value','');
        $('#image_preview p').empty();
    });

    if( jQuery("#datepicker").length > 0 ){
        jQuery("#datepicker").datepicker();
    }
    if(jQuery("#datepicker2").length > 0 ){
        jQuery("#datepicker2").datepicker();
    }
});
