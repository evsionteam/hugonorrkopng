<?php
get_header();
global $hugo_opt;

$img_overlay_class = '';
if (isset($hugo_opt['arrangemang-img-overlay'])) {
    if (1 == $hugo_opt['arrangemang-img-overlay']) {
        $img_overlay_class = 'overlay';
    }
}
?>
<?php
if (isset($hugo_opt['arrangemang-image'])) {
    $arrangemang_img = $hugo_opt['arrangemang-image']['url'];
    if (!empty($arrangemang_img)) {
        ?>
        <div class="image-cover">
            <div class="jarallax img-wrapper <?php echo $img_overlay_class; ?>" data-jarallax='{"speed": 0.2}' style="background-image:url('<?php echo $arrangemang_img ?>')">
                <?php
                if (isset($hugo_opt['arrangemang-title'])) {
                    ?>
                    <!-- Caption -->
                    <div class="caption">
                        <header class="entry-header"><?php echo $hugo_opt['arrangemang-title']; ?></header>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div><!-- /.img-wrapper -->
        <?php
    }
}
?>


<!-- ARRANGEMANG -->
<?php
if (have_posts()):
    ?>
    <div class=" restaurant-block arrangemang">
        <div class="container">
            <div class="row">
                <h2>
                    <?php
                    if (isset($hugo_opt['arrangemang-title'])) {
                        echo $hugo_opt['arrangemang-title'];
                    }
                    ?>
                </h2>
                <div class="info-wrapper">
                    <?php
                    $counter = 1;
                    while (have_posts()):the_post();
                        $link = get_post_meta(get_the_ID(),'fb_link',true);
                        if(empty($link)){
                            $link =  get_permalink();
                        }
                        if (strpos($link, 'facebook') !== false){
                            $target = 'target="_blank"';
                        }
                        else{
                            $target = '';
                        }

                        ?>
                        <div class="col-sm-4  col-xs-12 restro-block">
                            <?php if(has_post_thumbnail()):?>
                                <a href="<?php echo $link; ?>" <?php echo ' '.$target;?>>
                                    <div class="img">
                                        <img src="<?php the_post_thumbnail_url();?>" class="img-responsive" alt="img"/>
                                    </div>
                                </a>
                            <?php endif;?>
                            <div class="resturant-title">
                                <a href="<?php echo $link; ?>" <?php echo ' '.$target;?>><?php the_title(); ?></a>
                            </div>
                            <?php echo '<p>'.mb_strimwidth(get_the_excerpt(), 0, 60, " ...").'</p>';?>
                            <?php //the_excerpt(); ?>
                            <a class="see-more" href="<?php echo $link; ?>" <?php echo ' '.$target;?>>Läs mer</a>
                        </div><!-- /.restro-block -->
                        <?php
                        if($counter % 2 == 0){
                            echo '<div class="clearfix visible-xs"></div>';
                        }
                        if($counter % 3 == 0){
                            echo '<div class="clearfix hidden-xs"></div>';
                        }
                        $counter++;
                    endwhile;
                    wp_reset_postdata();
                    ?>
                </div><!-- info-wrapper -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.arrangemang -->
<?php endif; ?>

<!-- EVENT NAME SLIDER -->
<?php
$args = array('post_type' => 'arrangements_slider');
$query = new WP_Query($args);
$j = 0;
if($query->have_posts()):
?>
    <div id="home-slider" class="carousel slide event-name" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">

            <?php for ($i = 0; $i < $query->post_count; $i++) { ?>
                <li data-target="#home-slider" data-slide-to="<?php echo $i; ?>" <?php if ($i == 0) { ?>class="active"><?php } ?></li>
            <?php } ?>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner restro-slider" role="listbox">
            <?php
            while ($query->have_posts()) {
                $query->the_post();
                ?>
                <?php
                $url = '';
                $thumb = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'home-large-slider');
                $url = $thumb[0];
                        ?>
                        <div class="item <?php
                        if ($j == 0) {
                            echo 'active';
                        }
                        ?>">
                            <div class="restro-design restro-design-1" style="background-image:url('<?php echo $url; ?>');">
                                        <!-- <img src="<?php //echo $url;            ?>"> -->
                                <div class="restro-text">
                                    <div class="restro-title"><?php the_title(); ?></div>
                                    <?php
                                    if(has_excerpt()){
                                        ?>
                                        <div class="restro-sub-title"><?php the_excerpt(); ?></div>
                                        <?php
                                    }?>
                                    <?php
                                    $btn_link = get_post_meta(get_the_ID(),'btn_link',true);
                                    $btn_text = get_post_meta(get_the_ID(),'btn_text',true);
                                    if( !empty($btn_text) && !empty($btn_link)){
                                        echo '<a href="'.$btn_link.'" class="restro-btn">'.$btn_text.'</a>';
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <?php
                $j++;
            } wp_reset_query();
            ?>
        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#home-slider" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true">
                <img src="<?php echo get_template_directory_uri() ?>/assets/build/img/arrow-left.png" alt="arror left"/>
            </span>
            <span class="sr-only"><?php _e('Previous', 'hugonorrkopng'); ?></span>
        </a>
        <a class="right carousel-control" href="#home-slider" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true">
                <img src="<?php echo get_template_directory_uri() ?>/assets/build/img/arrow-right.png" alt="arror right"/>
            </span>
            <span class="sr-only"><?php _e('Next', 'hugonorrkopng'); ?></span>
        </a>
    </div><!-- /.event-name -->
<?php endif;?>

<?php
if (isset($hugo_opt['arrangemang-archive-desc'])) {
    echo '<div class="fornoles-skull arr-fornoles-skull">'.$hugo_opt['arrangemang-archive-desc'].'</div>';
}
?>

<?php
get_footer();
