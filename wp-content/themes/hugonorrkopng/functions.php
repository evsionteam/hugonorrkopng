<?php

add_action('after_setup_theme', 'hugonorrkopng_setup');
function hugonorrkopng_setup() {
    load_theme_textdomain('hugonorrkopng', get_template_directory() . '/languages');
    add_theme_support('automatic-feed-links');
    add_theme_support('title-tag');
    add_theme_support('post-thumbnails');

    add_post_type_support('page', 'excerpt');

    register_nav_menus(array(
        'left_primary_menu' => esc_html__('Left Header Menu', 'hugonorrkopng'),
        'right_primary_menu' => esc_html__('Right Header Menu', 'hugonorrkopng'),
    ));

    add_theme_support('html5', array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ));

    $GLOBALS['content_width'] = apply_filters('hugonorrkopng_content_width', 640);

    add_image_size('home-large-slider', 1349, 392, true);
    add_image_size('home-square-slider', 392, 392, true);
}


add_action('widgets_init', 'hugonorrkopng_widgets_init');
function hugonorrkopng_widgets_init() {
    register_sidebar(array(
        'name' => esc_html__('Sidebar', 'hugonorrkopng'),
        'id' => 'sidebar-1',
        'description' => esc_html__('Add widgets here.', 'hugonorrkopng'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
    register_sidebar(array(
        'name' => esc_html__('Footer 1', 'hugonorrkopng'),
        'id' => 'footer-1',
        'description' => esc_html__('Add widgets here.', 'hugonorrkopng'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
}

require_once __DIR__ . '/inc/template-tags.php';
require_once __DIR__ . '/inc/helpers.php';
require_once __DIR__ . '/inc/maid.php';
require_once __DIR__ . '/inc/script-loader.php';
require_once __DIR__ . '/inc/Mobile_Detect.php';
require_once __DIR__ . '/inc/shortcodes.php';
require_once __DIR__ . '/inc/ajax-callback.php';

require_once __DIR__ . '/inc/admin/config.php';
require_once __DIR__ . '/inc/module-daily-lunch/pdf.php';

require_once __DIR__ . '/inc/post-types/daily-lunch-post.php';
require_once __DIR__ . '/inc/post-types/home-slider-post.php';
require_once __DIR__ . '/inc/post-types/menu-post-types.php';
require_once __DIR__ . '/inc/post-types/arrangement-post.php';
require_once __DIR__ . '/inc/post-types/vip-packet.php';
require_once __DIR__ . '/inc/post-types/night-arr-slider.php';
require_once __DIR__ . '/inc/post-types/bowling-page-meta-box.php';
require_once __DIR__ . '/inc/post-types/kontakt-page-meta-box.php';
require_once __DIR__ . '/inc/post-types/arrangement-post-meta-box.php';
require_once __DIR__ . '/inc/post-types/activities-post-type.php';