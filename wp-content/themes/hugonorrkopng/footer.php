<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package hugonorrkopng
 */
global $hugo_opt;
$fb_link = isset($hugo_opt['fb-link']) ? esc_url($hugo_opt['fb-link']) : '';
$insta_link = isset($hugo_opt['insta-link']) ? esc_url($hugo_opt['insta-link']) : '';
?>

<?php
$night_class = '';
if (is_multisite()) {
    $blog_details = get_blog_details();
    if (strpos($blog_details->path, 'nojen') !== false) {
        $night_class = 'footer-night';
    }
}
?>

</div><!-- #content -->

<footer id="colophon" class="site-footer <?php echo $night_class; ?>" role="contentinfo">
    <div class="container">
        <div class="row">
            
            <div class="col-md-3">
                <div class="footer-contact-detail">
                    <?php dynamic_sidebar('footer-1'); ?>
                    <div class="social-links">

                        <?php if (!empty($fb_link)): ?>
                            <a href="<?php echo $fb_link; ?>" target="_blank">
                                <svg width="35" version="1.1" class="hugo-facebook" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     viewBox="0 0 112.196 112.196" style="enable-background:new 0 0 112.196 112.196;" xml:space="preserve">
                                    <g>
                                        <path class="icon" d="M70.201,58.294h-10.01v36.672H45.025V58.294h-7.213V45.406h7.213v-8.34
                                              c0-5.964,2.833-15.303,15.301-15.303L71.56,21.81v12.51h-8.151c-1.337,0-3.217,0.668-3.217,3.513v7.585h11.334L70.201,58.294z"/>
                                    </g>

                                </svg>
                            </a>
                        <?php endif; ?>

                        <?php if (!empty($insta_link)): ?>
                            <a href="<?php echo $insta_link; ?>" target="_blank">
                                <svg width="35" version="1.1" class="hugo-instragram" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     viewBox="0 0 291.319 291.319" style="enable-background:new 0 0 291.319 291.319;" xml:space="preserve">
                                    <path class="icon" d="M195.93,63.708H95.38c-17.47,0-31.672,14.211-31.672,31.672v100.56
                                          c0,17.47,14.211,31.672,31.672,31.672h100.56c17.47,0,31.672-14.211,31.672-31.672V95.38
                                          C227.611,77.919,213.4,63.708,195.93,63.708z M205.908,82.034l3.587-0.009v27.202l-27.402,0.091l-0.091-27.202
                                          C182.002,82.116,205.908,82.034,205.908,82.034z M145.66,118.239c22.732,0,27.42,21.339,27.42,27.429
                                          c0,15.103-12.308,27.411-27.42,27.411c-15.121,0-27.42-12.308-27.42-27.411C118.23,139.578,122.928,118.239,145.66,118.239z
                                          M209.65,193.955c0,8.658-7.037,15.704-15.713,15.704H97.073c-8.667,0-15.713-7.037-15.713-15.704v-66.539h22.759
                                          c-2.112,5.198-3.305,12.299-3.305,18.253c0,24.708,20.101,44.818,44.818,44.818s44.808-20.11,44.808-44.818
                                          c0-5.954-1.193-13.055-3.296-18.253h22.486v66.539L209.65,193.955z"/>
                                </svg>
                            </a>
                        <?php endif; ?>

                    </div><!-- /.social-links -->
                </div><!-- /.contact-detail -->
            </div> <!-- /.col-md-3 -->

            <div class="col-md-9">
                <div class="footer-opening-hours">
                    <?php echo do_shortcode('[opening_hours in_footer="yes"]'); ?>
                </div>
            </div>
            
        </div><!-- /.row -->
    </div><!-- container -->


</footer><!-- #colophon -->
</div><!-- #page -->

<?php
//require get_template_directory() . '/template-parts/ajax-page-content.php';
wp_footer();
?>



</body>
</html>
