<?php
get_header();
global $hugo_opt;

$img_overlay_class = '';
if (isset($hugo_opt['vip-packets-img-overlay'])) {
    if (1 == $hugo_opt['vip-packets-img-overlay']) {
        $img_overlay_class = 'overlay';
    }
}
?>
<?php
if (isset($hugo_opt['vip-packets-image'])) {
    $vip_packets_img = $hugo_opt['vip-packets-image']['url'];
    if (!empty($vip_packets_img)) {
        ?>
        <div class="image-cover">
            <div class="jarallax img-wrapper <?php echo $img_overlay_class; ?>" data-jarallax='{"speed": 0.2}' style="background-image:url('<?php echo $vip_packets_img; ?>')">
                <?php
                if (isset($hugo_opt['vip-packets-title'])) {
                    ?>
                    <!-- Caption -->
                    <div class="caption">
                        <header class="entry-header"><?php echo $hugo_opt['vip-packets-title']; ?></header>
                    </div>
                <?php
                }
                ?>
            </div>
        </div><!-- /.img-wrapper -->
    <?php
    }
}
?>

<?php
if (isset($hugo_opt['vip-packets-archive-desc'])) {
    ?>
    <div class="fornoles-skull arr-fornoles-skull">
        <?php echo $hugo_opt['vip-packets-archive-desc'];?>
        <div class="down-arrow" id="fs-down-arrow" data-target="#vip-packets"></div>
    </div>
    <?php
}
?>

    <!-- ARRANGEMANG -->
<?php
if (have_posts()):
    ?>
    <div id="vip-packets" class="restaurant-block arrangemang">
        <div class="container">
            <div class="row">
                <div class="info-wrapper">
                    <?php while (have_posts()):the_post(); ?>
                        <div class="col-sm-4 col-xs-12 restro-block">
                            <div class="resturant-title">
                                <?php the_title(); ?>
                            </div>
                            <?php the_content(); ?>
                        </div><!-- /.restro-block -->
                    <?php
                    endwhile;
                    wp_reset_postdata();
                    ?>
                </div><!-- info-wrapper -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.arrangemang -->
<?php endif; ?>
<?php
get_footer();
