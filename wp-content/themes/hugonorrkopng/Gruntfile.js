module.exports = function (grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        sass: {// Task
            dist: {// Target
                options: {// Target options
                    style: 'expanded'
                },
                files: {// Dictionary of files
                    'assets/src/css/main.css': './assets/src/sass/main.scss',  //'destination': 'source'
                    'assets/src/css/above-fold.css': './assets/src/sass/above-fold.scss' 
                }
            }
        },
        concat: {
            options: {
                separator: "\n /*** New File ***/ \n"
            },
            /* Added new JS here*/
            js: {
                src: [
                    './assets/src/js/wrapper/start.js',
                    './assets/src/js/skip-link-focus-fix.js',
                    './node_modules/jquery.nicescroll/jquery.nicescroll.js',
                    './node_modules/bootstrap-sass/assets/javascripts/bootstrap.js',
                    './assets/src/js/vendor/parallax.min.js',
                    './assets/src/js/classToggler.js',
                    './assets/src/js/vendor/jarallax/jarallax.js',
                    './assets/src/js/vendor/jarallax/jarallax-video.js',
                    './assets/src/js/script.js',
                    './assets/src/js/wrapper/end.js'
                ],
                dest: './assets/build/js/script.js'
            },
            /* Add CSS here*/
            atfcss: {
                src: [
                    './assets/src/css/accessibility.css',
                    './assets/src/css/above-fold.css'
                ],
                dest: './assets/build/css/above-fold.css'
            },

            css: {
                src: [
                    //'./node_modules/font-awesome/css/font-awesome.css',
                    './assets/src/css/vendor/*.css',
                    './assets/src/css/main.css'
                ],
                dest: './assets/build/css/main.css'
            }
        },
        uglify: {
            options: {
                report: 'gzip'
            },
            main: {
                src: ['./assets/build/js/script.js'],
                dest: './assets/build/js/script.min.js'
            },
            admin: {
                src: ['./assets/build/js/admin.js'],
                dest: './assets/build/js/admin.min.js'
            }
        },
        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1,
                keepSpecialComments: 0,
                sourceMap: false
            },
            target: {
                files: [{
                        expand: true,
                        cwd: './assets/build/css/',
                        src: ['*.css', '!*.min.css'],
                        dest: './assets/build/css/',
                        ext: '.min.css'
                    }]
            }
        },
        copy: {
            main: {
                files: [
                    {expand: true, cwd: './assets/src/img', src: '**', dest: './assets/build/img/', filter: 'isFile'},
                    {expand: true, cwd: './assets/src/svg', src: '**', dest: './assets/build/svg/', filter: 'isFile'},
                    {expand: true, cwd: './assets/src/js/', src: 'admin.js', dest: './assets/build/js/', filter: 'isFile'},                    
                    {expand: true, flatten: true, cwd: './node_modules/bootstrap-sass/assets/fonts/bootstrap', src: '**', dest: './assets/build/fonts/bootstrap', filter: 'isFile'},
                    {expand: true, flatten: true, cwd: './node_modules/font-awesome/fonts', src: '**', dest: './assets/build/fonts/font-awesome', filter: 'isFile'},
                    {expand: true, flatten: true, cwd: './assets/src/fonts', src: '**', dest: './assets/build/fonts/', filter: 'isFile'},
                ],
            },
        },
        watch: {
            js: {
                files: ['./assets/src/js/*.js'],
                tasks: ['js']
            },
            css: {
                files: ['./assets/src/sass/**/*.scss'],
                tasks: ['css']
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-sass');

    //register grunt default task
    grunt.registerTask('css', ['sass', 'concat', 'cssmin', 'copy']);
    grunt.registerTask('js', ['concat', 'uglify']);

    grunt.registerTask('default', ['sass', 'concat', 'uglify', 'cssmin', 'copy']);
}